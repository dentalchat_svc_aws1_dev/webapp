<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	
##############################################################################

$router->group([
  'namespace' => 'Admin',
  'middleware' => 'auth',
], function () {
	
   
    resource('admin/home', 'HomeController');
    resource('admin/admin-profile', 'HomeController@getProfile');
    resource('admin/change-password', 'HomeController@changePass');
	resource('admin/cms', 'CmspageController');
    resource('admin/faq', 'FaqController');
	resource('admin/sitesetting', 'SitesettingController');
	

	
	Route::get('/admin/change-cmspage-status/', 'CmspageController@change_cmspage_status');
	Route::get('/admin/check-added-lmd-hotels-for-home/', 'SitesettingController@check_added_lmd_hotels_for_home_page');
	
	Route::get('/admin/multiple-record-operations/', 'HomeController@multiple_record_operations');
	
	############################Patient Routes Starts###############################
	Route::get('/admin/patient/ajax-patients', 'UserController@ajaxPatientsList');
	Route::get('/admin/patient/edit/{id}', 'UserController@getEdit');
	Route::post('/admin/patient/edit/{id}', 'UserController@postEdit');
	Route::get('/admin/patient/remove/{id}', 'UserController@postRemove');
	Route::get('/admin/patient/preview/{id}', 'UserController@getViewPatient');
	Route::controller('/admin/patient', 'UserController');

	############################Dentist Routes Starts###############################
	 Route::get('/admin/dentist/ajax-dentists', 'UserController@ajaxDoctorsList');
	 Route::get('/admin/dentist/list', 'UserController@doctorsList');
	 Route::get('/admin/dentist/remove/{id}', 'UserController@getRemoveDentist');
	 Route::get('/admin/dentist/dentist-step1', 'UserController@getDoctorStep1');
	 Route::post('/admin/dentist/dentist-step1', 'UserController@postDoctorStep1');
	 Route::get('/admin/dentist/dentist-step1/{id}', 'UserController@getDoctorStep1');
	 Route::post('/admin/dentist/dentist-step1/{id}', 'UserController@postDoctorStep1');
	 Route::get('/admin/dentist/dentist-step2/{id}', 'UserController@getDoctorStep2');
	 Route::post('/admin/dentist/dentist-step2/{id}', 'UserController@postDoctorStep2');
	 Route::get('/admin/dentist/dentist-step3/{id}', 'UserController@getDoctorStep3');
	 Route::post('/admin/dentist/dentist-step3/{id}', 'UserController@postDoctorStep3');
	 Route::get('/admin/dentist/dentist-step4/{id}', 'UserController@getDoctorStep4');
	 Route::post('/admin/dentist/dentist-step4/{id}', 'UserController@postDoctorStep4');
	 Route::get('/admin/dentist/dentist-step5/{id}', 'UserController@getDoctorStep5');
	 Route::post('/admin/dentist/dentist-step5/{id}', 'UserController@postDoctorStep5');
	 Route::get('/admin/dentist/dentist-step6/{id}', 'UserController@getDoctorStep6');
	 Route::post('/admin/dentist/dentist-step6/{id}', 'UserController@postDoctorStep6');
	 Route::get('/admin/dentist/dentist-step7/{id}', 'UserController@getDoctorStep7');
	 Route::post('/admin/dentist/dentist-step7/{id}', 'UserController@postDoctorStep7');
	 Route::get('/admin/dentist/dentist-preview/{id}', 'UserController@getDoctorAll');
	 Route::get('/admin/dentist/dentist-compare/{id}', 'UserController@getCompareDentist');
	 Route::post('/admin/item/approved-profile', 'UserController@postApprovedProfile');
	 Route::post('/admin/item/publish-profile', 'UserController@postPublishProfile');
	 
	 

	 //Route::get('/admin/item/edit/{id}', 'UserController@getRemoveItem');
	 Route::post('/admin/item/remove-item', 'UserController@postRemoveItem');

	############################Notification Routes start###############################
	Route::get('/admin/notification/list', 'NotificationController@getallnotificatios');
	Route::get('/admin/notification/ajax-notification', 'NotificationController@ajaxnotificationList');

	############################Notification Routes Ends###############################

	/*************************Testimonial Management***********************/
	Route::get('/admin/testimonial/ajax-testimonial', 'TestimonialController@ajaxTestimonialList');
	Route::get('/admin/testimonial/list', 'TestimonialController@testimonialList');
	Route::get('/admin/testimonial/ajax-blog', 'TestimonialController@ajaxTestimonialList');
	Route::get('/admin/add/testimonial', 'TestimonialController@addtestimonial');
	Route::post('/admin/add/testimonial', 'TestimonialController@postAddtestimonial');
	Route::get('/admin/add/edit/{id}', 'TestimonialController@getedittestimonial');
	Route::post('/admin/add/edit', 'TestimonialController@postedittestimonial');
	Route::post('/admin/testimonial/checkslug', 'TestimonialController@postCheckSlug');
	Route::get('/admin/deletetestimonial/{id}','TestimonialController@deleteTestimonial');		
	/*************************Testimonial Management***********************/

	/*************************Home Page Management***********************/
	get('/admin/edit-homepage/{id}', 'HomePageSettingController@homePageSetting');
	post('/admin/edit-homepage/{id}', 'HomePageSettingController@updatehomePageSetting');
	/*************************Home Page Management***********************/

});

// Logging in and out
get('/', 'Auth\AuthController@getLogin');
get('/auth/login', 'Auth\AuthController@getLogin');
get('/admin', 'Auth\AuthController@getLogin');
post('/auth/login', 'Auth\AuthController@postLogin');
get('/auth/logout', 'Auth\AuthController@getLogout');




Route::group(
    array('prefix' => 'admin'), 
    function() {
        Route::get('forgotpassword', 'Admin\HomeController@forgotPassword');
        Route::post('forgotpasswordcheck', 'Admin\HomeController@forgotpasswordcheck');
        Route::get('resetpassword/{id}', 'Admin\HomeController@resetpassword');
        Route::post('updatepassword/{id}', 'Admin\HomeController@updatePassword');
        Route::get('send-approve-mail/{docid}', 'Admin\HomeController@sendApprovalmail');
		
	}
);

Route::group(
    array('prefix' => 'service'), 
    function() {
    	Route::controller('/patient', 'Service\PatientController');
    	Route::controller('/homesetting', 'Service\HomeSettingController');
    	Route::controller('/dentistservice', 'Service\DentistController');
		Route::controller('/cms', 'Service\CmsController');
		Route::controller('/', 'Service\RegisterController');
	}
);

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route:: get('/patient-registration','Service\PatientController@patient_registration');
Route:: post('/patient-registration','Service\PatientController@patient_registration');

Route:: get('/patient-profile','Service\PatientController@EditProfile');
Route:: post('/patient-profile','Service\PatientController@EditProfile');

Route:: get('/patient-login','Service\PatientController@patient_login');
Route:: post('/patient-login','Service\PatientController@patient_login');

Route:: get('/patient-logout','Service\PatientController@patient_logout');
Route:: post('/patient-logout','Service\PatientController@patient_logout');

Route:: get('/create-patient-post','Service\PatientController@create_patient_post');
Route:: post('/create-patient-post','Service\PatientController@create_patient_post');

Route:: get('/list-patient-post','Service\PatientController@list_patient_post');
Route:: post('/list-patient-post','Service\PatientController@list_patient_post');

Route:: get('/delete-patient-post','Service\PatientController@delete_patient_post');
Route:: post('/delete-patient-post','Service\PatientController@delete_patient_post');

Route:: get('/get-patients-post','Service\DentistController@get_patients_post');
Route:: post('/get-patients-post','Service\DentistController@get_patients_post');

Route:: get('/get-attendant-post','Service\DentistController@get_attendant_post');
Route:: post('/get-attendant-post','Service\DentistController@get_attendant_post');

Route:: get('/get-unattendant-post','Service\DentistController@get_unattendant_post');
Route:: post('/get-unattendant-post','Service\DentistController@get_unattendant_post');

Route:: get('/tot-attendant-post-count','Service\DentistController@tot_attendant_post_count');
Route:: post('/tot-attendant-post-count','Service\DentistController@tot_attendant_post_count');

Route:: get('/tot-unattendant-post-count','Service\DentistController@tot_unattendant_post_count');
Route:: post('/tot-unattendant-post-count','Service\DentistController@tot_unattendant_post_count');

Route:: get('/get-chat-history','Service\PatientController@get_chat_history');
Route:: post('/get-chat-history','Service\PatientController@get_chat_history');

Route:: get('/view-notifications-from-patient','Service\PatientController@view_notifications_from_patient');
Route:: post('/view-notifications-from-patient','Service\PatientController@view_notifications_from_patient');


Route:: get('/read-post-notification','Service\PatientController@read_post_notification');
Route:: post('/read-post-notification','Service\PatientController@read_post_notification');

Route:: get('/save-xml','Service\PatientController@save_xml');
Route:: post('/save-xml','Service\PatientController@save_xml');


Route:: get('/save-doctors-xml','Service\PatientController@save_doctors_xml');
Route:: post('/save-doctors-xml','Service\PatientController@save_doctors_xml');

Route:: get('/mark-favourite-doctors','Service\PatientController@mark_favourite_doctors');
Route:: post('/mark-favourite-doctors','Service\PatientController@mark_favourite_doctors');

Route:: get('/get-favourite-doctors','Service\PatientController@get_favourite_doctors');
Route:: post('/get-favourite-doctors','Service\PatientController@get_favourite_doctors');

Route:: get('/get-doctor-favourite','Service\PatientController@get_doctor_favourite');
Route:: post('/get-doctor-favourite','Service\PatientController@get_doctor_favourite');

Route:: get('/save-chat-history','Service\PatientController@save_chat_history');
Route:: post('/save-chat-history','Service\PatientController@save_chat_history');

Route:: get('/get-recent-post','Service\PatientController@get_recent_post');
Route:: post('/get-recent-post','Service\PatientController@get_recent_post');

Route:: get('/get-recent-post-count','Service\PatientController@get_recent_post_count');
Route:: post('/get-recent-post-count','Service\PatientController@get_recent_post_count');

Route:: get('/read-chat-for-patient','Service\PatientController@read_chat_for_patient');
Route:: post('/read-chat-for-patient','Service\PatientController@read_chat_for_patient');

Route:: get('/list-doctor-details','Service\DentistController@list_doctor_details');
Route:: post('/list-doctor-details','Service\DentistController@list_doctor_details');

Route:: get('/unread-chat-count-for-patient','Service\PatientController@unread_chat_count_for_patient');
Route:: post('/unread-chat-count-for-patient','Service\PatientController@unread_chat_count_for_patient');


Route:: get('/save-review-rating-for-doctors','Service\PatientController@save_review_rating_for_doctors');
Route:: post('/save-review-rating-for-doctors','Service\PatientController@save_review_rating_for_doctors');

Route:: get('/closed-post-chat','Service\PatientController@closed_post_chat');
Route:: post('/closed-post-chat','Service\PatientController@closed_post_chat');

Route:: get('/get-patient-post-details','Service\PatientController@get_patient_post_details');
Route:: post('/get-patient-post-details','Service\PatientController@get_patient_post_details');

Route:: get('/patient-social-login','Service\PatientController@patient_social_login');
Route:: post('/patient-social-login','Service\PatientController@patient_social_login');

Route:: get('/change-patient-post-status','Service\PatientController@change_patient_post_status');
Route:: post('/change-patient-post-status','Service\PatientController@change_patient_post_status');

Route:: get('/docter-mail-send/{id}','Service\PatientController@docter_mail_send');
Route:: post('/docter-mail-send/{id}','Service\PatientController@docter_mail_send');

//Route:: get('/{param}','Frontend\CmsController@showContent');   /* For Show content */
//Route:: post('/{param}','Frontend\CmsController@showContent');   /* For Show content */
