<?php 
$authuser =  Auth::user();
$open = "class='collapsed'";    
$open1 = "class='unstyled collapse'";
    if(!isset($home_class))
        $home_class = '';
    if(!isset($cms_class))
        $cms_class = '';
    if(!isset($sitesetting_class))
        $sitesetting_class = '';
    if(!isset($blog_class))
    	$blog_class = '';
    if(!isset($country_class))
        $country_class = '';
	if(!isset($restaurant_class))
        $restaurant_class = '';
	if(!isset($category_class))
        $category_class = '';
	if(!isset($user_class))
        $user_class = '';
    if(!isset($testimonial_class))
        $testimonial_class = '';
    if(!isset($banner_class))
        $banner_class = '';
    if(!isset($doctor_class))
        $doctor_class = '';
    if(!isset($job_class))
    	$job_class = '';
    if(!isset($question_class))
    	$question_class= '';
    if(!isset($noti_class)){
    	$noti_class = "";
    }
?>
<div class="sidebar">
    <ul class="widget widget-menu unstyled">
	
        <li class="{!! $home_class !!}">
			<a href="<?php echo url().'/admin/home'?>">
				<i class="menu-icon icon-dashboard"></i>Dashboard
			</a>
        </li>
		<li class="{!! $banner_class !!}">
			<a href="<?php echo url().'/admin/edit-homepage/1'?>">
				<i class="menu-icon fa fa-picture-o"></i>Home Page Management
			</a>
		</li>	
       <li class="{!! $cms_class !!}">
			<a href="<?php echo url().'/admin/cms'?>">
				<i class="menu-icon icon-bolt"></i>Content Management
			</a>
        </li>
        <li class="{!! $sitesetting_class !!}">
			<a href="<?php echo url().'/admin/sitesetting'?>">
				<i class="menu-icon fa fa-cogs"></i>Site Settings
			</a>
		</li>

    </ul>
    
 	<ul class="widget widget-menu unstyled">				
		<li class="{!! $user_class !!}">
			<a href="<?php echo url();?>/admin/patient/list">
				<i class="menu-icon fa fa-user"></i>Patient Management
			</a>
		</li>
		<li class="{!! $doctor_class !!}">
			<a href="<?php echo url();?>/admin/dentist/list">
				<i class="menu-icon fa fa-user-md"></i>Dentist Management
			</a>
		</li>
		 <li class="{!! $noti_class !!}">
			<a href="<?php echo url();?>/admin/notification/list">
			<?php
				$admin_notificatin_count = Session::get('admin_notificatin_count');
			?>
				<i class="menu-icon fa fa-bell"></i><span> Notifications <?php if(isset($admin_notificatin_count) && $admin_notificatin_count != 0){?><span id="admin_notify_count" class="badge notify_cnt">(<?php echo $admin_notificatin_count;?>)</span><?php } ?></span>
			</a>
		</li>
		
    </ul> 

    <ul class="widget widget-menu unstyled">
    	<li class="{!! $blog_class !!}">
			<a href="<?php echo url();?>/admin/testimonial/list">
				<i class="menu-icon fa fa-tasks"></i>Testimonial
			</a>
		</li>
	</ul>
		
    <ul class="widget widget-menu unstyled">
       
        <li><a href="<?php echo url();?>/auth/logout">
				<i class="menu-icon icon-signout"></i>Logout
			</a>
		</li>
			
    </ul>
</div>
