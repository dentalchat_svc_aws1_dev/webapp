// page init
jQuery(function(){
"use strict";
	//FixedHeader();
	ImageFitCont();
});

// Fixed Header
function FixedHeader() {
	var lastScrollTop = 0, delta = 5;
	
		$(window).scroll(function(event){
		   var st = $(this).scrollTop();
		   
		   if(Math.abs(lastScrollTop - st) <= delta)
			  return;
		   
		   if (st > lastScrollTop){
				// downscroll code
				$('.site-header').addClass('fix-head').css({top:'-40px'})
				.hover(function(){$(".site-header").css({top: '0px'})})
		   } 
		   if (st < 50){
		   // downscroll code
		   	$('.site-header').removeClass('fix-head').css({top:'0px'});
		   }
		   else {
			  // upscroll code
			  //$('.site-header').removeClass('fix-head');
		   }
		   lastScrollTop = st;
		});

}


// Image Fit Container
function ImageFitCont() {
	"use strict";
	$('.imageFit').imgLiquid({
		fill: true,
        horizontalAlign: 'center',
        verticalAlign: 'center'
	});
}



$( function() {
	"use strict";
	
	$('.scroll-top').on('click', function() {
    	$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
	// Child Tab
	// $('#PatientReports').easyResponsiveTabs({
	// 	type: 'vertical',
	// 	width: 'auto',
	// 	fit: true,
	// 	tabidentify: 'ver_1'
	// });

	// $('.pickdate').datetimepicker({
	// 	format: 'DD/MM/YYYY'
	// });
	
	// $("[data-toggle=popover]").popover({
	// 	container:'body'	
	// });
	
	
});

// $(window).on('load',function() {
// 	"use strict";
// 	// setTimeout(function(){
// 	// 	activeuser.innerHTML = 1200;
// 	// }, 1500);
	
// 	// setTimeout(function(){
// 	// 	patreport.innerHTML = 356;
// 	// }, 1000);
// });

$(document).on('click', '.close-panel', function(event){
	event.preventDefault();
	//alert();
	$(this).closest('.collapse').collapse('hide');
});

$(document).on({
    mouseenter: function () {
        $('.update-report').addClass('drk-bdr');
    },
    mouseleave: function () {
        $('.update-report').removeClass('drk-bdr');
    }
}, '.close-report');


function readImage(file,id,width,height,size,backgroundimage) 
  {
    //alert(id);
    var reader = new FileReader();
    var image  = new Image();

    reader.readAsDataURL(file);  
    reader.onload = function(_file) {
    image.src    = _file.target.result;              // url.createObjectURL(file);
    image.onload = function() {
    var w = this.width,
      h = this.height,
      t = file.type,                           // ext only: // file.type.split('/')[1],
      n = file.name,
      s = ~~(file.size/1024);
      //alert(w);
      // alert(t);
      // alert(s);
      var image_type_arr = t.split("/");
      
      if(image_type_arr.length<2)
      {
        sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
        return;
      }

      var image_type = image_type_arr[1].toLowerCase();
      
        if(id=='profile_pics')
        {
          if(!(image_type ==='jpg' || 
          image_type ==='jpeg' ||  
          image_type ==='png'  ||  
          image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<300)
          {
            sweetAlert("Oops...", "Image width should not be less than 300 px", "error");
            return;
          }
          if(h<300)
          {
            sweetAlert("Oops...", "Image height should not be less than 300 px", "error");
            return;
          }
            
          document.getElementById("profile_pics").style.backgroundImage = 'url('+this.src+')';

          setTimeout(function(){
          },1000);
        }
        else if(id=="cover_image")
        {
          if(!(image_type ==='jpg' || 
          image_type ==='jpeg' ||  
          image_type ==='png'  ||  
          image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<150)
          {
            sweetAlert("Oops...", "Image width should not be less than 150 px", "error");
            return;
          }
          if(h<150)
          {
            sweetAlert("Oops...", "Image height should not be less than 150 px", "error");
            return;
          }
          //document.getElementById("logoImage").style.backgroundImage = 'url('+this.src+')';
          document.getElementById("restaurant_cover_image").src = image.src;
        }
        else if(id=="logo")
        {
          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          /*if(w<1024)
          {
            sweetAlert("Oops...", "Image width should not be less than 1024 px", "error");
            return;
          }
          if(h<512)
          {
            sweetAlert("Oops...", "Image height should not be less than 512 px", "error");
            return;
          }*/
            
          document.getElementById("logo_image").src = image.src;
        }
        else if(id=="image")
        {
          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<300)
          {
            sweetAlert("Oops...", "Image width should not be less than 300 px", "error");
            return;
          }
          if(h<300)
          {
            sweetAlert("Oops...", "Image height should not be less than 300 px", "error");
            return;
          }

          document.getElementById("profile_image").src = image.src;
          angular.element('#image').scope().showImage();
          $(document).click();
        }
        else
        {
          size = (size === undefined)?1024:size;
          height = (height === undefined)?1024:height;
          width = (width === undefined)?1024:width;

          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size); 
          if(s> size* 1024)
          {
            sweetAlert("Oops...", "Image size should be less than "+size+"MB", "error");
            return;
          }
          /*if(w<width)
          {
            sweetAlert("Oops...", "Image width should not be less than "+width+" px", "error");
            return;
          }
          if(h<height)
          {
            sweetAlert("Oops...", "Image height should not be less than "+height+" px", "error");
            return;
          }*/
          /*document.getElementById("image").src = image.src;
          if($('#'+backgroundimage))
            $('#'+backgroundimage).css('background-image','url('+this.src+')');*/
        }
      
      };
      image.onerror= function() 
      {
        sweetAlert("Oops...", "Invalid file type: "+ file.type, "error");
      };      
    };

  }