@extends('admin/layout/admin_template')

@section('content')
	{!! Form::open(['url' => 'admin/doctor/edit/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'cms_form','onsubmit'=>'userJs.checkUserEmail("EDIT")']) !!}
		
		{!! Form::hidden('hid_frm_submit_res',1,['class'=>'span8','id'=>'hid_frm_submit_res']) !!}
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}
		{!! Form::hidden('form_type','EDIT',['class'=>'span8','id'=>'form_type']) !!}
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<div class="control-group" style="display:block;">
		 <div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Title *</label>
			<div class="controls">
				<select class="span8" id="title" name="title">
		    		<option value="Mr">Mr</option>
					<option value="Dr">Dr</option>
		    	</select>
			</div>
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">First Name *</label>
			<div class="controls">
				{!! Form::text('name',$user_details->name,['class'=>'span8','id'=>'name']) !!}
			</div>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Last Name *</label>
				
			<div class="controls">
				{!! Form::text('last_name',$user_details->last_name,['class'=>'span8','id'=>'last_name']) !!}
			</div>
				
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Email *</label>
				
			<div class="controls">
				{!! Form::email('email',$user_details->email,['class'=>'span8','id'=>'email']) !!}
				<div style="color:#F00;font-size:13px;clear:both;display:none;" id="user_email_msg"></div>
			</div>
				
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Mobile Number *</label>
				
			<div class="controls">
				<input type="text" name="contact" id="contact" class="span8 input-medium bfh-phone" data-format=" (ddd) ddd-dddd" data-number="{{$user_details->contact}}" value="{{$user_details->contact}}">
			</div>
				
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Password</label>
				
			<div class="controls">
				<input type="password" name="password" id="password" class="span8" onblur="userJs.rulesWhileEditingUser();"  />
			</div>
				
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Confirm Password</label>
			<div class="controls">
				<input type="password" name="conf_password" id="conf_password" class="span8"  />
			</div>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Image</label>
				<div class="controls">
				<input type="file" id="profile_img" name="profile_img"/>
			</div>
			<?php if(!empty($user_details->image)){?>
				<p class="new_avatar" style="padding-left: 180px;padding-top: 10px;"><img src="<?php echo url();?>/uploads/profile_image/{{ $user_details->image }}" class="nav-avatar"></p>
			<?php } ?>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Profile Title</label>
			<div class="controls">
				<textarea name="profile_title" class="span8">{{$user_details->profile_title}}</textarea>
			</div>
		</div>
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">About You</label>
			<div class="controls">
				<textarea name="about_u" rows="5" class="span8">{{$user_details->about}}</textarea>
			</div>
		</div>

		<div class="control-group" style="display:block;" id="lang_sec">
			<label class="control-label" for="basicinput">Languages</label>
			<div class="controls">
				<select id="languages" name="languages[]" multiple="multiple">
				<?php
					$doc_lang = explode(",", $user_details->languages);
				?>
					<?php foreach ($all_active_lang as $languages) {?>
					<option <?php if(in_array($languages->name, $doc_lang)){?> selected <?php } ?> value="{{$languages->name}}">{{$languages->name}}</option>
				<?php } ?>
				</select>
			</div>
		</div>

		<div class="control-group" style="display:block;" id="medical_proced_sec">
			<label class="control-label" for="basicinput">Medical Procedures (Category)</label>
			<div class="controls">
				<select id="category" name="category[]" multiple="multiple">
				<?php 
				$exist_cat = $user_details->docs_Category->lists('category_id')->toArray();
				foreach ($all_active_category as $category_val) {?>
					<option <?php if(in_array($category_val->id, $exist_cat)){?> selected <?php } ?> value="{{$category_val->id}}">{{$category_val->name}}</option>
				<?php } ?>
				</select>
			</div>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Registered With</label>
			<div class="controls">
				<input type="text" name="registered" id="registered" class="span8" value="{{$user_details->registered_with}}"  />
			</div>
		</div>
		<div class="control-group" style="display:block;" id="edu_sec">
			<label class="control-label" for="basicinput">Qualifications</label>
			<?php if(!empty($user_details->docs_qualification->toArray())){
					foreach ($user_details->docs_qualification as $key => $qualification) {
						if($key == 0){
			?>
				<div class="controls">
					<input type="text" value="{{$qualification->education}}" name="education[]" id="education[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_edu" id="add_more_edu" onclick="add_more_html('education[]','edu_sec')">Add More</a>
				</div>
			<?php }else{?>
				<div class="controls" style="padding-top:5px;">
					<input type="text" name="education[]" id="education[]" class="span8" value="{{$qualification->education}}">
					<span class="action-btns" style="margin-left:5px;"><a href="javascript:void(0);" onclick="$(this).closest('.controls').remove();"><i class="fa fa-trash-o" aria-hidden="true"></i>
					</a></span>
				</div>
				<?php
					}
					}
				?>
			<?php }else{ ?>
				<div class="controls">
					<input type="text" name="education[]" id="education[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_edu" id="add_more_edu" onclick="add_more_html('education[]','edu_sec')">Add More</a>
				</div>

			<?php } ?>
				
		</div>

		<div class="control-group" style="display:block;" id="spec_sec">
			<label class="control-label" for="basicinput">Specialist Interest</label>

			<?php if(!empty($user_details->docs_speciality->toArray())){
					foreach ($user_details->docs_speciality as $key => $speciality) {
						if($key == 0){
			?>
				<div class="controls">
					<input type="text" value="{{$speciality->specialist}}" name="specialist[]" id="specialist[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_spe" id="add_more_spe" onclick="add_more_html('specialist[]','spec_sec')">Add More</a>
				</div>
			<?php }else{?>
				<div class="controls" style="padding-top:5px;">
					<input type="text" name="specialist[]" id="specialist[]" class="span8" value="{{$speciality->specialist}}">
					<span class="action-btns" style="margin-left:5px;"><a href="javascript:void(0);" onclick="$(this).closest('.controls').remove();"><i class="fa fa-trash-o" aria-hidden="true"></i>
					</a></span>
				</div>
				<?php
					}
					}
				?>
			<?php }else{ ?>
				<div class="controls">
					<input type="text" name="specialist[]" id="specialist[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_spe" id="add_more_spe" onclick="add_more_html('specialist[]','spec_sec')">Add More</a>
				</div>

			<?php } ?>
			<!-- <div class="controls">
				<input type="text" name="specialist[]" id="specialist[]" class="span8"  />
				<a href="javascript:void(0);" class="btn" name="add_more_spe" id="add_more_spe" onclick="add_more_html('specialist[]','spec_sec')">Add More</a>
			</div> -->
		</div>

		<div class="control-group" style="display:block;" id="research_sec">
			<label class="control-label" for="basicinput">Research Interest</label>
				<?php if(!empty($user_details->docs_research->toArray())){
					foreach ($user_details->docs_research as $key => $research) {
						if($key == 0){
			?>
				<div class="controls">
					<input type="text" value="{{$research->research}}" name="research[]" id="research[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_res" id="add_more_res" onclick="add_more_html('research[]','research_sec')">Add More</a>
				</div>
			<?php }else{?>
				<div class="controls" style="padding-top:5px;">
					<input type="text" name="research[]" id="research[]" class="span8" value="{{$research->research}}">
					<span class="action-btns" style="margin-left:5px;"><a href="javascript:void(0);" onclick="$(this).closest('.controls').remove();"><i class="fa fa-trash-o" aria-hidden="true"></i>
					</a></span>
				</div>
				<?php
					}
					}
				?>
			<?php }else{ ?>
				<div class="controls">
					<input type="text" name="research[]" id="research[]" class="span8"  />
					<a href="javascript:void(0);" class="btn" name="add_more_res" id="add_more_res" onclick="add_more_html('research[]','research_sec')">Add More</a>
				</div>
			<?php } ?>
			<!-- <div class="controls">
				<input type="text" name="research[]" id="research[]" class="span8"  />
				<a href="javascript:void(0);" class="btn" name="add_more_res" id="add_more_res" onclick="add_more_html('research[]','research_sec')">Add More</a>
			</div> -->
		</div>
		<?php
			$city_arr = array();
			if(!empty($user_details->docs_location)){
				foreach ($user_details->docs_location as  $doctor_location) {
					$city_arr[] = $doctor_location->city_name->name;
				}
				$city_str = implode(",", $city_arr);
			}
			
		?>
		<div class="control-group" style="display:block;" id="practice_sec">
			<label class="control-label" for="basicinput">Practice(s) Address</label>
			<div class="controls">
					<input type="text" name="practices" id="practices" class="span8" style="display:none;" value="{{$city_str}}">
			</div>
		</div>
			
		<div class="control-group">
		
			<div class="controls">
				{!! Form::submit('Save',array('class'=>'btn','name'=>'action','value'=>'save')) !!}
				<a href="{!! url('admin/doctor/list')!!}" class="btn">Back</a>
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
	<script>
	function add_more_html(name,afterelement){
		var html = '<div class="controls" style="padding-top:5px;"><input type="text" name="'+name+'" id="'+name+'" class="span8"/><span class="action-btns" style="margin-left:5px;"><a href="javascript:void(0);" onclick="$(this).closest(\'.controls\').remove();"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div>';
		$("#"+afterelement).append(html);
	}
	$(document).ready(function(){
		$("#languages").multiselect({
		 });
		 $("#category").multiselect({
		 });
		var user_title = "<?php echo $user_details->title;?>";
		$("#title").val(user_title);
		var city_id = [];
		var city_name = <?php echo json_encode($city_arr);?>;
       jQuery("#practices").tagit({
        singleField: true,
        singleFieldNode: $('#practices'),
        allowSpaces: true,
        minLength: 2,
        removeConfirmation: true,
        tagSource: function( request, response ) {
            $.ajax({
                url: site_url+"/admin/getautoCitylist",
                data: { term:request.term },
                dataType: "json",
                success: function( data ) {
                    response( $.map( data, function( item ) {
                    	//city_name.push(item.value);
                    	city_name[item.id] = item.value;
                        return {
                            label: item.value,
                            value: item.value
                        }
                    }));
                }
            });
        },

        beforeTagAdded: function(event, ui) {
		      if ($.inArray(ui.tagLabel, city_name) == -1) {
		        return false;
		      }
   		 }
   		 });
	});
	</script>
@stop