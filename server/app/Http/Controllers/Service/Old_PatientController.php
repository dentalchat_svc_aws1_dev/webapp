<?php
namespace App\Http\Controllers\Service;

use \PDO;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
/*************** Model name ***************/
///*use App\Model\Country;
//use App\Model\Category;
//use App\Model\SiteUser; */
//use App\Model\Doctor;
//use App\Model\DoctorDetails;
//use App\Model\DoctorClinic;
//use App\Model\DoctorEducation;
//use App\Model\DoctorAward;
//use App\Model\DoctorInsurance;
//use App\Model\DoctorExperience;
//use App\Model\DoctorLicense;
//use App\Model\DoctorSkill;
//use App\Model\DoctorLanguage;
//use App\Model\DoctorLoginDetails;
//
//use App\Model\TmpDoctorDetails;
//use App\Model\TmpDoctorClinic;
//use App\Model\TmpDoctorEducation;
//use App\Model\TmpDoctorAward;
//use App\Model\TmpDoctorInsurance;
//use App\Model\TmpDoctorExperience;
//use App\Model\TmpDoctorLicense;
//use App\Model\TmpDoctorSkill;
use App\Model\Cmspage;
use App\Model\Patient;
use App\Model\PatientAuthToken;
use App\Model\PatientPost;
use App\Model\PatientPostAttachment;
use App\Model\DoctorNotification;
use App\Model\Doctor;
use App\Model\DoctorDetails;
use App\Model\FavouriteDoctor;
use App\Model\PatientDoctorAppointments;
use App\Model\ChatHistory;
use App\Model\DoctorEducation;
use App\Model\DoctorClinic;
use App\Model\DoctorReviewRating;
use App\Model\ClosedPostChat;
use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;
//58ac8d7c5310850cd6100607---58ac8d7c5310850cd6100609
class PatientController extends BaseController {

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
        if(file_exists('../blog/wp-load.php')){
            require_once '../blog/wp-load.php';
        }
  		parent::__construct();
    }

   function getTest(){
        echo 1;
    }

    /* Registration Verified Link */
    function postPatientActivation(){

        $data       = Request::all();
        $email      = $data['email'];
        //print_r($email);exit('1223456');
        // check with email and user status
        $doc_table = Patient::where('email',$email)->first();
        $patient_id = $doc_table->id;

        if($patient_id!='')
        {
            $check_user_query   = Patient::where('id',$patient_id)->where('status',0);
            $check_user = $check_user_query->first();

            if($check_user!=""){

                $user_id_with_time  = $check_user['id'].':'.date("Y-m-d H:i:s");
                $token              = Hash::make($user_id_with_time);

                Patient::where('id',$patient_id)->update(array('status'    => 1));


     /******************************** email send ***********************************/

		//	$p_details = DoctorDetails::where('doctor_id', $doctor_id)->first();
        $dname          = $check_user['name'].' '.$check_user['last_name'];
		$demail         = $check_user['email'];
  	    $this->mailsend($patient_id,'Patient Activation Confirmation', $demail, $dname , 'unactivatepatient' , 'Patient');

      /******************************** Email End here*******************************/

                echo json_encode(
                            array(
                                'status'    => 1,
                                'data' => array()
                            )
                        );

            }
            else{

                echo json_encode(
                    array(
                        'status'    => 2,
                        'data' => array()
                    )
                );
            }
        }
        else
        {
            echo json_encode(
                    array(
                        'status'    => 3,
                        'data' => array()
                    )
                );
        }


        exit;
    }

    /* Forget password */
    public function postPatientForgotPassword(){

         $email = Request::input('email');


        // Check If email exist in DB
        $email_query = Patient::where('email',$email);

        $email_exist_cnt =  $email_query->count();
        $email_details = $email_query->first();

        if($email_exist_cnt==0){

            echo json_encode(
                    array(
                        'status'    => 0
                    )
                ); exit();
        }
        else{

            $user_details = $email_query->first();
            $user_name = $user_details->first_name;
            $user_email = $user_details->email;
            $user_cnt = Patient::where('status', 1)->where('id',$user_details->id)->count();

            if($user_cnt){
            // Get randon number
                $random_code = mt_rand();

                // Update the random code in DB
                $email_query->update(array('code_number' => $random_code));
                $length = strrpos(url(), "/");
                $forgot_mail_url = substr(url(),0,$length);
                $reset_password_link = $forgot_mail_url.'/patient-reset-password/'.base64_encode($user_email).'@@'.base64_encode($random_code);

                $perm_domain    = $forgot_mail_url;
                $sitesettings = DB::table('sitesettings')->where('id',1)->first();
                if(!empty($sitesettings))
                {
                    $admin_users_email = $sitesettings->value;
                }

                $sent = Mail::send('service.dentalchat.reset_password_link', array('name'=>$user_name,'email'=>$user_email,'reset_password_link'=>$reset_password_link,'admin_users_email'=>$admin_users_email,'perm_domain'=>$perm_domain),
                function($message) use ($admin_users_email, $user_email,$user_name)
                {
                    $message->from($admin_users_email,'Dental Chat');
                    $message->to($user_email, $user_name)->subject('DentalChat::Reset your DentalChat.com password!');
                });

                echo json_encode(
                                array(
                                    'status'    => 1
                                )
                            );
            }else{
                echo json_encode(
                            array(
                                'status'    => 2
                            )
                        );
            }

        }


    }


    /* Check forget password link */
    public function postPatientCheckLink(){

        $param      = Request::input('param');

        // Explaode the parameter to get email id and random number
        list($email,$random_code) = explode('@@', $param);
        $email          = base64_decode($email);
        $random_code    = base64_decode($random_code);

        // Check If random code is deleted
        $email_query =      Patient::where('email',$email)->where('code_number',$random_code);
        $email_exist_cnt =  $email_query->count();

        if($email_exist_cnt==0){

            echo json_encode(
                            array(
                                'status'    => 0,
                                'data'      => array()
                            )
                        );
        }
        else{

            echo json_encode(
                            array(
                                'status'    => 1,
                                'data'      => array('email'=>$email)
                            )
                        );

        }
    }


    /* user and restaurant owner forgot password */
    public function postPatientChangeForgotPassword(){

        $password   = Request::input('password');
        $param      = Request::input('param');

        // Update Password
        list($email,$random_code) = explode('@@', $param);
        $email          = base64_decode($email);
        $random_code    = base64_decode($random_code);

        $email_query_sql = Patient::where('email',$email);
        $email_query_sql->update(array('code_number' => '','password' => md5($password)));
        /* wp function start here for password update */
		$userdata = Patient::where('email',$email)->first();
		if($userdata){
            if ( email_exists( $email ) ) {
                $wp_user = get_user_by( 'email', $email );
                if($wp_user){
                    $wp_user_id = $wp_user->ID;
                    wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $password ) );
                    wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                }
            }else{
                $wpuserdata = array(
                    'user_email' => $email,
                    'user_login' => $email,
                    'display_name'=>$userdata->name.' '.trim($userdata->last_name),
                    'nickname'=>$userdata->name,
                    'first_name'=>$userdata->name,
                    'last_name' => $userdata->last_name,
                    'user_pass'  => $password,
                    'status'=>'1'
                );
                $wp_user_id = wp_insert_user( $wpuserdata ) ;
                if($wp_user_id){
                    wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                    update_user_meta($wp_user_id, 'gender', $userdata->gender);
                    update_user_meta($wp_user_id, 'dob',$userdata->dob);
                    update_user_meta($wp_user_id, 'insurance_type',$userdata->insurance);
                    update_user_meta($wp_user_id, 'registration_type','1');
                }
            }
		}

        /* wp function end here */
        echo json_encode(
                            array(
                                'status'    => 1
                            )
                        );

    }


     /*****************************************************************/
      /***************** SAVE PREVIEW END **************/
/*****************************************************************/

    function postChangePassword(){

        $data               = Request::all();
        $access_token       = Request::header('Access-Token');
        $user_dtls          = PatientAuthToken::where('auth_token',$access_token)->first();
        $patient_id          = $user_dtls['patient_id'];
        $input = $data['dentist'];
        $old_password = md5($input['old_password']);
        $user_details = Patient::where('id', $patient_id)->where('password','=',$old_password)->count();
        if($user_details > 0)
        {
            $create_user = Patient::where('id', $patient_id)
                                      ->update([
                                        'password'  => md5($data['dentist']['password'])
                                    ]);



/******************************** email send ***********************************/

        $user_detailse = Patient::where('id', $patient_id)->first();
        $dname      = $user_detailse['name'].' '.$user_detailse['last_name'];
	 	$demail     = $user_detailse['email'];
		$this->mailsend($patient_id,'Your DentalChat.com password has been changed', $demail,$dname,$user_detailse['name'],$user_detailse['last_name'], 'patientpasswordchnage' , 'Patient');

/******************************** Email End here*******************************/

            /* wp function start here for password update */
        	$userdata = Patient::where('id', $patient_id)->first();
        	if($userdata){
        	    if ( email_exists( $userdata->email ) ) {
        	        $wp_user = get_user_by( 'email', $userdata->email );
        	        if($wp_user){
        	            $wp_user_id = $wp_user->ID;
        	            wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $data['dentist']['password'] ) );
        	            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
        	        }
        	    }else{
        	        $wpuserdata = array(
        	            'user_email' => $userdata->email,
        	            'user_login' => $userdata->email,
        	            'display_name'=>$userdata->name.' '.trim($userdata->last_name),
        	            'nickname'=>$userdata->name,
        	            'first_name'=>$userdata->name,
        	            'last_name' => $userdata->last_name,
        	            'user_pass'  => $data['dentist']['password'],
        	            'status'=>'1'
        	        );

        	        $wp_user_id = wp_insert_user( $wpuserdata ) ;
        	        if($wp_user_id){
        	            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
        	            update_user_meta($wp_user_id, 'gender', $userdata->gender);
        	            update_user_meta($wp_user_id, 'dob',$userdata->dob);
        	            update_user_meta($wp_user_id, 'insurance_type',$userdata->insurance);
        	            update_user_meta($wp_user_id, 'registration_type','1');
        	        }
        	    }
        	}
        	if($wp_user_id){
            	wp_set_current_user( $wp_user_id, $userdata->email );
    	        wp_set_auth_cookie( $wp_user_id );
    	        do_action( 'wp_login', $userdata->email );
        	}
        	/* wp function end here */
            // postPatientLogout();



            echo json_encode(array('status'=>1,'name'=>$dname,'email'=>$demail));
        }
        else
        {
            echo json_encode(array('status'=>0));
        }


    }

  function postPatientEmailCheck(){

        $email = Request::input('email');
        $check_user_email   = Patient::where('email',$email)->count();
        if($check_user_email>0){
            echo json_encode(array('status'=>2));exit();
        }
        else
        {
             echo json_encode(array('status'=>1));exit();
        }
  }



    public function change_patient_post_status()
    {
        if(Request::isMethod('post'))
        {
            $data = Request::all();
            $post_id = $data['post_id'];
            $status = $data['status'];

            PatientPost::where('id','=',$post_id)
                            ->update([
                                'is_active' =>  $status
                            ]);

            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  "Post status changed successfully."
            );
            echo json_encode($response_arr);exit();
        }
    }


    public function patient_registration()
    {
        $data = Request::all();
        
        if(!isset($data['patient']['mobile_request']))
        {
        ##############Captcha validation --STARTS###############
        $captcha = $data['g_recaptcha_response'];
        $postdata = http_build_query(
          array(
            'secret' => '6LehIR8UAAAAAB7zIctT4uiWYd9ruxzD7Q50ksPH', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );

        //Build options for the post request
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );

        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

        /* Send request to Googles siteVerify API */
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
        $response = json_decode($response, true);
        if($response["success"]===false)
        {
            $response_arr = array(
                'status'    =>  '3',
                'msg'       =>  "Captcha mismatch error."
            );
            echo json_encode($response_arr);exit();
        }
        ##############Captcha validation --ENDS###############

        }

        $check_user_email   = Patient::where('email',$data['patient']['email'])->count();
        if($check_user_email>0){
            echo json_encode(array('status'=>2));exit();
        }
        else{
            $create_patient      = Patient::create([
                                    'name'        => $data['patient']['first_name'],
                                    'last_name'   => $data['patient']['last_name'],
                                    'email'       => $data['patient']['email'],
                                    'gender'     =>  isset($data['patient']['gender']) ? $data['patient']['gender'] : '',
                                    'dob'       =>   isset($data['patient']['date_of_birth'])?date('Y-m-d',strtotime($data['patient']['date_of_birth'])):'0000-00-00',
                                    'insurance_type' =>  isset($data['patient']['insurance']) ? $data['patient']['insurance'] : '',
                                    'status'      => '1',
                                    'password'    => md5($data['patient']['password']),
                                    'registration_type' =>  '1',
                                    'created_at'  => date('Y-m-d H:i:s'),
                                    'updated_at'  => date('Y-m-d H:i:s')
                                ]);

            $patient_id = $create_patient->id;    // last  inserted id
            if($patient_id){
                /* wp function start here to check user exists or not*/
                if ( email_exists( $data['patient']['email'] ) ) {
                    $wp_user = get_user_by( 'email', $data['patient']['email'] );
                    if($wp_user){
                        $wp_user_id = $wp_user->ID;
                        wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $data['patient']['password'] ) );
                        wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                    }
                }else{
                    $userdata = array(
                        'user_email' => $data['patient']['email'],
                        'user_login' => $data['patient']['email'],
                        'display_name'=>$data['patient']['first_name'].' '.trim($data['patient']['last_name']),
                        'nickname'=>$data['patient']['first_name'],
                        'first_name'=>$data['patient']['first_name'],
                        'last_name' => $data['patient']['last_name'],
                        'user_pass'  => $data['patient']['password'],
                        'status'=>'1'
                    );

                    $wp_user_id = wp_insert_user( $userdata ) ;
                    if($wp_user_id){
                        wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;

                        $gender = isset($data['patient']['gender']) ? $data['patient']['gender'] : '';
                        $dob =  isset($data['patient']['date_of_birth'])?date('Y-m-d',strtotime($data['patient']['date_of_birth'])):'0000-00-00';
                        $insurance_type = isset($data['patient']['insurance']) ? $data['patient']['insurance'] : '';

                        update_user_meta($wp_user_id, 'gender', $gender);
                        update_user_meta($wp_user_id, 'dob',$dob);
                        update_user_meta($wp_user_id, 'insurance_type',$insurance_type);
                        update_user_meta($wp_user_id, 'registration_type','1');
                    }
                }
                if($wp_user_id){
                    wp_set_current_user( $wp_user_id, $data['patient']['email'] );
                    wp_set_auth_cookie( $wp_user_id );
                    do_action( 'wp_login', $data['patient']['email'] );
                }
                /*wp function end here*/
                /*$sitesettings = DB::table('sitesettings')->where('id',1)->first();
                if(!empty($sitesettings))
                {
                    $admin_users_email = $sitesettings->value;
                }*/
                $admin_users_email = 'notification@dentalchat.com';
                $user_email = $data['patient']['email'];
                $user_name = $data['patient']['first_name'];


                $length = strrpos(url(), "/");
                $activation_url = substr(url(),0,$length);
                $activation_password_link = $activation_url.'/#/activate-patient/'.base64_encode($user_email);
                //echo $activation_password_link; exit;
                /*$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url),
                function($message) use ($admin_users_email, $user_email,$user_name)
                {
                    $message->from($admin_users_email,'Dental Chat');
                    $message->to($user_email, $user_name)->subject('Dental Chat::Activation link!');
                });*/

/******************************** email send ***********************************/

        $dname  =$data['patient']['first_name'].' '.trim($data['patient']['last_name']);
	 	$demail = $user_email;
		$this->mailsend($patient_id,'Welcome to DentalChat', $demail, $dname ,$data['patient']['first_name'],$data['patient']['last_name'], 'patientregister' , 'Patient');

/******************************** Email End here*******************************/

                if(!empty($data['patient']['patient_id'])){
                    DB::table('patient_posts')->where('id', $data['patient']['patient_id'])->update(['patient_id' => $patient_id]);
                    $auth_token = $this->get_unique_alphanumeric_no(16);
                    //echo $auth_token;
                    $patient_details = Patient::where('email','=',$data['patient']['email'])
                        ->first();
                        
                    $patient_name = $patient_details->name.' '.substr($patient_details->last_name, 0, 1);
                    $profile_pic  = $patient_details->image;
                    $login_type = $patient_details->registration_type;

                    PatientAuthToken::create([
                        'patient_id'            =>  $patient_details->id,
                        'auth_token'            =>  $auth_token,
                        'last_login'            =>  time(),
                        'status'                =>  '1',
                        //'patient_name'          =>  $patient_name,
                    ]);
                    
                    $is_any_post = PatientPost::where('patient_id','=',$patient_details->id)->count();
                    
                    /* wp function start here to check user exists or not*/
                    if ( email_exists( $email ) ) {
                        $wp_user = get_user_by( 'email', $email );
                        if(!empty($wp_user)){
                            $wp_user_id = $wp_user->ID;
                            wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $request_details['password'] ) );
                            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                        }
                    }else{
                        $userdata = array(
                            'user_email' => $email,
                            'user_login' => $email,
                            'display_name'=>$patient_details->name.' '.trim($patient_details->last_name),
                            'nickname'=>$patient_details->name,
                            'first_name'=>$patient_details->name,
                            'last_name' => $patient_details->last_name,
                            'user_pass'  => $request_details['password'],
                            'status'=>'1'
                        );

                        $wp_user_id = wp_insert_user( $userdata ) ;
                        if($wp_user_id){
                            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                            update_user_meta($wp_user_id, 'gender', $patient_details->gender);
                            update_user_meta($wp_user_id, 'dob',$patient_details->dob);
                            update_user_meta($wp_user_id, 'insurance_type',$patient_details->insurance);
                            update_user_meta($wp_user_id, 'registration_type','1');
                        }
                    }
                    $wp_error = '';
                    if(!empty($wp_user_id)){
                        wp_set_current_user( $wp_user_id, $email );
                        $wp_error = wp_set_auth_cookie( $wp_user_id ,true);
                        do_action( 'wp_login', $wp_user_id );
                    }
                    /*wp function end here*/

                    $response_arr = array(
                        'status'            =>  4,
                        'patient_id'        =>  $this->simple_encrypt($patient_details->id),
                        'patient_id_raw'    =>  $patient_details->id,
                        'auth_token'        =>  $auth_token,
                        'patient_name'      =>  $patient_name,
                        'profile_pic'       =>  $profile_pic,
                        'is_any_post'       =>  $is_any_post,
                        'login_type'      	=>  $login_type,
                        'wp_user'      	=>  $wp_user,
                        'wp_error'      	=>  $wp_error,
                        'password'      	=>  $password,
                        'wp_user_id'      	=>  $wp_user_id,
                        'redirect_story'            =>   $story,
                    );
                    //print_r($is_any_post); die;    
                    echo json_encode($response_arr);exit();
                    /*echo json_encode(array(
                        'status'    =>4,
                        'auth_token' =>  $auth_token,
                        'useremail' =>$user_email,
                        'password'  =>$data['patient']['password']
                    ));*/
                }else{
                    echo json_encode(array(
                        'status'    =>1,
                        'useremail' =>$user_email,
                        'password'  =>$data['patient']['password']
                    ));    
                }
                
            }
            else{
                echo json_encode(array('status'=>0));
            }
        }
        exit;
    }

    public function patient_social_login()
    {
        if(Request::isMethod('post'))
        {
            $data = Request::all();
            $email = $data['email'];
            $name = $data['name'];
            $login_type = 2;//$data['name'];
            $imageUrl = $data['imageUrl'];

            $is_email_exists = Patient::where('email','=',$email)->count();
            if($is_email_exists > 0)
            {
                //$user_detls =   Patient::where('email','=',$email)->first();

                $auth_token = $this->get_unique_alphanumeric_no(16);
                $patient_details = Patient::where('email','=',$email)
                        ->first();

                $patient_name = $patient_details->name.' '.$patient_details->last_name;
                $profile_pic  = $patient_details->image;
                $login_type   = $patient_details->registration_type;
            
                PatientAuthToken::create([
                    'patient_id'            =>  $patient_details->id,
                    'auth_token'            =>  $auth_token,
                    'last_login'            =>  time(),
                    'status'                =>  '1',
                    //'patient_name'          =>  $patient_name,
                ]);

                $is_any_post = PatientPost::where('patient_id','=',$patient_details->id)->count();

                $response_arr = array(
                    'status'            =>  '1',
                    'patient_id'        =>  $this->simple_encrypt($patient_details->id),
                    'patient_id_raw'    =>  $patient_details->id,
                    'auth_token'        =>  $auth_token,
                    'patient_name'      =>  $patient_name,
                    'profile_pic'       =>  $profile_pic,
                    'is_any_post'       =>  $is_any_post,
                    'login_type'        =>  $login_type,
                );
            }
            else
            {
                 $create_patient        = Patient::create([
                                            'name'        => $name,
                                            'email'       => $email,
                                            'image'       => $imageUrl,
                                            'registration_type'       => $login_type,
                                            'created_at'  => date('Y-m-d H:i:s'),
                                            'updated_at'  => date('Y-m-d H:i:s')
                                        ]);

                $auth_token = $this->get_unique_alphanumeric_no(16);
                $patient_details = Patient::where('email','=',$email)
                        ->first();

                $patient_name = $patient_details->name.' '.$patient_details->last_name;
                $profile_pic  = $patient_details->image;
                $login_type = $patient_details->registration_type;

                PatientAuthToken::create([
                    'patient_id'            =>  $patient_details->id,
                    'auth_token'            =>  $auth_token,
                    'last_login'            =>  time(),
                    'status'                =>  '1',
                    //'patient_name'          =>  $patient_name,
                ]);

                $is_any_post = PatientPost::where('patient_id','=',$patient_details->id)->count();

                $response_arr = array(
                    'status'            =>  '1',
                    'patient_id'        =>  $this->simple_encrypt($patient_details->id),
                    'patient_id_raw'    =>  $patient_details->id,
                    'auth_token'        =>  $auth_token,
                    'patient_name'      =>  $patient_name,
                    'profile_pic'       =>  $profile_pic,
                    'is_any_post'       =>  $is_any_post,
                    'login_type'        =>  $login_type,
                );
            }

            echo json_encode($response_arr);exit();
        }
    }

    public function patient_login()
    {

        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $email = $request_details['email'];
            $password = md5($request_details['password']);
            $story = $request_details['redirecttostory'];
          
            
            //echo $email.'------'.$password;exit();

            $is_patient_exists = Patient::where('email','=',$email)
                        ->where('password','=',$password)
                        ->count();

            //print_r($is_patient_exists);exit();
            //echo $is_patient_exists;exit();
            if($is_patient_exists > 0)
            {
                $is_patient_status = Patient::where('email','=',$email)
                        ->where('password','=',$password)->where('status', 1)
                        ->count();
                if($is_patient_status > 0)
                {
                    $auth_token = $this->get_unique_alphanumeric_no(16);
                    $patient_details = Patient::where('email','=',$email)
                            ->where('password','=',$password)
                            ->first();
                    if(!empty($request_details['patient_id'])){
                        DB::table('patient_posts')->where('id', $request_details['patient_id'])->update(['patient_id' => $patient_details->id]);
                    }
                    $patient_name = $patient_details->name.' '.substr($patient_details->last_name, 0, 1);
                    $profile_pic  = $patient_details->image;
                    $login_type = $patient_details->registration_type;

                    PatientAuthToken::create([
                        'patient_id'            =>  $patient_details->id,
                        'auth_token'            =>  $auth_token,
                        'last_login'            =>  time(),
                        'status'                =>  '1',
                        //'patient_name'          =>  $patient_name,
                    ]);

                    $is_any_post = PatientPost::where('patient_id','=',$patient_details->id)->count();
                    /* wp function start here to check user exists or not*/
                    if ( email_exists( $email ) ) {
                        $wp_user = get_user_by( 'email', $email );
                        if(!empty($wp_user)){
                            $wp_user_id = $wp_user->ID;
                            wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $request_details['password'] ) );
                            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                        }
                    }else{
                        $userdata = array(
                            'user_email' => $email,
                            'user_login' => $email,
                            'display_name'=>$patient_details->name.' '.trim($patient_details->last_name),
                            'nickname'=>$patient_details->name,
                            'first_name'=>$patient_details->name,
                            'last_name' => $patient_details->last_name,
                            'user_pass'  => $request_details['password'],
                            'status'=>'1'
                        );

                        $wp_user_id = wp_insert_user( $userdata ) ;
                        if($wp_user_id){
                            wp_update_user( array ('ID' => $wp_user_id, 'role' => 'contributor') ) ;
                            update_user_meta($wp_user_id, 'gender', $patient_details->gender);
                            update_user_meta($wp_user_id, 'dob',$patient_details->dob);
                            update_user_meta($wp_user_id, 'insurance_type',$patient_details->insurance);
                            update_user_meta($wp_user_id, 'registration_type','1');
                        }
                    }
                    $wp_error = '';
                    if(!empty($wp_user_id)){
                        wp_set_current_user( $wp_user_id, $email );
                        $wp_error = wp_set_auth_cookie( $wp_user_id ,true);
                        do_action( 'wp_login', $wp_user_id );
                    }
                    /*wp function end here*/

                    $response_arr = array(
                        'status'            =>  '1',
                        'patient_id'        =>  $this->simple_encrypt($patient_details->id),
                        'patient_id_raw'    =>  $patient_details->id,
                        'auth_token'        =>  $auth_token,
                        'patient_name'      =>  $patient_name,
                        'profile_pic'       =>  $profile_pic,
                        'is_any_post'       =>  $is_any_post,
                        'login_type'      	=>  $login_type,
                        'wp_user'      	=>  $wp_user,
                        'wp_error'      	=>  $wp_error,
                        'password'      	=>  $password,
                        'wp_user_id'      	=>  $wp_user_id,
                        'redirect_story'            =>   $story,
                    );
                }else{
                   $response_arr = array(
                        'status'    =>  '2',
                        'msg'       =>  "Your account is inactive!",
                        'auth_token'    =>  ''
                    );
                }

            }
            else
            {
                $response_arr = array(
                    'status'    =>  '0',
                    'msg'       =>  "Invalid Email/Password!",
                    'auth_token'    =>  ''
                );
            }

            echo json_encode($response_arr);exit();
        }
    }




    function postUpdateProfile() // For dentist profile page  Data came from temporary files
    {
        $data = Request::all();
        //print_r($data);exit;
        $check_patient = Patient::where('id',$data['id'])->first();

        if($check_patient != NUll)
        {
            if (Input::hasFile('profile_pics'))
            {
              $destinationPath = 'uploads/patient_profile_image/';
              $extension = Input::file('profile_pics')->getClientOriginalExtension(); // getting image extension
              $fileName = time().'.'.$extension; // renaming image
              Input::file('profile_pics')->move($destinationPath, $fileName); // uploading file to given path

              $data['image']=$fileName;
              // unlink old photo
              @unlink('uploads/patient_profile_image/'.$check_patient->image);
            }
            else
            {
               $data['image'] = $check_patient->image;
            }

            $doc_details = Patient::where('id',$data['id'])->update([
                                        'name'    => trim($data['name']),
                                        'last_name'     => trim($data['last_name']),
                                        'contact'=> preg_replace('/[^0-9]+/', '',$data['contact']),
                                        'country_code' => $data['country_code'],
                                        'dob' => $data['dob']?date('Y-m-d',strtotime($data['dob'])):'0000-00-00',
                                        'gender'        => $data['gender'],
                                        'image'  => $data['image'],
                                        'updated_at'      => date('Y-m-d h:i:s')
                                    ]);

            echo json_encode(array(
                'status'=>1,
                'patient_name' => trim($data['name']).' '.trim($data['last_name']),
                'profile_pic'  => $data['image']
            ));
        }
        else
        {
            $check_patient = array();
        }
    }

  /**********************************************************/
  function postEditProfile() // Preview page  all data to show from tmp tables
  {
    $access_token   = Request::header('Access-Token');
    $user_dtls      = PatientAuthToken::where('auth_token',$access_token)->first();
    $patient_id     = $user_dtls['patient_id'];
    $patient_details = Patient::where('id',$patient_id)->first()->toArray();

    echo json_encode(array('status'=>1,'patient_details'=>$patient_details));
  }


    public function postPatientLogout()
    {
        if(Request::isMethod('post'))
        {
            $patientId      = Request::input('patientId');
            $access_token           = Request::header('Access-Token');
            $patient_id = $this->simple_decrypt($patientId);

            PatientAuthToken::where('auth_token','=',$access_token)
                                ->where('patient_id','=',$patient_id)
                                ->update([
                                    'status'    =>  '0'
                                ]);
            /* wp function start here to check user exists or not*/
            wp_logout();
            /*wp function end here*/
            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  "You have logged out successfully."
            );

            echo json_encode($response_arr);exit();
        }
    }

    public function create_patient_post()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();

            //print_r($request_details); die;
            //$patient_id = $this->simple_decrypt($request_details['patient_id']);
            $patient_id = $request_details['patient_id'];
            $auth_token = $request_details['auth_token'];
 	       $address=$request_details['address'];
	      $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCECNx6YKAjaYfP9Eq7FXAMB1QmjUKvMZk&address='.urlencode($address).'&sensor=false');

	     $geo = json_decode($geo, true);
	    if ($geo['status'] = 'OK') {
	        $latitude = @$geo['results'][0]['geometry']['location']['lat'];
	        $longitude = @$geo['results'][0]['geometry']['location']['lng'];
	     }else {
				$latitude='0.0';
				$longitude='0.0';
			}

            //print_r($request_details['current_location']);exit();
            $patient_post = PatientPost::create([
                'patient_id'            =>  $patient_id,
                'current_location'      =>  $request_details['address'],
                'pain_level'            =>  $request_details['pain_level'],
                'post_title'            =>  $request_details['post_title'],
                'description'           =>  $request_details['description'],
                'insurance'             =>  ($request_details['insurance']) ? $request_details['insurance'] : '0',
                'insurance_information'             =>($request_details['insurance_inforamtion']) ? $request_details['insurance_inforamtion'] : '',
                'appointment'             =>  ($request_details['appointment']) ? $request_details['appointment'] : '0',
                'appointment_datetime'             =>($request_details['appointment_datetime']) ? $request_details['appointment_datetime'] : '',
                'appointment_time'             =>  $request_details['appointment_time'],
                 'last_cleaning'             =>($request_details['last_cleaning']) ? $request_details['last_cleaning'] : '',
                 'src_syst'             =>($request_details['src_syst']) ? $request_details['src_syst'] : 'dentalchat.com',
                'emergency'             =>  $request_details['emergency'],
                'lat'                   =>  isset($latitude)?$latitude:'',
                'lang'                  =>  isset($longitude)?$longitude:'',
                'is_active'             =>  "1",
            ]);


            if(isset($_FILES['attachments']))
            {
                $attachments = $_FILES['attachments'];

                $tot_image_uploaded = count($attachments['name']);
                for($i=0;$i<$tot_image_uploaded;$i++)
                {
                    if(isset($attachments['name'][$i]) && $attachments['name'][$i]!='')
                    {
                        $ext = $this->get_image_ext($attachments['name'][$i]);
                        $attachment_new_name = $this->get_unique_alphanumeric_no(10).'.'.$ext;
                        move_uploaded_file($attachments['tmp_name'][$i],"uploads/patient_post_attachments/".$attachment_new_name);
                            
                        PatientPostAttachment::create([
                            'patient_post_id'   => $patient_post->id,
                            'file_name'         => $attachment_new_name,
                        ]);
                    }
                }
            }

              $cmd = "wget -bq --spider ".url()."/docter-mail-send/".$patient_post->id;
              $pid = shell_exec(escapeshellcmd($cmd));
            if($patient_id==0){
                $response_arr = array(
                'status'    =>  0,
                'post_id'       =>  $patient_post->id
                );    
            }else{
                $response_arr = array(
                'status'    =>  1,
                'msg'       =>  "Post added successfully."
                );    
            }
            


/******************************** email send ***********************************/

        $user_detailse = Patient::where('id', $patient_id)->first();
        $dname      = $user_detailse['name'].' '.$user_detailse['last_name'];
	 	$demail         = $user_detailse['email'];
        $this->mailsend($patient_id,'Your DentalChat post has been submitted', $demail, $dname ,$user_detailse['name'],$user_detailse['last_name'], 'patientpostprob' , 'Patient');

 /******************************** Email End here*******************************/
            echo json_encode($response_arr);exit();
        }
    }

    public function docter_mail_send($post_id='')
    {

            $sitesettings = DB::table('sitesettings')->where('id',1)->first();
            if(!empty($sitesettings))
            {
                $admin_users_email = $sitesettings->value;
            }

            $doctors = Doctor::where('email_status','=',1)->where('admin_status','=',1)->get();

            $patient_post_details = PatientPost::where('id','=',$post_id)->first();
            $pain_level = $patient_post_details->pain_level;
            $post_title = $patient_post_details->post_title;
            $description = $patient_post_details->description;
            $emergency =  $patient_post_details->emergency== 1 ? 'Yes' : 'No';
            $lat = $patient_post_details->lat;
            $lang = $patient_post_details->lang;

            if($lat!="" && $lang!=""){

                $patient_details = Patient::where('id','=',$patient_post_details->patient_id)->first();
                $patient_name = $patient_details['name'].' '.$patient_details['last_name'];


                foreach($doctors as $doctor)
                {
                   $doctor_clinics = DoctorClinic::select('id','doctor_id', DB::raw('(3959 * acos(cos(radians('.$lat.')) * cos(radians(lat)) * cos(radians(lang) - radians('.$lang.')) + sin(radians('.$lat.')) * sin(radians(lat)))) AS distance'))
                     ->having('distance', '<' ,30)->where('doctor_id','=',$doctor->id)->get();

                    if(!empty($doctor_clinics))
                    {

                        foreach ($doctor_clinics as $doctor_clinic)
                        {
                            Doctor::where('id',$doctor_clinic->doctor_id)->update(array('is_active' => 1));
                            DoctorNotification::create([
                            'patient_post_id'    =>  $post_id,
                            'doctor_id'     =>  $doctor_clinic->doctor_id,
                            ]);

                            ########Sending email to doctors --STARTS###########

                            $doctor_details = DoctorDetails::where('doctor_id','=',$doctor_clinic->doctor_id)->first();
                            $doctor_name = $doctor_details['first_name'].' '.$doctor_details['last_name'];
                            $doctor_email = $doctor_details['email'];


                           $sent = Mail::send('service.dentalchat.dentist_post_mail', array('doctor_name'=>$doctor_name,'doctor_email'=>$doctor_email,'post_title'=>$post_title,'pain_level'=>$pain_level,'description'=>$description,'emergency'=>$emergency,'patient_name'=>$patient_name),
                           function($message) use ($admin_users_email, $doctor_email,$doctor_name)
                            {
                                $message->from($admin_users_email,'Dental Chat');
                                $message->to($doctor_email, $doctor_name)->subject('Dental Chat::Patient Post');
                            });

                            ########Sending email to doctors --ENDS###########

                        }

                    }
                }
            }
    }

    public function get_patient_post_details()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_post_id = $request_details['post_id'];

            $patient_post_detls = PatientPost::with('get_patient')->with('get_attachments')->where('id','=',$patient_post_id)->first();

            $response_arr = array(
                'status'    =>  '1',
                'patient_post_detls'    =>  $patient_post_detls
            );
            echo json_encode($response_arr);exit();
        }
    }


    public function list_patient_post()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            //$patient_id = $this->simple_decrypt($request_details['patient_id']);
            $patient_id = $request_details['patient_id'];
            $auth_token = $request_details['auth_token'];

            $post_limit = $request_details['post_limit'];
            $post_offset = $request_details['post_offset'];

            $post_list = PatientPost::with('get_attachments')
                                    ->with(array('chat_details'=>function($q){
                                        return $q->where('is_read','=','0')->count();
                                    }))

                                    ->where('patient_id','=',$patient_id)
                                    //->where('is_active','=','1')
                                    ->orderBy('id','DESC')
                                    ->limit($post_limit)
                                    ->offset($post_offset)
                                    ->get();

            $post_list_count = PatientPost::with('get_attachments')
                                    ->where('patient_id','=',$patient_id)
                                    ->where('is_active','=','1')
                                    ->count();


            foreach($post_list as $post){
                $allchat=ChatHistory::where('patient_id','=',$patient_id)->where('post_id','=',$post['id'])->count();

                $unread=ChatHistory::where('patient_id','=',$patient_id)->where('post_id','=',$post['id'])
                                    ->where('is_read','=',0)
                                    ->count();
            $distinct_doctor_count = ChatHistory::where('patient_id','=',$patient_id)->where('post_id','=',$post['id'])->groupBy('doctor_id')->get(['doctor_id']) ->toArray();
              $post_lista[]=array('id'=>$post['id'],
                                 'patient_id'=>$post['patient_id'],
                                 'current_location'=>$post['current_location'],
                                 'emergency'=>$post['emergency'],
                                 'pain_level'=>$post['pain_level'],
                                 'post_title'=>$post['post_title'],
                                 'description'=>$post['description'],
                                 'appointment'=>$post['appointment'],
                                 'appointment_datetime'=>$post['appointment_datetime'],
                                 'is_active'=>$post['is_active'],
                                 'posted_date'=>$post['posted_date'],
                                 'lat'=>$post['lat'],
                                 'lang'=>$post['lang'],
                                 'all_chat_count'=>$allchat,
                                 'unread_chat_count'=>$unread,
                                 'doctor_count'=>count($distinct_doctor_count),
                                 'get_attachments'=>$post['get_attachments'],
                                 'chat_details'=>$post['chat_details']
                                );

            }
            $response_arr = array(
                'status'    =>  '1',
                'post_list' =>  $post_lista,
                'post_list_count'   =>  $post_list_count
            );

            echo json_encode($response_arr);exit();
        }
    }

    public function delete_patient_post()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_id = $request_details['patient_id'];
            $auth_token = $request_details['auth_token'];

            $post_id = $request_details['post_id'];

            $is_post_exists = PatientPost::where('patient_id','=',$patient_id)
                                        ->where('id','=',$post_id)
                                        ->count();

            if($is_post_exists > 0)
            {
                PatientPost::where('id','=',$post_id)->delete();
                PatientPostAttachment::where('patient_post_id','=',$post_id)->delete();

                $response_arr = array(
                    'status'    =>  '1',
                    'msg'       =>  "Post deleted successfully."
                );

               $user_detail   = Patient::select('name','last_name','email')->where('id',$patient_id)->first();
                $dname=$user_detail['name'].' '.$user_detail['last_name'];
                $demail=$user_detail['email'];

    /******************************** email send ***********************************/
               $this->mailsend($patient_id,'Post Deleted', $demail, $dname ,$user_detail['name'],$user_detail['last_name'], 'unactivatepatient' , 'Patient');

    /******************************** Email End here*******************************/

            }
            else
            {
                $response_arr = array(
                    'status'    =>  '0',
                    'msg'       =>  "This post does not exists."
                );
            }

            echo json_encode($response_arr);exit();
        }
    }


    ############For doctor's account  --STARTS##################
    public function view_notifications_from_patient()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $doctor_id = $request_details['doctor_id'];
            $notifications = DoctorNotification::with('get_post_details','get_post_details.get_patient')
            ->where('doctor_id','=',$doctor_id)
            ->get();
            $response_arr = array(
                'status'    =>  '1',
                'notifications' =>  $notifications
            );

            echo json_encode($response_arr);exit();
        }
    }


    public function read_post_notification()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $doctor_id = $request_details['doctor_id'];
            $patient_post_id = $request_details['patient_post_id'];

            DoctorNotification::where('doctor_id','=',$doctor_id)
                                ->where('patient_post_id','=',$patient_post_id)
                                ->update([
                                    'is_read'   =>  '1'
                                ]);

            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  "Status changes successfully."
            );
        }
    }

    ############For doctor's account  --ENDS##################


    ###########Patient Marking favourite & unfavourite doctors --STARTS##########
    public function mark_favourite_doctors()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_id = $request_details['patient_id'];
            $auth_token = $request_details['auth_token'];
            $doctor_id = $request_details['doctor_id'];
            $is_favourite = $request_details['is_favourite'];
            //$rating = $request_details['rating'];
            //$reviews = $request_details['reviews'];

            $is_record_exists = FavouriteDoctor::where('patient_id','=',$patient_id)
                                ->where('doctor_id','=',$doctor_id)
                                ->count();

            if($is_record_exists > 0)
            {
                FavouriteDoctor::where('patient_id','=',$patient_id)
                                ->where('doctor_id','=',$doctor_id)
                                ->update([
                                    'is_favourite'      =>  $is_favourite,
                                    //'rating'            =>  $rating,
                                    //'reviews'          =>  $reviews,
                                ]);
            }
            else
            {
                FavouriteDoctor::create([
                    'patient_id'        =>  $patient_id,
                    'doctor_id'         =>  $doctor_id,
                    'is_favourite'      =>  $is_favourite,
                    //'rating'            =>  $rating,
                    //'reviews '          =>  $reviews,
                ]);
            }



            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  'Doctor has been marked favourite successfully.',
            );
            echo json_encode($response_arr);exit();
        }
    }
    ###########Patient Marking favourite & unfavourite doctors --ENDS##########

    /* start code by covetus 1-9-2017*/
    ########### Patient Marking Apponitment To Doctors With Respect To Post --STARTS##########
    public function set_appointment_patient()
    {
        $patient_id = Request::get('patient_id');
        $doctor_id = Request::get('doctor_id');
        $post_id = Request::get('post_id');
        $is_appoitment = Request::get('is_appoitment');

        $appointment = PatientDoctorAppointments::where(array('doctor_id'=>$doctor_id,'patient_id'=>$patient_id,'post_id'=>$post_id))->count();
        $current_date = date("Y-m-d H:i:s");
        if(!empty($appointment)){
            PatientDoctorAppointments::where(array('doctor_id'=>$doctor_id,'patient_id'=>$patient_id,'post_id'=>$post_id))->update(['is_appointment'=> $is_appoitment,'updated_at'=>$current_date]);
            $msg = 'Appointment has been cancelled successfully.';
            switch ($is_appoitment) {
                case 1:
                    $msg = 'Request Appointment has been done successfully.';
                    break;
                case 2:
                    $msg = 'You have declined the appointment.';
                    break;
                default:
                    $msg = 'You have confirmed the appointment.';
            }
            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  $msg,
            );
        }else{
            PatientDoctorAppointments::insert(array('doctor_id'=>$doctor_id,'patient_id'=>$patient_id,'post_id'=>$post_id,'is_appointment'=> $is_appoitment,'created_at'=>$current_date,'updated_at'=>$current_date));
            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  'Appointment has been done successfully.',
            );
        }

        echo json_encode($response_arr);
        exit(0);
    }
    ########### Patient Marking Apponitment To Doctors With Respect To Post --ENDS##########
    /* end code by covetus 1-9-2017 */

    ###########Get Doctor's reviews and ratings --STARTS################
    public function get_doctor_favourite()
    {
        if(Request::isMethod('post'))
        {
        	$request_details =	Request::all();
            $patient_id = $request_details['patient_id'];
            $auth_token = $request_details['auth_token'];
            $doctor_id  = $request_details['doctor_id'];

            $is_favourite = FavouriteDoctor::where('patient_id','=',$patient_id)
                                    ->where('doctor_id','=',$doctor_id)
                                    ->where('is_favourite','=','1')
                                    ->count();

            $response_arr = array(
                'status'    =>  '1',
                'is_favourite'       =>  $is_favourite
            );

            echo json_encode($response_arr);exit();
        }
    }
    ###########Get Doctor's reviews and ratings --ENDS################


    ###########Get favourite doctors for patient --STARTS###################
    public function get_favourite_doctors()
    {
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_id = $this->simple_decrypt($request_details['patient_id']);
            $auth_token = $request_details['auth_token'];

            $favourite_doctors = FavouriteDoctor::where('patient_id','=',$patient_id)
                                    ->where('is_favourite','=','1')
                                    ->get();

            $response_arr = array(
                'status'    =>  '1',
                'favourite_doctors'       =>  $favourite_doctors
            );

            echo json_encode($response_arr);exit();
        }
    }
    ###########Get favourite doctors for patient --ENDS###################


    ##################Save chat history --STARTS#######################
    public function save_chat_history()
    {
    	//date_default_timezone_set('Asia/Kolkata');
        if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_id = $request_details['patient_id'];
            $doctor_id = $request_details['doctor_id'];
            $post_id = $request_details['post_id'];
            $patient_content = $request_details['patient_content'];
            $doctor_content = $request_details['doctor_content'];
            $auth_token = $request_details['auth_token'];
            ChatHistory::create([
                'patient_id'            =>  $patient_id,
                'doctor_id'             =>  $doctor_id,
                'post_id'               =>  $post_id,
                'patient_content'       =>  $patient_content,
                'doctor_content'        =>  $doctor_content,
                'sent_time'             =>  time(),
            ]);


           $sitesettings = DB::table('sitesettings')->where('id',1)->first();
            if(!empty($sitesettings))
            {
                $admin_users_email = $sitesettings->value;
            }


           $patient_post_details = PatientPost::where('id','=',$post_id)->first();
           $patient_post_title = $patient_post_details->post_title;
           $doctor_details = DoctorDetails::where('doctor_id','=',$doctor_id)->first();
           $doctor_name = $doctor_details['first_name'].' '.$doctor_details['last_name'];
           $doctor_email = $doctor_details['email'];

           $patient_details = Patient::where('id','=',$patient_id)->first();
           $patient_name = $patient_details['name'].' '.$patient_details['last_name'];
           $patient_email = $patient_details['email'];
           $today = date("G:i:s T, F j Y");

          // print_r($patient_details);exit;

           if($patient_content != '')
           {
                 $reply_url = 'https://dentalchat.com/patient-profile/my-message/all/recent';
                $sent = Mail::send('service.dentalchat.chat_message_mail', array('name'=>$patient_name,'post_title'=>$patient_post_details->post_title,'profile_image'=>$patient_details->image,'content'=>$patient_content,'date'=>$today,'reply_url'=>$reply_url),
                       function($message) use ($admin_users_email, $doctor_email,$doctor_name,$patient_post_title)
                        {
                            $message->from($admin_users_email,'Dental Chat');
                            $message->to($doctor_email, $doctor_name)->subject("You have unread messages about the post ".' '.$patient_post_title);
                        });
           }
         else if($doctor_content != '')
           {

                $reply_url = 'https://dentalchat.com/dentist-messageboard';
               $sent = Mail::send('service.dentalchat.chat_message_mail', array('name'=>$doctor_name,'post_title'=>$patient_post_details->post_title,'profile_image'=>$doctor_details->profile_pics,'content'=>$doctor_content,'date'=>$today,'reply_url'=>$reply_url),
                       function($message) use ($admin_users_email, $patient_email,$patient_name,$patient_post_title)
                        {
                            $message->from($admin_users_email,'Dental Chat');
                            $message->to($patient_email, $patient_name)->subject("You have unread messages about the post ".' '.$patient_post_title);
                        });

           }



            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  "Chat history saved successfully."
            );

            echo json_encode($response_arr);exit();
        }
    }
    ##################Save chat history --ENDS#######################

    ###########Fetching recent post count from doctors for a post--STARTS############
    public function get_recent_post_count()
    {
    	if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $patient_id = $request_details['patient_id'];
            $is_read = $request_details['is_read'];
            $patient_posts = PatientPost::where('patient_id','=',$patient_id)
            ->get()
            ->toArray();
            $patient_post_arr = array();
            foreach($patient_posts as $v)
	        {
	            //$patient_post_arr = arra
	            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
	            ->where('is_read','=',$is_read)
	            ->count();

	            if($chat_history_count > 0)
            	{
            		$chat_history = ChatHistory::with('get_doctor_detail')->where('post_id','=',$v['id'])
	                ->where('is_read','=',$is_read)
	                ->orderBy('id','DESC')
	                ->first()
	                ->toArray();

	                $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
	                ->where('is_read','=',$is_read)
	                ->orderBy('id','DESC')
	                ->get()
	                ->toArray();

	                $sent_time  =   date("Y-m-d",$chat_history['sent_time']);
	                if($sent_time == date("Y-m-d"))
	                {
	                    $sent_time = date("g:i a",$chat_history['sent_time']);
	                }
	                else
	                {
	                    $sent_time = date("Y-m-d",$chat_history['sent_time']);
	                }

	                $doctor_education = DoctorEducation::where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
	                					->get();
	                $doctor_education_list = '';
	                foreach($doctor_education as $doctor_education_val)
	                {
	                	$doctor_education_list .= $doctor_education_val->degree.',';
	                }
	                $doctor_education_list = substr($doctor_education_list, 0, -1);

	                $doctor_clinic = DoctorClinic::where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
	                					->first();

	               	$is_favourite = FavouriteDoctor::where('patient_id','=',$patient_id)
	               								->where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
	               								->where('is_favourite','=','1')
	               								->count();
	               	if($is_favourite > 0)
	               	{
	               		$is_favourite_raw = "Favourite = Yes";
	               	}
	               	else
	               	{
	               		$is_favourite_raw = "Favourite = No";
	               	}

	                $chat_history_arr   =   array(
	                    'id'                    =>  $chat_history['id'],
	                    'patient_id'            =>  $chat_history['patient_id'],
	                    'doctor_id'             =>  $chat_history['doctor_id'],
	                    'post_id'               =>  $chat_history['post_id'],
	                    'patient_content'       =>  $chat_history['patient_content'],
	                    'doctor_content'        =>  $chat_history['doctor_content'],
	                    'get_doctor'        	=>  $chat_history['get_doctor_detail'],
	                    'doctor_education'      =>  $doctor_education_list,
	                    'doctor_clinic'      	=>  $doctor_clinic,
	                    'is_favourite'      	=>  $is_favourite,
	                    'is_favourite_raw'      =>  $is_favourite_raw,
	                    'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
	                );

	                $is_closed_chat = ClosedPostChat::where('patient_id','=',$chat_history['patient_id'])
	                					->where('doctor_id','=',$chat_history['doctor_id'])
	                					->where('post_id','=',$chat_history['post_id'])
	                					->count();

	                $doctor_avg_rating = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                                                ->avg('rating');

                    $tot_review_for_doctor = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                    							->where('review','<>','')
                    							->count();

                    $tot_rating_for_doctor = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                    							->where('rating','<>','0')
                    							->count();

	                $patient_post_arr[] = array(
		                'post_id'               	=>  $v['id'],
		                'patient_id'            	=>  $v['patient_id'],
		                'current_location'     		=>  $v['current_location'],
		                'emergency'             	=>  $v['emergency'],
		                'post_title'            	=>  $v['post_title'],
		                'description'           	=>  $v['description'],
		                'is_active'             	=>  $v['is_active'],
		                'is_closed_chat'        	=>  $is_closed_chat,
		                'chat_history_count'    	=>  $chat_history_count,
		                'chat_history_arr'      	=>  $chat_history_arr,
		                'chat_history_list'     	=>  $chat_history_list,
		                'doctor_avg_rating'     	=>  $doctor_avg_rating,
		                'tot_review_for_doctor'     =>  $tot_review_for_doctor,
		                'tot_rating_for_doctor'     =>  $tot_rating_for_doctor,

		            );
            	}

	        }

	        $response_arr = array(
	        	'status'	=>	'1',
	        	'patient_post_count'	=>	count($patient_post_arr)

        	);
        	echo json_encode($response_arr);exit();
        }
    }
    ###########Fetching recent post count from doctors for a post--ENDS############


    ###########Fetching recent post from doctors for a post--STARTS############
    public function get_recent_post()
    {
    	if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $post_id = '';
            if(isset($request_details['post_id']))
            {
               $post_id = $request_details['post_id'];
            }

    $page=(isset($request_details['page'])) ? $request_details['page'] : 0 ;
    $perpage=(isset($request_details['perpage'])) ? $request_details['perpage'] : 2000;

            $patient_id = $request_details['patient_id'];
            $is_read = $request_details['is_read'];

           if($post_id != '' && $post_id != 'all')
           {
                $patient_posts  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
                ->where('patient_posts.id','=',$post_id)->where('patient_posts.is_active','=','1')
                ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC')->get()->toArray();
           }
         else {
                $patient_post  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id`  ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('chat_histories', 'patient_posts.id', '=', 'chat_histories.post_id')->where('chat_histories.is_read','=',$is_read)
                 ->where('patient_posts.patient_id','=',$patient_id)->where('patient_posts.is_active','=','1')
                ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC')->groupBy('patient_posts.id');
                
                 $patient_posts_count = $patient_post->get()->count();
                 $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
          }


            $patient_post_arr = array();
            foreach($patient_posts as $v)
	        {
	            //$patient_post_arr = arra
	            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
	            ->where('is_read','=',$is_read)
	            ->count();

                $patient_des = Patient::where('id','=',$v['patient_id'])->orderBy('id','DESC')->get()->toArray();

	            if($chat_history_count > 0)
            	{
                $distinct_doctor = ChatHistory::where('post_id','=',$v['id'])
                                                ->where('is_read','=',$is_read)
                                                ->groupBy('doctor_id')
                                                ->orderBy('id','DESC')
                                                ->get(['doctor_id'])
                                                ->toArray();
                    foreach($distinct_doctor as $distinct_doctor_val)
                    {
                        $chat_history = ChatHistory::with('get_doctor_detail')->where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$distinct_doctor_val['doctor_id'])
                        ->where('is_read','=',$is_read)
                        ->orderBy('id','DESC')
                        ->first()
                        ->toArray();

                        /*$chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$distinct_doctor_val['doctor_id'])
                        ->where('is_read','=',$is_read)
                        ->orderBy('id','DESC')
                        ->get()
                        ->toArray();*/

                        $sent_time  =   $chat_history['sent_time'];
                        /*$sent_time  =   date("Y-m-d",$chat_history['sent_time']);
                        if($sent_time == date("Y-m-d"))
                        {
                            $sent_time = date("h:i a",$chat_history['sent_time']);
                        }
                        else
                        {
                            $sent_time = date("Y-m-d",$chat_history['sent_time']);
                        }*/

                        $doctor_education = DoctorEducation::where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
                                            ->get();
                        $doctor_education_list = '';
                        foreach($doctor_education as $doctor_education_val)
                        {
                            $doctor_education_list .= $doctor_education_val->degree.',';
                        }
                        $doctor_education_list = substr($doctor_education_list, 0, -1);

                        $doctor_clinic = DoctorClinic::where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
                                            ->first();

                        $is_favourite = FavouriteDoctor::where('patient_id','=',$patient_id)
                                                    ->where('doctor_id','=',$chat_history['get_doctor_detail']['doctor_id'])
                                                    ->where('is_favourite','=','1')
                                                    ->count();
                        $appointment = PatientDoctorAppointments::select('is_appointment','is_status','appointment_time')->where(array('patient_id'=>$patient_id,'post_id'=>$v['id'],'doctor_id'=>$chat_history['get_doctor_detail']['doctor_id']))->first();
                        $is_appointment = 0;
                        $is_status = 0;
                        $appointment_time = "0000-00-00 00:00:00";
                        if($appointment){
                            $is_appointment = $appointment->is_appointment;
                            $is_status = $appointment->is_status;
                            $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                        }
                        if($is_favourite > 0)
                        {
                            $is_favourite_raw = "Favourite = Yes";
                        }
                        else
                        {
                            $is_favourite_raw = "Favourite = No";
                        }



                        $chat_history_arr   =   array(
                            'id'                    =>  $chat_history['id'],
                            'patient_id'            =>  $chat_history['patient_id'],
                            'doctor_id'             =>  $chat_history['doctor_id'],
                            'post_id'               =>  $chat_history['post_id'],
                            'patient_content'       =>  $chat_history['patient_content'],
                            'doctor_content'        =>  $chat_history['doctor_content'],
                            'get_doctor'            =>  $chat_history['get_doctor_detail'],
                            'doctor_education'      =>  $doctor_education_list,
                            'doctor_clinic'         =>  $doctor_clinic,
                            'is_favourite'          =>  $is_favourite,
                            'appointment'           =>  $is_appointment,
                            'is_status'             =>  $is_status,
                            'appointment_time'      =>  $appointment_time,
                            'is_favourite_raw'      =>  $is_favourite_raw,
                            'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                        );

                        $is_closed_chat = ClosedPostChat::where('patient_id','=',$chat_history['patient_id'])
                                            ->where('doctor_id','=',$chat_history['doctor_id'])
                                            ->where('post_id','=',$chat_history['post_id'])
                                            ->count();

                        $doctor_avg_rating = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                                                    ->avg('rating');

                        $tot_review_for_doctor = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                                                    ->where('review','<>','')
                                                    ->count();

                        $tot_rating_for_doctor = DoctorReviewRating::where('doctor_id','=',$chat_history['doctor_id'])
                                                    ->where('rating','<>','0')
                                                    ->count();

                        $all_chat_count_for_this_post = ChatHistory::where('patient_id','=',$v['patient_id'])
                                                        ->where('doctor_id','=',$distinct_doctor_val['doctor_id'])
                                                        ->where('post_id','=',$v['id'])
                                                        ->count();

                        $all_unread_chat_count_for_this_post = ChatHistory::where('patient_id','=',$v['patient_id'])
                                                        ->where('doctor_id','=',$distinct_doctor_val['doctor_id'])
                                                        ->where('post_id','=',$v['id'])
                                                        ->where('is_read','=',0)
                                                        ->count();

                        $patient_post_arr[] = array(
                            'post_id'                   =>  $v['id'],
                            'patient_id'                =>  $v['patient_id'],
                            'current_location'          =>  $v['current_location'],
                            'emergency'                 =>  $v['emergency'],
                            'pain_level'                 =>  $v['pain_level'],
                            'post_title'                =>  $v['post_title'],
                            'description'               =>  $v['description'],
                            'is_active'                 =>  $v['is_active'],
                            'posted_date'               =>  $v['posted_date'],
                            'is_closed_chat'            =>  $is_closed_chat,
                            'chat_history_count'        =>  $all_chat_count_for_this_post,
                            'all_unread_chat_count_for_this_post'        =>  $all_unread_chat_count_for_this_post,
                            'chat_history_arr'          =>  $chat_history_arr,
                            //'chat_history_list'         =>  $chat_history_list,
                            'doctor_avg_rating'         =>  floor($doctor_avg_rating),
                            'tot_review_for_doctor'     =>  $tot_review_for_doctor,
                            'tot_rating_for_doctor'     =>  $tot_rating_for_doctor,

                        );


                    }


            	}

	        }

	        $response_arr = array(
	        	'status'	=>	'1',
	        	'patient_post_arr'	=>	$patient_post_arr,
	        	'post_count'=>$patient_posts_count

        	);
        	echo json_encode($response_arr);exit();
        }
    }
    ###########Fetching recent post from doctors for a post--ENDS############


    ##########Read chat for patients --STARTS################
    public function read_chat_for_patient()
    {
    	if(Request::isMethod('post'))
        {
            $request_details = Request::all();
            $doctor_id = $request_details['doctor_id'];
            $post_id = $request_details['post_id'];

            ChatHistory::where('doctor_id','=',$doctor_id)
            				->where('post_id','=',$post_id)
            				->where('is_read','=','0')
            				->update([
            					'is_read'	=>	'1'
        					]);

        	$response_arr = array(
        		'status'	=>	'1',
        		'msg'		=>	"You read the message successfully."
    		);
    		echo json_encode($response_arr);exit();
        }
    }
    ##########Read chat for patients --ENDS################


    ##########Patient unread chats --STARTS#################
    public function unread_chat_count_for_patient()
    {
    	if(Request::isMethod('post'))
        {
        	$request_details = Request::all();
            $patient_id = $request_details['patient_id'];

            $chat_history_count = ChatHistory::where('patient_id','=',$patient_id)
            						->where('is_read','=','0')
            						->count();

           	$response_arr = array(
           		'status'	=>	'1',
           		'unread_history_count'	=>	$chat_history_count
       		);
       		echo json_encode($response_arr);exit();
        }
    }
    ##########Patient unread chats --ENDS#################


    ###########Save review & rating for doctors --STARTS#############
    public function save_review_rating_for_doctors()
    {
    	if(Request::isMethod('post'))
        {
        	$request_details = Request::all();
            $patient_id = $request_details['patient_id'];
            $doctor_id = $request_details['doctor_id'];
            $review = $request_details['review'];
            $rating = $request_details['rating'];
            $post_id = $request_details['post_id'];

            DoctorReviewRating::create([
            	'patient_id'	=>	$patient_id,
            	'doctor_id'		=>	$doctor_id,
            	'review'		=>	$review,
            	'rating'		=>	$rating,
            	'post_id'		=>	$post_id,
            	'review_date'	=>	date("M d,Y"),
        	]);

        	$response_arr = array(
        		'status'	=>	'1',
        		'msg'		=>	"You have successfully saved review and rating"
    		);
    		echo json_encode($response_arr);exit();
        }
    }
    ###########Save review & rating for doctors --ENDS#############


    ###########Closed Post Chat from patients --STARTS############
    public function closed_post_chat()
    {
    	if(Request::isMethod('post'))
        {
        	$request_details = Request::all();
        	$patient_id = $request_details['patient_id'];
        	$doctor_id = $request_details['doctor_id'];
        	$post_id = $request_details['post_id'];

        	$is_post_closed = ClosedPostChat::where('patient_id','=',$patient_id)
        										->where('doctor_id','=',$doctor_id)
        										->where('post_id','=',$post_id)
        										->count();
        	if($is_post_closed == 0)
        	{
        		ClosedPostChat::create([
	        		'patient_id'		=>	$patient_id,
	        		'doctor_id'			=>	$doctor_id,
	        		'post_id'			=>	$post_id,
	        		'closed_time'		=>	time(),
	    		]);
        	}


    		$response_arr = array(
    			'status'	=>	'1',
    			'msg'		=>	"Mesaages has been closed successfully."
			);
			echo json_encode($response_arr);exit();
        }
    }
    ###########Closed Post Chat from patients --ENDS############


    ##################List Chat history --STARTS####################
    public function list_chat_history()
    {
        if(Request::isMethod('post'))
        {
            /*$request_details = Request::all();
            $patient_id = $this->simple_decrypt($request_details['patient_id']);
            $doctor_id = $request_details['doctor_id'];
            $post_id = $request_details['post_id'];
            $patient_content = $request_details['patient_content'];
            $doctor_content = $request_details['doctor_content'];
            $auth_token = $request_details['auth_token'];

            ChatHistory::create([
                'patient_id'            =>  $patient_id,
                'doctor_id'             =>  $doctor_id,
                'post_id'               =>  $post_id,
                'patient_content'       =>  $patient_content,
                'doctor_content'        =>  $doctor_content,
                'sent_time'             =>  time(),
            ]);

            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  "Chat history saved successfully."
            );
            echo json_encode($response_arr);exit();*/
        }
    }
    ##################List Chat history --ENDS####################


    public function get_chat_history()
    {
        //phpinfo();exit();
        $match = '1';

        $all_result=DB::connection("mongodb")->collection('chats')->raw(function($collection) use ($match)
        {

            return $collection->find();

            /*return $collection->aggregate(array(

            array(
                  '$match' => $match
            ),
              array(
                  '$group' => array(
                      '_id' => '$website',
                      'website'=>array('$first'=>'$website'),
                      'count' => array(
                          '$sum' => 1
                      ),
                      'maxprice' => array(
                          '$max' => '$price'
                      ),
                      'minprice' => array(
                          '$min' => '$price'
                      )
                  )
              )
          ));*/
        });

        echo "<pre>";print_r($all_result);exit();
    }

    public function save_doctors_xml()
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        $doctor_xml = DoctorDetails::get();
        foreach($doctor_xml as $v)
        {
            $xmlString .= '<url>';
            $xmlString .= '<Doctor_Name>'.$v->first_name.' '.$v->last_name.'</Doctor_Name>';
            $xmlString .= '<Date_Of_Birth>'.$v->date_of_birth.'</Date_Of_Birth>';
            $xmlString .= '<Email>'.$v->email .'</Email>';
            $xmlString .= '<Contact_No>'.$v->conuntry_code.' '.$v->contact_number .'</Contact_No>';
            $xmlString .= '<Image>'.$v->profile_pics.'</Image>';
            $xmlString .= '</url>';
        }

        $xmlString .= '</urlset>';

        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xmlString);
        $dom->save('xml/doctors.xml');
    }

    public function save_xml()
    {
        /*$xmlString = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <url>
                <loc>http://www.codexworld.com</loc>
                <lastmod>2016-07-04T07:46:18+00:00</lastmod>
                <changefreq>always</changefreq>
                <priority>1.00</priority>
            </url>
        </urlset>';

        $dom = new \DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xmlString);

        //View XML document
        //$dom->formatOutput = TRUE;
        //echo $dom->saveXml();

        //Save XML as file
        $dom->save('xml/sitemap.xml');
        */

        $xml = new \DOMDocument;
        $xml_album = $xml->createElement("Album");
        $xml_track = $xml->createElement("Track");
        $xml_album->appendChild( $xml_track );
        $xml->appendChild( $xml_album );

        $xml->save("xml/test1.xml");


    }
    /* start code by covetus 7-11-2017*/
    public function read_chat_patient(){
        $request_details = Request::all();
        $patient_id = $request_details['patient_id'];
        $post_id = $request_details['post_id'];
        ChatHistory::where(['patient_id'=>$patient_id,'post_id'=>$post_id,'patient_content'=>''])
        				->update(['is_read'	=>	'1']);

    	$response_arr = array(
    		'status'	=>	'1',
    		'msg'		=>	"You read the message successfully."
		);
		echo json_encode($response_arr);
		exit();
    }
    /* end code by covetus 7-11-2017*/
  /* code start by coetus 18-12-2017 */
  public function getUnreadmsgcronpatient(){

            $chat_history_count = ChatHistory::where('patient_content','=','')->where('is_read','=','0')->where('remind_count_patient','<','7')->get()->toArray();
            $repeat_array=array();
           foreach($chat_history_count as $chat_history){
                $remind_count= $chat_history['remind_count_patient']+1;
                ChatHistory::where(['id'=>$chat_history['id']])->update(['remind_count_patient'=>$remind_count]);
               if (!in_array($chat_history['patient_id'], $repeat_array)){
               $patient_id=$chat_history['patient_id'];
               $user_detail   = Patient::select('name','last_name','email')->where('id',$patient_id)->first();
                $dname=$user_detail['name'].' '.$user_detail['last_name'];
                $demail=$user_detail['email'];

    /******************************** email send ***********************************/
         $this->mailsend($patient_id,'Your DentalChat.com has unread message', $demail, $dname,$user_detail['name'],$user_detail['last_name'], 'unreadchatpatient' , 'Patient');

    /******************************** Email End here*******************************/
               }
                $repeat_array[]=$chat_history['patient_id'];

           }


  }
  
    public function  emailunsubscribe(){
           	$data 		= Request::all();
	        $patient_id = $data['patient_id'];
	        Patient::where('id', $patient_id)->update(['email_unsubscribe'=>1]);
    } 
    
    public function getUnreadmsgcrondoctor(){


            $chat_history_count = ChatHistory::where('doctor_content','=','')->where('is_read','=','0')->where('remind_count_doctor','<','7')->get()->toArray();
            $repeat_array=array();
           foreach($chat_history_count as $chat_history){
                $remind_count= $chat_history['remind_count_doctor']+1;
                ChatHistory::where(['id'=>$chat_history['id']])->update(['remind_count_doctor'=>$remind_count]);
                
               if (!in_array($chat_history['doctor_id'], $repeat_array)){
               $doctor_id=$chat_history['doctor_id'];
               
               $user_detail   =DoctorDetails::select('first_name','last_name','email')->where('doctor_id', $doctor_id)->first();
                $dname=$user_detail['first_name'].' '.$user_detail['last_name'];
                $demail=$user_detail['email'];
                $post_detail   = PatientPost::select('post_title')->where('id',$chat_history['post_id'])->first();
/******************************** email send ***********************************/
         $this->mailsend($doctor_id,'Unread Message', $demail, $post_detail['post_title'],$user_detail['first_name'],$user_detail['last_name'], 'unreadchat' , 'Patient');

/******************************** Email End here*******************************/
               }
                $repeat_array[]=$chat_history['doctor_id'];

           }


  }

private function mailsend($user_id=false,$subject= false, $email = false, $name = false,$fname=false,$lname=false, $mailtemplate = false, $business_name= false){

  $url = 'http://outbound.dentalchat.com/mailsend';

   try{
        $post = array(
        'user_id'=>$user_id,
        'name' => $name,
        'firstname' => $fname,
        'lastname' => $lname,
        'business_name'=>$business_name,
        'template_name'=>$mailtemplate,
        'email'=>$email,
        'subject'=>$subject
        );

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $result=curl_exec ($ch);
        curl_close ($ch);
         }
         catch(Exception $ex){
             //echo $ex->getMessage();
         }
    }
  /* code end by covetus  18-12-2017  */
}
