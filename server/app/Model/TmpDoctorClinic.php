<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TmpDoctorClinic extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'tmp_doctor_clinics';

     public function docDetails(){
    	return $this->belongsTo('App\Model\DoctorDetails', 'doctor_id','id');
    }
}

