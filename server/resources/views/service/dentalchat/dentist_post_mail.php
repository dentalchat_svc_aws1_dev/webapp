<!doctype html>
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta charset="utf-8">
<title>Patient Post</title>

</head>
 
<body>
<center>
  <style>
@import url('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
</style>
  <table width="620" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td style="background-repeat:no-repeat;background-position:0 0;background-size:cover;padding-left:30px;padding-top:30px;padding-right:30px;"><table width="560" border="0" cellpadding="0" cellspacing="0" style="">
          <thead>
            <tr>
              <td align="center" colspan="2" style="padding-bottom:22px; padding-top:22px;" bgcolor="#eef3fa">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td>
                        <a href="" target="_blank"><img src="<?php echo url();?>/public/backend/images/logo.png" alt="" style="width: 150px; height: 150px; display: block; object-fit: cover;"></a>
                      </td>
                    </tr>
                </table>
              
              </td>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#eef3fa">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td id="top_part" height="65" background="<?php echo url();?>/public/backend/images/top_image.png" style="background-repeat:no-repeat;background-size:cover;background-position:right 0;"></td>
            </tr>
            <tr>
              <td style="padding-left:30px;padding-right:30px; padding-top: 15px;" bgcolor="#f8f8f8"><table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                 <tr>
                  <td align="left">
                  <p style="font-size:20px;line-height:22px;color:#2e3192;font-family:'Lato', sans-serif;font-weight:700;display:block;text-align:left;margin:0;margin-bottom:20px;display:block;">Hello Dr. <?php echo $doctor_name; ?>,</p>
                  </td>
                  </tr> 
                  <tr>
                    <td align="center">
                      <div style="width:96%; font-size:16px;line-height:22px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:700;background:#fff;border:1px solid #2e3192;border-radius:25px;  display: inline-block;margin-bottom:30px;">
                      
                       <p style="margin:0;margin-top:13px;margin-bottom:13px;margin-left:25px;margin-right:25px;">
                          <span>A patient <?php echo $patient_name; ?> has some dental problem. He created a post regarding this.Please look below for details:-</span>
                        </p>
                         <p style="margin:0;margin-top:13px;margin-bottom:13px;margin-left:25px;margin-right:25px;">
                          <span>Title : </span><?php echo $post_title; ?><br />    
                          <span>Description : </span><?php echo $description; ?><br />
                          <span>Pain Level : </span><?php echo $pain_level; ?><br />
                          <span>Emergency : </span><?php echo $emergency; ?><br />
                          <span><a href="https://dentalchat.com/dentist-signin">Click here to reply</a></span>

                        </p>
                      </div>
                      </td>
                  </tr>
                  
                  
                </table></td>
            </tr>
            <tr>
              <td id="bot_part" background="<?php echo url();?>/public/backend/images/bot_image.png" height="65" style="background-size:cover;background-position:right;background-repeat:no-repeat;"></td>
            </tr>
              </table>
              
              </td>
            </tr>
          </tbody>
          <tfoot>
           <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#2e3192">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td align="center" style="font-size: 18px;color: #ffffff;line-height: 22px;font-weight: 300;font-family: 'Lato', sans-serif;padding-top: 17px;padding-bottom: 17px;">Thank you, <a href="<?php //echo $activation_url;?>" target="_blank" style="font-weight:700;color:#fff;text-decoration:none;">Dental Chat<?php //echo $sitename;?></a></td>
            </tr>
            </table>
            </td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</center>
</body>
</html>

<!-- <html>
  <head>
    <title>Patient Post Email</title>
  </head>
  <body>
    Hi Dr. <?php //echo $doctor_name; ?>
    <br />
    A patient <?php //echo $patient_name; ?> has some problem. He created a post for his problem.
    <br />
    Please look below for hsi problem:-
    <br />
    <br />

    Title : <?php //echo $post_title; ?><br />    
    Description : <?php //echo $description; ?><br />
    Pain Level : <?php //echo $pain_level; ?><br />
    Emergency : <?php //echo $emergency; ?><br />

    <br /><br />
    Regards,
    Dental Chat Team.
  </body>
</html> -->