/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.home')
        .controller('HomePageCtrl', HomePageCtrl);

    /** @ngInject */
    /* start code by covetus 3-aug-2017 */
    /* Inject two more dependency here [$localStorage,$state]*/
    /* end code by covetus 3-aug-2017 */
    function HomePageCtrl($scope, $timeout, $http, configService, $rootScope, $location,  homeService,$localStorage,$stateParams,$state) {
        
        if ($rootScope.curr_state == 'unverifiedcron') {
            $http.get(configService.getEnvConfig().apiURL + "/unverifiedcron")
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        }
     if ($rootScope.curr_state == 'unreadmsgcronpatient') {
            $http.get(configService.getEnvConfig().apiURL + "/unreadmsgcronpatient")
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        }
          if ($rootScope.curr_state == 'unreadmsgcrondoctor') {
            $http.get(configService.getEnvConfig().apiURL + "/unreadmsgcrondoctor")
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        }   
     if ($rootScope.curr_state == 'uncomprofile') {
            $http.get(configService.getEnvConfig().apiURL + "/uncomprofile")
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        } 
     if ($rootScope.curr_state == 'pateintemailunsubscribe') {
         
            var para = $stateParams.parameter;
            var data = {
                patient_id: para
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }};
            $http.post(configService.getEnvConfig().apiURL + "/pateintemailunsubscribe/"+$stateParams.parameter,data,config)
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        }  
         if ($rootScope.curr_state == 'dentistemailunsubscribe') {
            var para = $stateParams.parameter;
            var data = {
                dentist_id: para
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }};
            $http.post(configService.getEnvConfig().apiURL + "/dentistemailunsubscribe/"+$stateParams.parameter,data,config)
            .success(function(data, status, headers, config) {
               console.log(data.home_setting);
            })
        }      
        
        /* start code by covetus 4-aug-2017 */
        /* for initialize error messages model value*/
        $scope.postTitle_error_required = $scope.postTitle_error_minlength = $scope.postDesc_error_required = $scope.postDesc_error_minlength = false;
        /* end code by covetus 4-aug-2017 */
    	/*check the accomodation_id is for that user or not*/
        $scope.homesetting = function(page_id) {
			var data = {
				page_id:page_id            
            };
            /* start code by covetus 3-aug-2017 */
            /* for clear localstorage variable used in Get Started form */
			$localStorage.getStartedData = {};
			/* start code by covetus 3-aug-2017 */
			$rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/homesetting/home-settings",data)
            .success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;                
				$scope.home_details  = data.home_setting;
				if(data.wp_blogs){
				    $scope.wp_blogs = data.wp_blogs;
				}
				//console.log(data.home_setting);
            })
            
            .error(function(data, status, header, config) {
                //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }

        $scope.testimonialDetails = function() {
            
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/homesetting/testimonial-details")
            .success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;                
                $scope.testimonial_details  = data.all_testimonial;
                //console.log($scope.testimonial_details);

                setTimeout(function(){
                    clientSpeak();
                },500);
            })
            
            .error(function(data, status, header, config) {
               //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        /* start code by covetus 4-aug-2017 */     
        /* function for Get Start form field  */
        $scope.get_started_fun = function(getPost){
            
            var post_desc = typeof getPost.post_desc != 'undefined'?getPost.post_desc:'';
            var post_title = typeof getPost.post_title != 'undefined'?getPost.post_title:'';
            //$scope.patient = 
            $localStorage.setStartedData = {'title':post_title ,'description':post_desc};
            
            $state.go('create-post', {'title':post_desc});
            /*if($scope.is_front_logged==2){
                $state.go('create-post', {'title':post_title});
            }else{
                $state.go('create-post', {'title':post_title});
            }*/ 
            
        }
        /* end code by covetus 4-aug-2017 */
    }
})();
