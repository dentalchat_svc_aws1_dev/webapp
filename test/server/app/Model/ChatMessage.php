<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'chat_messages';
    /*public function getUserDetail()
	{
		return $this->belongsTo('App\Model\SiteUser','user_id');
	}*/
}
