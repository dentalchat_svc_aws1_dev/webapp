<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route:: post('/mailsend',array('uses'=>'Elasticmail\ElasticController@postMailsend','middleware' => 'csrf'));
Route:: post('/mailsend','Elasticmail\ElasticController@postMailsend');
Route:: post('/dentistmailsend','Elasticmail\ElasticController@postMailsendpateintpost');
Route:: get('/viewtemp','Elasticmail\ElasticController@getViewtem');

