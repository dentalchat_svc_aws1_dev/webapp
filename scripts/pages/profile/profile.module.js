/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.profile', ['chart.js','ui.bootstrap', 'ui.bootstrap.datetimepicker'])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) 
	{
		$stateProvider.state('dentistProfileStep1', {
								url: '/dentist-profile-step1',
								templateUrl: 'scripts/pages/profile/dentist_profile_step1.html',
								controller: 'profilePageCtrl',
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
						.state('dentistProfileStep2', {
								url: '/dentist-profile-step2',
								templateUrl: 'scripts/pages/profile/dentist_profile_step2.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
						.state('dentistProfileStep3', {
								url: '/dentist-profile-step3',
								templateUrl: 'scripts/pages/profile/dentist_profile_step3.html',
								controller: 'profilePageCtrl',
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }									
							})
						.state('dentistProfileStep4', {
								url: '/dentist-profile-step4',
								templateUrl: 'scripts/pages/profile/dentist_profile_step4.html',
								controller: 'profilePageCtrl',
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }									
							})
						.state('dentistProfileStep5', {
								url: '/dentist-profile-step5',
								templateUrl: 'scripts/pages/profile/dentist_profile_step5.html',
								controller: 'profilePageCtrl',
								activename:'step',	
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
						.state('dentistProfileStep6', {
								url: '/dentist-profile-step6',
								templateUrl: 'scripts/pages/profile/dentist_profile_step6.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
						.state('dentistProfileStep7', {
								url: '/dentist-profile-step7',
								templateUrl: 'scripts/pages/profile/dentist_profile_step7.html',
								controller: 'profilePageCtrl',
								activename:'step',	
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                        $rootScope.stepAlreadyFill();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
						.state('dentistprofile', {
								url: '/dentist-profile/:params',
								templateUrl: 'scripts/pages/profile/dentist_profile.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                   else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }	
							})
						.state('dentistpreview', {
								url: '/dentist-preview',
								templateUrl: 'scripts/pages/profile/dentist_preview.html',
								controller: 'profilePageCtrl',
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }					
							})
						.state('changepassword', {
								url: '/change-password',
								templateUrl: 'scripts/pages/profile/change_password.html',
								controller: 'profilePageCtrl',	
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})
                        .state('dentistsettings', {
								url: '/dentist-settings',
								templateUrl: 'scripts/pages/profile/dentist_settings.html',
								controller: 'profilePageCtrl',	
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }								
							})    
						.state('dentistmessageboard', {
								url: '/dentist-messageboard',
								templateUrl: 'scripts/pages/profile/dentist_messageboard.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                	console.log($rootScope.is_front_logged);
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }	
							})
						.state('dentistcheckout', {
								url: '/dentist-checkout/:amt',
								templateUrl: 'scripts/pages/profile/dentist_checkout.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                	console.log($rootScope.is_front_logged);
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }	
							})        	
						.state('dentistmydashboard', {
								url: '/dentist-dashboard',
								templateUrl: 'scripts/pages/profile/dentist_dashboard.html',
								controller: 'profilePageCtrl',	
								activename:'step',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                	console.log($rootScope.is_front_logged);
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }	
							})
							.state('dentistpricing', {
								url: '/dentist-pricing',
								templateUrl: 'scripts/pages/profile/dentist_pricing.html',
								controller: 'profilePageCtrl',	
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                	console.log($rootScope.is_front_logged);
				                                    if ($rootScope.is_front_logged==1){				                                    	
				                                        $rootScope.checkLastServieCallTimeAfterLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/dentist-signin');
				                                    }
				                                    
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }	
							});




    }
	
})();
