/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.components')
        .directive('pageTop', pageTop);

    /** @ngInject */
    function pageTop($state) {
        return {
            restrict: 'E',            
            templateUrl:function(){
                /*if($state.current.name =='promopage'){
                    return;
                }else{*/
                    return 'scripts/components/pageTop/pageTop.html'
               // }
                
            }, 
            controller: 'pageTopCtrl'
        };
    }

})();
