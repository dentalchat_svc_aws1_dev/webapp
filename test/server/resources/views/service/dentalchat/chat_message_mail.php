<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>dental chat</title>
  </head>
  <body style="background: #eef3fa">
      <table cellpadding="0" cellspacing="0" border="0" width="600" align="center"
      style="font-family: arial; font-size: 14px; line-height: 15px; color: #000;">
        <tr>
          <th align="center" valign="top" style="padding-top: 15px;">
             <a href="<?php echo $siteurl;?>" target="_blank"><img src="<?php echo url();?>/public/backend/images/logo.png" alt=""></a>
          </th>
        </tr>
        <tr>
          <td align="left" valign="top" style="padding-top: 30px;">
            <table width="100%" bgcolor="#FFF" style="padding: 20px;">
              <tr>
                <td align="center" valign="top">
                  <h2 style="margin: 0; padding: 0 0 15px 0;
                  border-bottom: 1px solid #c9c9c9; font-weight: normal; font-size: 12px;
                  line-height: 14px;">You have unread messages about the post "<?php echo $post_title; ?>"</h2>
                </td>
              </tr>
              <tr>
                <td align="left" valign="top" style="padding-top: 30px;">
                  <table width="100%">
                    <tr>
                      <td align="left" valign="top" width="51">
                       <?php 
                         if($profile_image != '')
                          { ?>
                                <span style="display: block; width:51px; height: 51px; border-radius: 100%; 
                                background: #aaaaaa;">

                                  <img src="<?php echo url();?>/uploads/patient_profile_image/<?php echo $profile_image; ?>" alt="" style="width: 100%; height: 100%; object-fit: cover;">
                                </span>

                         <?php } 
                              else
                              {?>
                               
                               <span style="display: block; width:51px; height: 51px; border-radius: 100%; 
                                background: #aaaaaa;">
                                  <img src="<?php echo url();?>/uploads/dentist_profile_image/no_image.jpg" alt="" style="width: 100%; height: 100%; object-fit: cover;">
                                </span>

                            <?php  } ?>
                       

                      </td>
                      <td align="left" valign="top" width="85%">
                        <h3 style="margin: 0; padding: 10px 0 10px 0;
                        font-weight: bold; font-size: 12px;
                  line-height: 14px;"><?php echo $name; ?><span style="font-weight: normal;
                  font-size: 11px; color: #a9a6a6; display: block; padding-top: 10px; margin: 0;"><?php echo $date; ?></span></h3>
                      <p style="font-weight: normal; margin: 0;
                  font-size: 12px; line-height: 22px; color: #a9a6a6; display: block; 
                  padding-top: 10px;">
                   <?php echo $content; ?>
                  </p>
                  <h4 style="margin: 0; padding: 30px 0 10px 0;
                        font-weight: normal; font-size: 12px;
                  line-height: 14px;">Cheers <span style="font-weight: normal;
                  font-size: 11px; color: #a9a6a6; display: block; padding-top: 10px; margin: 0;"><?php echo $name; ?></span></h4>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="padding-top: 30px;">
                <a href="<?php echo $reply_url; ?>" style="background: #2e3192; color: #FFF;
                  padding: 15px 30px; font-size: 14px; line-height: 14px;
                  border:none; cursor: pointer;">Reply</a>
                  
                </td>
              </tr>
             
            </table>
          </td>
        </tr>
      </table>
  </body>
</html>