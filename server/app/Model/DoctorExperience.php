<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorExperience extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_experiences';
     
}

