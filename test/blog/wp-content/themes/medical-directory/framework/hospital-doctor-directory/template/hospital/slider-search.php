<?php


$radius=get_option('_iv_radius');
$keyword_post='';
$back_ground_color='0099fe';
if(isset($atts['bgcolor']) and $atts['bgcolor']!="" ){
	$back_ground_color=$atts['bgcolor'];
}
$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='hospital';}	

$directory_url_2=get_option('_iv_directory_url_2');					
if($directory_url_2==""){$directory_url_2='doctor';}	
?>

<form  action="<?php echo get_post_type_archive_link( $directory_url_2) ; ?>" method="POST"  class="form-inline advanced-serach" id="searchformhd" onkeypress="return event.keyCode != 13;">
	<div class="container">
	 <div class="input-field">

			<!-- <div class="" >
          <div class="form-group" >
					   <input type="text" class="cbp-search-input" id="keyword" name="keyword"  placeholder="<?php esc_html_e( 'Filter By Keyword', 'medico' ); ?>" value="<?php echo $keyword_post; ?>">
			     </div>
        </div>-->


				<div class="" >
					<div class="form-group" >
						<input type="text" class="cbp-search-input location-input" id="address" name="address"  placeholder="<?php esc_html_e( 'Location', 'medico' ); ?>"
						value="">
						<i class="fa fa-map-marker marker"></i>
						<input type="hidden" id="latitude" name="latitude"  value="" >
						<input type="hidden" id="longitude" name="longitude"  value="">
					</div>
			  </div>
			  
			 <div class="" >
				  <div class="form-group" >
						<i class="fa fa-chevron-down arrow"></i>
						<select name="dir_specialties"  id="dir_specialties" class="cbp-search-select">
							<option  class="cbp-search-select" value=""><?php esc_html_e('Choose a Specialty','medico'); ?></option>	
							<?php 
							$taxonomy = $directory_url_2.'-category';
							$args = array(
								'orderby'           => 'name',
								'order'             => 'ASC',
								'hide_empty'        => true,
								'exclude'           => array(),
								'exclude_tree'      => array(),
								'include'           => array(),
								'number'            => '',
								'fields'            => 'all',
								'slug'              => '',
								//'parent'            => '0',
								'hierarchical'      => true,
								'child_of'          => 0,
								'childless'         => false,
								'show_count'               => '1',

							);
				$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
				if ( $terms && !is_wp_error( $terms ) ) {
						$i=0;
						foreach ( $terms as $term_parent ) {

							if($term_parent->count>0){
								?>
								<option  class="cbp-search-select" value="<?php echo $term_parent->name; ?>"><?php echo $term_parent->name; ?></option>
								<?php
							}
						}
					}
								?>	
											
						</select>
				  </div>
			  </div>

			 <div class="" style="display:none;">
				  <div class="form-group" >
						<i class="fa fa-chevron-down arrow"></i>
						<select name="dir_type"  id="dir_type" class="cbp-search-select">												<option class="cbp-search-select"  value="rurl_2"><?php echo ucfirst($directory_url_2); ?></option>
						<option  class="cbp-search-select" value="rurl_1"><?php echo ucfirst($directory_url_1); ?></option>
						
						</select>
				  </div>
			  </div>

				<div class="" >
          <div class="form-group search" >
					     <button type="button" id="search_submit_m" name="search_submit_m"  onClick='submitSearchForm()' class="btn-new btn-custom-search "> <i class="fa fa-search"></i> <span><?php esc_html_e( 'Search', 'medico' ); ?></span></button>
				  </div>
        </div>
		 </div>
  </div>
</form>

<?php 
 wp_enqueue_script( 'search-form-js', medicaldirectory_JS.'search-form.js', array('jquery'), $ver = true, true );
 wp_localize_script( 'search-form-js', 'jsdata', array( 'hospital_url' => get_post_type_archive_link( $directory_url_1),'doctor_url'=> get_post_type_archive_link( $directory_url_2 )) );
 
?>   
 

