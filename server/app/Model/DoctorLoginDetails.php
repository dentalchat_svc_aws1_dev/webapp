<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorLoginDetails extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_login_details';
	
	public function getUserDetail()
	{
		return $this->belongsTo('App\Model\DoctorDetails','doctor_id');
	}
}

