    <div class="box-shadow-for-ui">
      <div class="uou-block-2a">
        <div class="container">
          <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> <img src="<?php echo esc_url(medicaldirectory_IMAGE); ?>logo.png" alt="<?php esc_html_e( 'image', 'medical-directory' ); ?>"> </a>
          <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>

          <div class="contact">
            <span><?php esc_html_e( 'Call Us:', 'medical-directory' ); ?>  </span>
            <a href="tel:(02)1234567890">(02) 123-456-7890</a>
          </div>
        </div>
      </div> <!-- end .uou-block-2a -->
    </div>