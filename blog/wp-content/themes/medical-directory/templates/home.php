<?php
/**
 * Template Name: Home Page
 *
 */
wp_enqueue_style( 'iv_directories-font', 'https://fonts.googleapis.com/css?family=Raleway');

?>
<?php get_header(); ?>
<?php
$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='hospital';}	

$directory_url_2=get_option('_iv_directory_url_2');					
if($directory_url_2==""){$directory_url_2='doctor';}


 if(isset($medicaldirectory_option_data['medicaldirectory-show-page-banner-image']) AND $medicaldirectory_option_data['medicaldirectory-show-page-banner-image']==1){
		$top_banner_image= medicaldirectory_IMAGE."home-top.jpg";
		 if(isset($medicaldirectory_option_data['medicaldirectory-home-banner-image']['url']) AND $medicaldirectory_option_data['medicaldirectory-home-banner-image']['url'] !=""){
			$top_banner_image=  $medicaldirectory_option_data['medicaldirectory-home-banner-image']['url'];
		}
		$page_name_reg=get_option('_iv_directories_registration' );
		$register_link= get_permalink($page_name_reg);
		$hospital_link=get_post_type_archive_link($directory_url_1);
		$doctor_link=get_post_type_archive_link( $directory_url_2 );
 ?>
<div class="box-2">
<div class="container">
<div class="col-xs-12 col-sm-6 box-2-inner">
<h1> Do You Need Dental Help? </h1>
<p>Fill out this quick <span style="color: rgb(13, 153, 163); font-size: 18px;"><b>DentalChat</b></span> general information form to start your <b>Communication</b> with <b>Local Dental Offices</b> and <b> Dentists.</b></p>
<a href="http://blog.dentalchat.com/support/" ><button type="submit" class="btn btn-default">Create Dental Request</button></a>
</div>
<div class="col-xs-12 col-sm-6">
<img src="http://blog.dentalchat.com/wp-content/themes/medical-directory/assets/img/box-2ig.jpg">
</div>
</div>
</div>
 <div class="medicaldirectory-home-banner" style="background: url('<?php echo esc_attr($top_banner_image);?>') top center no-repeat;">
		<div class="overlay"></div>
		<div class="banner-content">
			<div class="container">
				<div  class="home-banner-text">
					<div class="row">
						<div class="text-center">
							<div class="banner-icon">
								<i class="fa fa-user-md"></i>
							</div>
							<h2>
								<?php
									echo esc_attr($medicaldirectory_option_data['medicaldirectory-home-banner-text']);
								?>
							</h2>

						</div>
					</div>
					<div class="row">
						<div class="text-center">
							<p>
								<?php
									$banner_subtitle= (isset($medicaldirectory_option_data['medicaldirectory-home-banner-subtitle'])?$medicaldirectory_option_data['medicaldirectory-home-banner-subtitle']:"");
									echo esc_attr($banner_subtitle);
								?>
							</p>

						</div>
					</div>

				</div>
				<div class="home-banner-button text-center">
					<?php
					$hospital_banner_button_text= (isset($medicaldirectory_option_data['medicaldirectory-home-banner-button1'])?$medicaldirectory_option_data['medicaldirectory-home-banner-button1']:"");
					echo '<button type="button" class="btn-new btn-custom" onclick="location.href=\''.$hospital_link.'\'" >'. $hospital_banner_button_text.'</button>';
					?>

					<?php
					$hospital_banner_button_text2= (isset($medicaldirectory_option_data['medicaldirectory-home-banner-button2'])?$medicaldirectory_option_data['medicaldirectory-home-banner-button2']:"");
					echo '<button type="button" class="btn-new btn-custom-white" onclick="location.href=\''.$doctor_link.'\'" >'. $hospital_banner_button_text2.'</button>';
					?>
					<button style="background-color: #f58127; border: medium none;" onclick="location.href='http://empirestudioz.com/demo/doctor'" class="btn-new btn-custom-white" type="button">Chat with Dentist</button>		
				</div>
			</div>
			<div class="home-search-content">
				<?php
					echo do_shortcode("[search_box bgcolor='1d1d1d']");
				?>
			</div>

		</div>


</div>
<?php } ?>

<?php echo apply_filters('the_content',$post->post_content); ?>

<?php get_footer();?>