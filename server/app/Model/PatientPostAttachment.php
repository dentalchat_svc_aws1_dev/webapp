<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PatientPostAttachment extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'patient_post_attachments';
}
