<?php namespace App\Http\Controllers\Admin;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Country; /* Model name*/
//use App\Model\Restaurant; /* Model name*/
use App\Model\Category; /* Model name*/
use App\Model\SiteUser; /* Model name*/
use App\Model\Sitesetting; /* Model name*/
use App\Model\Language; /* Model name*/
use App\Model\City; /* Model name*/
use App\Model\Job; /* Model name*/
use App\Model\DocJob;

use App\Model\Testimonial;

use App\Book;
use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;
use Cookie;
use Yajra\Datatables\Datatables;


class TestimonialController extends BaseController {

  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
    	parent::__construct();
    }
    
	/********************************************************************************
	 *								GET USER LIST									*
	 *******************************************************************************/
    function testimonialList()
    {
    	$module_head 		= "Testimonial List";
        $blog_class 		= "active";
        $active 			= Request::query('active');
        $title				= "Testimonial List";

        return view('admin.testimonial.testimonial_list',compact('blog_class',
												'module_head',
												'active','title'));
    }
	
	/********************************************************************************
	 *									EDIT USER 									*
	 *******************************************************************************/
	function ajaxTestimonialList(){
		
        $where = "1";
		
        $all_blog =  Testimonial::select('testimonial.*');

		
		return Datatables::of($all_blog)
		->addColumn('checkbox_td', function ($all_blog) {
                return '<input type="checkbox"  recordType="multipleRecord" multipleRecord="'.$all_blog->id.'" />';
            })
		 ->addColumn('action', function ($all_blog) {
                return '<a href="'.url().'/admin/add/edit/'.$all_blog->id.'" title="View"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;<a href="javascript:void(0);" onclick="removetestimonial('.$all_blog->id.')" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            })
		 ->editColumn('status', function ($all_blog) {
		 	if($all_blog->status == 1){
             	$status_html = '<select disabled style="width:110px;" name="status" id="user_active_'.$all_blog->id.'" onchange="changeJobStatus(this.value,'.$all_blog->id.')"><option value="1" selected>Approve</option><option value="0">Unapprove</option></select><br /><span class="alert-success" id="success_status_span_'.$all_blog->id.'" style="display:none;"></span>';
		 	}else{
		 		$status_html = '<select style="width:110px;" name="status" id="user_active_'.$all_blog->id.'" onchange="changeJobStatus(this.value,'.$all_blog->id.')"><option value="1">Approve</option><option value="0" selected>Unapprove</option></select><span class="alert-success" id="success_status_span_'.$all_blog->id.'" style="display:none;"></span>';

		 	}
		 	return $status_html;
            })
		 ->editColumn('name', function ($all_blog) {
		 	//return $all_blog->users_details->name." ".$all_blog->users_details->last_name;
		  })
		->make(true);
	}

	function addtestimonial()
	{
		$module_head 		= "Add Testimonial";
        $blog_class 		= "active";
        $active 			= Request::query('active');
        $title				= "Add Testimonial";
        return view('admin.testimonial.add_testimonial',compact('blog_class',
												'module_head',
												'active','title'));
	}
	function postAddtestimonial()
	{
		if(Request::isMethod('post'))
		{
			$data = Request::all();

			$add_blog		= Testimonial::create([
									'reviewer_name'         => trim($data['reviewer_name']),
									'occupation'       => $data['occupation'],
									'feedback'          => $data['feedback'],
									'posted_date'	=> date('Y-m-d H:i:s')
								]);

			Session::flash('success_message', 'Testimonial has been added successfully.'); 

			Session::flash('alert-class', 'alert alert-success'); 
			return redirect('admin/testimonial/list');
		}
		
	}
	public function getedittestimonial($blog_id)
	{
		$module_head 		= "Edit Testimonial";
        $blog_class 		= "active";
        $active 			= Request::query('active');
        $title				= "Edit Testimonial";
		
		$blog_arr = Testimonial::where("id",$blog_id)->first();
		//print_r($blog_arr);exit;
		
        return view('admin.testimonial.testimonial_blog',compact('blog_class',
												'module_head',
												'active','title','blog_arr'));
	}
	function postedittestimonial()
	{
		if(Request::isMethod('post'))
		{
			$data     = Request::all();
			$testimonial 		 	= Testimonial::find($data['blog_id']);
			$testimonial->reviewer_name 	= trim($data['reviewer_name']);
			$testimonial->occupation 	= $data['occupation'];
			$testimonial->feedback 	= $data['feedback'];
			$testimonial->save();
			
			Session::flash('success_message', 'Testimonial has been updated successfully.'); 

			Session::flash('alert-class', 'alert alert-success'); 
			return redirect('admin/testimonial/list');
		}
	}
	function postCheckSlug()
	{
		$data = Request::all();
		echo $blog_slug_count = Blog::where("slug",$data['blog_slug'])->count();
		die;
	}
	function deleteTestimonial($blog_id)
	{
		$blog_arr = Testimonial::where("id",$blog_id)->first();
		Testimonial::where('id', $blog_id)->delete();
		
		Session::flash('success_message', 'Testimonial has been deleted successfully.'); 

		Session::flash('alert-class', 'alert alert-success'); 
		return redirect('admin/testimonial/list');
		
	}
	
}
