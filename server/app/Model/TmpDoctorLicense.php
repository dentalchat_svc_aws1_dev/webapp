<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TmpDoctorLicense extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'tmp_doctor_licenses';

     
}

