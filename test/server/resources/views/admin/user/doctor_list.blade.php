@extends('admin/layout/admin_template')
 
@section('content')
	
<style>
.country input{margin-right:0px;}
#search_key{
	margin-right:10px;
}
.country select{margin-right:10px;}

.country .btn{
	    position: relative;
    top: -5px;
}

.country tr{
	float:right;
	width:100%;

}

.country select {
    margin-right: 10px;
    width: 190px;
}


.country button, .country input[type="button"], .country input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer;
    width: 90px;
}
.tractive td{
    background-color: #ffe7dc !important;
}
.searchtr{
    margin-bottom: 17px;border-bottom: 1px solid #ccc;padding-bottom:9px;
}
</style>
@if(Session::has('success_message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success_message') }}</p>
@endif

@if(Session::has('failure_message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('failure_message') }}</p>
@endif

<script type="text/javascript">
function goForSearch(){
    var search_key 		= $('#search_key').val();
    var signup_page     = $('#signup_page').val();
    var is_active  		= $('#is_active').val();
    var date  		    = $('#date').val();
    var url = '<?php echo url(); ?>/admin/dentist/list?';
    url += 'search_key='+search_key+'&date='+date+'&active='+is_active+'&signup_page='+signup_page;
    window.location.href = url;
}
</script>

<div class="country">
    <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
        <tr class="searchtr">
            <td>
        <tr>
            <td widht="40%">
				<input type="text" class="span4" value="<?php echo isset($search_key) ? $search_key : ''; ?>" name="search_key" id="search_key" placeholder="Search Dentists Name, Email, Contact" class="field" />
			</td>
			<td widht="30%">
				<input type="text" value="<?php echo isset($date) ? $date : ''; ?>" name="date" data-date-format="mm/dd/yyyy" id="date" placeholder="Select Date" class="field" readonly/>
			</td>
            <td width="30%">
                <select name="is_active" id="is_active" class="field">
                    <option value="">Status</option>
                    <option value="1" <?php echo $active=='1' ? "selected='selected'" : ""; ?>>Active</option>
                    <option value="0" <?php echo $active=='0' ? "selected='selected'" : ""; ?>>Inactive</option>
                </select>
            </td>
            </tr>
            <tr>
            <td width="50%">
                <select name="signup_page" id="signup_page" class="field">
                    <option value="">Signup page</option>
                    foreach ($signup_page as $signup_by) {
                        echo"<pre>";
                        print_r($signup_by->src_sys);
                    }
                    @foreach ($signup_page as $signup_by)
                        @if (!empty($signup_by->src_sys))
                            <option value="{{ $signup_by->src_sys }}"<?php echo $signup_by_user==$signup_by->src_sys ? "selected='selected'" : ""; ?>>{{ $signup_by->src_sys }}</option>
                        @endif
                    
                    @endforeach
                    
                </select>
            </td>
           
            <td width="50%" style="padding:0px 1px; text-align:right;">
                <input type="button" onclick="goForSearch();" class="btn" name="btn_search_hotel" id="btn_search_hotel" value="Search">
				<input type="button" onclick="window.location.href='<?php echo url(); ?>/admin/dentist/list';" class="btn" name="btn_search_hotel" id="btn_search_hotel" value="Reset" />
            </td>
            </tr>
            </td>
        </tr>
		<tr>
             <td>
                    <select name="multiple_operation_status" id="multiple_operation_status">
                        <option value="">Change Status</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    <a href="javascript:void(0);" onclick="globalActiveInactiveMultipleRecords('doctors');" class="btn">Save</a>
                    <span style="color:red;" id="selected_record_msg"></span>
                </td>

        </tr>
        <tr>
            <td class="text-right" style="display:block">
                <a href="<?php echo url(); ?>/admin/dentist/dentist-step1" class="btn">Add Dentist</a>
            </td>
        </tr>
	
    </table>
</div>

{!! Form::open(['url' => 'admin/dentist-list','method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'']) !!}
<div class="module">
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped  display user-datatable" width="100%">
    <thead>
        <tr style="color:#FFFFFF; background:#141b27;">
            <th>Id</th>
			<th widht="5%" style="align:center;"><input type="checkbox" name="check_all_records" id="check_all_records" onclick="selectAllRecords();" /></th>
			<th width="5%">Dentist Id</th>
			<th width="10%">Dentist Name</th>
            <th width="5%">Email</th>
            <th width="5%">Contact</th>
            <th width="15%">Signup Date</th>
            <th width="15%">Signup page</th>
            <th width="10%">Promotion Code</th>
            <th width="10%">Subscription code</th>
            <th width="10%">Status</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
</table>    
</div>
</div>
{!! Form::close() !!}
<script>
$(document).ready(function($) {
    var serch_key_value = $("#search_key").val();
    var status_value = $("#is_active").val();
    var signup_page = $("#signup_page").val();
    var date = $("#date").val();
     $(".user-datatable").dataTable({
                   "iDisplayLength": 10,
                   "displayStart": 0,
                   "searching": false,
                   "paging": true,
                    "bPaginate": true,
                    "sPaginationType": "full_numbers",
                    "bFilter": false,
                    "bInfo": false,
                    "pagingType": "full_numbers",
                    aaSorting: [[0, 'DESC']],
                    language : {
                      sLengthMenu: "Show _MENU_",
                      emptyTable: "No record found",
                      processing: ""
                    },
                    processing: true,
                    serverSide: true,
                    ajax: '<?php echo url();?>/admin/dentist/ajax-dentists/?search_key='+serch_key_value+'&date='+date+'&active='+status_value+'&signup_page='+signup_page,
                    columns: [
                        { data: 'id', name: 'id',"bVisible": false},
                        { data: 'checkbox_td', name: 'checkbox_td',"bSortable": false},
                        { data: 'docs_details.doctor_id', name: 'docs_details.doctor_id' },
                        { data: 'docs_details.first_name', name: 'docs_details.first_name' },
                        { data: 'docs_details.email', name: 'docs_details.email' },
                        { data: 'docs_details.contact_number', name: 'docs_details.contact_number' },
                        { data: 'docs_details.created_at', name: 'docs_details.created_at' },
                        { data: 'src_sys', name: 'src_sys' },
                        { data: 'referral_code', name: 'referral_code' },
                        { data: 'subscription_code', name: 'subscription_code' },
                        { data: 'status', name: 'admin_status'},
                        { data: 'action', name: 'action',"bSortable": false,'sClass' : 'action-btns'},
                    ],
            });
     $("th").removeClass("action-btns");
     $('#date').datepicker({
         autoclose:true
     });
});
</script>
{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
@endsection
