<?php namespace App\Http\Controllers\Admin;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/*************** Model name ***************/
use App\Model\Country; 
use App\Model\Category; 
use App\Model\SiteUser; 
use App\Model\Doctor; 	
use App\Model\DoctorDetails;
use App\Model\DoctorClinic;
use App\Model\DoctorEducation;
use App\Model\DoctorAward;
use App\Model\DoctorInsurance;
use App\Model\DoctorExperience;
use App\Model\DoctorLicense;
use App\Model\DoctorSkill;
use App\Model\DoctorLanguage; 

use App\Model\TmpDoctorDetails;
use App\Model\TmpDoctorClinic;
use App\Model\TmpDoctorEducation;
use App\Model\TmpDoctorAward;
use App\Model\TmpDoctorInsurance;
use App\Model\TmpDoctorExperience;
use App\Model\TmpDoctorLicense;
use App\Model\TmpDoctorSkill;
use App\Model\TmpDoctorLanguage; 

use App\Model\Patient; 	
use App\Model\Sitesetting; 
use App\Model\Language; 

/*use App\Model\City; 
use App\Model\DocCategory; 
use App\Model\DocPractice; 
use App\Model\DocQualification; 
use App\Model\DocResearch; 
use App\Model\DocSpecialist; */
/*************** Model name ***************/

use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;
use Cookie;
use Yajra\Datatables\Datatables;
/*
require_once('vendor/Stripe/init.php');
*/
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe_CardError;
use Stripe\Stripe_InvalidRequestError;



class UserController extends BaseController {

  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
    	parent::__construct();
    }
    
	/********************************************************************************
	 *								GET USER LIST									*
	 *******************************************************************************/
    function getList()
    {

    	$module_head 		= "Patient List";
        $user_class 		= "active";
        $search_key 		= Request::query('search_key');
        $active 			= Request::query('active');
		$title				= "Patient Management";

        return view('admin.user.user_list',compact('user_class',
														'module_head','search_key','search_email','search_contact',
														'active','title'
													)
					);
    }

    function getViewPatient($id)
	{
		$user_class 			= "active";
        $module_head 			= "Patient Preview";
        $title					= "Patient Preview";
        $user_id 				= $id;

		$user_details 			= Patient::where('id', '=', $id)->first();
		if(empty($user_details)){
        	Session::flash('failure_message', 'Patient not found.'); 
        	Session::flash('alert-class', 'alert alert-danger'); 
        	return redirect('admin/patient/list');
        	exit;
        }
		return view('admin.user.patient_preview',compact('module_head','user_class','title','user_details','user_id'));
	}
	
	/********************************************************************************
	 *									EDIT USER 									*
	 *******************************************************************************/
    function getEdit($id='')
	{
        $user_class 			= "active";
        $module_head 			= "Edit Patient Details";
		$user_id				= $id;
        $user_details 			= Patient::where('id', '=', $id)->first();
        if(!$user_details){
        	Session::flash('failure_message', 'Patient not found.'); 
        	Session::flash('alert-class', 'alert alert-danger'); 
        	return redirect('admin/patient/list');
        	exit;
        }
        $title					= "Edit Patient Details";
		return view('admin.user.edit_user_details',compact(
			'module_head',
			'user_id',
			'user_class',
			'user_details',
			'title'
			));
    }
	function postEdit($id='')
	{
		$data				= Request::all();
		$user_id			= $id;
		$user_pass			= "";
		$user_details		= Patient::where('id', $user_id)->first();
		$user_pass			= $user_details->password;
		
		if($data['password']!=''){
			
			if($data['password']!=$data['conf_password']){
				Session::flash('success_message', 'Patient details has been updated successfully.'); 
				Session::flash('alert-class', 'alert alert-success'); 
				return redirect('admin/patient/list');
			}
		}
		
		
		
		/*$update_user		= Patient::where('id', $user_id)
									->update([
										'title'         => $data['title'],
										'name'          => trim($data['name']),
										'last_name'     => trim($data['last_name']),
										'email'       	=> $data['email'],
										'contact'       => preg_replace('/[^0-9]+/', '',$data['contact']),
										'age'           => $data['age'],
										'gender'        => $data['gender'],
										'postcode'      => $data['postcode'],
										'city'          => $data['city_id'],

									]);*/
	    $update_user		= Patient::where('id', $user_id)
									->update([
										'note'         => $data['note']
									]);
		if($data['password']!=''){
			$user_pass				= $data['password'];
			$update_password		= Patient::where('id', $user_id)
										->update([
											'password'      => md5($data['password'])
										]);
										
		}
		Session::flash('success_message', 'Patient details has been updated successfully.'); 
        Session::flash('alert-class', 'alert alert-success'); 
        return redirect('admin/patient/list');
    }
	
	/********************************************************************************
	 *						CHECK USER EXISTS OR NOT								*
	 *******************************************************************************/
	function getCheck(){
		$data	= Request::all();

		$where_raw = "1=1";
		$where_raw .= " AND `email` = '".$data['email']."'";
		if($data['hid_user_id']!=0){
			$where_raw .= " AND `id`!= '".$data['hid_user_id']."'";
		}
		$user_details 	= DoctorDetails::whereRaw($where_raw)->first();
		if($data['hid_user_id']==0){
			if(count($user_details)>0){
				echo 1;
			}
			else{
				echo 0;
			}
		}
		exit;
	}
	
	/********************************************************************************
	 *								REMOVE USER										*
	 *******************************************************************************/
	function getRemoveDentist($id="")
	{
		  $all_dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$id)->first()->toArray(); 
		
		  $tmp_dentist_details = Doctor::with('tmp_docs_details','tmp_docs_clinics','tmp_docs_education','tmp_docs_award','tmp_docs_insurance','tmp_docs_experiece','tmp_docs_license','tmp_docs_skill','tmp_docs_language')->where('id',$id)->first()->toArray(); 
		  
		  $dentist_details = Doctor::find($id);
				
		  $dentist_details->delete();
			
			/* All Dentist Images And Documents Delete */
			
			if(!empty($all_dentist_details['docs_details']))
			{
				@unlink('uploads/dentist_profile_image/'.$all_dentist_details['docs_details']['profile_pics']);
			}
			if(!empty($all_dentist_details['docs_clinics']))
			{
				@unlink('uploads/clinic_picture/'.$all_dentist_details['docs_clinics']['clinic_picture']);
			}
			
			if(!empty($all_dentist_details['docs_education']))
			{
				foreach($all_dentist_details['docs_education'] as $each_education)
				{
					@unlink('uploads/certificate/'.$each_education['certificate']);;
				}
			}
			if(!empty($all_dentist_details['docs_experiece']))
			{				
				foreach($all_dentist_details['docs_experiece'] as $each_exp)
				{
					@unlink('uploads/exp_document/'.$each_exp['exp_photo']);
				}
			}
			if(!empty($all_dentist_details['docs_license']))
			{			
				foreach($all_dentist_details['docs_license'] as $each_license)
				{
					@unlink('uploads/license_photo/'.$each_license['license_photo']);
				}
			}
			
			  /* All Dentist Temporary(prev) Images And Documents Delete */
			  
			if(!empty($tmp_dentist_details['docs_details']))
			{
				@unlink('uploads/tmp_dentist_profile_image/'.$tmp_dentist_details['docs_details']['profile_pics']);
			}
			if(!empty($tmp_dentist_details['docs_clinics']))
			{
				@unlink('uploads/tmp_clinic_picture/'.$tmp_dentist_details['docs_clinics']['clinic_picture']);
			}
			
			if(!empty($tmp_dentist_details['docs_education']))
			{
				foreach($tmp_dentist_details['docs_education'] as $each_education)
				{
					@unlink('uploads/tmp_certificate/'.$each_education['certificate']);
				}
			}
			if(!empty($tmp_dentist_details['docs_experiece']))
			{				
				foreach($tmp_dentist_details['docs_experiece'] as $each_exp)
				{
					@unlink('uploads/tmp_exp_document/'.$each_exp['exp_photo']);
				}
			}
			if(!empty($tmp_dentist_details['docs_license']))
			{			
				foreach($tmp_dentist_details['docs_license'] as $each_license)
				{
					@unlink('uploads/tmp_license_photo/'.$each_license['license_photo']);
				}
			}
				Session::flash('success_message', 'Dentist has been removed successfully.'); 
				Session::flash('alert-class', 'alert alert-success'); 
				return redirect('admin/dentist/list');
	}
	
	
	function postRemove($id=""){
		$user_details = Patient::find($id);
		if(!empty($user_details['image'])){
			@unlink('uploads/profile_image/'.$user_details['image']);
		}
        $user_details->delete();
        	Session::flash('success_message', 'Patients has been removed successfully.'); 
        	Session::flash('alert-class', 'alert alert-success'); 
        	return redirect('admin/patient/list');
	}
	
	/********************************************************************************
	 *							CHANGE STATUS										*
	 *******************************************************************************/
	function getDentistStatus(){
		$post_data  = Request::all();
        $status 	= $post_data['this_val'];
        $id 		= $post_data['this_id'];
        $site_user 	= Doctor::find($id);
       
       if($site_user->email_status == 0)
        {
       	 $site_user->admin_status = $status;
         $site_user->email_status = $status;
        }
       else
       {
       	 $site_user->admin_status = $status;
       }
        
        $site_user->save();
        
                $dr_name = DoctorDetails::where('doctor_id', $id)->first();

                if($status==0){
                 $template='profiledeactivate';
                 $subject=$dr_name['last_name'].' '.'your DentalChat profile is deactivated';
                    
                }else{
                    
                $template='adminactivedentist';
                $subject="Your DentalChat.com account is activated";
                }
                    
        	/*************************** email send **************************/
      
      $url = 'http://outbound.dentalchat.com/mailsend';
try{
       
       $post = array(
        'user_id'=>$id,  
        'name' => trim($dr_name['first_name']).' '.trim($dr_name['last_name']),
        'firstname'=>$dr_name['first_name'],
        'lastname'=>$dr_name['last_name'],
        'business_name'=>$dr_name['first_name'],
        'template_name'=>$template,
        'email'=>$dr_name['email'],
        'subject'=>$subject
        );
        
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false
        ));
       
        $result=curl_exec ($ch);
        curl_close ($ch);
       
       // echo $result;  
         }
         catch(Exception $ex){
             //echo $ex->getMessage();
         }
      /************************* Email End here *******************************/ 	    
      
        echo "1";
		exit();
	
	}
	
	function getStatus(){
		
		$post_data  = Request::all();
        $status 	= $post_data['this_val'];
        $id 		= $post_data['this_id'];
        $site_user 	= Patient::find($id);
        $site_user->status = $status;
        $site_user->save();
        echo "1";
		exit();
	
	}

	function ajaxPatientsList(){
		$search_key 		= Request::query('search_key');
        $active 			= Request::query('active');
        $where = "";
		 if($search_key != ''){
	            $where 			.= "(`name` LIKE '%".$search_key."%' OR CONCAT(name,' ',last_name) LIKE '%".$search_key."%' OR `last_name` LIKE '%".$search_key."%' OR `email` LIKE '%".$search_key."%' OR `contact` LIKE '%".$search_key."%') AND ";
	     }
        if($active != ''){
            $where 		   .= "`status`= '".$active."' AND ";
        }
        $where 		   .= '1';
        $all_patients =  Patient::select(['id', 'name', 'last_name', 'email', 'contact','status','note'])->whereRaw($where);

		return Datatables::of($all_patients)
		->addColumn('checkbox_td', function ($all_patients) {
                return '<input type="checkbox"  recordType="multipleRecord" multipleRecord="'.$all_patients->id.'" />';
            })
		 ->addColumn('action', function ($all_patients) {
                return '<a href="'.url().'/admin/patient/edit/'.$all_patients->id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;<a href="javascript:void(0);" onclick="userJs.removePatient('.$all_patients->id.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;<a href="'.url().'/admin/patient/preview/'.$all_patients->id.'"><i class="fa fa-bars" aria-hidden="true"></i></a>';
            })
		 ->editColumn('name', function ($all_patients) {
		 		$fullname = $all_patients->name." ".$all_patients->last_name;
            	return $fullname;

            })
		  ->editColumn('note', function ($all_patients) {
		 		$note = $all_patients->note;
		 		$note = strlen($note) > 50 ? substr($note,0,50)."..." : $note;
            	return $note;

            })
		 ->editColumn('status', function ($all_patients) {
		 	if($all_patients->status == 1){
             	$status_html = '<select style="width:80px;" name="status" id="user_active_'.$all_patients->id.'" onchange="userJs.changeStatus(this.value,'.$all_patients->id.')"><option value="1" selected>Active</option><option value="0">Inactive</option></select><br /><span class="alert-success" id="success_status_span_'.$all_patients->id.'" style="display:none;"></span>';
		 	}else{
		 		$status_html = '<select style="width:80px;" name="status" id="user_active_'.$all_patients->id.'" onchange="userJs.changeStatus(this.value,'.$all_patients->id.')"><option value="1">Active</option><option value="0" selected>Inactive</option></select><span class="alert-success" id="success_status_span_'.$all_patients->id.'" style="display:none;"></span>';

		 	}
		 	return $status_html;
            })
		->make(true);
	}

	/********************************************* Doctor management *******************************/
	function doctorsList(){

		$module_head 		= "Dentist List";
        $doctor_class 		= "active";
       // $search_key 		= Request::query('search_key');
      //  $active 			= Request::query('active');
        
        $field 		= Request::query('field');
        $operator 			= Request::query('operator');
        $field_text 			= Request::query('field_text');
        $data['signup_by_user']= Request::query('signup_page');
        $data['date']= Request::query('date');
        $title				= "Dentist Management";
	    
        $data['signup_page'] = DB::table('doctors')->select('src_sys')->groupBy('src_sys')->get();
		
        return view('admin.user.doctor_list',$data,compact('doctor_class',
														'module_head','field','search_email','search_contact',
														'active','title','operator','field_text'
													)
					);
	}

	function ajaxDoctorsList(){
	//	$search_key 		= Request::query('search_key');
      //  $active 			= Request::query('active');
        $signup_page 		= Request::query('signup_page');
        $date        		= Request::query('date');
        
          $field 		= Request::query('field');
        $operator 			= Request::query('operator');
        $field_text 			= Request::query('field_text');
        $where = "";
        
        $all_doctors = Doctor::with('docs_details')->select('doctors.*');
        
        //doctor search from doctor details table
      /*  if($search_key != ''){
         	  $subwherequery 	= "(`doctor_details`.`first_name` LIKE '%".$search_key."%' OR CONCAT(`doctor_details`.first_name,' ',`doctor_details`.last_name) LIKE '%".$search_key."%' OR `doctor_details`.`last_name` LIKE '%".$search_key."%' OR `doctor_details`.`email` LIKE '%".$search_key."%' OR `doctor_details`.`contact_number` LIKE '%".$search_key."%' OR `referral_code` LIKE '%".$search_key."%' OR `subscription_code` LIKE '%".$search_key."%')";
         		$all_doctors =  $all_doctors->whereHas('docs_details',function ($query) use ($subwherequery) {
    					$query->whereRaw($subwherequery);
         	    });
	     }
	     if($active != ''){
	     	$all_doctors =  $all_doctors->where('admin_status','=',$active);
        }*/
        if($field != '' && $field =='first_name' && $field_text && $operator=="LIKE"){
            
          $subwherequery = "(`doctor_details`.`first_name` LIKE '%".$field_text."%' OR CONCAT(`doctor_details`.first_name,' ',`doctor_details`.last_name) LIKE '%".$field_text."%' )";
         		$all_doctors =  $all_doctors->whereHas('docs_details',function ($query) use ($subwherequery) {
    					$query->whereRaw($subwherequery);
         	    });
         	    
        }
        else if($field != '' && ($field =='id' || $field=='admin_status') && $field_text && $operator!="LIKE"){
           // $date = date("Y-m-d", strtotime($date));
            $all_doctors =  $all_doctors->where($field,$operator,$field_text);
        }
        else if($field!='' && ($field=='src_sys' || $field=='referral_code' || $field=='subscription_code')){
             $all_doctors =  $all_doctors->where($field,'LIKE',"%{$field_text}%");
        }
        
        else if($field!='' && $field_text!=''){
                   $subwherequery = "(`doctor_details`.$field LIKE '%".$field_text."%')";
         		$all_doctors =  $all_doctors->whereHas('docs_details',function ($query) use ($subwherequery) {
    					$query->whereRaw($subwherequery);
         	    }); 
            
        }

	//	print_r($all_doctors); exit;
		return Datatables::of($all_doctors)
		->addColumn('checkbox_td', function ($all_doctors) {
                return '<input type="checkbox"  recordType="multipleRecord" multipleRecord="'.$all_doctors->id.'" />';
            })
		 ->addColumn('action', function ($all_doctors) {
                return '<a href="'.url().'/admin/dentist/dentist-step1/'.$all_doctors->id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;<a href="javascript:void(0);" onclick="userJs.remove('.$all_doctors->id.')"><i class="fa fa-trash-o" aria-hidden="true"></i></a><a href="'.url().'/admin/dentist/dentist-preview/'.$all_doctors->id.'"><i class="fa fa-bars" aria-hidden="true"></i></a>&nbsp;<a href="'.url().'/admin/dentist/dentist-compare/'.$all_doctors->id.'"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>';
            })
		 ->editColumn('docs_details.first_name', function ($all_doctors) {
		 		$fullname = $all_doctors->docs_details['first_name']." ".$all_doctors->docs_details['last_name'];
            	return $fullname;

            })

		 ->editColumn('docs_details.contact_number', function ($all_doctors) {
		 		$contact_number = ($all_doctors->docs_details['conuntry_code'])?$all_doctors->docs_details['conuntry_code']."-".$all_doctors->docs_details['contact_number']:$all_doctors->docs_details['contact_number'];
            	return $contact_number;

            })
		 
		 ->editColumn('status', function ($all_doctors) {
		 	if($all_doctors->admin_status == 1){
             	$status_html = '<select style="width:80px;" name="status" id="user_active_'.$all_doctors->id.'" onchange="userJs.changeDentistStatus(this.value,'.$all_doctors->id.')"><option value="1" selected>Active</option><option value="0">Inactive</option></select><br /><span class="alert-success" id="success_status_span_'.$all_doctors->id.'" style="display:none;"></span>';
		 	}else{
		 		$status_html = '<select style="width:80px;" name="status" id="user_active_'.$all_doctors->id.'" onchange="userJs.changeDentistStatus(this.value,'.$all_doctors->id.')"><option value="1">Active</option><option value="0" selected>Inactive</option></select><span class="alert-success" id="success_status_span_'.$all_doctors->id.'" style="display:none;"></span>';

		 	}
		 	return $status_html;
            })
		->make(true);
	}
	function getDoctorStep1($id="") // 1st Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-1";
        $title					= "Add Dentist : Step-1";
        $doc_details = array();
        if($id!='')
    	{
    		$user_id = $id;
    		$doc_details = Doctor::with('docs_details')->where('id',$user_id)->first();
    		if($doc_details->docs_details['date_of_birth'] == '00-00-0000' || $doc_details->docs_details['date_of_birth'] == '0000-00-00' || $doc_details->docs_details['date_of_birth'] == ''){

    			$doc_details->docs_details['date_of_birth'] = '';
    		}
    	}
    	else
    	{
    		$user_id = '';
    	} 
    	//print_r($doc_details); exit;
		return view('admin.user.doctor_step1',compact('module_head','doctor_class','title','user_id','doc_details'));
		
	}

	function postDoctorStep1($id="") // 1st Step Add from admin
	{
		$data = Request::all();
		//print_r($data); exit;		
		$reg_doctor_id = '';
		if($id!='')
	   	{
	   		$doc_details = Doctor::with('docs_details')->where('id',$id)->first();
	   		$reg_doctor_id = $doc_details->id;
	   	}

		if (Input::hasFile('profile_img'))
	    {
	      $destinationPath = 'uploads/dentist_profile_image/'; 
	      $extension = Input::file('profile_img')->getClientOriginalExtension(); // getting image extension
	      $fileName = time().'.'.$extension; // renaming image
	      Input::file('profile_img')->move($destinationPath, $fileName); // uploading file to given path
	      
	      $data['image']=$fileName;

	      // unlink old photo
	      @unlink('uploads/dentist_profile_image/'.$doc_details->docs_details['profile_pics']);
	    }
	    else
	       $data['image'] = isset($doc_details->docs_details['profile_pics'])?$doc_details->docs_details['profile_pics']:'';

	   	

	   	// For  1st table  insert data //
	   	$main_doctor['created_at'] = date('Y-m-d h:i:s');
	   	$main_doctor['updated_at'] = date('Y-m-d h:i:s');

	   	
	   
	   	if($reg_doctor_id!='') // For updating the data with previous button and edit
	   	{
	   		$redirect_id = $reg_doctor_id;

	   		$doctor = Doctor::where('id',$reg_doctor_id)->update([
											'updated_at'      => date('Y-m-d h:i:s')
										]);

	   		if($data['date_of_birth'] == '00-00-0000' || $data['date_of_birth'] == '0000-00-00' || $data['date_of_birth'] == '')  // Age calculation
	   		{
	   			$age ='';
	   			
	   		}
	   		else
	   		{
	   			//echo $data['date_of_birth'];
	   			$age = (date('Y') - date('Y',strtotime($data['date_of_birth'])));
	   			//echo $age; exit;
	   		}

	   		$doc_details = DoctorDetails::where('doctor_id',$reg_doctor_id)->update([
											'first_name'    => trim($data['first_name']),
											'last_name'     => trim($data['last_name']),
											'age' 			=> $age,
											'conuntry_code' => $data['country_code'],
											'contact_number'=> preg_replace('/[^0-9]+/', '',$data['contact_number']),
											'date_of_birth' => $data['date_of_birth']?date('Y-m-d',strtotime($data['date_of_birth'])):'0000-00-00',
											'gender'		=> $data['gender'],
											'profile_pics'  => $data['image'],
											'updated_at'      => date('Y-m-d h:i:s')
										]);

	   		############Generating XML --STARTS#########################
	        /*$cmd = "wget -bq --spider http://demo.uiplonline.com/dentist-amar/dev/server/save-doctors-xml";
            shell_exec(escapeshellcmd($cmd));*/
            $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
	        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	            xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
	            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	        $doctor_xml = DoctorDetails::get();
	        foreach($doctor_xml as $v)
	        {
	            $xmlString .= '<url>';
	            $xmlString .= '<Doctor_Name>'.$v->first_name.' '.$v->last_name.'</Doctor_Name>';
	            $xmlString .= '<Date_Of_Birth>'.$v->date_of_birth.'</Date_Of_Birth>';
	            $xmlString .= '<Email>'.$v->email .'</Email>';
	            $xmlString .= '<Contact_No>'.$v->conuntry_code.' '.$v->contact_number .'</Contact_No>';
	            $xmlString .= '<Image>http://demo.uiplonline.com/dentist-amar/dev/server/uploads/dentist_profile_image/'.$v->profile_pics.'</Image>';
	            $xmlString .= '</url>';
	        }

	        $xmlString .= '</urlset>';

	        $dom = new \DOMDocument;
	        $dom->preserveWhiteSpace = FALSE;
	        $dom->loadXML($xmlString);
	        //$dom->save('xml/doctors.xml');
	        $dom->save('../sitemap.xml');
			############Generating XML --ENDS#########################


	   	}
	   	else // for creating new dentist entry
	   	{
	   		$password = Hash::make($data['password']); // Hash make password

	   		$doctor = Doctor::create($main_doctor);  // Doctor create
		   	$doctor_id = $doctor->id;    // last  inserted id
		   	$redirect_id = $doctor_id;

		   	if($data['date_of_birth'] == '00-00-0000' || $data['date_of_birth'] == '0000-00-00') // Age calculation
	   		{
	   			$age ='';	   			
	   		}
	   		else
	   		{
	   			$age = (date('Y') - date('Y',strtotime($data['date_of_birth'])));	   			
	   		}

			$doc_details		= DoctorDetails::create([
											'doctor_id'     => $doctor_id,
											'first_name'    => trim($data['first_name']),
											'last_name'     => trim($data['last_name']),
											'email'       	=> $data['email'],
											'age' 			=> $age,
											'conuntry_code' => $data['country_code'],
											'contact_number'=> preg_replace('/[^0-9]+/', '',$data['contact_number']),
											'date_of_birth' => $data['date_of_birth']?date('Y-m-d',strtotime($data['date_of_birth'])):'0000-00-00',
											'gender'		=> $data['gender'],
											'profile_pics'  => $data['image'],
											'password'      => $password,
											'created_at'    => date('Y-m-d h:i:s'),
											'updated_at'    => date('Y-m-d h:i:s')
										]);

			############Generating XML --STARTS#########################
	        /*$cmd = "wget -bq --spider http://demo.uiplonline.com/dentist-amar/dev/server/save-doctors-xml";
            shell_exec(escapeshellcmd($cmd));*/
            $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
	        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	            xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
	            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	        $doctor_xml = DoctorDetails::get();
	        foreach($doctor_xml as $v)
	        {
	            $xmlString .= '<url>';
	            $xmlString .= '<Doctor_Name>'.$v->first_name.' '.$v->last_name.'</Doctor_Name>';
	            $xmlString .= '<Date_Of_Birth>'.$v->date_of_birth.'</Date_Of_Birth>';
	            $xmlString .= '<Email>'.$v->email .'</Email>';
	            $xmlString .= '<Contact_No>'.$v->conuntry_code.' '.$v->contact_number .'</Contact_No>';
	            $xmlString .= '<Image>http://demo.uiplonline.com/dentist-amar/dev/server/uploads/dentist_profile_image/'.$v->profile_pics.'</Image>';
	            $xmlString .= '</url>';
	        }

	        $xmlString .= '</urlset>';

	        $dom = new \DOMDocument;
	        $dom->preserveWhiteSpace = FALSE;
	        $dom->loadXML($xmlString);
	        //$dom->save('xml/doctors.xml');
	        $dom->save('../sitemap.xml');
			############Generating XML --ENDS#########################
	   	}	   	
	   	
		return redirect('admin/dentist/dentist-step2/'.$redirect_id);
	}

	function getDoctorStep2($id="") // 2nd Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-2";
        $title					= "Add Dentist : Step-2";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = '';
    	}

    	$clinic_det = DoctorClinic::where('doctor_id',$user_id)->first();
    	if($clinic_det !=NULL)
    	{
    		$clinic_det = $clinic_det->toArray();
    	}
    	else
    	{
    		$clinic_det = array();
    	}
    	//$doc_clinics_count = count($clinic_det);
    	//print_r($clinic_det); exit;
		return view('admin.user.doctor_step2',compact('module_head','doctor_class','title','user_id','clinic_det'));		
	}

	function postDoctorStep2($id="") // 2nd Step Add from admin
	{
		$data = Request::all();
		
		$doctor_id=$data['hid_user_id'];
		
		if($id!='')
	   	{
	   		$clinic_det = DoctorClinic::where('doctor_id',$id)->first();
	   		//print_r($clinic_det); exit;
	   		// $reg_doctor_id = $doc_details->id;
	   	}

		if (Input::hasFile('clinic_picture'))
		{
		  $destinationPath = 'uploads/clinic_picture/'; 
		  $extension = Input::file('clinic_picture')->getClientOriginalExtension(); // getting image extension
		  $fileName = time().'.'.$extension; // renaming image
		  Input::file('clinic_picture')->move($destinationPath, $fileName); // uploading file to given path
		  // $this->create_thumbnail($thumb_path,$fileName,$extension); 
		  $data['image']=$fileName;

		  // unlink old photo
		  @unlink('uploads/clinic_picture/'.$clinic_det->clinic_picture);
		}
		else
		   $data['image'] = isset($clinic_det->clinic_picture)?$clinic_det->clinic_picture:'';


		$Clinic_table_check = DoctorClinic::where('doctor_id',$doctor_id)->count();

		if($Clinic_table_check>0) // For updating the data with previous button and edit
	   	{   
		   
			$doc_details	= DoctorClinic::where('doctor_id',$doctor_id)->update([

					'business_name'    		=> trim($data['business_name']),
					'address'     			=> trim($data['address']),
					'zip_code'				=> trim($data['zip_code']),
					'city'					=> trim($data['place_city']),
					'state'					=> trim($data['place_state']),
					'country'				=> trim($data['place_country']),
					'lat'                   => trim($data['place_lat']),
					'lang'					=> trim($data['place_lng']),
					'sun_opening_hours_from'=> trim($data['sun_opening_hours_from']),
					'sun_opening_hours_to' 	=> trim($data['sun_opening_hours_to']),

					'mon_opening_hours_from'=> trim($data['mon_opening_hours_from']),
					'mon_opening_hours_to' 	=> trim($data['mon_opening_hours_to']),

					'tue_opening_hours_from'=> trim($data['tue_opening_hours_from']),
					'tue_opening_hours_to' 	=> trim($data['tue_opening_hours_to']),

					'wed_opening_hours_from'=> trim($data['wed_opening_hours_from']),
					'wed_opening_hours_to' 	=> trim($data['wed_opening_hours_to']),

					'thu_opening_hours_from'=> trim($data['thu_opening_hours_from']),
					'thu_opening_hours_to' 	=> trim($data['thu_opening_hours_to']),

					'fri_opening_hours_from'=> trim($data['fri_opening_hours_from']),
					'fri_opening_hours_to' 	=> trim($data['fri_opening_hours_to']),

					'sat_opening_hours_from'=> trim($data['sat_opening_hours_from']),
					'sat_opening_hours_to' 	=> trim($data['sat_opening_hours_to']),

					'clinic_picture' 		=> $data['image'],
					'updated_at'      		=> date('Y-m-d h:i:s')
				]);
		}
		else // For creating new clinic details 
		{
			$doc_details	= DoctorClinic::create([
							'doctor_id'     		=> $doctor_id,
							'business_name'    		=> trim($data['business_name']),
							'address'     			=> trim($data['address']),
							'zip_code'				=> trim($data['zip_code']),
							'city'					=> trim($data['place_city']),
							'state'					=> trim($data['place_state']),
							'country'				=> trim($data['place_country']),
							'lat'                   => trim($data['place_lat']),
							'lang'					=> trim($data['place_lng']),
							'sun_opening_hours_from'=> trim($data['sun_opening_hours_from']),
							'sun_opening_hours_to' 	=> trim($data['sun_opening_hours_to']),

							'mon_opening_hours_from'=> trim($data['mon_opening_hours_from']),
							'mon_opening_hours_to' 	=> trim($data['mon_opening_hours_to']),

							'tue_opening_hours_from'=> trim($data['tue_opening_hours_from']),
							'tue_opening_hours_to' 	=> trim($data['tue_opening_hours_to']),

							'wed_opening_hours_from'=> trim($data['wed_opening_hours_from']),
							'wed_opening_hours_to' 	=> trim($data['wed_opening_hours_to']),

							'thu_opening_hours_from'=> trim($data['thu_opening_hours_from']),
							'thu_opening_hours_to' 	=> trim($data['thu_opening_hours_to']),

							'fri_opening_hours_from'=> trim($data['fri_opening_hours_from']),
							'fri_opening_hours_to' 	=> trim($data['fri_opening_hours_to']),

							'sat_opening_hours_from'=> trim($data['sat_opening_hours_from']),
							'sat_opening_hours_to' 	=> trim($data['sat_opening_hours_to']),

							'clinic_picture' 		=> $data['image'],
							'created_at'      		=> date('Y-m-d h:i:s'),
							'updated_at'      		=> date('Y-m-d h:i:s')
						]);
		}
		
		
		return redirect('admin/dentist/dentist-step3/'.$doctor_id);		
		
	}

	//****************************************************//
	//******************** STEP 3 ************************//
	//****************************************************//

	function getDoctorStep3($id="") // 4th Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-3";
        $title					= "Add Dentist : Step-3";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = 0;
    	}

    	$doc_education = DoctorEducation::where('doctor_id',$user_id)->get()->toArray();
    	$doc_education_count = count($doc_education);
    	$doc_award = DoctorAward::where('doctor_id',$user_id)->get()->toArray();
    	$doc_award_count = count($doc_education);
    	$doc_insurance = DoctorInsurance::where('doctor_id',$user_id)->get()->toArray();
    	$doc_insurance_count = count($doc_education);
    	//print_r($doc_clinics); exit;
		return view('admin.user.doctor_step3',compact('module_head','doctor_class','title','user_id','doc_education','doc_education_count','doc_award','doc_award_count','doc_insurance','doc_insurance_count'));		
	}

	function postDoctorStep3($id="") // Education  & Traning Form
	{
		$data = Request::all();
		//print_r($data); exit;
		$doctor_id=$data['hid_user_id'];		
		
		// Get value from databse
		$doc_education = DoctorEducation::where('doctor_id',$doctor_id)->get()->toArray();    	
    	$doc_award = DoctorAward::where('doctor_id',$doctor_id)->get()->toArray();    	
    	$doc_insurance = DoctorInsurance::where('doctor_id',$doctor_id)->get()->toArray();
    	
		// Delete all edited user's data before update

		DoctorEducation::where('doctor_id',$doctor_id)->delete();
		DoctorAward::where('doctor_id',$doctor_id)->delete();
		DoctorInsurance::where('doctor_id',$doctor_id)->delete();

		if(!empty($data['data']))
		{			
			//print_r($data['data']); exit;
			foreach($data['data'] as $k=>$eachdegree)
			{
				if(array_filter($eachdegree))  // checking atleast one value is there in array
				{
					if(isset($_FILES['data']['name'][$k]['certificate'])&&$_FILES['data']['name'][$k]['certificate']!='')
					{
					  $org_name = $_FILES['data']['name'][$k]['certificate'];
					  $destinationPath = 'uploads/certificate/'; 
					  $extension = Input::file()['data'][$k]['certificate']->getClientOriginalExtension(); // getting image extension
					  $fileName = time().$k.'.'.$extension; // renameing image
					  Input::file()['data'][$k]['certificate']->move($destinationPath, $fileName); // uploading file to given path
					  // $this->create_thumbnail($thumb_path,$fileName,$extension); 
					  $data['image']=$fileName;

					  // unlink old photo
					  @unlink('uploads/certificate/'.$eachdegree['hid_certificate']);
					}
					else
					{
						$data['image'] = ($eachdegree['hid_certificate']!='')?$eachdegree['hid_certificate']:'';
						$org_name =  ($eachdegree['hid_certificate_name']!='')?$eachdegree['hid_certificate_name']:'';
					}
					   
					   //print_r($eachdegree['degree']); exit;
					
					   $doc_degrees		= DoctorEducation::create([
											'doctor_id'      => $doctor_id,
											'degree'    	 => isset($eachdegree['degree'])?trim($eachdegree['degree']):'',
											'completed_year' => isset($eachdegree['completed_year'])?trim($eachdegree['completed_year']):'',
											'medical_school' => isset($eachdegree['medical_school'])?trim($eachdegree['medical_school']):'',
											'location'		 => isset($eachdegree['location'])?trim($eachdegree['location']):'',
											'certificate' 	 => $data['image'],
											'org_certificate'=> isset($org_name)?$org_name:'',
											'created_at'     => date('Y-m-d h:i:s'),
											'updated_at'     => date('Y-m-d h:i:s')
										]);
				}
			} // Foreach End
			
		} // Data If End
		
		if(!empty($data['data_award']))
		{
			foreach($data['data_award'] as $eachaward)
			{
				if(array_filter($eachaward))  // checking atleast one value is there in array
				{
					$doc_awards		= DoctorAward::create([
											'doctor_id'      => $doctor_id,
											'award'    	 => trim($eachaward['award']),									
											'created_at'     => date('Y-m-d h:i:s'),
											'updated_at'     => date('Y-m-d h:i:s')
										]);
				}
			}
		}	

		if(!empty($data['data_insurance']))
		{
			foreach($data['data_insurance'] as $eachinsurance)
			{
				if(array_filter($eachinsurance))  // checking atleast one value is there in array
				{
					$doc_insurances		= DoctorInsurance::create([
											'doctor_id'      => $doctor_id,
											'insurance'    	 => trim($eachinsurance['insurance']),							
											'created_at'     => date('Y-m-d h:i:s'),
											'updated_at'     => date('Y-m-d h:i:s')
										]);
				}
			}
		}		   
		
		
		return redirect('admin/dentist/dentist-step4/'.$doctor_id);		
		
	}	


	//****************************************************//
	//******************** STEP 4 ************************//
	//****************************************************//

	function getDoctorStep4($id="") // 4th Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-4";
        $title					= "Add Dentist : Step-4";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = '';
    	}

    	$doc_licenses = DoctorLicense::where('doctor_id',$user_id)->get()->toArray();
    	$doc_license_count = count($doc_licenses);
    	//print_r($doc_clinics); exit;
		return view('admin.user.doctor_step4',compact('module_head','doctor_class','title','user_id','doc_licenses','doc_license_count'));		
	}

	function postDoctorStep4() // Education  & Traning Form
	{
		$data = Request::all();
		//print_r($data); exit;
		$doctor_id=$data['hid_user_id'];		
		
		// Get value from databse
		$doc_license = DoctorLicense::where('doctor_id',$doctor_id)->get()->toArray();  
		
		// Delete all edited user's data before update

		DoctorLicense::where('doctor_id',$doctor_id)->delete();

		if(!empty($data['data']))
		{
			foreach($data['data'] as $k=>$eachlicense)
			{
				if(array_filter($eachlicense))  // checking atleast one value is there in array
				{
					if(isset($_FILES['data']['name'][$k]['license_photo'])&&$_FILES['data']['name'][$k]['license_photo']!='')
					{
					  $org_license_name = $_FILES['data']['name'][$k]['license_photo'];
					  $destinationPath = 'uploads/license_photo/'; 
					  $extension = Input::file()['data'][$k]['license_photo']->getClientOriginalExtension(); // getting image extension
					  $fileName = time().$k.'.'.$extension; // renameing image
					  Input::file()['data'][$k]['license_photo']->move($destinationPath, $fileName); // uploading file to given path
					  $data['image']=$fileName;
	
					  // unlink old photo
					  @unlink('uploads/license_photo/'.$eachlicense['hid_license_photo']);
					}
					else
					{
						$data['image'] = ($eachlicense['hid_license_photo']!='')?$eachlicense['hid_license_photo']:'';
						$org_license_name =  ($eachlicense['hid_license_photo_name']!='')?$eachlicense['hid_license_photo_name']:'';

					}
					   
					   
					   $doc_degrees		= DoctorLicense::create([
											'doctor_id'      => $doctor_id,
											'license_details'=> trim($eachlicense['license_details']),						
											'license_photo'  => $data['image'],
											'org_license'    => isset($org_license_name)?$org_license_name:'',
											'created_at'     => date('Y-m-d h:i:s'),
											'updated_at'     => date('Y-m-d h:i:s')
										]);	
				}
			} // Foreach End
			
		} // Data If End		
		
		return redirect('admin/dentist/dentist-step5/'.$doctor_id);			
	}


	//****************************************************//
	//******************** STEP 5 ************************//
	//****************************************************//

	function getDoctorStep5($id="") // 5th Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-5";
        $title					= "Add Dentist : Step-5";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = '';
    	}

    	$doc_exp = DoctorExperience::where('doctor_id',$user_id)->get()->toArray();
    	$doc_exp_count = count($doc_exp);
		return view('admin.user.doctor_step5',compact('module_head','doctor_class','title','user_id','doc_exp','doc_exp_count'));	
	}

	function postDoctorStep5() // Experience Form
	{
		$data = Request::all();
		//print_r($data); exit;*/
		$doctor_id=$data['hid_user_id'];		
		
		// Get value from databse
		$doc_license = DoctorExperience::where('doctor_id',$doctor_id)->get()->toArray();  
		
		// Delete all edited user's data before update

		DoctorExperience::where('doctor_id',$doctor_id)->delete();

		if(!empty($data['data']))
		{
			foreach($data['data'] as $k=>$eachexp)
			{
				if(array_filter($eachexp))  // checking atleast one value is there in array
				{
					if(isset($_FILES['data']['name'][$k]['exp_photo'])&&$_FILES['data']['name'][$k]['exp_photo']!='')
					{
					  $org_exp_photo = $_FILES['data']['name'][$k]['exp_photo'];
					  $destinationPath = 'uploads/exp_document/'; 
					  $extension = Input::file()['data'][$k]['exp_photo']->getClientOriginalExtension(); // getting image extension
					  $fileName = time().$k.'.'.$extension; // renameing image
					  Input::file()['data'][$k]['exp_photo']->move($destinationPath, $fileName); // uploading file to given path		  
					  $data['image']=$fileName;
	
					  // unlink old photo
					  @unlink('uploads/exp_document/'.$eachexp['hid_exp_photo']);
					}
					else
					{
						$data['image'] = ($eachexp['hid_exp_photo'])?$eachexp['hid_exp_photo']:'';
						$org_exp_photo =  ($eachexp['hid_exp_photo_name']!='')?$eachexp['hid_exp_photo_name']:'';
					}
					   
					   
					   $doc_exp		= DoctorExperience::create([
											'doctor_id' => $doctor_id,
											'prof_exp'=> trim($eachexp['prof_exp']),
											'dental_business'=> trim($eachexp['dental_business']),
											'location'=> trim($eachexp['location']),
											'exp_from'=> trim($eachexp['exp_from']),
											'exp_to'=> isset($eachexp['exp_to'])?trim($eachexp['exp_to']):'',
											'present'=> isset($eachexp['present'])?$eachexp['present']:0,
											'exp_photo'  => $data['image'],
											'org_exp'    =>($org_exp_photo)?$org_exp_photo:'',
											'created_at' => date('Y-m-d h:i:s'),
											'updated_at' => date('Y-m-d h:i:s')
										]);	
				} 
			} // Foreach End
			
		} // Data If End	
		 
		
		return redirect('admin/dentist/dentist-step6/'.$doctor_id);			
	}


	//****************************************************//
	//******************** STEP 6 ************************//
	//****************************************************//

	function getDoctorStep6($id="") // 6th Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-6";
        $title					= "Add Dentist : Step-6";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = '';
    	}

    	$doc_skill = DoctorSkill::where('doctor_id',$user_id)->get()->toArray();
    	$doc_skill_count = count($doc_skill);
		return view('admin.user.doctor_step6',compact('module_head','doctor_class','title','user_id','doc_skill','doc_skill_count'));		
	}

	function postDoctorStep6() // Skill Form
	{
		$data = Request::all();
	//print_r($data); exit;
		$doctor_id=$data['hid_user_id'];		
		
		// Get value from databse
		$doc_license = DoctorSkill::where('doctor_id',$doctor_id)->get()->toArray();  
		
		// Delete all edited user's data before update

		DoctorSkill::where('doctor_id',$doctor_id)->delete();

		if(!empty($data['data']))
		{
			foreach($data['data'] as $k=>$eachskill)
			{
				if(array_filter($eachskill))  // checking atleast one value is there in array
				{
				   $doc_skill		= DoctorSkill::create([
										'doctor_id' => $doctor_id,
										'skills'=> trim($eachskill['skills']),
										'created_at' => date('Y-m-d h:i:s'),
										'updated_at' => date('Y-m-d h:i:s')
									]);				  
				}
			} // Foreach End
			
		} // Data If End	
		 
		
		return redirect('admin/dentist/dentist-step7/'.$doctor_id);			
	}

	//****************************************************//
	//******************** STEP 7 ************************//
	//****************************************************//

	function getDoctorStep7($id="") // 6th Step Add from admin
	{
		$doctor_class 			= "active";
        $module_head 			= "Add Dentist : Step-6";
        $title					= "Add Dentist : Step-6";
        if($id!='')
    	{
    		$user_id = $id;
    	}
    	else
    	{
    		$user_id = 0;
    	}

    	$languages_det = Language::where('status',1)->get()->toArray();

    	$languages = [];
    	foreach($languages_det as $each_lang)
    	{    		
    		$languages[$each_lang['id']] = $each_lang['name'];
    	}
    	$doc_language = DoctorLanguage::where('doctor_id',$user_id)->get()->toArray();
    	$doc_language_count = count($doc_language);
    	//print_r($doc_language); exit;
		$selected_lang_array = array();
    	$selected_lang_array_name =  '';
		
		if($doc_language_count)
		{
			$selected_lang_array = explode(',',$doc_language[0]['languages']);
			$selected_lang_array_name =  explode(',',$doc_language[0]['language_name']);
		}
    	
    	//print_r($doc_language); exit;
		return view('admin.user.doctor_step7',compact('module_head','doctor_class','title','user_id','doc_language','doc_language_count','languages','selected_lang_array','selected_lang_array_name'));		
	}

	function postDoctorStep7() // Skill Form
	{
		$data = Request::all();
		//print_r($data); exit;
		$doctor_id=$data['hid_user_id'];	
		$pub_prev = $data['pub_prev'];	  // 1 if publish  0 if preview	
		//echo $pub_prev; exit;
		
		// Get value from databse
		$doc_language = DoctorLanguage::where('doctor_id',$doctor_id)->get()->toArray();  
		
		// Delete all edited user's data before update

		DoctorLanguage::where('doctor_id',$doctor_id)->delete();

		$doc_language = Language::whereIn('id', $data['languages'])->get(['name'])->toArray(); 
		//print_r($doc_language); exit;
		$doc_language_arr = [];
		foreach($doc_language as $doc_lang)
		{
		$doc_language_arr[] = $doc_lang['name'];
		}
		//print_r($doc_language_arr); exit;

		if(!empty($data))		
		{
			$doc_language	= DoctorLanguage::create([
									'doctor_id' => $doctor_id,
									'languages'=> implode(',',$data['languages']),
									'language_name'=>implode(',',$doc_language_arr),
									'personal_statement'=> trim($data['personal_statement']),
									'created_at' => date('Y-m-d h:i:s'),
									'updated_at' => date('Y-m-d h:i:s')
								]);				  
		
		} // Data If End	

		if($pub_prev ==1)
		{
			$need_approval = Doctor::where('id', $doctor_id)->update(['publish_status'=>1]);
		}
		
		 
		
		return redirect('admin/dentist/dentist-preview/'.$doctor_id);			
	}



	/*  FOR REMOVE THE ITEMS ON ALL 7 STEPS START */
	function postRemoveItem()
	{
		$data = Request::all();
		$item_id = $data['itemid'];
		$model = $data['model'];
		$model='App\Model\\'.$model;  // use for dynamic model name sent from  view page function
		$del_item = $model::where('id',$item_id)->delete();
		
		if($del_item)
		{
		 echo 1; exit;
		}
	}

	function getDoctorAll($id)
	{
		$doctor_class 			= "active";
        $module_head 			= "Dentist Preview";
        $title					= "Dentist Preview";
        $user_id 				= $id;

		$dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$id)->first()->toArray(); 


		//print_r($dentist_details); exit;
		return view('admin.user.doctor_preview',compact('module_head','doctor_class','title','dentist_details','user_id'));
	}

	function postPublishProfile() // Publish dentist profile by admin
	{
		$data = Request::all();
		$dentist_id = $data['dentist_id'];
		$doctor_update = Doctor::where('id',$dentist_id)->update(['publish_status'=>1,'updated_at'=>date('Y-m-d h:i:s')]);

		Session::flash('success_message', 'Your Profile has been successfully publish.');
	}

	function postApprovedProfile()
	{
		$data = Request::all();
		//print_r($data); exit;
		$dentist_id = $data['dentist_id'];
		$dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$dentist_id)->first()->toArray(); 

		$doctor_update = Doctor::where('id',$dentist_id)->update(['email_status'=>1,'admin_status'=>1,'publish_status'=>1,'complete_profile_status'=>1,'updated_at'=>date('Y-m-d h:i:s')]);

		/* TEMPORARY TABLE DELETE AND INSERT */

		if(!empty($dentist_details['docs_details']))
		{
			$srcPathDet = 'uploads/dentist_profile_image/'; 
			$destPathDet = 'uploads/tmp_dentist_profile_image/';

			TmpDoctorDetails::where('doctor_id',$dentist_id)->delete();
			
				@unlink('uploads/tmp_dentist_profile_image/'.$dentist_details['docs_details']['profile_pics']);

			TmpDoctorDetails::create($dentist_details['docs_details']);

			if ($dentist_details['docs_details']['profile_pics'] !='' && file_exists($srcPathDet . $dentist_details['docs_details']['profile_pics'])) 
        	{			
				copy($srcPathDet . $dentist_details['docs_details']['profile_pics'], $destPathDet . $dentist_details['docs_details']['profile_pics']);
			}
		}
		if(!empty($dentist_details['docs_clinics']))
		{			
			$srcPathClinic = 'uploads/clinic_picture/'; 
			$destPathClinic = 'uploads/tmp_clinic_picture/';

			TmpDoctorClinic::where('doctor_id',$dentist_id)->delete();

				@unlink('uploads/tmp_clinic_picture/'.$dentist_details['docs_clinics']['clinic_picture']);

			TmpDoctorClinic::create($dentist_details['docs_clinics']);

			if ($dentist_details['docs_clinics']['clinic_picture'] !='' && file_exists($srcPathClinic . $dentist_details['docs_clinics']['clinic_picture']))			 
        	{
				copy($srcPathClinic . $dentist_details['docs_clinics']['clinic_picture'], $destPathClinic . $dentist_details['docs_clinics']['clinic_picture']);
			}
		}
		if(!empty($dentist_details['docs_education']))
		{
			$srcPathEdu = 'uploads/certificate/';  
			$destPathEdu = 'uploads/tmp_certificate/';

			TmpDoctorEducation::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_education'] as $each_education)
			{
				@unlink('uploads/tmp_certificate/'.$each_education['certificate']);

				TmpDoctorEducation::create($each_education);
				
				if ($each_education['certificate']!='' && file_exists($srcPathEdu . $each_education['certificate'])) 
        		{
					copy($srcPathEdu . $each_education['certificate'], $destPathEdu . $each_education['certificate']);
				}

			}
			
		}
		if(!empty($dentist_details['docs_award']))
		{
			TmpDoctorAward::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_award'] as $docs_award)
			{
				TmpDoctorAward::create($docs_award);
			}
		}
		if(!empty($dentist_details['docs_insurance']))
		{
			TmpDoctorInsurance::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_insurance'] as $each_insurance)
			{
				TmpDoctorInsurance::create($each_insurance);
			}
		}
		if(!empty($dentist_details['docs_experiece']))
		{
			$srcPathExp = 'uploads/exp_document/';  
			$destPathExp = 'uploads/tmp_exp_document/';

			TmpDoctorExperience::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_experiece'] as $each_exp)
			{
				@unlink('uploads/tmp_exp_document/'.$each_exp['exp_photo']);

				TmpDoctorExperience::create($each_exp);
				if ($each_exp['exp_photo']!=''  && file_exists($srcPathExp . $each_exp['exp_photo'])) 
        		{
					copy($srcPathExp . $each_exp['exp_photo'], $destPathExp . $each_exp['exp_photo']);
				}
			}
		}
		if(!empty($dentist_details['docs_license']))
		{
			$srcPathLicense = 'uploads/license_photo/'; 
			$destPathLicense =  'uploads/tmp_license_photo/';

			TmpDoctorLicense::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_license'] as $each_license)
			{
				@unlink('uploads/tmp_license_photo/'.$each_license['license_photo']);

				TmpDoctorLicense::create($each_license);
				if ($each_license['license_photo']!='' && file_exists($srcPathLicense . $each_license['license_photo'])) 
        		{
					copy($srcPathLicense . $each_license['license_photo'], $destPathLicense . $each_license['license_photo']);
			    }
			}
		}
		if(!empty($dentist_details['docs_skill']))
		{
			TmpDoctorSkill::where('doctor_id',$dentist_id)->delete();
			
			foreach($dentist_details['docs_skill'] as $each_skill)
			{
				TmpDoctorSkill::create($each_skill);
			}
		}
		if(!empty($dentist_details['docs_language']))
		{
			TmpDoctorLanguage::where('doctor_id',$dentist_id)->delete();
			
			TmpDoctorLanguage::create($dentist_details['docs_language']);
		}

		// update status fields //
		Doctor::where('id', $dentist_id)->update(['publish_status'=>1,'complete_profile_status'=>1,'profile_updated'=>1,'sent_notification'=>0]);

		Session::flash('success_message', 'Dentist Profile has been successfully approved and publish.'); 

		//return redirect('admin/dentist/dentist-preview/'.$doctor_id);		

	}

	

	function getCompareDentist($id)
	{
		$doctor_class 			= "active";
        $module_head 			= "Dentist Preview";
        $title					= "Dentist Preview";
        $user_id 				= $id;

        $dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$id)->first()->toArray(); 

		$tmp_dentist_details = Doctor::with('tmp_docs_details','tmp_docs_clinics','tmp_docs_education','tmp_docs_award','tmp_docs_insurance','tmp_docs_experiece','tmp_docs_license','tmp_docs_skill','tmp_docs_language')->where('id',$id)->first()->toArray(); 


		//print_r($tmp_dentist_details); exit;
		return view('admin.user.doctor_compare',compact('module_head','doctor_class','title','dentist_details','tmp_dentist_details','user_id'));
	}

}
