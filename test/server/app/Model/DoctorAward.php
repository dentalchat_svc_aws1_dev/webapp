<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorAward extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_awards';
     
}

