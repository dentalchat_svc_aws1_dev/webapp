(function() {
var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
angular.module('countrySelect', []).directive('countrySelect', function(shttp) {
var allCountries;

return {

restrict: 'AE',
replace: true,
scope: {
country:'@csCountries',
priorities: '@csPriorities',
only: '@csOnly',
except: '@csExcept',
index:"@csIndex",
ngModel:'='
}, 
template: '<select  ng-options="country.code as country.name disable when country.disabled for country in countries"> <option value="" ng-if="isSelectionOptional"></option> </select>',

controller: [
'$scope', '$attrs', function($scope, $attrs) {
	console.log("WOWOWOW",$scope.ngModel);
var countryCodesIn, findCountriesIn, includeOnlyRequestedCountries, removeCountry, removeExcludedCountries, separator, updateWithPriorityCountries;
separator = {
code: '-',
name: '────────────────────',
disabled: true
};
countryCodesIn = function(codesString) {
var codes;
codes = codesString ? codesString.split(',') : [];
return codes.map(function(code) {
return code.trim();
});
};
findCountriesIn = (function(_this) {
return function(codesString) {
var country, countryCodes, i, len, ref, ref1, results;
countryCodes = countryCodesIn(codesString);
console.log(_this);
ref = $scope.countries;
results = [];
for (i = 0, len = ref.length; i < len; i++) {
country = ref[i];
if (ref1 = country.code, indexOf.call(countryCodes, ref1) >= 0) {
results.push(country);
}
}
return results;
};
})(this);
removeCountry = (function(_this) {
return function(country) {
return $scope.countries.splice($scope.countries.indexOf(country), 1);
};
})(this);
includeOnlyRequestedCountries = (function(_this) {
return function() {
if (!$scope.only) {
return;
}
return $scope.countries = findCountriesIn($scope.only);
};
})(this);
removeExcludedCountries = function(c) {
var country, i, len, ref, results;
// if (!$scope.except) {
// return;
// }
console.log("RESULTS00",c);

ref = findCountriesIn(c);
results = [];
for (i = 0, len = ref.length; i < len; i++) {
country = ref[i];
console.log("RESULTS01",country);
results.push(removeCountry(country));
console.log("RESULTS",results);
}
return results;
};
updateWithPriorityCountries = (function(_this) {
return function() {
var i, len, priorityCountries, priorityCountry, ref, results;
priorityCountries = findCountriesIn($scope.priorities);
if (priorityCountries.length === 0) {
return;
}
$scope.countries.unshift(separator);
ref = priorityCountries.reverse();
results = [];
for (i = 0, len = ref.length; i < len; i++) {
priorityCountry = ref[i];
removeCountry(priorityCountry);
results.push($scope.countries.unshift(priorityCountry));
}
return results;
};
})(this);


$scope.countries=$scope.country;

console.log($scope.countries);
includeOnlyRequestedCountries();
removeExcludedCountries($scope.$parent.excluded);
updateWithPriorityCountries();





// console.log("ARRAY1",allCountries);
// console.log("ARRAY",allCountries.slice());
// includeOnlyRequestedCountries();
// removeExcludedCountries();
// var a = updateWithPriorityCountries();
// $scope.countries = this.countries;
// console.log(JSON.stringify(a));

return $scope.isSelectionOptional = $attrs.csRequired === void 0;
}
]
};
});
}).call(this);