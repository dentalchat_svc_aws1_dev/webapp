<?php namespace App\Http\Controllers\Elasticmail;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;



class ElasticController extends Controller {


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {

        //parent::__construct();
    }

public function getViewtem(){
    $data=array('firstname'=>"Anil",'lastname'=>"Pal","business"=>"business",'name'=>"Anil Pal",'user_id'=>1);
 return  view('elasticemail.uncompleteprofilcron', $data);
  
}

public function postMailsend(Request $request){

       $fromname="DentalChat";
       $user_id=$request->input('user_id');
       $name = $request->input('name');
       $bname = $request->input('business_name');
       $firstname = $request->input('firstname');
       $lastname = $request->input('lastname');
       $to=$request->input('email');//"anil.pal2006@gmail.com";
       $subject = $request->input('subject');
       $template_name=$request->input('template_name');
       $cc=$request->input('cc');
       
$data=array('firstname'=>$firstname,'lastname'=>$lastname,"business"=>$bname,'name'=>$name,'user_id'=>$user_id);

$url = 'https://api.elasticemail.com/v2/email/send';
if($cc){
   // $to=$to.";dentist@dentalchat.com";
    //$to=$to.";anilp8021@gmail.com";

}
try{
        $post = array('from' => 'notification@dentalchat.com',
		'fromName' => $fromname,
		'firstname'=>$name,
		'apikey' => 'c692f75d-70af-4a01-820f-8a50f5ec43d6',
		'subject' => $subject,
		'to' => $to,
		'bodyHtml' => view('elasticemail.'.$template_name, $data),
		'bodyText' => 'Text Body',
		'isTransactional' => false
		 );

		$ch = curl_init();
		curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
			CURLOPT_SSL_VERIFYPEER => false
        ));
		
        $result=curl_exec ($ch);
        curl_close ($ch);
	// echo $result;	
         	
}
catch(Exception $ex){
//echo $ex->getMessage();
}
}    

public function postMailsendpateintpost(Request $request){

       $fromname="DentalChat";
       $user_id=$request->input('user_id');
       $dentist_name = $request->input('dentist_name');
       $patient_name = $request->input('patient_name');
       $post_title = $request->input('post_title');
       $post_desc = $request->input('post_desc');
       $pain_level = $request->input('pain_level');
       $emergency=$request->input('emergency');
       $to=$request->input('email');
       $subject = $request->input('subject');
       $template_name='dentistreceivepost';

$data=array('post_title'=>$post_title,'post_desc'=>$post_desc,'pain_level'=>$pain_level,'name'=>$dentist_name,'emergency'=>$emergency,'user_id'=>$user_id,'patient_name'=>$patient_name);
 
$url = 'https://api.elasticemail.com/v2/email/send';

try{
        $post = array('from' => 'notification@dentalchat.com',
		'fromName' => $fromname,
		'firstname'=>$name,
		'apikey' => 'c692f75d-70af-4a01-820f-8a50f5ec43d6',
		'subject' => $subject,
		'to' => $to,
		'bodyHtml' => view('elasticemail.'.$template_name, $data),
		'bodyText' => 'Text Body',
		'isTransactional' => false
		 );

		$ch = curl_init();
		curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
			CURLOPT_SSL_VERIFYPEER => false
        ));
		
        $result=curl_exec ($ch);
        curl_close ($ch);
	// echo $result;	
         	
}
catch(Exception $ex){
//echo $ex->getMessage();
}
}        
    
}
