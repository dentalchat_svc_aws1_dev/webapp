@extends('admin/layout/admin_template')

@section('content')
<script>
		$(document).ready(function(){
			var _URL = window.URL || window.webkitURL;
			$("#clinic_picture").change(function (e) {

				$('#img_msg').html('');

			    var file, img;
			    var allowed_coverimage_width = parseInt(300);
				var allowed_coverimage_height = parseInt(300);
				var file, img;
			    if ((file = this.files[0])) {
			        img = new Image();
			        img.onload = function () {
			            var wid = this.width;
			            var ht = this.height;

			            if(wid < allowed_coverimage_width)
						{
							$('#img_msg').html("Profile Image width should be minimum "+allowed_coverimage_width+" px");
							$('#blah').attr('src', '');
							$('#clinic_picture').val('');
							$('#subbtn').attr('type','button');
							return false;

						}else{
							$('#subbtn').attr('type','submit');
						}
						if(ht < allowed_coverimage_height)
						{
							$('#img_msg').html("Profile Image height should be minimum "+allowed_coverimage_height+" px");
							$('#blah').attr('src', '');
							$('#clinic_picture').val('');
							$('#subbtn').attr('type','button');
							return false;
						}else{
							$('#subbtn').attr('type','submit');
						}
						
			        };

			        img.src = _URL.createObjectURL(file);
			    }
			});
		});
	</script>
	{!! Form::open(['url' => 'admin/dentist/dentist-step2/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step2']) !!}
		
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}
		
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCECNx6YKAjaYfP9Eq7FXAMB1QmjUKvMZk&libraries=places"></script>
		<script type="text/javascript" src="<?php echo url();?>/public/backend/scripts/datepair.js"></script>
		<script type="text/javascript" src="<?php echo url();?>/public/backend/scripts/jquery.datepair.js"></script>

		<div class="control-group" style="display:block;" id="div_id">       
					
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Business Name </label>
				<div class="controls">
					<input type="text" name="business_name" class="span8" id="business_name" value="<?php echo (isset($clinic_det['business_name']))?$clinic_det['business_name']:''?>">
				</div>
			</div>
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Business Address </label>
				<div class="controls">
					<!-- <textarea rows="2" class="span8" name="address" id="address"><?php echo (isset($clinic_det['address']))?$clinic_det['address']:''?></textarea> -->
					<input type="text" name="address" id="address" value="<?php echo (isset($clinic_det['address']))?$clinic_det['address']:''?>" class="span8" >
					<input type="hidden" name="place_lat" id="place_lat" value="<?php echo (isset($clinic_det['lat']))? $clinic_det['lat']:'';?>">
					<input type="hidden" name="place_lng" id="place_lng" value="<?php echo (isset($clinic_det['lang']))? $clinic_det['lang']:'';?>">

					<input type="hidden" name="place_city" id="place_city" value="<?php echo (isset($clinic_det['city']))? $clinic_det['city']:'';?>">
					<input type="hidden" name="place_state" id="place_state" value="<?php echo (isset($clinic_det['state']))? $clinic_det['state']:'';?>">
					<input type="hidden" name="place_country" id="place_country" value="<?php echo (isset($clinic_det['country']))? $clinic_det['country']:'';?>">
				</div>
			</div>
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Zip Code *</label>
				<div class="controls">
					<input type="text" name="zip_code" class="span8" id="zip_code" value="<?php echo (isset($clinic_det['zip_code']))?$clinic_det['zip_code']:''?>">
				</div>				
			</div>

			<hr>
			<h4 class="text-center">Opening Hours</h4>
			<div class="control-group" style="display:block;">
				<h5 class="text-center">Sunday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="sun_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="sun_opening_hours_from" value="<?php echo (isset($clinic_det['sun_opening_hours_from']))?$clinic_det['sun_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="sun_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="sun_opening_hours_to" value="<?php echo (isset($clinic_det['sun_opening_hours_to']))?$clinic_det['sun_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Monday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="mon_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="mon_opening_hours_from" value="<?php echo (isset($clinic_det['mon_opening_hours_from']))?$clinic_det['mon_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="mon_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="mon_opening_hours_to" value="<?php echo (isset($clinic_det['mon_opening_hours_to']))?$clinic_det['mon_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Tuesday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="tue_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="tue_opening_hours_from" value="<?php echo (isset($clinic_det['tue_opening_hours_from']))?$clinic_det['tue_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="tue_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="tue_opening_hours_to" value="<?php echo (isset($clinic_det['tue_opening_hours_to']))?$clinic_det['tue_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Wednesday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="wed_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="wed_opening_hours_from" value="<?php echo (isset($clinic_det['wed_opening_hours_from']))?$clinic_det['wed_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="wed_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="wed_opening_hours_to" value="<?php echo (isset($clinic_det['wed_opening_hours_to']))?$clinic_det['wed_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Thursday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="thu_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="thu_opening_hours_from" value="<?php echo (isset($clinic_det['thu_opening_hours_from']))?$clinic_det['thu_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="thu_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="thu_opening_hours_to" value="<?php echo (isset($clinic_det['thu_opening_hours_to']))?$clinic_det['thu_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Friday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="fri_opening_hours_from" class="span8 ui-timepicker-input dropmenu start timeclass" id="fri_opening_hours_from" value="<?php echo (isset($clinic_det['fri_opening_hours_from']))?$clinic_det['fri_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="fri_opening_hours_to" class="span8 ui-timepicker-input dropmenu end timeclass" id="fri_opening_hours_to dropmenu" value="<?php echo (isset($clinic_det['fri_opening_hours_to']))?$clinic_det['fri_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

				<h5 class="text-center">Saturday</h5>
				<div class="row datepair_class">
				<div class="span6">
					<label class="control-label" for="basicinput">From</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="sat_opening_hours_from" class="span8 ui-timepicker-input dropmenu  start timeclass" id="sat_opening_hours_from" value="<?php echo (isset($clinic_det['sat_opening_hours_from']))?$clinic_det['sat_opening_hours_from']:''?>" autocomplete="off">
					</div>
				</div>
				<div class="span6">
					<label class="control-label" for="basicinput">To</label>
					<div class="controls">
						<input type="text" data-format="hh:mm" name="sat_opening_hours_to" class="span8 ui-timepicker-input dropmenu  end timeclass" id="sat_opening_hours_to" value="<?php echo (isset($clinic_det['sat_opening_hours_to']))?$clinic_det['sat_opening_hours_to']:''?>" autocomplete="off">
					</div>
				</div>  
				</div>

			</div>

			
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Upload Picture</label>
				<div class="controls">
					<div class="file-upload upload-btn">
				<div class="upload">Upload</div>
					<input type="file" id="clinic_picture" name="clinic_picture"/>
					</div>
					<span id="img_msg" style="color:#F00;font-size:12px;clear:both;"></span>
				</div>	
                <?php if(isset($clinic_det['clinic_picture']) && $clinic_det['clinic_picture']!=''){?>			
					<p class="new_avatar" style="padding-left: 180px;padding-top: 10px;"><img src="<?php echo url();?>/uploads/clinic_picture/{{$clinic_det['clinic_picture']}}" class="nav-avatar"></p>	
                    <?php } ?>			
			</div>			
		</div>	
			
		<div class="control-group">		
			<div class="controls">
				<a href="{!! url('admin/dentist/dentist-step1/'.$user_id)!!}" class="btn">Prev</a>
				{!! Form::submit('Next',array('class'=>'btn','name'=>'action','value'=>'Next','id'=>'subbtn')) !!}				
			</div>				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
	
<script>

/**** SOF Remove First li from generated list ***/
$('.end').click(function(){
    $('.ui-timepicker-list').find('li:first').remove();
});
/**** SOF Remove First li from generated list ***/

	$(function() {    
    	$('.dropmenu').timepicker({
        timeFormat: 'h:i a'
    }); 
    	 $('.datepair_class').datepair();
  	});

	/******** Google Auto complete Address Start **********/

    google.maps.event.addDomListener(window, 'load', initialize);
    function initialize() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();

            
            for (var i = 0; i < place.address_components.length; i++) {
	          var addressType = place.address_components[i].types[0];

				if(addressType=='postal_code'){
					var val = place.address_components[i];
					console.log(val.long_name);
					$('#zip_code').val(val.long_name);
				}

				if (addressType == "locality") {
				        //this is the object you are looking for
				      var  city = place.address_components[i];
				}
				if (addressType == "administrative_area_level_1") {
				        //this is the object you are looking for
				       var region = place.address_components[i];
				}
				if (addressType == "country") {
				        //this is the object you are looking for
				       var country = place.address_components[i];
				}
	          
	          
	        }
	         
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            var city = city.long_name;
            var state = region.long_name;
            var country = country.long_name;
            $("#place_lat").val(lat);
            $("#place_lng").val(lng);
            $("#place_city").val(city);
            $("#place_state").val(state);
            $("#place_country").val(country);
            
        });

    }

    /******** Google Auto complete Address Start **********/


</script>


@stop