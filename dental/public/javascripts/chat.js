var socket = io.connect('/');

socket.emit("set_name", { name: uname, buddy: chatbuddy, roomName: room });

//socket.emit('create', {roomName : room });

socket.on('name_set', function(data) {

    //alert(data.name);

    $('#welcome').append('<div class="systemMessage">' +
        'Hello ' + data.name + '</div>');
    $('#send').click(function() {
        var msg = $('#message').val();
        if (msg != "") {
            var inner = $("#messageAlert").html();
            if (inner != "") {
                $("#messageAlert").html('');
            }
            var data = {
                message: $('#message').val(),
                type: 'userMessage'
            };
            socket.send(JSON.stringify(data));
            $('#message').val('');
        } else {
            $("#messageAlert").html('');
            $("#messageAlert").append('cannot send blank message');
        }
    });

    socket.on('message', function(data) {
        data = JSON.parse(data);
        console.log(data);
        if (data.username) {
            $('#messages').append('<div class="' + data.type +
                '"><span class="name">' +
                data.username + ":</span> " +
                data.message + '</div>');
        } else {
            $('#welcome').append('<div class="' + data.type + '">' +
                data.message +
                '</div>');
        }
    });

    // socket.on("user_entered", function(user){
    // $('#messages').append('<div class="systemMessage">' +
    // user.name + ' has joined the chat.' + '</div>');
    // });
});
