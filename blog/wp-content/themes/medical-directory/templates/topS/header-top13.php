    <div class="box-shadow-for-ui">
      <div class="uou-block-2d">
        <div class="container">
          <div class="contact">
            <span>Call Us:</span>
            <a href="tel:(02)1234567890">(02) 123-456-7890</a>
          </div>

          <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> <img src="<?php echo esc_url(medicaldirectory_IMAGE); ?>logo.png" alt="<?php esc_html_e( 'image', 'medical-directory' ); ?>"> </a>
          <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>

          <a href="#" class="cart"><i class="fa fa-shopping-cart"></i> <?php esc_html_e( 'Shopping Cart', 'medical-directory' ); ?>   (0)</a>
        </div>
      </div> <!-- end .uou-block-2d -->
    </div>