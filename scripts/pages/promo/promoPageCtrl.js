/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.promo').controller('PromoPageCtrl', PromoPageCtrl)
        .filter('renderhtml',function($sce){

             return function(input){
                return $sce.trustAsHtml(input);
            }
        });
    /** @ngInject */
    function PromoPageCtrl($sce,$scope, $timeout,  $http, configService, $rootScope, $location, cmsService, ngMeta) {
        $scope.viewDetails = {};
        
        /*check the accomodation_id is for that user or not*/
        $scope.allCms = function(page_id) {
           
			var data = {
				page_id:page_id            
            };
			
			$rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/cms/all-cms",data)
            .success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;                
				$scope.cms_details  = data.all_cms;

                ngMeta.setTitle(data.all_cms.page_title);
                ngMeta.setTag('description', data.all_cms.meta_description);
                ngMeta.setTag('keywords', data.all_cms.meta_keyword);
            })
            .error(function(data, status, header, config) {
                //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }


        /* Valid Email Checking Start */
        $scope.checkEmail=function(email) {

            var patt = /^$|^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;
            var res = patt.test(email);
    
            if(res)
            {
                return 1;
            }
            else
            {
                if(typeof email == 'undefined')
                {
                    return 1;
                }
                return 0;
            }
        };
        
             
        $scope.registerDentist = function(user){
            $rootScope.showLoader = 1;
            $scope.succ_msg         = '';
            var website             = 0;
            var amount              = 0;
            
            var data = {
                user:user
            };
            //console.log(data);
            var config = {              
                            headers: {
                                'Content-Type': 'application/json; charset=utf-8'
                            }         
            };
            
            $http.post(configService.getEnvConfig().apiURL + "service/register",data, config)
            .success(function(data, status, headers, config) {
                
                $rootScope.showLoader = 0;
                if (data.status==2) 
                {
                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = 'Email already exists.';
                    $rootScope.showSuccMsg  = 1;
                }
                else
                {
                    $rootScope.reg_success_msg = "Thank you for registering with us. A verification link has been sent to "+data.useremail+"";  
                    //console.log($scope.reg_success_msg);
                    $location.path('/registration-success');
                    
                }                               
    
            })
            .error(function(data, status, header, config) {
                //console.log(data);
            });
        }
        

       /* $scope.renderHtml = function(html_code)
        {
            return $sce.trustAsHtml(html_code);
        }
*/
        

       
    }
    angular.module('doctorchat.pages.promo').service('vsGooglePlaceUtility', function() {
    function isGooglePlace(place) {
        if (!place) return false;
        return !!place.place_id;
    }

    function isContainTypes(place, types) {
        var placeTypes, placeType, type;
        if (!isGooglePlace(place)) return false;
        placeTypes = place.types;
        for (var i = 0; i < types.length; i++) {
            type = types[i];
            for (var j = 0; j < placeTypes.length; j++) {
                placeType = placeTypes[j];
                if (placeType === type) {
                    return true;
                }
            }
        }
        return false;
    }

    function getAddrComponent(place, componentTemplate) {
        var result;
        if (!isGooglePlace(place)) return;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentTemplate[addressType]) {
                result = place.address_components[i][componentTemplate[addressType]];
                return result;
            }
        }
        return;
    }

    function getPlaceId(place) {
        if (!isGooglePlace(place)) return;
        return place.place_id;
    }

    function getStreetNumber(place) {
        var COMPONENT_TEMPLATE = {
                street_number: 'short_name'
            },
            streetNumber = getAddrComponent(place, COMPONENT_TEMPLATE);
        return streetNumber;
    }

    function getStreet(place) {
        var COMPONENT_TEMPLATE = {
                route: 'long_name'
            },
            street = getAddrComponent(place, COMPONENT_TEMPLATE);
        return street;
    }

    function getCity(place) {
        var COMPONENT_TEMPLATE = {
                locality: 'long_name'
            },
            city = getAddrComponent(place, COMPONENT_TEMPLATE);
        return city;
    }

    function getState(place) {
        var COMPONENT_TEMPLATE = {
                administrative_area_level_1: 'short_name'
            },
            state = getAddrComponent(place, COMPONENT_TEMPLATE);
        return state;
    }

    function getDistrict(place) {
        var COMPONENT_TEMPLATE = {
                administrative_area_level_2: 'short_name'
            },
            state = getAddrComponent(place, COMPONENT_TEMPLATE);
        return state;
    }

    function getCountryShort(place) {
        var COMPONENT_TEMPLATE = {
                country: 'short_name'
            },
            countryShort = getAddrComponent(place, COMPONENT_TEMPLATE);
        return countryShort;
    }

    function getCountry(place) {
        var COMPONENT_TEMPLATE = {
                country: 'long_name'
            },
            country = getAddrComponent(place, COMPONENT_TEMPLATE);
        return country;
    }

    function getPostCode(place) {
        var COMPONENT_TEMPLATE = {
                postal_code: 'long_name'
            },
            postCode = getAddrComponent(place, COMPONENT_TEMPLATE);
        return postCode;
    }

    function isGeometryExist(place) {
        return angular.isObject(place) && angular.isObject(place.geometry);
    }

    function getLatitude(place) {
        if (!isGeometryExist(place)) return;
        return place.geometry.location.lat();
    }




    function getLongitude(place) {
        if (!isGeometryExist(place)) return;
        return place.geometry.location.lng();
    }
    return {
        isGooglePlace: isGooglePlace,
        isContainTypes: isContainTypes,
        getPlaceId: getPlaceId,
        getStreetNumber: getStreetNumber,
        getStreet: getStreet,
        getCity: getCity,
        getState: getState,
        getCountryShort: getCountryShort,
        getCountry: getCountry,
        getLatitude: getLatitude,
        getLongitude: getLongitude,
        getPostCode: getPostCode,
        getDistrict: getDistrict
    };
}).directive('promoGoogleAutocomplete', ['vsGooglePlaceUtility', '$timeout', function(vsGooglePlaceUtility, $timeout) {
    return {
        restrict: 'A',
        require: ['promoGoogleAutocomplete', 'ngModel'],
        scope: {
            promoGoogleAutocomplete: '=',
            promoPlace: '=?',
            promoPlaceId: '=?',
            promoStreetNumber: '=?',
            promoStreet: '=?',
            promoCity: '=?',
            promoState: '=?',
            promoCountryShort: '=?',
            promoCountry: '=?',
            promoPostCode: '=?',
            promoLatitude: '=?',
            promoLongitude: '=?',
            promoDistrict: '=?'
        },
        controller: ['$scope', '$attrs', function($scope, $attrs) {
            this.isolatedScope = $scope;
            this.updatePlaceComponents = function(place) {
                $scope.promoPlaceId = !!$attrs.promoPlaceId && place ? vsGooglePlaceUtility.getPlaceId(place) : undefined;
                $scope.promoStreetNumber = !!$attrs.promoStreetNumber && place ? vsGooglePlaceUtility.getStreetNumber(place) : undefined;
                $scope.promoStreet = !!$attrs.promoStreet && place ? vsGooglePlaceUtility.getStreet(place) : undefined;
                $scope.promoCity = !!$attrs.promoCity && place ? vsGooglePlaceUtility.getCity(place) : undefined;
                $scope.promoPostCode = !!$attrs.promoPostCode && place ? vsGooglePlaceUtility.getPostCode(place) : undefined;
                $scope.promoState = !!$attrs.promoState && place ? vsGooglePlaceUtility.getState(place) : undefined;
                $scope.promoCountryShort = !!$attrs.promoCountryShort && place ? vsGooglePlaceUtility.getCountryShort(place) : undefined;
                $scope.promoCountry = !!$attrs.promoCountry && place ? vsGooglePlaceUtility.getCountry(place) : undefined;
                $scope.promoLatitude = !!$attrs.promoLatitude && place ? vsGooglePlaceUtility.getLatitude(place) : undefined;
                $scope.promoLongitude = !!$attrs.promoLongitude && place ? vsGooglePlaceUtility.getLongitude(place) : undefined;
                $scope.promoDistrict = !!$attrs.promoDistrict && place ? vsGooglePlaceUtility.getDistrict(place) : undefined;
            };
        }],
        link: function(scope, element, attrs, ctrls) {
            var autocompleteCtrl = ctrls[0],
                modelCtrl = ctrls[1];
            var autocompleteOptions = scope.promoGoogleAutocomplete || {},
                autocomplete = new google.maps.places.Autocomplete(element[0], autocompleteOptions);
            var place;
            var viewValue;
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                place = autocomplete.getPlace();
                viewValue = place.formatted_address || modelCtrl.$viewValue;
                scope.$apply(function() {
                    scope.vsPlace = place;
                    autocompleteCtrl.updatePlaceComponents(place);
                    modelCtrl.$setViewValue(viewValue);
                    modelCtrl.$render();
                });
            });
            element.on('blur', function(event) {
                viewValue = (place && place.formatted_address) ? viewValue : modelCtrl.$viewValue;
                $timeout(function() {
                    scope.$apply(function() {
                        modelCtrl.$setViewValue(viewValue);
                        modelCtrl.$render();
                    });
                });
            });
            google.maps.event.addDomListener(element[0], 'keydown', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
            });
        }
    };
}]);
})();
