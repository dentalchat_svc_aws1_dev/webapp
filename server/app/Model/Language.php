<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'languages';
}

