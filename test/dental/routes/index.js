var express = require('express');
var router = express.Router();
var User = require('../models/account');
var Chat = require('../models/chat');
/* GET home or login  page. */
router.get('/', function (req, res, next) {
    if (req.session.user) {
        res.render('contact', {
            title: 'Chat',
            user: req.session.user
        });
    } else {
        res.render('index', {
            title: 'Chat'
        }); 
    }
});

/* POST login page*/
router.post('/login', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    console.log(username + password);
    User.findOne({
        "username": username
    }, function (err, user) {
        if (user) {
            if (user.password == password) {
                console.log("loggedIn");
                req.session.user = username;
                res.redirect('/contact');
            } else {
                res.render('index', {
                    'error': 'wrong password'
                });
                console.log("wrong password");
            }
        } else {
            res.render('index', {
                'error': 'wrong username'
            });
            console.log("wrong username");
        }

    })

});
/* logout*/

router.get('/logout', function (req, res, next) {
    req.session.destroy();
    res.redirect('/');
});


/* GET contact page. */
router.get('/contact', function (req, res, next) {
    if (req.session.user) {
        res.render('contact', {
            title: 'Chat',
            user: req.session.user
        });
    } else {
        res.render('index', {
            title: 'Chat'
        });
    }

});



/* GET chatroom page. */
router.get('/chat', function (req, res, next) {
    //var chats_data = {}; 
    if (req.session.user) {
        var buddy = req.query.buddy;
        var holder = req.session.user;
        //console.log(buddy);
        Chat.find({
            $or: [
                {
                    $and: [{
                        'from': buddy
                    }, {
                        'to': holder
                    }]
                },
                {
                    $and: [{
                        'from': holder
                    }, {
                        'to': buddy
                    }]
                }
            ]
        }, function (err, chats) {
            if (chats == "") {
                res.render('chatroom', {
                    title: 'Chat',
                    user: req.session.user,
                    receiver: buddy,
                    chat: 'null'
                });
                //console.log("null");
            } else {
                //console.log(JSON.stringify(chats));   
                res.render('chatroom', {
                    title: 'Chat',
                    user: req.session.user,
                    receiver: buddy,
                    chat: chats
                });
            }

        });
    } else {
        res.render('index', {
            title: 'Chat'
        });
    }

});

router.post('/chatroom', function (req, res, next) {
    var sendData = {};
    sendData.user = req.session.user;
    sendData.receiver = req.body.PostBuddy;
    res.send(sendData);
});
////////
//////////////////////////////////////////////////////////////////////////////////////

router.get('/room', function (req, res, next) {
    roomName = req.query.room;
    if (req.session.user) {
        Chat.find({
                'to': roomName
            },
            function (err, chats) {
                if (chats == "") {
                    res.render('chatroom', {
                        title: 'Chat',
                        user: req.session.user,
                        room: roomName,
                        chat: 'null'
                    });
                    //console.log("null");
                } else {
                    //console.log(JSON.stringify(chats));   
                    res.render('chatroom', {
                        title: 'Chat',
                        user: req.session.user,
                        room: roomName,
                        chat: chats
                    });
                }

            });
    } else {
        //console.log(JSON.stringify(chats));   
        res.render('index', {
            title: 'Chat'
        });
    }

});

/*
|---------------------------------------------------------
| image upload service
|---------------------------------------------------------
*/
var multer = require('multer');

var uploading = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'upload')
        },
        filename: function (req, file, cb) {
            var fn_arr = file.mimetype.split('/');
            var fntype = fn_arr[1];
            cb(null, Date.now() + '.' + fntype) //Appending .jpg
        }
    })
});

router.post('/image-uploaded', uploading.single('image'), function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    console.log(req.file);
    //console.log(req.body);
    if (!req.file) {
        return res.send({
            status: 404,
            msg: "no files provided"
        });
    }

    var user = req.body.from;
    var receiver = req.body.buddy;
    var message = req.body.message;
    var senderName = req.body.senderName;
    var receiverName = req.body.receiverName;
    var post_id = req.body.post_id;
    var status = req.body.status;
 //Comment By Shailendra - 5 May 2018
    if (user > receiver) {
       var uniquecode = user + '_' + receiver;
  } else {
        var uniquecode = receiver + '_' + user;
   }
 //Add By Shailendra - 5 May 2018
//var uniquecode = receiver + '_' + user;
//console.log("UserType", data.usertype);
//var uniquecode = "";
//if(usertype == "Patient")
//uniquecode = user + '_' +receiver ;
//else
//uniquecode = receiver + '_' + user;
//alert(uniquecode);
//console.log(uniquecode);
 
    if (!user) return res.send({
        status: 404,
        msg: "user not provided"
    });
    if (!receiver) return res.send({
        status: 404,
        msg: "receiver not provided"
    });
    caption = (!message) ? "" : message;

    var file_extension = req.file.mimetype.split('/');
    Chat.create({
        from: user,
        to: receiver,
        message: message,
        chat_file: req.file.path,
        file_ext: file_extension[0],
        uniquecode: uniquecode,
        post_id: post_id,
        senderName: senderName,
        receiverName: receiverName,
        status: status,
        createdAt: new Date()
    }, function (err, chat) {
        if (err) {
            return res.send({
                status: 0,
                error: err
            });
        } else {
            return res.send({
                status: 1,
                result: chat
            });
        }
    });
});



module.exports = router;