<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
	protected $guarded=[];
	public $timestamps = false;

    protected $table = 'notifications';
	public function from_details()
    {
    	return $this->belongsTo("App\Model\DoctorDetails","from","doctor_id");
    }

}
