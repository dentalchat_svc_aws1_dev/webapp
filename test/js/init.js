$( document ).ready(function() {

/*for nav*/
var lastScrollTop = 0, delta = 5;
 $(window).scroll(function(event){
    var st = $(this).scrollTop();
 
    if (Math.abs(lastScrollTop - st) <= delta)
    return;
 
   if (st > lastScrollTop){
     $('.site-header').addClass('fixed-theme');
    } else {
    $('.site-header').removeClass('fixed-theme');
    }
    lastScrollTop = st;
 });

/*banner*/
    var swiper = new Swiper('.swiper-banner', {
        zoom: true,
        autoplay:3000,
        // pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });



});


function clientSpeak()
{
  var swiper = new Swiper('.clients_say .swiper-container', {
        pagination: '.clients_say .swiper-pagination',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        autoplay:3000,
        // Responsive breakpoints
        breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 2,
      spaceBetween: 30
    }
  }
    });
}

function readImage(file,id,width,height,size,backgroundimage) 
  {
    //alert(id);
    var reader = new FileReader();
    var image  = new Image();

    reader.readAsDataURL(file);  
    reader.onload = function(_file) {
    image.src    = _file.target.result;              // url.createObjectURL(file);
    image.onload = function() {
    var w = this.width,
      h = this.height,
      t = file.type,                           // ext only: // file.type.split('/')[1],
      n = file.name,
      s = ~~(file.size/1024);
      //alert(w);
      // alert(t);
      // alert(s);
      var image_type_arr = t.split("/");
      
      if(image_type_arr.length<2)
      {
        sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
        return;
      }

      var image_type = image_type_arr[1].toLowerCase();
      
        if(id=='profile_pics')
        {
          if(!(image_type ==='jpg' || 
          image_type ==='jpeg' ||  
          image_type ==='png'  ||  
          image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<300)
          {
            sweetAlert("Oops...", "Image width should not be less than 300 px", "error");
            return;
          }
          if(h<300)
          {
            sweetAlert("Oops...", "Image height should not be less than 300 px", "error");
            return;
          }
           document.getElementById("profile_image").src = this.src; 
          document.getElementById("profile_pics").style.backgroundImage = 'url('+this.src+')';
          setTimeout(function(){
          },1000);
        }
        else if(id=="cover_image")
        {
          if(!(image_type ==='jpg' || 
          image_type ==='jpeg' ||  
          image_type ==='png'  ||  
          image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<150)
          {
            sweetAlert("Oops...", "Image width should not be less than 150 px", "error");
            return;
          }
          if(h<150)
          {
            sweetAlert("Oops...", "Image height should not be less than 150 px", "error");
            return;
          }
          //document.getElementById("logoImage").style.backgroundImage = 'url('+this.src+')';
          document.getElementById("restaurant_cover_image").src = image.src;
        }
        else if(id=="logo")
        {
          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          /*if(w<1024)
          {
            sweetAlert("Oops...", "Image width should not be less than 1024 px", "error");
            return;
          }
          if(h<512)
          {
            sweetAlert("Oops...", "Image height should not be less than 512 px", "error");
            return;
          }*/
            
          document.getElementById("logo_image").src = image.src;
        }
        else if(id=="image")
        {
          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size);
          if(s>2 * 1024)
          {
            sweetAlert("Oops...", "Image size should be less than 2MB", "error");
            return;
          }
          if(w<300)
          {
            sweetAlert("Oops...", "Image width should not be less than 300 px", "error");
            return;
          }
          if(h<300)
          {
            sweetAlert("Oops...", "Image height should not be less than 300 px", "error");
            return;
          }
            
          document.getElementById("profile_image").src = image.src;
          angular.element('#image').scope().showImage();
          $(document).click();
        }
        else
        {
          size = (size === undefined)?1024:size;
          height = (height === undefined)?1024:height;
          width = (width === undefined)?1024:width;

          if(!(image_type ==='jpg' || image_type ==='jpeg' ||  image_type ==='png'  ||  image_type ==='gif'))
          {
            sweetAlert("Oops...", "Not a valid image type.Please use jpg/jpeg/png/gif type image.", "error");
            return;
          }

          if(!t.match("image.*")) {
            return;
          }
          //alert(f.size); 
          if(s> size* 1024)
          {
            sweetAlert("Oops...", "Image size should be less than "+size+"MB", "error");
            return;
          }
          /*if(w<width)
          {
            sweetAlert("Oops...", "Image width should not be less than "+width+" px", "error");
            return;
          }
          if(h<height)
          {
            sweetAlert("Oops...", "Image height should not be less than "+height+" px", "error");
            return;
          }*/
          /*document.getElementById("image").src = image.src;
          if($('#'+backgroundimage))
            $('#'+backgroundimage).css('background-image','url('+this.src+')');*/
        }
      
      };
      image.onerror= function() 
      {
        sweetAlert("Oops...", "Invalid file type: "+ file.type, "error");
      };      
    };

  }