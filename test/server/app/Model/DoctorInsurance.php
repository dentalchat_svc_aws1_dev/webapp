<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorInsurance extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_insurances';
     
}

