<?php
/**
 * Template Name: My Story Template
 */

get_header(); 
?><div class="blog-content">
    <div class="mystory_banner">    
        <div class="row">
            <!--<div class="mystory_slider">-->
                <?php /*echo do_shortcode("[metaslider id=1025]");*/ ?>
            <!--</div>-->
            <div class="dentalstory_content">
            <p><?php dynamic_sidebar('mystory-page-text-widgets'); ?></p>
            <?php if ( is_page_template( 'template-mystory.php' ) ) { ?>
            <p><a href="<?php echo site_url(); ?>/my-story-form/" class="btn btn-primary btn-lg">Post My Story</a></p>
            <?php } ?>
            </div>
        </div>
    </div>   
   <div class="container">
      <div class="col-md-12">
        
    <div class="row">
        <div class="blogs_colgrid">
         <?php 
         $query_args = array(
            'post_type' => 'mystory',
            'posts_per_page' => -1, 
            'post_status'=>'publish',
            'orderby'=>'DESC'
         );
         $the_query = new WP_Query( $query_args ); 
          if ( $the_query->have_posts() ) { 
          while ( $the_query->have_posts() ) : $the_query->the_post();       
         ?>      
          <div class="col-sm-12">
            <article class="uou-block-7g blog post-679 post type-post status-publish format-standard has-post-thumbnail hentry category-dental-chat-dentalchat category-featured category-online-dental-chat" data-badge-color="ff0099">
                <div class="mystory_images">
                <?php if ( has_post_thumbnail() ) { echo get_the_post_thumbnail( $post_id, 'large' ); } else { echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
        . '/assets/img/teeth_default.jpg" />'; }
                //echo get_the_post_thumbnail( $post_id, array( 360, 220) );
                ?>
                </div><div class="content">
                    <!--<span class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';  ?></span>-->
                     <h1><a href="<?php echo get_the_permalink(); ?>">  <?php the_title(); ?>  </a></h1>
                     <p><?php echo wp_trim_words( get_the_content(), 50, '..' ); ?></p>
                     <a href="<?php echo get_the_permalink(); ?>" class="btn btn-small btn-primary">Read More</a>
                  </div>
            </article>  
            </div>
            
         <?php endwhile; }
          wp_reset_postdata();
            wp_reset_query();
           /* echo do_shortcode('[ajax_load_more post_type="mystory" repeater="default" scroll="false" offset="3" posts_per_page="3" transition="fade" button_label="More Story"]');Without login*/ 
          /*  if ( is_user_logged_in() ) {
     echo do_shortcode('[ajax_load_more post_type="mystory" repeater="default" scroll="false" offset="" posts_per_page="3" transition="fade" button_label="More Story"]');
           
} else {
   echo do_shortcode('[ajax_load_more post_type="mystory" repeater="default" scroll="false" offset="3" posts_per_page="3" transition="fade" button_label="More Story"]');
}*/
            
          
            ?>
            </div>
         </div>          
      </div>
   </div>
</div>
<?php get_footer(); ?>