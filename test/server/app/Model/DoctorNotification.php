<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorNotification extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_notifications';
    /*public function getUserDetail()
	{
		return $this->belongsTo('App\Model\SiteUser','user_id');
	}*/

	public function get_post_details()
	{
		return $this->belongsTo('App\Model\PatientPost','patient_post_id');
	}
}
