/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.components')
        .directive('pageFooter', pageFooter)
        .directive('pageFooterPromo', pageFooterPromo);

    /** @ngInject */
    function pageFooter() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/components/pageFooter/pageFooter.html',
            controller: 'pageFooterCtrl'
        };
    }

    function pageFooterPromo() {
        return {
            restrict: 'E',
            templateUrl: 'scripts/components/pageFooter/pageFooterPromo.html',
            controller: 'pageFooterCtrl'
        };
    }

})();
