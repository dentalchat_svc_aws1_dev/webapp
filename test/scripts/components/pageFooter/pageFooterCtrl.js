/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.components')
        .controller('pageFooterCtrl', pageFooterCtrl);

    /** @ngInject */
    function pageFooterCtrl($scope, $timeout,  $http, configService, $rootScope, $location, pageFooterService) {

        /*scope objects/variables initializing*/
        $scope.username = '';

        /*logout logic*/
        $scope.logout = function() {
            pageFooterService.logout(function(result) {
                if (result == 1) {
                    localStorage.removeItem("user_auth_token");
                    window.location.href = configService.getEnvConfig().frontApiURL;
                } else {
                    alert("cannot logout due to some problem");
                }
            });
        }

        /*user information loading*/
        $scope.load_user = function() {
            pageFooterService.load_user(function(result) {
                $scope.username = result;
            });
        }

        $scope.socailLink = function(link_id) {
            var data = {
                link_id:link_id            
            };
            
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/homesetting/social-media-link",data)
            .success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;                
                $scope.social_link  = data.home_link;
                console.log(data.home_link);
            })
            
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }

        /*home page link*/
        $scope.goToHomePage = function() {
            window.location.href = configService.getEnvConfig().frontApiURL;
        }
        
        /*start code by covetus 11-aug-2017 */
        $scope.showLink1 = $scope.showLink2 = false ;
        $scope.showHideLinks = function(id){
            if(id==2){
                $scope.showLink2 = !$scope.showLink2; 
                $scope.showLink1 = false;
            }
            if(id==1){
                 $scope.showLink1 = !$scope.showLink1;
                 $scope.showLink2 = false;
            }
        };
        /* end code by covetus */
    }
})();


// var data = $.param({
//     auth_token: localStorage.getItem("user_auth_token")
// });

// $http.post(configService.getEnvConfig().serverURL + 'logout', data)
//     .success(
//         function(data, status, headers, config) {
//             console.log(data);
//             if (data.status == 1) {
//                 //localStorage.setItem("admin_auth_token", "");
//                 localStorage.removeItem("user_auth_token");
//                 console.log("logged out successfully!");
//                 //console.log(configService.getEnvConfig().apiURL);
//                 window.location.href = configService.getEnvConfig().frontApiURL;
//             } else {
//                 console.log("cannot logged out");
//             }
//         }).error(
//         function(data, status, header, config) {
//             console.log(status);
//             // to prevent interaction outside of dialog
//         });
