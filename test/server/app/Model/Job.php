<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
	protected $guarded=[];
	public $timestamps = false;

    protected $table = 'jobs';
    public function users_details(){
    	return $this->belongsTo('App\Model\SiteUser', 'user_id','id');
    }
    public function category_details(){
    	return $this->belongsTo('App\Model\Category', 'category_id','id');
    }
    public function city_details(){
    	return $this->belongsTo('App\Model\City', 'city','id');

    }
    public function job_bid_count(){
        return $this->hasMany('App\Model\JobSiteUser', 'job_id','id')->selectRaw('job_id, count(job_id) as count')->groupBy('job_id');
        //return $this->hasMany('App\Model\JobSiteUser', 'job_id','id');
    }
    public function job_bid_details(){
        return $this->hasMany('App\Model\JobSiteUser', 'job_id','id');
    }
    public function review_details(){
        return $this->hasOne('App\Model\Review', 'job_id','id');
    }

}
