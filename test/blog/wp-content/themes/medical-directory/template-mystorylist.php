<?php
/**
 * Template Name: MyStoryList Template
 */

get_header();
global $current_user;
global $wpdb;
$story_id = $_GET['story_id'];
$count = 1;
$results_delete = $wpdb->get_results("DELETE FROM `wp_posts` WHERE `ID`=$story_id AND `post_author`='$current_user->ID'");
$results = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_type`='mystory' AND `post_author`='$current_user->ID'",OBJECT);
global $post; 
?>

   <div class="container">
      <div class="row">
        <?php   if( "yes" == $_GET['success'] ){ ?>
        <div class="alert alert-success alert-dismissable">
          <a href="<?php echo site_url(); ?>/story-list/" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> You have successfully posted Story.It will be reviewed by admin and publish soon.
        </div>
        <?php } ?>
        <div class="col-md-12">
            <h2>Story List</h2>
            <table class="table table-condensed">
            <thead>
                <tr>
                    <th>S. No.</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>
                     <th>Status</th>
                     <th>Delete</th>
                </tr>
            </thead>
            <tbody>
      <?php if(!empty($results)){ 
          foreach ($results as $post) {
             ?> 
              <?php setup_postdata($post); ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><a href="<?php the_permalink() ?>"><?php echo get_the_title(); ?></a></td>
                <td><?php echo wp_trim_words( get_the_content(), 20, '..' ); ?></td>
                <td><img src="<?php echo get_the_post_thumbnail_url(); ?>" height="50px;" width="50px;"/></td>
                <td><?php echo get_post_status();  ?></td>
                <td><a href="<?php echo site_url(); ?>/story-list/?story_id=<?php echo $post->ID; ?>" onclick="return ConfirmDelete();">Delete</a></td>
            </tr>     
            <?php $count ++; } ?>
            <?php } else{ echo "No Story"; }?>
            </tbody>
            </table>
      </div> 
      </div>
   </div>
   
   <script>
        function ConfirmDelete()
        {
         var x = confirm("Are you sure you want to delete?");
            if (x)
             return true;
            else
             return false;
        }
   </script>
<?php get_footer(); ?>