<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'patients';


}
