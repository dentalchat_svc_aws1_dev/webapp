<?php
/**
 * Template Name: MyStoryForm Template
 */

get_header();
//$story_id = $_GET['story_id'];
if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post") {
     $error ='';
     $title =  $_POST['title'];
     $description = $_POST['description'];
    if (empty($title)) {
        $error = 'Please enter the title.';
        $hasError = true;
    }elseif(empty($description)){
        
         $error = 'Please enter the content.';
         $hasError = true;
    }
    /*elseif(empty( $_FILES['profile_image']["name"])){ 
         
         $error = 'Please upload Audio/Video/Image';
         $hasError = true;
    }elseif(empty( $_FILES['thumbnail_image']["name"])){ 
        
         $error = 'Please select the featured image.';
         $hasError = true;
    }*/
    else{
     
    if (!function_exists('wp_generate_attachment_metadata')){
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');
    }
  	$thumbnail_image_attach_id = media_handle_upload( 'thumbnail_image', $new_post );
	$audio_video_image_attach_id = media_handle_upload( 'profile_image', $new_post );
	
	if ( $thumbnail_image_attach_id > 0 && $_FILES['thumbnail_image']['type'] =='image/jpeg'  || $_FILES['thumbnail_image']['type'] =='image/jpg' || $_FILES['thumbnail_image']['type'] =='image/png' || $_FILES['thumbnail_image']['type'] =='image/gif' ) {
		update_post_meta($new_post,'_thumbnail_id',$thumbnail_image_attach_id);
	}
    $post_cat = $_POST['cat'];
    $new_post = array(
        'post_title'    =>  $title,
        'post_content'  =>  $description,
        'post_category' =>   $post_cat,
        'tags_input'    =>  array($tags),
        'post_status'   =>  'draft',              // Choose: publish, preview, future, draft, etc.
        'post_type'     =>  'mystory',               //'post',page' or use a custom post type if you want to
        'taxonomy'      => 'mystory-categories'  
    );
     //SAVE THE POST
    $pid = wp_insert_post($new_post);
    wp_set_post_terms( $pid, $_POST['cat'], 'mystory-categories', false );
     if(!empty($pid) && $thumbnail_image_attach_id > 0){
      set_post_thumbnail(  $pid, $thumbnail_image_attach_id );
     }
    if($audio_video_image_attach_id > 0 && $_FILES['profile_image']['type'] =='video/mp4' || $_FILES['profile_image']['type'] =='audio/mp3' || $_FILES['profile_image']['type'] =='audio/mpeg'){ 
       $video_audio_url =  wp_get_attachment_url( $audio_video_image_attach_id );
       $file_type_for_check = $_FILES['profile_image']['type'];
       update_post_meta($pid,'file_type_for_check',$file_type_for_check);
       update_post_meta($pid,'video_audio_url',$video_audio_url);
    }
    
    }
    if(!$hasError) {
      wp_redirect(site_url().'/story-list/?success=yes');
    exit;

        
    } 

}

//POST THE POST YO
do_action('wp_insert_post', 'wp_insert_post');
?>
<style>fieldset{
margin-top:15px;
}
#description{
    height:200px;
}
.mce-widget.mce-notification.mce-notification-error.mce-has-close.mce-in{
    display:none;
}
</style>
<div class="blog-content pt60">
   <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-push-2 mystory_form_main">
            <?php if(!empty($error)){ ?>
          <div class="alert alert-danger">
  <strong>Required !</strong> <?php echo $error; ?>
</div> 
<?php } ?>
     <?php if(is_user_logged_in() && empty($story_id)){ ?>
    <form id="new_post" name="new_post" method="post" action="" class="wpcf7-form" enctype="multipart/form-data">
    <div class="row">
    <div class="col-md-12">
    <!-- post name -->
    <fieldset name="name">
        <label for="title">Title:</label>
        <input type="text" id="title" value="<?php if(isset($_POST['title'])){ echo $_POST['title']; }else{ echo ""; } ?>" tabindex="5" name="title" />
    </fieldset>
    <fieldset class="content">
        <label for="description">Description:</label>
        <?php 
    if(isset($_POST['description'])){ echo $content = $_POST['description']; }else{ echo $content =''; } 
        
        $editor_id = 'description';
        $settings =   array(
            'wpautop' => true, // use wpautop?
            'media_buttons' => true, // show insert/upload button(s)
            'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
            'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
            'tabindex' => '',
            'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
            'editor_class' => '', // add extra class(es) to the editor textarea
            'teeny' => false, // output the minimal editor config used in Press This
            'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
            'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
            'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        );    
        wp_editor( $content, $editor_id, $settings = array() ); ?> 
    </fieldset>
   
    <fieldset class="profile_image">
    <label for="image">Upload Audio/Video/Image:(jpeg,jpg,png,gif,mp4,mp3)</label>
       <input type="file" name="profile_image" id="profile_image" multiple="false">
       <input type="hidden" name="profile_image_id" id="profile_image_id" value="" />
        <div id="profile-image-show"></div>
    </fieldset>
     <!-- post Content -->
    <fieldset class="thumbnail_image">
        <label for="image">Featured Image:(jpeg,jpg,png,gif)</label>
           <input type="file" name="thumbnail_image" id="thumbnail_image" multiple="false">
           <input type="hidden" name="thumbnail_image_id" id="thumbnail_image_id" value="" />
            <div id="thumbnail-image-show"></div>
    </fieldset>
     
    </div>
    </div><div class="row">
    <div class="col-md-12">
    <fieldset class="submit">
        <input type="submit" value="Post Story" tabindex="40" id="submit" name="submit" class="btn btn-info post_form_submit_btn"/>
    </fieldset>
    </div>
     
    </div>
    <input type="hidden" name="action" value="new_post" />
    <?php wp_nonce_field( 'new-post' ); ?>
</form>
<?php } else { wp_redirect('https://test.dentalchat.com/patient-login-story'); } ?>
        <div class="post_my_story">
            <!-- <img src="https://test.dentalchat.com/blog/wp-content/uploads/2017/07/post_story.png" />-->
             <a href="<?php echo site_url(); ?>/story-list/" class="btn btn-primary storylist_btn">Story List</a>
             
        </div>
      </div> 
        
      </div>
   </div>
</div>

<?php get_footer(); ?>