/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.promo', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {       

			$stateProvider.state('promopage', {
						url: '/promo',
						templateUrl: 'scripts/pages/promo/promo.html',
						controller: 'PromoPageCtrl',
						})
		    .state('specialpromo', {
						url: '/special-promo',
						templateUrl: 'scripts/pages/promo/specialpromo.html',
						controller: 'PromoPageCtrl',
						})
		   .state('promotion', {
						url: '/promotion',
						templateUrl: 'scripts/pages/promo/promotion.html',
						controller: 'PromoPageCtrl',
						})
		   .state('pro', {
						url: '/pro',
						templateUrl: 'scripts/pages/promo/pro.html',
						controller: 'PromoPageCtrl',
						})
			
			
    }
})();
