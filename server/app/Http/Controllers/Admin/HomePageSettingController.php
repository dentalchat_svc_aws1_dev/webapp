<?php namespace App\Http\Controllers\Admin;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Model\Country; /* Model name*/
use App\Model\Category; /* Model name*/
use App\Model\Sitesetting; /* Model name*/
use App\Model\BusinessList; /* Model name*/
use App\Model\Testimonial; /* Model name*/
use App\Model\HomePage; /* Model name*/

use App\Book;
use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image; // Use this if you want facade style code
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;
use Cookie;

class HomePageSettingController extends BaseController {

  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
    	parent::__construct();
		
    }
    
	/**************************************************************/
	/*                       BANNER MANAGEMENT                    */
	/**************************************************************/
	public function homePageSetting($id)
	{
		$module_head 		= "Edit HomePage Setting";
        $banner_class 		= "active";
        $title				= "HomePage Setting";
		
		$setting_arr = HomePage::where("id",$id)->first();
		//echo '<pre>';print_r($setting_arr);exit();
		if(count($setting_arr) == 0)
		{
			Session::flash('failure_message', 'This banner does not exists'); 
			Session::flash('alert-class', 'alert alert-error'); 
			return redirect('admin/edit-homepage/'.$id);
		}
		
		return view('admin.homepagesetting.edit_homesetting',compact(
														'module_head',
														'banner_class',
														'title',
														'setting_arr'
													)
					);
	}
	public function updatehomePageSetting()
	{
		if(Request::isMethod('post'))
		{
			$data = Request::all();
			$filename = "";
						
			if(!empty($data['banner_image']))
			{
				$image = Input::file('banner_image');
				$filename  = time() . '.' . $image->getClientOriginalExtension(); 
				$big_image_path = 'uploads/banner_image/big/' . $filename;
				Image::make($image->getRealPath())->save($big_image_path);
				
				/*$thumb_image_path = 'uploads/banner_image/thumb/' . $filename;
				Image::make($image->getRealPath())->resize(200, 80)->save($thumb_image_path);*/
			}

			$setting 		 	= HomePage::find($data['id']);

			if($filename!="")
			{
				/********** delete old image from folder ************/
				@unlink("uploads/banner_image/thumb/".$banner_arr->banner_image);
				//@unlink("uploads/banner_image/big/".$banner_arr->banner_image);
				
				/********** delete old image from folder ************/
				$setting->banner	= $filename;
			}
			
			 $setting->banner_text 	= $data['banner_text'];
			$setting->how_work_title 	= $data['how_work_title'];
			$setting->how_it_work_text 	= $data['how_it_work_text'];
			$setting->how_work_1_title  = $data['how_work_1_title'];
			$setting->how_work_1_text  = $data['how_work_1_text'];
			$setting->how_work_2_title  = $data['how_work_2_title'];
			$setting->how_work_2_text  = $data['how_work_2_text'];
			$setting->how_work_3_title  = $data['how_work_3_title'];
			$setting->how_work_3_text  = $data['how_work_3_text'];
			$setting->testimonial_title  = $data['testimonial_title'];
			$setting->save();

			Session::flash('success_message', 'Banner updated successfully'); 
			Session::flash('alert-class', 'alert alert-success'); 
			return redirect('admin/edit-homepage/'.$data['id']);
        }
	}
}
