(function() {
    'use strict';
    angular.module('doctorchat.pages.patient').factory('chatSocket', function(socketFactory) {
        var myIoSocket = io.connect('https://dentalchat.com:8005/');
        var socket = socketFactory({
            ioSocket: myIoSocket
        });
        return socket;
    }).value('messageFormatter', function(date, nick, message) {
        return date.toLocaleTimeString() + ' - ' + nick + ' - ' + message + '\n';
    }).value('chatImageApiPath', 'https://dentalchat.com:8005/').value('chatImagePath', 'https://dentalchat.com:8005/').directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    }).directive('starRating', function() {
        return {
            restrict: 'EA',
            template: '<ul class="star-rating" ng-class="{readonly: readonly}">' + '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' + '    <i class="fa fa-star"></i>' + '  </li>' + '</ul>',
            scope: {
                ratingValue: '=ngModel',
                max: '=?',
                onRatingSelect: '&?',
                readonly: '=?'
            },
            link: function(scope, element, attributes) {
                if (scope.max == undefined) {
                    scope.max = 5;
                }

                function updateStars() {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }
                };
                scope.toggle = function(index) {
                    if (scope.readonly == undefined || scope.readonly === false) {
                        scope.ratingValue = index + 1;
                        scope.onRatingSelect({
                            rating: index + 1
                        });
                    }
                };
                scope.$watch('ratingValue', function(oldValue, newValue) {
                    if (newValue) {
                        updateStars();
                    }
                });
            }
        };
    }).controller('patientPageCtrl', patientPageCtrl);

/* start code by covetus 4-aug-2017 */
/* Inject two more dependency here [$localStorage]*/
/* end code by covetus 4-aug-2017 */

    function patientPageCtrl($scope, $timeout, $window, $http, configService, $rootScope, $location, homeService, $stateParams, $state, $log, $anchorScroll, SweetAlert,
    POSTCONFIG, chatSocket, chatImageApiPath, chatImagePath, vcRecaptchaService, ngMeta,$localStorage) {
        var currentState = $state.current.name;
        //console.log($state.params.act);
        //console.log($stateparams);
        $scope.minDate = new Date().toDateString();
        $scope.title = $stateParams.title;
        $scope.resPostId =  $state.params.postId;
        $scope.autocompleteOptions = {}
        $scope.apiUrl = configService.getEnvConfig().apiURL;
        $rootScope.base_url = configService.getEnvConfig().apiURL;
        $scope.showLoader = 0;
        $window.scrollTo(0, 0);
        $scope.dentist = {};
        $rootScope.obj = {};
        $scope.expiredlink = 0;
        $rootScope.showSuccessMsg = 0;
        $rootScope.showErrMsg = 0;
        $rootScope.showErrMsgFrgt = 0;
        $rootScope.show_message = '';
        $scope.dentist = {};
        $scope.showimgprev = 0;
        $scope.show_message_old_password = '';
        $scope.show_color = "";
        $scope.showSuccMsg = 0;
        $scope.old_password_class = '';
        $scope.dentistLanguage = {};
        $scope.allLanguage = [];
        $scope.dentistSkill = {};
        $scope.docsMore = [];
        $scope.docfiles = [];
        $scope.docsExp = [];
        $scope.docexpfiles = [];
        $scope.docsEdu = [];
        $scope.docedufiles = [];
        $scope.dentistAwards = {};
        $scope.dentistInsurance = {};
        $scope.currentDate = new Date().toString();
        $rootScope.logged_user_name = (localStorage.getItem('patientName')) ? localStorage.getItem('patientName') : $rootScope.logged_user_name;
        $rootScope.loginType = (localStorage.getItem('loginType')) ? localStorage.getItem('loginType') : $rootScope.loginType;
        $rootScope.user_profile_pics = (localStorage.getItem('patientImage')) ? localStorage.getItem('patientImage') : $rootScope.user_profile_pics;
        $scope.searchSelectAllSettings = {
            enableSearch: true,
            showSelectAll: true,
            keyboardControls: true,
            smartButtonMaxItems: 5,
            smartButtonTextConverter: function(itemText, originalItem) {
                return itemText;
            }
        };
        $scope.rating ={};
        $scope.rating.rate = [{current: 0}];

        $scope.cappatient = {};
        $scope.cappatient.captchavar = 0;
        $scope.helloW = function(){
            $scope.cappatient.captchavar = 1;
        }
        $scope.resetSearch = function(){
            $scope.searchKeyword = '';
        }
        
        $scope.patientSignup = function(patient) {
            
            $rootScope.showLoader = 1;
            $scope.succ_msg = '';
            var website = 0;
            var amount = 0;
            patient['password'] = patient.cpassword;
            var data = {
                patient: patient,
                g_recaptcha_response: vcRecaptchaService.getResponse()
            };
            
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "patient-registration", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                /*console.log(data);
                    return false;*/
                if (data.status == 2) {
                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = 'Email already exists.';
                    $rootScope.showSuccMsg = 1;
                } else if (data.status == 3) {
                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = 'Captcha mismatched';
                    $rootScope.showSuccMsg = 1;
                } else if (data.status == 4) {
                    $rootScope.post_id ="";
                    /*console.log(data);
                    return false;*/
                    $rootScope.is_front_logged = 2;
                    localStorage.setItem("auth_token", data.auth_token);
                    localStorage.setItem("patient_id", data.patient_id);
                    localStorage.setItem("patient_id_raw", data.patient_id_raw);
                    localStorage.setItem("patient_name", data.patient_name);
                    localStorage.setItem('patientImage', data.profile_pic);
                    $rootScope.logged_user_name = data.patient_name;
                    localStorage.setItem('patientName', data.patient_name);
                    $rootScope.hasPost = data.is_any_post;
                    localStorage.setItem('loginType', data.login_type);
                    $rootScope.loginType = data.login_type;
                    $rootScope.user_profile_pics = data.profile_pic;
                    
                    SweetAlert.swal("Your request is submitted.", "Dentist(s) will be responding to you shortly.  Please click on Messages tab to see  responses.", "success");
                    
                    $state.go('my-post', {});
                } else {
                    $rootScope.reg_success_msg = "Thank you for registering with us. Please wait while we are logging you in.";
                    localStorage.setItem('loginEmail', data.useremail);
                    localStorage.setItem('loginPass', data.password);
                    $location.path('/patient-registration-success');
                }
            }).error(function(data, status, header, config) {});
        }
        $scope.activatedPatient = function() {
            var base64email = $stateParams.parameter;
            var data = {
                email: atob(base64email)
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-activation", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 1) {
                    $rootScope.showLoader = 0;
                    $rootScope.showErrMsgVar = 1;
                    $rootScope.show_class = "alert alert-success";
                    $rootScope.show_messagevar = 'Your acoount is activated.Please try to login with your valid credentials.';
                    $location.path('/patient-login');
                } else if (data.status == 2) {
                    $rootScope.showErrMsgVar = 1;
                    $rootScope.show_class = "alert alert-danger";
                    $rootScope.show_messagevar = 'Your account is already activated.';
                    $location.path('/patient-login');
                } else {
                    $rootScope.showErrMsgVar = 1;
                    $rootScope.show_class = "alert alert-danger";
                    $rootScope.show_messagevar = 'Some problem occured.';
                    $location.path('/patient-login');
                }
            }).error(function(data, status, header, config) {
                //console.log(data);
            });
        }
        var currentState = $state.current.name;
        if (currentState == 'patient-registration-success') {
            var patientLogin = {
                email: localStorage.getItem('loginEmail'),
                password: localStorage.getItem('loginPass')
            }
            $timeout(function() {
                $scope.patientLogin(patientLogin);
            }, 1);
        }
        if (currentState == 'activatepatient') $scope.activatedPatient();
        $scope.patientLogin = function(patient) {
            $rootScope.show_my_message = '';
            $rootScope.showSuccessMsg = 0;
            
            var data = {
                email: patient.email,
                password: patient.password,
                patient_id: patient.patient_id,
                redirecttostory: patient.story
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "patient-login", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                
                if (data.status == 0) {
                    $rootScope.show_message = 'Wrong email id or password.';
                    $rootScope.showErrMsg = 1;
                }else if (data.status == 2) {
                    $rootScope.show_message = data.msg;
                    $rootScope.showErrMsg = 1;
                } else if (data.status == 1) {
                    $rootScope.is_front_logged = 2;
                    localStorage.setItem("auth_token", data.auth_token);
                    localStorage.setItem("patient_id", data.patient_id);
                    localStorage.setItem("patient_id_raw", data.patient_id_raw);
                    localStorage.setItem("patient_name", data.patient_name);
                    localStorage.setItem('patientImage', data.profile_pic);
                    $rootScope.logged_user_name = data.patient_name;
                    localStorage.setItem('patientName', data.patient_name);
                    $rootScope.hasPost = data.is_any_post;
                    localStorage.setItem('loginType', data.login_type);
                    $rootScope.loginType = data.login_type;
                    $rootScope.user_profile_pics = data.profile_pic;
                   // if(typeof $localStorage.getStartedData != 'undefined'){
                      //  $state.go('my-post', {});
                  //  }
                    //else{
                    if(data.msg==1){
                        SweetAlert.swal("Your request is submitted.", "Dentist(s) will be responding to you shortly.  Please click on Messages tab to see  responses.", "success");
                        $rootScope.post_id ="";
                        $('#myModal').modal('hide');
                    }
                    
                    if(data.redirect_story){
                   
        window.location =  "https://test.dentalchat.com/blog/my-story-form/";
                   
                     
                    }else{
                        if (data.is_any_post > 0) {
                            $state.go('my-post', {});
                        } else {
                            $state.go('create-post', {});
                        }
                    }
                    //}
                } else {
                    $rootScope.show_message = 'Something Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        };
               /* $scope.patientLoginStory = function(patient) {
            $rootScope.show_my_message = '';
            $rootScope.showSuccessMsg = 0;
            var data = {
                email: patient.email,
                password: patient.password
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "patient-login-story", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $rootScope.show_message = 'Wrong email id or password.';
                    $rootScope.showErrMsg = 1;
                }else if (data.status == 2) {
                    $rootScope.show_message = data.msg;
                    $rootScope.showErrMsg = 1;
                } else if (data.status == 1) {
                    $rootScope.is_front_logged = 2;
                    localStorage.setItem("auth_token", data.auth_token);
                    localStorage.setItem("patient_id", data.patient_id);
                    localStorage.setItem("patient_id_raw", data.patient_id_raw);
                    localStorage.setItem("patient_name", data.patient_name);
                    localStorage.setItem('patientImage', data.profile_pic);
                    $rootScope.logged_user_name = data.patient_name;
                    localStorage.setItem('patientName', data.patient_name);
                    $rootScope.hasPost = data.is_any_post;
                    localStorage.setItem('loginType', data.login_type);
                    $rootScope.loginType = data.login_type;
                    $rootScope.user_profile_pics = data.profile_pic;
          
                        if (data.is_any_post > 0) {
                            $state.go('my-post', {});
                        } else {
                            $state.go('create-post', {});
                        }
                
                } else {
                    $rootScope.show_message = 'Something Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }; */
        
         $scope.addContact = function(user)
        {
            console.log(user);
            var data = {
                    user:user
                      };
            var token = localStorage.getItem("access_token");

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "add-contact",data,config)
            .success(function(data, status, headers, config) {
               // $rootScope.showLoader = 0;
               
               if (data.status == 2) {
                     $rootScope.showLoader = 0;
                    $scope.emailexitmsg = 'Email already exists.';
                    //contactForm.reset();
                    $scope.showalert = 0;
                   /* $timeout(function(){
                        $scope.showalert = 1;
                        $scope.emailexitmsg = '';
                   },5000);*/
                 // $scope.user = {};
                 exit();
               }
               
              
               /* else if(data.status == 1){
                    
                    $rootScope.showLoader = 0;
                    $scope.showquestionmsg = 'Your message has been successfully sent.We will contact you soon.';
                    
                     /* added on 2-2-18 */
                    //  window.location =  "https://test.dentalchat.com/registration-success";
                    /* close here */
                    
                   /* contactForm.reset();
                    $scope.showalert = 0;
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },5000);
                  $scope.user = {};*/
                 
                
                   /*$scope.user = {};
                   localStorage.setItem("thankmessage", "Your message has been successfully sent.We will contact you soon.");
                    $location.path('/thankyou');
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },2000); */
             //   }*/
                
                else if (data.status == 3) {
                    $scope.change_color = "color:red";
                    $scope.show_message = 'Captcha mismatched';
                    $scope.showSuccMsg = 1;
                    
                }
                
                else {
                    $rootScope.reg_success_msg = ["Thank you for registering your Dental Practice with us.","A verification link has been sent to email id  " + data.useremail + ""];
                    $location.path('/registration-success');
                }
            })
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        
         $scope.registerexclusive = function(user)
        {
            console.log(user);
            var data = {
                    user:user
                      };
            var token = localStorage.getItem("access_token");

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "add-exclusive",data,config)
            .success(function(data, status, headers, config) {
               // $rootScope.showLoader = 0;
               
                if (data.status == 2) {
                     $rootScope.showLoader = 0;
                    $scope.emailexitmsg = 'Email already exists.';
                    //contactForm.reset();
                    $scope.showalert = 0;
                   /* $timeout(function(){
                        $scope.showalert = 1;
                        $scope.emailexitmsg = '';
                   },5000);*/
                 // $scope.user = {};
                 exit();
               }
               
              /*  if(data.status == 1){
                    
                    $rootScope.showLoader = 0;
                    $scope.showquestionmsg = 'Your message has been successfully sent.We will contact you soon.';
                     /* added on 2-2-18 */
                   //   window.location =  "https://test.dentalchat.com/registration-success";
                    /* close here */
                   /* regForm.reset();
                    $scope.showalert = 0;
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },5000);
                  $scope.user = {};*/
                 
                
                   /*$scope.user = {};
                   localStorage.setItem("thankmessage", "Your message has been successfully sent.We will contact you soon.");
                    $location.path('/thankyou');
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },2000); */
               // }*/
                else if (data.status == 3) {
                    $scope.change_color = "color:red";
                    $scope.show_message = 'Captcha mismatched';
                    $scope.showSuccMsg = 1;
                    
                }
                
                 else {
                    $rootScope.reg_success_msg = ["Thank you for registering your Dental Practice with us.","A verification link has been sent to email id  " + data.useremail + ""];
                    $location.path('/registration-success');
                }
            })
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        
         $scope.registeridealistic = function(user)
        {
            console.log(user);
            var data = {
                    user:user
                      };
            var token = localStorage.getItem("access_token");

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "add-idealistic",data,config)
            .success(function(data, status, headers, config) {
               // $rootScope.showLoader = 0;
                if (data.status == 2) {
                     $rootScope.showLoader = 0;
                    $scope.emailexitmsg = 'Email already exists.';
                    //contactForm.reset();
                    $scope.showalert = 0;
                   /* $timeout(function(){
                        $scope.showalert = 1;
                        $scope.emailexitmsg = '';
                   },5000);*/
                 // $scope.user = {};
                 exit();
               }
               
                /*if(data.status == 1){
                    
                    $rootScope.showLoader = 0;
                    $scope.showquestionmsg = 'Your message has been successfully sent.We will contact you soon.';
                     /* added on 2-2-18 */
                      //window.location =  "https://test.dentalchat.com/registration-success";*/
                    /* close here */
                    
                   /* regForm.reset();
                    $scope.showalert = 0;
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },5000);
                  $scope.user = {};*/
                 
                
                   /*$scope.user = {};
                   localStorage.setItem("thankmessage", "Your message has been successfully sent.We will contact you soon.");
                    $location.path('/thankyou');
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },2000); */
             //   }
                else if (data.status == 3) {
                    $scope.change_color = "color:red";
                    $scope.show_message = 'Captcha mismatched';
                    $scope.showSuccMsg = 1;
                    
                }
                
                 else {
                    $rootScope.reg_success_msg = ["Thank you for registering your Dental Practice with us.","A verification link has been sent to email id  " + data.useremail + ""];
                    $location.path('/registration-success');
                }
            })
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        
         $scope.registerposh = function(user)
        {
            console.log(user);
            var data = {
                    user:user
                      };
            var token = localStorage.getItem("access_token");

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "add-posh",data,config)
            .success(function(data, status, headers, config) {
               // $rootScope.showLoader = 0;
               
                if (data.status == 2) {
                     $rootScope.showLoader = 0;
                    $scope.emailexitmsg = 'Email already exists.';
                    //contactForm.reset();
                    $scope.showalert = 0;
                   /* $timeout(function(){
                        $scope.showalert = 1;
                        $scope.emailexitmsg = '';
                   },5000);*/
                 // $scope.user = {};
                 exit();
               }
               
               
               /* if(data.status == 1){
                    
                    $rootScope.showLoader = 0;
                    $scope.showquestionmsg = 'Your message has been successfully sent.We will contact you soon.';
                    
                     /* added on 2-2-18 */
                      //window.location =  "https://test.dentalchat.com/registration-success";*/
                    /* close here */
                    
                    
                    
                   // regForm.reset();
                    /* only this part cmd $scope.user.first_name = "";
                    $scope.showalert = 0;
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },5000); */
                //  $scope.user = {};
                 
                
                   /*$scope.user = {};
                   localStorage.setItem("thankmessage", "Your message has been successfully sent.We will contact you soon.");
                    $location.path('/thankyou');
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },2000); */
             //   }
                else if (data.status == 3) {
                    $scope.change_color = "color:red";
                    $scope.show_message = 'Captcha mismatched';
                    $scope.showSuccMsg = 1;
                    
                }
                
                 else {
                    $rootScope.reg_success_msg = ["Thank you for registering your Dental Practice with us.","A verification link has been sent to email id  " + data.useremail + ""];
                    $location.path('/registration-success');
                }
            })
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        
        $scope.patientImage = localStorage.getItem('patientImage');
        $scope.ForgotPassword = function(user) {

            $scope.showErrMsg = 0;
            var data = {
                email: user.email,
                user_type: user.user_type
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-forgot-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $scope.show_class = "iferror_mess alert alert-danger";
                    $scope.show_message = 'Email Id does not exist.';
                    $scope.showErrMsg = 1;
                } else if (data.status == 2) {
                    $scope.show_class = "iferror_mess alert alert-danger";
                    $scope.show_message = 'You are unable to retrieve your password as your account is inactive.';
                    $scope.showErrMsg = 1;
                } else {
                    $scope.showErrMsg = 1;
                    $scope.show_class = "ifsucc_mess alert alert-success";
                    $scope.show_message = 'Change password link is sent to your email.';
                    
                     $timeout(function() {
                        $location.path('/patient-login');
                    }, 45000);
                    $scope.forgotForm.reset();
                 }

             

            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        };
        $scope.checkPatientValidUrl = function() {
            var url = $location.url();
            var param = url.split('/').pop();
            var data = {
                param: param
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-check-link", data, config).success(function(data, status, headers, config) {
                //console.log(data);
                if (data.status == 0) {
                    $scope.show_class = "alert alert-danger";
                    $scope.show_message = 'This link is expired';
                    $scope.showErrMsg = 1;
                    $scope.expiredlink = 1;
                } else {
                    $scope.user.email = data.data.email;
                }
            }).error(function(data, status, header, config) {
               // console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.PatientResetPassword = function(patient) {
            var base64email = $stateParams.parameter;
            var data = {
                password: patient.password,
                param: base64email
            };
            //console.log(patient);
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-change-forgot-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $scope.show_class = "ifsucc_mess alert alert-success";
                $scope.show_message = 'Your Password has been changed. Please logged in with new password.';

                $timeout(function() {
                        $location.path('/patient-login');
                    }, 1000);
                   // $scope.forgotForm.reset();

                $scope.showErrMsg = 1;
            }).error(function(data, status, header, config) {
                //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        };

        
        $scope.changePatientPassword = function(dentist) {
            var data = {
                dentist: dentist
            };
            var token = localStorage.getItem("auth_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/patient/change-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                
                if (data.status == 0) {
                    $scope.show_color = "color:red";
                    $scope.show_message_old_password = 'Invalid old password..';
                    $scope.old_password_class = 'has-error';
                } else {
                    $scope.change_class = "alert alert-success";
                    $scope.show_message = 'Password has been updated successfully.';
                    $scope.showSuccMsg = 1;
                    $window.scrollTo(0, 0);
                }
            }).error(function(data, status, header, config) {
               // console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }

        $scope.patientEmailCheck = function(test) {
        
         var fieldval = document.getElementById("patient_email").value;
         if(fieldval != '')
         {
           var data = {
                email: fieldval,
             };

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
     
            $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-email-check", data, config).success(function(data, status, headers, config) {
             if (data.status == 2) {

                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = 'Email already exists.';
                    $rootScope.show_email_validation = 1;
                    $rootScope.showSuccMsg = 1;  
                } else{

                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = '';
                    $rootScope.show_email_validation = 0;
                    $rootScope.showSuccMsg = 0;
                }

            }).error(function(data, status, header, config) {});

         }
         

      }




        $scope.imageUploadPatient = function(files) {
            $scope.patient.patient_image_data = files[0];
        }
        $scope.imageUploadClinic = function(files) {
            $scope.dentistClient.clinic_picture_data = files[0];
        }
        $scope.showImage = function() {
            $scope.showimgprev = 1;
        }
        $scope.delete_image = function() {
            $scope.showimgprev = 0;
        }
        $scope.getUnreadChatCount = function() {
            var data = {
                'auth_token': token,
                'patient_id': localStorage.getItem('patient_id_raw'),
            }
            $http({
                method: 'post',
                dataType: "jsonp",
                crossDomain: true,
                url: configService.getEnvConfig().apiURL + "unread-chat-count-for-patient",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(response, status, headers, config) {
                if (response.status == 1) {
                    $scope.unreadChatCounter = response.unread_history_count;
                }
            }).error(function(response) {});
        }
        $scope.checkEmail = function(email) {
            var patt = /^$|^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;
            var res = patt.test(email);
            if (res) {
                return 1;
            } else {
                if (typeof email == 'undefined') {
                    return 1;
                }
                return 0;
            }
        };
        $scope.getPatientProfileData = function() {
            $rootScope.showLoader = 1;
            var token = localStorage.getItem("auth_token");
            var data = {};
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/patient/edit-profile", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {} else {
                    $scope.patient = data.patient_details;
                   // console.log($scope.patient);
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.updatePatientProfile = function(patient) {
            var fd = new FormData();
            for (var key in $scope.patient) {
                if (typeof($scope.patient[key]) == "object") {
                    fd.append(key, JSON.stringify($scope.patient[key]));
                } else {
                    fd.append(key, $scope.patient[key]);
                }
            }
            fd.append('profile_pics', $scope.patient.patient_image_data);
            fd.append('profile_pics_exists', $scope.showimgprev);
           // console.log(fd);
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/patient/update-profile", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 1) {
                    $rootScope.showLoader = 0;
                    $scope.showErrMsg = 1;
                    $scope.show_class = "ifsucc_mess alert alert-success";
                    $scope.show_message = "Profile Updated Successfully";
                    $window.scrollTo(0, 0);
                    localStorage.setItem('patientImage', data.profile_pic);
                    $rootScope.logged_user_name = data.patient_name;
                    localStorage.setItem('patientName', data.patient_name);
                    $rootScope.user_profile_pics = data.profile_pic;
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message_email = 'Email already exists..';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        if (currentState == 'patientProfile') {
            $scope.getPatientProfileData();
            $scope.getUnreadChatCount();
        }
        if (currentState == 'my-post') {
            $scope.postList = 1
        }
        if (currentState == 'my-message') {
            $scope.postList = 0
        }
        if ($rootScope.curr_state == 'post-details') {
            var post_ids = $stateParams.postId;
            $scope.postAttachmentPath = configService.getEnvConfig().apiURL;
            var data = {
                'post_id': post_ids,
                'auth_token': token
            }
            $http({
                method: 'post',
                dataType: "jsonp",
                crossDomain: true,
                url: configService.getEnvConfig().apiURL + "get-patient-post-details",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(response, status, headers, config) {
                if (response.status == 1) {
                    $scope.postDetails = response.patient_post_detls;
                } else {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                }
            }).error(function(response) {
                SweetAlert.swal("Oops!", "Please try again", "error");
            });
        }
        if ($rootScope.curr_state == 'create-post' || $rootScope.curr_state == 'my-post' || $rootScope.curr_state == 'my-message') {
            var token = localStorage.getItem("auth_token");
            var patient_id = localStorage.getItem("patient_id_raw");
            $scope.currentPage = 1;
            $scope.limit = 50;
            /* start code by covetus 3-aug-2017 */
            /* for set model to title and description field from localStorage in Get Started*/
            $scope.post_title = "I'm looking for an ";
            if(typeof $localStorage.getStartedData != 'undefined'){
                if(typeof $localStorage.getStartedData.title != 'undefined'){
                    $scope.post_title += $localStorage.getStartedData.title;
                }
                $scope.post_title += " for ";
                if(typeof $localStorage.getStartedData.description != 'undefined'){
                    $scope.post_title += $localStorage.getStartedData.description;
                }
            }
            /* end code by covetus 3-aug-2017 */
            $scope.postAttachmentPath = configService.getEnvConfig().apiURL;
            
            $scope.savePost = function(patientDetails) {
                $rootScope.showLoader = 1;
                //console.log(patientDetails);
                var formData = new FormData();
                formData.append('patient_id', patient_id);
                formData.append('auth_token', token);

               formData.append('lata', patientDetails.lat);
               formData.append('langa', patientDetails.lang);
                angular.forEach($scope.docedufiles, function(value, key) {
                    formData.append('attachments[]', value);
                });
                
                angular.forEach(patientDetails, function(value, key) {
                    formData.append(key, value);
                });
                formData.append('address', patientDetails.address.formatted_address);
                //console.log(formData);
                $http({
                    method: 'post',
                    url: configService.getEnvConfig().apiURL + "create-patient-post",
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formData,
                    transformRequest: angular.identity
                }).success(function(response, status, headers, config) {
                   // console.log(response.status);
                    if (response.status == 1) {
                        /* start code by covetus 3-aug-2017 */
                        /* To reset Local Storage empty */
                        $localStorage.getStartedData = {};
                        /* end code by covetus 3-aug-2017 */
                        $rootScope.showLoader = 0;
                        SweetAlert.swal("Your request is submitted.", "Dentist(s) will be responding to you shortly.  Please click on Messages tab to see  responses.", "success");
                         $location.path('/patient-profile/my-post');
                     }else{
                         $localStorage.getStartedData = {};
                        /* end code by covetus 3-aug-2017 */
                            $rootScope.post_id = response.post_id;
                            $rootScope.showLoader = 0;
                            /*console.log(response.post_id);
                            return false;*/
                            $location.path('/patient-registration');
                         }
                }).error(function(response) {
                    $scope.show_message = 'Something is Wrong';
                });
            }

            $scope.postList = [];
            $scope.fetchMyPost = function() {
               //console.log('=====fetchMyPost=======');
                $rootScope.showLoader = 1;
                $scope.offset = ($scope.currentPage - 1) * $scope.limit;
                var data = {
                    'patient_id': patient_id,
                    'auth_token': token,
                    'post_limit': $scope.limit,
                    'post_offset': $scope.offset
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "list-patient-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.totalItems = response.post_list_count;
                        $scope.postList = response.post_list;
                        
                    } else {
                        alert('There is some problem');
                    }
                }).error(function(response) {
                    alert('There is some problem');
                });
            }
            $scope.setPage = function(pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.pageChanged = function() {
                $scope.fetchMyPost();
            }
            $scope.fetchMyPost();
            $scope.activeInactive = function(activeVal, pid) {
                var data = {
                    'post_id': pid,
                    status: activeVal
                }
               // console.log(data);
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "change-patient-post-status",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        SweetAlert.swal("Great!", "Status successfully updated", "success");
                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.deletePost = function(pid, index) {
                SweetAlert.swal({
                    title: "Are you sure?",
                    text: "Your will loose the post and related chats!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        $rootScope.showLoader = 1;
                        var data = {
                            'patient_id': patient_id,
                            'auth_token': token,
                            'post_id': pid
                        }
                        $http({
                            method: 'post',
                            dataType: "jsonp",
                            crossDomain: true,
                            url: configService.getEnvConfig().apiURL + "delete-patient-post",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: $.param(data)
                        }).success(function(response, status, headers, config) {
                            if (response.status == 1) {
                                $rootScope.showLoader = 0;
                                SweetAlert.swal("Great!", "Post successfully deleted", "success");
                                $scope.postList.splice(index, 1);
                            } else {
                                SweetAlert.swal("Oops!", "Please try again", "error");
                            }
                        }).error(function(response) {
                            SweetAlert.swal("Oops!", "Please try again", "error");
                        });
                    }
                });
            }
            $scope.openChatHeadWindow = false;
            $scope.postList = [];
            
                $scope.currentPage = 10;
               $scope.currentPaged = 0;
               $scope.totalItems = 0;
               $scope.pageSize = 10;
               $scope.totalPage=0;
            $scope.fetchAllRecentPost = function(pagesize=false) {
                
                $scope.searchKeyword = '';
                $scope.selectedTab = 1;
                $rootScope.showLoader = 1;
                if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+10; 
                    $scope.currentPaged-=2;
                }
                var data = {
                    'auth_token': token,
                    'patient_id': localStorage.getItem('patient_id_raw'),
                    'post_id':$scope.resPostId,
                    'is_read': 1,
                     'page':  '0',
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-recent-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.recentpostList = response.patient_post_arr;
                        $scope.totalItems = response.post_count;
                        $scope.totalPage= Math.ceil($scope.totalItems/$scope.pageSize);
                        $scope.currentPaged=1;
                        angular.forEach($scope.recentpostList , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time)*1000);
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.recentpostList[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                           chatSocket.emit("set_name", {
                            	from: Number(localStorage.getItem('patient_id_raw')),
                            	senderName: localStorage.getItem('patient_name'),
                            	buddy: Number(value.chat_history_arr.get_doctor.doctor_id),
                            	receiverName: value.chat_history_arr.get_doctor.first_name + ' ' + value.chat_history_arr.get_doctor.last_name,
                            	roomName: '',
                            	post_id: Number(value.post_id)
                            });
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                            if($scope.openChatHeadWindow===false && $scope.recentpostList.length>0)
                            {
                                //console.log('======'+$state.params.postId);
                                if(typeof $state.params.postId!='undefined' && $state.params.postId!='all'){

                                    var list_index = 0; var flag = 0;
                                    $scope.recentpostList.forEach(function(entry_val) {
                                       // console.log(entry_val.post_id);
                                        if(entry_val.post_id==$state.params.postId)
                                            flag=1;

                                        if(flag==0)
                                        list_index++;

                                    });
                                    //console.log('list_index=>>'+list_index);
                                    var one_post=  $scope.recentpostList[list_index];
                                    if($scope.recentpostList[list_index])
                                {   
                                   $scope.recentpostList[list_index].all_unread_chat_count_for_this_post=0;
                                }
                                    
                                    $scope.openChatHead(one_post,list_index);

                                }
                                else{
                                    var one_post=  $scope.recentpostList[0];
                                    $scope.recentpostList[0].all_unread_chat_count_for_this_post=0;
                                    $scope.openChatHead(one_post,0);
                                }


                                /*var one_post=  $scope.recentpostList[1];

                                console.log($scope.recentpostList);

                                $scope.openChatHead(one_post,1);*/

                            }
                            

                        })
                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            
        $scope.fetchAllRecentPostpagi = function(pagesize=false) {
                
                $scope.searchKeyword = '';
                $scope.selectedTab = 1;
                $rootScope.showLoader = 1;
                if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+10; 
                    $scope.currentPaged-=2;
                }
                var data = {
                    'auth_token': token,
                    'patient_id': localStorage.getItem('patient_id_raw'),
                    'post_id':$scope.resPostId,
                    'is_read': 1,
                    'page':  $scope.currentPage,
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-recent-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.recentpostList = response.patient_post_arr;
                        $scope.totalItems = response.post_count;
                        $scope.currentPage+=$scope.pageSize;
                        $scope.currentPaged+=1;

                        $scope.totalPage= Math.ceil($scope.totalItems/$scope.pageSize);
                        angular.forEach($scope.recentpostList , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time)*1000);
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.recentpostList[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                           chatSocket.emit("set_name", {
                            	from: Number(localStorage.getItem('patient_id_raw')),
                            	senderName: localStorage.getItem('patient_name'),
                            	buddy: Number(value.chat_history_arr.get_doctor.doctor_id),
                            	receiverName: value.chat_history_arr.get_doctor.first_name + ' ' + value.chat_history_arr.get_doctor.last_name,
                            	roomName: '',
                            	post_id: Number(value.post_id)
                            });
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                            if($scope.openChatHeadWindow===false && $scope.recentpostList.length>0)
                            {
                                //console.log('======'+$state.params.postId);
                                if(typeof $state.params.postId!='undefined' && $state.params.postId!='all'){

                                    var list_index = 0; var flag = 0;
                                    $scope.recentpostList.forEach(function(entry_val) {
                                       // console.log(entry_val.post_id);
                                        if(entry_val.post_id==$state.params.postId)
                                            flag=1;

                                        if(flag==0)
                                        list_index++;

                                    });
                                    //console.log('list_index=>>'+list_index);
                                    var one_post=  $scope.recentpostList[list_index];
                                    if($scope.recentpostList[list_index])
                                {   
                                   $scope.recentpostList[list_index].all_unread_chat_count_for_this_post=0;
                                }
                                    
                                    $scope.openChatHead(one_post,list_index);

                                }
                                else{
                                    var one_post=  $scope.recentpostList[0];
                                    $scope.recentpostList[0].all_unread_chat_count_for_this_post=0;
                                    $scope.openChatHead(one_post,0);
                                }


                                /*var one_post=  $scope.recentpostList[1];

                                console.log($scope.recentpostList);

                                $scope.openChatHead(one_post,1);*/

                            }
                            

                        })
                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.fetchUnreadPost = function() {
               
                $scope.openChatHeadWindow = false;
                $scope.searchKeyword = '';
                $scope.selectedTab = 2;
                $rootScope.showLoader = 1;
                var data = {
                    'auth_token': token,
                    'patient_id': localStorage.getItem('patient_id_raw'),
                    'is_read': 0
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-recent-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    // console.log('==fetchUnreadPost======='+response.status);
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.unreadpostList = response.patient_post_arr;
                        angular.forEach($scope.unreadpostList , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time)*1000);
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.unreadpostList[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                        });
                        //console.log($scope.unreadpostList);
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        });

                        if($scope.openChatHeadWindow===false && $scope.unreadpostList.length>0)
                        {
                            //console.log('======'+$state.params.postId);
                            if(typeof $state.params.postId!='undefined' && $state.params.postId!='all'){

                                var list_index = 0; var flag = 0;
                                $scope.unreadpostList.forEach(function(entry_val) {
                                   // console.log(entry_val.post_id);
                                    if(entry_val.post_id==$state.params.postId)
                                        flag=1;

                                    if(flag==0)
                                    list_index++;

                                });
                                //console.log('list_index=>>'+list_index);
                                var one_post=  $scope.unreadpostList[list_index];
                                if($scope.unreadpostList[list_index])
                                {   
                                    $scope.unreadpostList[list_index].all_unread_chat_count_for_this_post=0;
                                }
                                

                                $scope.openChatHead(one_post,list_index);

                            }
                            else{
                                var one_post=  $scope.unreadpostList[0];
                                $scope.unreadpostList[0].all_unread_chat_count_for_this_post=0;
                                $scope.openChatHead(one_post,0);
                            }
                        }




                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.fetchUnreadPostCount = function() {
                var data = {
                    'auth_token': token,
                    'patient_id': localStorage.getItem('patient_id_raw'),
                    'is_read': 0
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-recent-post-count",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $scope.unreadpostCount = response.patient_post_count;
                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            
            $scope.messageLog = [];
            if($state.current.name=='my-message')
            {
                if(typeof $state.params.act!='undefined'){

	                if($state.params.act == 'unread'){

	                    $('#recent_unread_tab a[data-target="#messages"]').tab('show');
	                    $scope.fetchUnreadPost();
	                    

	                    $scope.fetchUnreadPostCount();
	                }
	                if($state.params.act == 'recent')
	                    $scope.fetchAllRecentPost();
                }
                else
                    $scope.fetchAllRecentPost();
            }

            


            $scope.chatImagePath = chatImagePath;
            $scope.message = '';
            $scope.chatImageApiPath = chatImageApiPath;
            $scope.readChat = function(){
                var datas = {
                    auth_token: token,
                    patient_id : $scope.patientId, 
                    post_id: $scope.postId
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "read-chat-patient",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(datas)
                }).success(function(response, status, headers, config) {
                    console.log(response);
                }).error(function(response) {
                    
                });
            }
            chatSocket.emit("send_message",{});
            $scope.openChatHead = function(postDetails, index,source) {
           
				
                if(source&&source=='unread')
                {
                    
                    $scope.unreadpostList[index].all_unread_chat_count_for_this_post=0;
                }
                else if(source&&source=='recent')
                {
                   
                    $scope.recentpostList[index].all_unread_chat_count_for_this_post=0;
                }
                $scope.messageLog = [];
                //console.log(postDetails);
				if(typeof postDetails != 'undefined'){
					$scope.openChatHeadWindow = true; 
					$scope.postDetail = postDetails;
					//console.log($scope.postDetail);
					var date = new Date();
					$scope.currentTime = date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');

                    // Convert Post date time into local time dateFormatter
                    if(typeof $scope.postDetail.posted_date!='undefined')
                    {
                        var postodate = new Date($scope.postDetail.posted_date);
                        //console.log('postodate===>'+postodate);
                        var posyr  = postodate.getFullYear();
                        var posmon = postodate.getMonth() + 1;
                        var posDt = postodate.getDate();
                        var poshr = postodate.getHours();
                        var posmn = postodate.getMinutes();
                        var possec = postodate.getSeconds();


                        var dd = $scope.postDetail.posted_date.split(" ");
                        var dd_date = dd[0].split("-");
                        var dd_time = dd[1].split(":");
                        
                        var posyr  	= dd_date[0];
                        var posmon  = dd_date[1];
                        var posDt  	= dd_date[2];
                        var poshr  	= dd_time[0];
                        var posmn  	= dd_time[1];
                        var possec  = dd_time[2];

                        //alert(posyr);

                        var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));

                        var localTimeConvert = utc_date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');

                        //console.log("posyr==>"+$scope.postDetail.posted_date);
                        $scope.postDetail.change_posted_date = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                    }
                    


					$scope.patientId = localStorage.getItem('patient_id_raw');
					$scope.patientName = localStorage.getItem('patient_name');
					//console.log(postDetails);
					//$scope.postId = typeof (postDetails.post_id=='undefined')?'':postDetails.post_id;
					$scope.postId = postDetails.post_id;
					
					$scope.readChat();
					if(source=='unread'){
                        if($scope.unreadpostList[index].all_unread_chat_count_for_this_post){
                           $scope.unreadpostList[index].all_unread_chat_count_for_this_post = 0; 
                        }
                    }else if(source=='recent'){
                        if($scope.recentpostList[index].all_unread_chat_count_for_this_post){
                           $scope.recentpostList[index].all_unread_chat_count_for_this_post=0; 
                        }
                    }
					$scope.chatIndex = index;
					if (postDetails.chat_history_arr.is_favourite == 0) {
						$scope.fav = 0;
					} else {
						$scope.fav = 1;
					}
					
					$scope.appointment = postDetails.chat_history_arr.appointment;
					$scope.is_status = postDetails.chat_history_arr.is_status;
					$scope.appointment_time = postDetails.chat_history_arr.appointment_time;
					//console.log('amit'+postDetails.chat_history_arr.get_doctor.first_name);
					$scope.receiverName = postDetails.chat_history_arr.get_doctor.first_name + ' ' + postDetails.chat_history_arr.get_doctor.last_name;
					$scope.doctorId = postDetails.chat_history_arr.get_doctor.doctor_id;
					chatSocket.emit("set_name", {
						from: Number($scope.patientId),
						senderName: $scope.patientName,
						buddy: Number($scope.doctorId),
						receiverName: $scope.receiverName,
						roomName: '',
						post_id: Number($scope.postId)
					});
				}
				else
					$scope.openChatHeadWindow = false;
				
                $timeout(function(){
                    
                    $scope.fetchUnreadPostCount();
                    $scope.getUnreadChatCount();
                    
                 
                },2500);
               
            }
            $scope.fetchDoctorReplyChat = function() {
                var data = {
                    'auth_token': token,
                    'patient_id': patient_id
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-patients-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $scope.postList = response.patient_posts;
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.sendMessage = function() {
                var match = $scope.message.match('^\/nick (.*)');
                //console.log('ok firing');
                $log.debug('sending message', $scope.message);
                if (!angular.isUndefined($scope.filename) && $scope.filename != '') {
                    var formData = new FormData();
                    formData.append('image', $scope.filename);
                    formData.append('from', Number($scope.patientId));
                    formData.append('senderName', $scope.patientName);
                    formData.append('buddy', Number($scope.doctorId));
                    formData.append('receiverName', $scope.receiverName);
                    formData.append('message', (!angular.isUndefined($scope.message)) ? $scope.message : '');
                    formData.append('status', 'unread');
                    formData.append('type', 'userMessage');
                    formData.append('post_id', Number($scope.postId));
                    $http({
                        method: 'post',
                        url: chatImageApiPath + "image-uploaded",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: formData,
                        transformRequest: angular.identity
                    }).success(function(response, status, headers, config) {
                        if (response.status == 1) {
                            chatSocket.emit('file-uploaded', response.result);
                            $scope.filename = '';
                            var data = {
                                'auth_token': token,
                                'patient_id': $scope.patientId,
                                'doctor_id': $scope.doctorId,
                                'post_id': $scope.postId,
                                'patient_content': (!angular.isUndefined($scope.message)) ? $scope.message : 'File',
                                'doctor_content': ''
                            }
                            $http({
                                method: 'post',
                                dataType: "jsonp",
                                crossDomain: true,
                                url: configService.getEnvConfig().apiURL + "save-chat-history",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: $.param(data)
                            }).success(function(response, status, headers, config) {

                                if (response.status == 1) {
                                    var data = {
                                        'auth_token': token,
                                        'doctor_id': $scope.doctorId,
                                        'post_id': $scope.postId,
                                    }
                                    $http({
                                        method: 'post',
                                        dataType: "jsonp",
                                        crossDomain: true,
                                        url: configService.getEnvConfig().apiURL + "read-chat-for-patient",
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        },
                                        data: $.param(data)
                                    }).success(function(response, status, headers, config) {
                                        if (response.status == 1) {
                                            $scope.fetchAllRecentPost();
                                        }
                                    }).error(function(response) {});
                                }
                            }).error(function(response) {});
                        }
                    }).error(function(response) {
                        $scope.show_message = 'Something is Wrong';
                    });
                } else {
                    if ($scope.message != '') {
                        var data = {
                            message: $scope.message,
                            file_name: '',
                            file_type: '',
                            status: 'unread',
                            type: 'userMessage'
                        };
                        var message = {};
                        message.username = $scope.patientId;
                        message.from = $scope.patientId;
                        message.to = $scope.doctorId;
                        message.senderName = $scope.patientName;
                        message.receiverName = $scope.receiverName;
                        message.post_id = $scope.postId;
                        message.message = $scope.message;
                        message.createdAt = new Date().toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                        $scope.messageLog.push(message);
                        if ($scope.selectedTab == 1) {
                            $scope.recentpostList[$scope.chatIndex].chat_history_count = ($scope.recentpostList[$scope.chatIndex].chat_history_count + 1);
                        }
                        if ($scope.selectedTab == 2) {
                            $scope.unreadpostList[$scope.chatIndex].chat_history_count = ($scope.unreadpostList[$scope.chatIndex].chat_history_count + 1);
                        }
                        $(".scrollBox").mCustomScrollbar("update");
                        $timeout(function() {
                            $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                        }, 1000);
                        chatSocket.emit('message', JSON.stringify(data));
                        var data = {
                            'auth_token': token,
                            'patient_id': $scope.patientId,
                            'doctor_id': $scope.doctorId,
                            'post_id': $scope.postId,
                            'patient_content': $scope.message,
                            'doctor_content': ''
                        }
                        $http({
                            method: 'post',
                            dataType: "jsonp",
                            crossDomain: true,
                            url: configService.getEnvConfig().apiURL + "save-chat-history",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: $.param(data)
                        }).success(function(response, status, headers, config) {
                            if (response.status == 1) {
                                var data = {
                                    'auth_token': token,
                                    'doctor_id': $scope.doctorId,
                                    'post_id': $scope.postId,
                                }
                                $http({
                                    method: 'post',
                                    dataType: "jsonp",
                                    crossDomain: true,
                                    url: configService.getEnvConfig().apiURL + "read-chat-for-patient",
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    },
                                    data: $.param(data)
                                }).success(function(response, status, headers, config) {
                                    if (response.status == 1) {
                                        $scope.fetchAllRecentPost();
                                    }
                                }).error(function(response) {});
                            }
                        }).error(function(response) {});
                    } else {}
                }
                $scope.message = '';
            };

            
            $scope.closeChat = function() {

              

                var data = {
                    'auth_token': token,
                    'patient_id': $scope.patientId,
                    'doctor_id': $scope.doctorId,
                    'post_id': $scope.postId,
                }
               
                swal({
                      title: "Are you sure?",
                      text: "You will not be able to reply this post!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-danger",
                      confirmButtonText: "Yes Close it!",
                      closeOnConfirm: true
                    },
                    function(){
                           /*swal("Deleted!", "Your imaginary file has been deleted.", "success");*/
                            $rootScope.showLoader = 1;
                            $http({
                                method: 'post',
                                dataType: "jsonp",
                                crossDomain: true,
                                url: configService.getEnvConfig().apiURL + "closed-post-chat",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: $.param(data)
                            }).success(function(response, status, headers, config) {
                                if (response.status == 1) {
                                    $rootScope.showLoader = 0;
                                    if ($scope.selectedTab == 1) {
                                        $scope.recentpostList[$scope.chatIndex].is_closed_chat = 1;
                                    }
                                    if ($scope.selectedTab == 2) {
                                        $scope.unreadpostList[$scope.chatIndex].is_closed_chat = 1;
                                    }
                                    $scope.postDetail.is_closed_chat = 1;
                                    $scope.openChatHeadWindow = false;
                                    $("#saveRatingModal").modal('show');
                                    var data_close = {
                                        'patient_id': $scope.patientId,
                                        'doctor_id': $scope.doctorId,
                                        'post_id': $scope.postId,
                                    };
									
									
                                    chatSocket.emit('close_message', JSON.stringify(data_close));
                                }
                            }).error(function(response) {});

                    });

           
            }

            $scope.rate = function() {}
            $scope.saveRating = function(rating) {
                var data = {
                    'auth_token': token,
                    'patient_id': $scope.patientId,
                    'doctor_id': $scope.doctorId,
                    'post_id': $scope.postId,
                    'rating': (rating.rate) ? rating.rate : 0,
                    'review': (rating.review) ? rating.review : ''
                }
                $rootScope.showLoader = 1;
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "save-review-rating-for-doctors",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $("#saveRatingModal").modal('hide');
                        SweetAlert.swal("Done!", "Review successfully posted", "success");
                        $scope.saveRatingForm.$setPristine();
						$scope.fetchAllRecentPost();
                       /* $timeout(function() {
                            location.reload();
                        }, 1000);*/
                    } else {
                        SweetAlert.swal("Oops", "Please try again", "success");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops", "Please try again", "success");
                });
            }
            $scope.uploadAttachmentChat = function(fileData) {
                $scope.filename = fileData[0];
                $timeout(function() {
                    $scope.sendMessage();
                }, 1);
            }
            $scope.getUnreadChatCount();
            var current_msg_id = '';
            if ($rootScope.is_front_logged == 2) {
                chatSocket.on('send_message', function(msg){
                    console.log('send_message');
                    console.log(msg);
                    var data = JSON.parse(msg);
                    var localtime = dateFormatter(data.createdAt);
                    angular.forEach($scope.unreadpostList, function(val, key) {
                        if(val.post_id == data.post_id){
                            $scope.unreadpostList[key].all_unread_chat_count_for_this_post = $scope.unreadpostList[key].all_unread_chat_count_for_this_post+1;
                            $scope.unreadpostList[key].chat_history_arr.doctor_content = data.message;
                            $scope.unreadpostList[key].chat_history_arr.patient_content = '';
                            if(data.createdAt.length>9)
                            $scope.unreadpostList[key].chat_history_arr.sent_time = localtime;
                        }
                    });
                    angular.forEach($scope.recentpostList, function(val, key) {
                        if(val.post_id == data.post_id){
                            $scope.recentpostList[key].all_unread_chat_count_for_this_post = $scope.recentpostList[key].all_unread_chat_count_for_this_post+1;
                            $scope.recentpostList[key].chat_history_arr.doctor_content = data.message;
                            $scope.recentpostList[key].chat_history_arr.patient_content = '';
                            if(data.createdAt.length>9)
                            $scope.recentpostList[key].chat_history_arr.sent_time = localtime;
                        }
                    });
                
                });
                chatSocket.on('message', function(data) {
                    //console.log(data);
                    if ($scope.messageLog.length == 0 && Array.isArray(data)) {
                        //console.log('arr======');
                        

                        angular.forEach(data, function(val, key) {
                            if(val.createdAt.length>9)
                             val.createdAt = dateFormatter(val.createdAt);

                        })
                        $scope.messageLog = data.reverse();
                    } else {
                        if (!Array.isArray(data)) {
                            data = JSON.parse(data);
                            if (data._id == current_msg_id) {} else {
                                if($scope.postId==data.post_id)
                                {
                                    $scope.messageLog.push(data);

                                     $scope.messageLog.forEach(function(item, index) {

                                        if ($scope.messageLog[index].createdAt.indexOf("AM") != -1 || $scope.messageLog[index].createdAt.indexOf("PM") != -1) {
                                           // console.log('elseid'+($scope.messageLogPatient[index].createdAt));
                                            var d = new Date();
                                            var n = d.toISOString();
                                            //console.log(dateFormatter(n));
                                            $scope.messageLog[index].createdAt = dateFormatter(n);

                                        }

                                       

                                        //$scope.messageLogPatient[index].createdAt = dateFormatter($scope.messageLogPatient[index].createdAt);

                                    });
                                     angular.forEach($scope.messageLog, function(val, key) {
                                        if(val.createdAt.length>9)
                                         val.createdAt = dateFormatter(val.createdAt);
                                        
                                    });
                                }   

                                //console.log('elsssss======');
                               // console.log($scope.messageLog);


                                
                                current_msg_id = data._id;
                                
                                /*if ($scope.selectedTab == 1) {
                                    $scope.recentpostList[$scope.chatIndex].chat_history_count = ($scope.recentpostList[$scope.chatIndex].chat_history_count + 1);
                                }
                                if ($scope.selectedTab == 2) {
                                    $scope.unreadpostList[$scope.chatIndex].chat_history_count = ($scope.unreadpostList[$scope.chatIndex].chat_history_count + 1);
                                }*/
                            }
                        }
                    }
                    $(".scrollBox").mCustomScrollbar("update");
                    $timeout(function() {
                        $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                    }, 1000);
                    var data = {
                        'auth_token': token,
                        'doctor_id': $scope.doctorId,
                        'post_id': $scope.postId,
                    }

                    if($scope.openChatHeadWindow === true && $state.current.name == 'my-message')
                    {
                        $http({
                        method: 'post',
                        dataType: "jsonp",
                        crossDomain: true,
                        url: configService.getEnvConfig().apiURL + "read-chat-for-patient",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param(data)
                        }).success(function(response, status, headers, config) {
                            if (response.status == 1) {}
                        }).error(function(response) {});

                    }
                });
                var current_id = '';
                chatSocket.on('file-uploaded', function(data) {
                    if ($rootScope.is_front_logged == 2) {
                        if (!Array.isArray(data)) {
                            data = JSON.parse(data);
                            if (data._id != current_id) {
                                $scope.messageLog.push(data);
                                current_id = data._id;
                                if ($scope.selectedTab == 1) {
                                    $scope.recentpostList[$scope.chatIndex].chat_history_count = ($scope.recentpostList[$scope.chatIndex].chat_history_count + 1);
                                }
                                if ($scope.selectedTab == 2) {
                                    $scope.unreadpostList[$scope.chatIndex].chat_history_count = ($scope.unreadpostList[$scope.chatIndex].chat_history_count + 1);
                                }
                            }
                        }
                        $(".scrollBox").mCustomScrollbar("update");
                        $timeout(function() {
                            $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                        }, 1000);
                        var data = {
                            'auth_token': token,
                            'doctor_id': $scope.doctorId,
                            'post_id': $scope.postId,
                        }
                         if($scope.openChatHeadWindow === true && $state.current.name == 'my-message')
                        {
                            $http({
                                method: 'post',
                                dataType: "jsonp",
                                crossDomain: true,
                                url: configService.getEnvConfig().apiURL + "read-chat-for-patient",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: $.param(data)
                            }).success(function(response, status, headers, config) {
                                if (response.status == 1) {}
                            }).error(function(response) {});
                        }
                    }
                });
            }
            var checkimage1=function(i){
                if(i)
                   return $rootScope.apiUrl+"uploads/patient_profile_image/"+i;
                else
                   return $rootScope.apiUrl+"uploads/patient_profile_image/no_image.jpg";

            }
            var checkimage2=function(i){
                if(i)
                   return i;
                else
                   return $rootScope.apiUrl+"uploads/patient_profile_image/no_image.jpg";

            }


            
            var checkimage3=function(i){
                if(i)
                   return $rootScope.apiUrl+"uploads/dentist_profile_image/"+i;
                else
                   return $rootScope.apiUrl+"uploads/patient_profile_image/no_image.jpg";

            }
            var dateFormatter = function(date) {
                var curd = new Date(),
                    curmonth = '' + (curd.getMonth() + 1),
                    curday = '' + curd.getDate(),
                    curyear = curd.getFullYear();
                if (curmonth.length < 2) curmonth = '0' + curmonth;
                if (curday.length < 2) curday = '0' + curday;
                var curdate = [curyear, curmonth, curday].join('-');
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                var postDate = [year, month, day].join('-');
                if (postDate < curdate) {
                    return postDate+" "+new Date(date).toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                } else {
                    return new Date(date).toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                }
            }
        }
        $scope.markFavourite = function(favVal) {
            var data = {
                'auth_token': token,
                'patient_id': $scope.patientId,
                'doctor_id': $scope.doctorId,
                'is_favourite': favVal
            }
            $rootScope.showLoader = 1;
            $http({
                method: 'post',
                dataType: "jsonp",
                crossDomain: true,
                url: configService.getEnvConfig().apiURL + "mark-favourite-doctors",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(response, status, headers, config) {
                if (response.status == 1) {
                    $rootScope.showLoader = 0;
                    if (favVal == 1) {
                        $scope.fav = 1;
                        SweetAlert.swal("Done!", "Doctor Marked as favorite", "success");
                    } else {
                        $scope.fav = 0;
                        SweetAlert.swal("Done!", "Doctor removed from favorite list", "success");
                    }
                } else {
                    SweetAlert.swal("Oops!", "There is some problem", "error");
                }
            }).error(function(response) {
                SweetAlert.swal("Oops!", "There is some problem", "error");
            });
        }
        $scope.setAppointment = function(aptVal) {
            var data = {
                'auth_token': token,
                'patient_id': $scope.patientId,
                'doctor_id': $scope.doctorId,
                'post_id': $scope.postId,
                'is_appoitment': aptVal
            }
            if(aptVal==2){
                swal({
                  title: "Are you sure?",
                  text: "Are you sure you want to cancel!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, cancel it!",
                  cancelButtonText: "No, plx!",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                function(isConfirm){
                  if(isConfirm){
                    $rootScope.showLoader = 1;
                    $http({
                        method: 'post',
                        dataType: "jsonp",
                        crossDomain: true,
                        url: configService.getEnvConfig().apiURL + "set-appointment-patient",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param(data)
                    }).success(function(response, status, headers, config) {
                        if (response.status == 1) {
                            $rootScope.showLoader = 0;
                            $scope.appointment = aptVal;
                            SweetAlert.swal("Success!", response.msg, "success");
                            $scope.message = 'You have declined the appointment for '.$scope.appointment_time;
                            $scope.sendMessage();
                            
                        } else {
                            SweetAlert.swal("Oops!", "There is some problem", "error");
                        }
                    }).error(function(response) {
                        SweetAlert.swal("Oops!", "There is some problem", "error");
                    });
                  }
                });
            }else{
                $rootScope.showLoader = 1;
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "set-appointment-patient",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.appointment = aptVal;
                        SweetAlert.swal("Success!", response.msg, "success");
                        if(aptVal=='3'){
                            $scope.message = 'You have confirmed the appointment for '.$scope.appointment_time;
                        }else{
                            $scope.message = 'Success! Request Appointment has been done successfully.';
                        }
                        $scope.sendMessage();
                        
                    } else {
                        SweetAlert.swal("Oops!", "There is some problem", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "There is some problem", "error");
                });
            }
        }
        $scope.postDetails = {};
        $scope.postDetails.attachments = [{
            id: 'choice1',
            'name': ''
        }];
        $scope.addNewAttachment = function() {
            var newItemNo = $scope.postDetails.attachments.length + 1;
            $scope.postDetails.attachments.push({
                'id': 'choice' + newItemNo,
                'name': ''
            });
        }
        $scope.removeAttachment = function(index) {
            $scope.postDetails.attachments.splice(index, 1);
            $scope.docedufiles.splice(index, 1);
        };
        
         $scope.myCheck = function(test) {
            alert(test);
        };

        
       
        $scope.docedufiles = [];
        $scope.docedufilesname = [];
        $scope.key_array = [];
        $scope.uploadAttachment = function(files, id) {
            
            var index = id.split("_");
            
            var Extension = files[0].name.substring(files[0].name.lastIndexOf('.') + 1).toLowerCase();
            
            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg") {
                $scope.postDetails.attachments[index[1]].name = files[0].name;
                if ($scope.postDetails.attachments[index[1]].name != '') {
                    var i = $.inArray(index[1], $scope.key_array);//console.log(i);
                    if (i > -1) {
                        $scope.key_array.push(index[1]);
                        $scope.docedufiles.splice(index[1], 1);
                        $scope.docedufiles.push(files[0]);
                    } else {
                        $scope.key_array.push(index[1]);
                        $scope.docedufiles.push(files[0]);
                    }
                   // console.log($scope.docedufiles);
                    //console.log($scope.key_array);
                } else {

                    $scope.docedufiles[index[1]] = files[0];

                }
               // console.log($scope.docedufiles);
                $scope.$digest();
            }else {
                SweetAlert.swal("Oops!", "Only images are allowed", "error");
            }
        };



        if ($rootScope.curr_state == 'dentistProfileStep3') {
            var token = localStorage.getItem("access_token");
            //console.log(token);
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
        }
        if ($rootScope.curr_state == 'viewdoctorprofile') {

            
            var doctor_id = $stateParams.doctorId;
            var data = {
                'auth_token': token,
                'doctor_id': doctor_id,
            }
            $rootScope.showLoader = 1;
            $http({
                method: 'post',
                dataType: "jsonp",
                crossDomain: true,
                url: configService.getEnvConfig().apiURL + "list-doctor-details",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(response, status, headers, config) {
                if (response.status == 1) {
                    $scope.dentistDetails = response;
                    //console.log(response);

                    // Set Meta
                    ngMeta.setTitle(response.doctor_details[0].first_name+' '+response.doctor_details[0].last_name); 

                    $rootScope.showLoader = 0;
                    var data = {
                        'auth_token': token,
                        'doctor_id': doctor_id,
                        'patient_id': localStorage.getItem('patient_id_raw')
                    }
                    $http({
                        method: 'post',
                        dataType: "jsonp",
                        crossDomain: true,
                        url: configService.getEnvConfig().apiURL + "get-doctor-favourite",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param(data)
                    }).success(function(response, status, headers, config) {
                        if (response.status == 1) {
                            $scope.isFavourite = response.is_favourite;
                        } else {
                            SweetAlert.swal("Oops!", "There is some problem", "error");
                        }
                    }).error(function(response) {
                        SweetAlert.swal("Oops!", "There is some problem", "error");
                    });
                } else {
                    SweetAlert.swal("Oops!", "There is some problem", "error");
                }
            }).error(function(response) {
                SweetAlert.swal("Oops!", "There is some problem", "error");
            });
            $scope.scrollTo = function(id) {
                $location.hash(id);
                $anchorScroll();
            }

            // $scope.toggle = function(index) {                   
            //     };
        }

        if ($state.current.name == 'create-post' || $state.current.name == 'my-post' || $state.current.name == 'my-message') {
    
            $scope.fetchUnreadPostCount();
            $scope.getUnreadChatCount();
        }

    }
})();
