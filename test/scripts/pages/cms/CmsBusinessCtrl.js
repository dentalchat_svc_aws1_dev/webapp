/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.cms')
        .controller('CmsBusinessCtrl', CmsBusinessCtrl);

    /** @ngInject */
    function CmsBusinessCtrl($sce,$scope, $timeout,  $http, configService, $rootScope, $location, cmsService, ngMeta,vcRecaptchaService) {
        $scope.viewDetails = {};
         $scope.capuser = {};
         $scope.capuser.captchavar = 1;
                $scope.helloW = function(){
                      alert('dsf');
                      
             $scope.capuser.captchavar = 1;
        }
        /*check the accomodation_id is for that user or not*/
        // $scope.allCms = function(page_id) {
        //    //alert(page_id);
		// 	var data = {
		// 		page_id:page_id            
        //     };
		// 	$scope.user = {};
		// 	$rootScope.showLoader = 1;
        //     $http.post(configService.getEnvConfig().apiURL + "service/cms/all-cms",data)
        //     .success(function(data, status, headers, config) {
             
        //         $rootScope.showLoader = 0;                
		// 		$scope.cms_details  = data.all_cms;

        //         ngMeta.setTitle(data.all_cms.page_title);
        //         ngMeta.setTag('description', data.all_cms.meta_description);
        //         ngMeta.setTag('keywords', data.all_cms.meta_keyword);
        //     })
        //     .error(function(data, status, header, config) {
        //         console.log(data);
        //         $scope.show_message = 'Something is Wrong';
        //     });
        // }

        $scope.renderHtml = function(html_code)
        {
            return $sce.trustAsHtml(html_code);
        }

        /**** Contact Us page Controller *****/
        $scope.submitContact = function(user)
        {
            console.log(user);
            var data = {
                    user:user
                      };
            var token = localStorage.getItem("access_token");

            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/add-contact",data,config)
            .success(function(data, status, headers, config) {
               // $rootScope.showLoader = 0;
                if(data.status == 1){
                    
                    $rootScope.showLoader = 0;
                    $scope.showquestionmsg = 'Your message has been successfully sent.We will contact you soon.';
                    contactForm123.reset();
                    $scope.showalert = 0;
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },5000);
                  $scope.user = {};
                 
                
                   /*$scope.user = {};
                   localStorage.setItem("thankmessage", "Your message has been successfully sent.We will contact you soon.");
                    $location.path('/thankyou');
                    $timeout(function(){
                        $scope.showalert = 1;
                        $scope.showquestionmsg = '';
                   },2000); */
                }
                else if (data.status == 3) {
                    $scope.change_color = "color:red";
                    $scope.show_message = 'Captcha mismatched';
                    $scope.showSuccMsg = 1;
                    
                }
            })
            .error(function(data, status, header, config) {
                console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
		
		 /* Valid Email Checking Start */
        $scope.checkEmail=function(email) {

            var patt = /^$|^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;
            var res = patt.test(email);
    
            if(res)
            {
                return 1;
            }
            else
            {
                if(typeof email == 'undefined')
                {
                    return 1;
                }
                return 0;
            }
        };

        /**** Contact Us page Controller *****/
    }
})();
