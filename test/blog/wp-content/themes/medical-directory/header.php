<!doctype html>
<html <?php language_attributes(); ?> >
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/custom.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<?php $medicaldirectory_option_data =get_option('medicaldirectory_option_data'); ?><?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
<?php wp_head(); ?>
<style>@media screen and (min-width: 689px) {#searchformhd .input-field .form-group {    width: 43.5% !important;}#searchformhd .input-field .form-group.search{    width: 10% !important;}}</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body <?php body_class(); ?> >
<div class="uou-block-11a mobileMenu">
<h5 class="title"><?php esc_html_e( 'Menu', 'medical-directory' ); ?></h5>
<a href="#" class="mobile-sidebar-close"><?php esc_html_e( 'X', 'medical-directory' ); ?> </a>
<?php get_template_part('templates/header','menuMobile'); ?><hr>
</div>
<div id="main-wrapper"> 
<div class="toolbar">
        <?php get_template_part('templates/topS/header','topbarChoose'); ?>
        <?php get_template_part('templates/headerS/header','choose'); ?>
        <?php
        if ( !is_front_page() && !is_page('my-story') ) {
			get_template_part('templates/breadcrumbs/header','crumbChoose');
		}
 ?> 
 </div> 