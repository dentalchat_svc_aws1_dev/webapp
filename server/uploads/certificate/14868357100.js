'use strict';


angular.module('dpiApp')
    .controller('SearchPropertyController', function($scope, $timeout, $http, configService, $rootScope, $location, $route, $routeParams)
{

  /*var host = $location.host();
  console.log(host);*/

  $scope.page_heading = '';
  $scope.userid = '';
  $scope.show_default = 1; // For default website show  default list
  $scope.property_list ={};
  $scope.recordLimit = 12;
  $scope.currentPage = 1; 
  $scope.property_found = 0;
  $scope.search_but_show_hide = 1;
  

  $scope.search = {};
  $scope.bedrating = 2;
  $scope.bathrating = 2;
  $scope.search.property_type = '';
  $scope.search.min_price = '150000'; 
  $scope.search.max_price = '400000';

  $scope.search_listing_sort = 'listprice_asc';

  $scope.sethbaths = {};

 
  $scope.front_spinner = 0;
  $scope.search_loader = 0;
  $scope.advance_open =0;

  $scope.property_id = $routeParams.property_id;
  $scope.latitude = 27.6648;
  $scope.longitude = -81.5158;
  $scope.address ='';
  $scope.filter_data = {};
  
  $scope.furnished_active = $scope.water_front_active = $scope.water_access_active = $scope.view_active = $scope.pool_active = $scope.pet_active = $scope.hbaths_active = $scope.complex_name_active = $scope.subdivision_name_active = $scope.development_name_active = $scope.county_active = $scope.zip_code_active = $scope.foreclosure_active = $scope.short_sale_active = '';

  $scope.advanced_search = {};
  $scope.optionsFromQuery =[];   // Filter Value array

  console.log($scope.advanced_search);


	setTimeout(function(){
		ImageFitCont();
	},500);

  /**/

  console.log($scope.search_but_show_hide);

  /****** Get Current Route Name ******/
    $scope.routeName= $route.current.$$route.name; 
  /****** Get Current Route Name ******/

  var markers=[];

  $scope.getAllProperty= function(location_id,criteria_id)
  {       
    var httpRequest = $http({
    method: 'POST',
    url: base_url+'/hometheme/all-property',
    data: {}
  
    }).success(function(data, status) { 
      
      $scope.property_type = data.all_property_type;  
      console.log($scope.property_type);  

    });
  }

  $scope.getAllProperty();

  /**/

  


  /************* After Full page content load collapse the menu for mobile view *************/
  
    $scope.$on('$viewContentLoaded', function(){
      //Here your view content is fully loaded !!
      $(".navbar-toggle").trigger( "click" );
    });
   

   /*  Bedroom Bathroom icon setup start */

      /* $scope.bedrating = 0;
       $scope.bathrating = 0;
       $scope.halfbath = 0;*/

          $scope.bedrooms = [{
              current: 2,
              max: 4
          }];
          $scope.bathrooms = [{
              current: 2,
              max: 4
          }];
          $scope.halfbathrooms = [{
              current: 0,
              max: 4
          }];

          $scope.getSelectedBed = function (bedrating) {
            $scope.bedrating = bedrating;
              console.log($scope.bedrating);
          }
          $scope.getSelectedBath = function (bathrating) {
            $scope.bathrating = bathrating;
              console.log($scope.bathrating);
          }
          $scope.getSelectedHalfBath = function (halfbath) {
            $scope.halfbath = halfbath;
            //console.log($scope.halfbath);
           
            //$scope.sethbaths = {hbaths: $scope.halfbath};
            $scope.advanced_search.hbaths=$scope.halfbath;
            console.log($scope.advanced_search);
			console.log($scope.advanced_search['hbaths']);
			if($scope.advanced_search['hbaths'] =='undefined')
			{
				$scope.hbaths_active ='';
			}
			else
			{
				$scope.hbaths_active ='active';
			}
			
          }

          /*  Bedroom Bathroom icon setup end */


    /******* SEARCH PROPERTY BUTTON AJAX START *******/

      $scope.searchProperty = function()
      {
          var httpRequest = $http({
          method: 'POST',
          url: base_url+'/hometheme/search-property-result',
          data: {location:$scope.search.location, property_type:$scope.search.property_type, bedroom:$scope.bedrating, bathroom:$scope.bathrating, min_price:$scope.search.min_price, max_price:$scope.search.max_price,search_listing_sort:$scope.search_listing_sort,limit:$scope.recordLimit,page:$scope.currentPage,advance_search:JSON.stringify($scope.advanced_search)}
        
          }).success(function(data, status) { 
            
            $scope.all_property_list = data.all_property;
            $scope.property_found = data.total_matched_property;
            console.log(data);    

            setTimeout(function(){
              ImageFitCont();
            },500);

            //***** Google map integration code here start *****//  
                  
              var ip=0;
              var tmp={};
              var tmp_arr=[];
              $scope.propertylatlngs=[];
              for(ip=0;ip<$scope.all_property_list.length;ip++)
              {
                tmp=$scope.all_property_list[ip];

                tmp_arr=[tmp.address+", "+tmp.city_name+", "+tmp.state+" - "+tmp.zip_code,tmp.lat,tmp.lng,tmp.address+", "+tmp.city_name+", "+tmp.state+" - "+tmp.zip_code];

                $scope.propertylatlngs.push(tmp_arr);

              }

              //console.log($scope.propertylatlngs);

              setTimeout(function(){initMap2($scope.propertylatlngs);},1000);
            
            //***** Google map integration code here start *****//

          });
      }

      $(document).ready(function(){    
        if(typeof(initmap2) == "function")
        {
          initMap2($scope.propertylatlngs);
        }    
      });




      $scope.checker=function(index,val) // index= filter field name ; val= value[ex: Yes/No]
      {
        if((typeof $scope.advanced_search[index] !='undefined') && ($scope.advanced_search[index]==val))
        {
          delete $scope.advanced_search[index];
		}
        else
		{
			$scope.advanced_search[index]=val; // Push advanced search filter in advanced_search array
		}
            console.log($scope.short_sale_active);   
			console.log($scope.advanced_search);

      }



    /******* SEARCH PROPERTY BUTTON AJAX END *******/

    if($scope.routeName == 'search_property')
    {
      $scope.searchProperty(); // Self invoking for property search
    }
    
    $scope.changePage = function() // Pagination
    {
        $scope.searchProperty({location:$scope.search.location, property_type:$scope.search.property_type, bedroom:$scope.bedrating, bathroom:$scope.bathrating, min_price:$scope.search.min_price, max_price:$scope.search.max_price,search_listing_sort:$scope.search_listing_sort,limit:$scope.recordLimit,page:$scope.currentPage,advance_search:JSON.stringify($scope.advanced_search)});
    }

    $scope.getDistinct = function(params) // Filter For Advanced Search
    {
        $scope.filter_data = '';
        var httpRequest = $http({
        method: 'POST',
        url: base_url+'/hometheme/search-filter',
        data: {filter:params}
      
        }).success(function(data, status) { 

            $scope.filter_data = data;
            $scope.optionsFromQuery = data.get_val;

            console.log(data); 
            console.log( $scope.filter_data.filter_param);
        });
    }

    /******* Search Details Page Start *******/

    $scope.propertyDetails = function()
    {
        $scope.property_id=$routeParams.property_id;
        var httpRequest = $http({
        method: 'POST',
        url: base_url+'/hometheme/property-details',
        data: {property_id:$scope.property_id}
      
        }).success(function(data, status) { 

          $scope.property_det = data.property_det;
          $scope.latitude = my_lat = parseFloat(data.property_det.lat);
          $scope.longitude = my_lng = parseFloat(data.property_det.lng);
          $scope.address = address = data.property_det.address;
          console.log($scope.property_det); 
          /*console.log($scope.latitude); 
          console.log($scope.longitude); */

          initMap(); // init map call 
          setTimeout(function(){
            ProjectGallery();
            ImageFitCont();
          },100);

        });
    }

    /******* Search Details Page End *******/

    /* Show Hide Advanced Searched Filter */
    $scope.advanceOpen = function()
    {
      if($scope.advance_open ==0)
        $scope.advance_open =1;
      else
        $scope.advance_open=0;
        //console.log($scope.advance_open);
    }

    /**** Remove Search Filter ****/
    $scope.removeSearchFilter = function(index)
    {
      if(typeof $scope.advanced_search[index] !='undefined')
        {//alert('ss');
			delete $scope.advanced_search[index]; 
			console.log($scope.advanced_search);
				  $scope.searchProperty({location:$scope.search.location, property_type:$scope.search.property_type, bedroom:$scope.bedrating, bathroom:$scope.bathrating, min_price:$scope.search.min_price, max_price:$scope.search.max_price,search_listing_sort:$scope.search_listing_sort,limit:$scope.recordLimit,page:$scope.currentPage,advance_search:JSON.stringify($scope.advanced_search)});
                        
        }
        
        //console.log($scope.advanced_search);
    }

    /**/
    $scope.getOptionValue = function()
    {
      alert('rrr');
    }
	
	/* make active with active class advanced filter */
	$scope.$watch('advanced_search', function(newVal, oldVal){
        //console.log('changed');
		//console.log($scope.advanced_search);
				
		if(typeof $scope.advanced_search['short_sale'] !='undefined')
		{
			$scope.short_sale_active ='active';
		}
		else
		{
			$scope.short_sale_active ='';
		}
		if(typeof $scope.advanced_search['foreclosure'] !='undefined')
		{
			$scope.foreclosure_active ='active';
		}
		else
		{
			$scope.foreclosure_active ='';
		}
		if(typeof $scope.advanced_search['zip_code'] !='undefined')
		{
			$scope.zip_code_active ='active';
		}
		else
		{
			$scope.zip_code_active ='';
		}
		if(typeof $scope.advanced_search['county'] !='undefined')
		{
			$scope.county_active ='active';
		}
		else
		{
			$scope.county_active ='';
		}
		if(typeof $scope.advanced_search['development_name'] !='undefined')
		{
			$scope.development_name_active ='active';
		}
		else
		{
			$scope.development_name_active ='';
		}
		if(typeof $scope.advanced_search['subdivision_name'] !='undefined')
		{
			$scope.subdivision_name_active ='active';
		}
		else
		{
			$scope.subdivision_name_active ='';
		}
		if(typeof $scope.advanced_search['complex_name'] !='undefined')
		{
			$scope.complex_name_active ='active';
		}
		else
		{
			$scope.complex_name_active ='';
		}
		if(typeof $scope.advanced_search['hbaths'] !='undefined')
		{
			$scope.hbaths_active ='active';
		}
		else
		{
			$scope.hbaths_active ='';
		}
		if(typeof $scope.advanced_search['pet'] !='undefined')
		{
			$scope.pet_active ='active';
		}
		else
		{
			$scope.pet_active ='';
		}
		if(typeof $scope.advanced_search['pool'] !='undefined')
		{
			$scope.pool_active ='active';
		}
		else
		{
			$scope.pool_active ='';
		}
		if(typeof $scope.advanced_search['view'] !='undefined')
		{
			$scope.view_active ='active';
		}
		else
		{
			$scope.view_active ='';
		}
		if(typeof $scope.advanced_search['water_access'] !='undefined')
		{
			$scope.water_access_active ='active';
		}
		else
		{
			$scope.water_access_active ='';
		}
		if(typeof $scope.advanced_search['water_front'] !='undefined')
		{
			$scope.water_front_active ='active';
		}
		else
		{
			$scope.water_front_active ='';
		}
		if(typeof $scope.advanced_search['furnished'] !='undefined')
		{
			$scope.furnished_active ='active';
		}
		else
		{
			$scope.furnished_active ='';
		}
    }, true);

});


/* ANGULAR DIRECTIVES START */ //<span class="bath-label"></span>

angular.module('dpiApp')
.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
        '<li ng-class="star" ng-click="toggle1(0)">' +
            '<span class="zero-label">0</span>' +
            '</li>' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '<span class="bed-label"></span>' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];

                for (var i = 0; i < scope.max; i++) {

                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
                console.log(scope.stars);
            };

            scope.toggle = function (index) {
              
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    bedrating: index + 1
                });                
            };

            scope.toggle1 = function () // For 0 bath selected
            {
              scope.ratingValue = 0;
            }

            scope.$watch('ratingValue', function (newVal, oldVal) {
             
                if (typeof newVal !='undefined')
                {
                   updateStars();
                }
            });
        }
    }
}); 

/**************************************************************************************/

/****** ANGULAR  DIRECTIVE FOR BATHROOM ******/  

angular.module('dpiApp')
.directive('starRatingBath', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
        '<li ng-class="star" ng-click="toggle1(0)">' +
            '<span class="zero-label">0</span>' +
            '</li>' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '<span class="bath-label"></span>' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];

                for (var i = 0; i < scope.max; i++) {

                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
                console.log(scope.stars);
            };

            scope.toggle = function (index) {
              
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    bathrating: index + 1
                });                
            };

            scope.toggle1 = function () // For 0 bath selected
            {
              scope.ratingValue = 0;
            }

            scope.$watch('ratingValue', function (newVal, oldVal) {
             
                if (typeof newVal !='undefined')
                {
                   updateStars();
                }
            });
        }
    }
});

/***************************************************************************************/

/****** ANGULAR  DIRECTIVE FOR HALF BATH ROOMS ******/  

angular.module('dpiApp')
.directive('starRatingHalfBath', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
        '<li ng-class="star" ng-click="toggle1(0)">' +
            '<span class="zero-label">0</span>' +
            '</li>' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '<span class="bath-label"></span>' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];

                for (var i = 0; i < scope.max; i++) {

                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
                console.log(scope.stars);
            };

            scope.toggle = function (index) {
              
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    halfbath: index + 1
                });                
            };

            scope.toggle1 = function () // For 0 bath selected
            {
              scope.ratingValue = 0;
			  scope.bathrating=0;
            }

            scope.$watch('ratingValue', function (newVal, oldVal) {
             
                if (typeof newVal !='undefined')
                {
                   updateStars();
                }
            });
        }
    }
});

/***************************************************************************************/

/*********************** tOGGLE ADVANCE SEARCH SLIDE BUTTON START **********************/
angular.module('dpiApp')
.directive('slideable', function () {
    return {
        restrict:'C',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'opacity': 1,
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
})
.directive('slideToggle', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var target = document.querySelector(attrs.slideToggle);
            attrs.expanded = false;
            element.bind('click', function() {
              if(scope.search_but_show_hide ==1)
              {
                scope.search_but_show_hide = 0;
              }
              else
              {
                scope.search_but_show_hide = 1;
              }
              //alert(scope.search_but_show_hide);
                var content = target.querySelector('.slideable_content');
                if(!attrs.expanded) {
                    content.style.border = '1px solid rgba(0,0,0,0)';
                    var y = content.clientHeight;
                    content.style.border = 0;
                    target.style.height = y + 'px';
                } else {
                    target.style.height = '0px';
                }
                attrs.expanded = !attrs.expanded;
            });
        }
    }
});

/*********************** tOGGLE ADVANCE SEARCH SLIDE BUTTON END **********************/

   