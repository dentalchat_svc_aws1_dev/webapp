@extends('admin.layout.layout')
<script src='https://www.google.com/recaptcha/api.js'></script>

@section('content')
  <div class="tab"><div class="tab_cell"><div class="container-fluid">
    <div class="row">
    
      <div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><i class="fa fa-lock"></i>Login</div>
          <div class="panel-body">

            @include('admin.partials.errors')
           <!-- For For Got password Success mail -->
            @if(Session::has('success'))
              <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{!! Session::get('success') !!}</strong>
              </div>
            @endif

              @if(Session::has('error'))
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session::get('error') !!}</strong>
                </div>
              @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="form-group">
                <label class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-7">
                  <input type="email" class="form-control" name="email"
                         value="{{ $admin_email }}" autofocus>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Password</label>
                <div class="col-md-7">
                  <input type="password" class="form-control" name="password" value="{{ $admin_pass }}">
                </div>
              </div>
				
			  <div class="form-group">
			    <label class="col-md-4 control-label"></label>
                <div class="col-md-7">

                <!--locahost -->
					 <!-- <div class="g-recaptcha" data-sitekey="6Lf-GQsUAAAAAPayBxAZpQGTXI3WV1HdWsk4KFFa" style="width:100%;"></div> --> 


          <!-- 192.168.1.142 -->
          <!-- <div class="g-recaptcha" data-sitekey="6LeyDxUUAAAAALFC0ahCstHZvbql1MqxFWgFgprp" style="width:100%;"></div> -->

          
          <!-- 192.168.1.87 -->
          <div class="g-recaptcha" data-sitekey="6LehIR8UAAAAAB2hZ70XwebOQz8ovMq56QvBPU88"  style="width:100%;"></div>

          <!--demo.uiplonline.com -->
          <!-- <div class="g-recaptcha" data-sitekey="6Le6ORkUAAAAACp7pdToD7jmVX1jwsOly0q6cSwu"  style="width:100%;"></div> -->
          
                </div>
              </div>

              <div class="form-group">
              <label class="col-md-4 control-label"></label>
                <div class="col-md-7">
                  <input type="checkbox" name="remember_me" value="1" <?php if($admin_email!=""){ ?> checked <?php } ?>>&nbsp;&nbsp;<label class="control-label">Remember Me</label>
                </div>
              </div>
				
              <div class="form-group">
                <div class="col-md-7 col-md-offset-4">
                  <div class="checkbox">
                    <!-- <label>
                      <input type="checkbox" name="remember"> Remember Me
                    </label> -->
                    <label>
                      <a href="<?php echo url().'/admin/forgotpassword'?>"> Forgot Password ? </a>
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">Login</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div></div></div>
<style>
  .panel{
    box-shadow: 0 1px 1px rgb(34, 195, 225)
  }
</style>
@endsection