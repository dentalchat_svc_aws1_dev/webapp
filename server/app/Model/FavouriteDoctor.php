<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FavouriteDoctor extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'favourite_doctors';
    /*public function getUserDetail()
	{
		return $this->belongsTo('App\Model\SiteUser','user_id');
	}*/
}
