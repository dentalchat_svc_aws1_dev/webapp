</div>
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage simple builder
 * @since 1.0
 */
?>
<style>.term_list_specialist .term-show,.term_list_common_dental .term-show{
display:none;
}
</style>
<?php

$medicaldirectory_option_data =get_option('medicaldirectory_option_data');

$medicaldirectory_option_data['medicaldirectory-multi-footer-image']=1;
$medicaldirectory_option_data['medicaldirectory-multi-bottom-image']=1;
?>
<!-- Start Footer Switch -->

<?php if($medicaldirectory_option_data['medicaldirectory-footer-switch']){?>

<!-- Start medicaldirectory Multifooter -->

<!-- Start Footer 7 -->
<?php if(isset($medicaldirectory_option_data['medicaldirectory-multi-footer-image'])&&($medicaldirectory_option_data['medicaldirectory-multi-footer-image']==1)){?>
  <!-- uou block 4e -->
  <div class="uou-block-4e">
    <div class="container">
      <div class="row">
		  <!-- Contact us section -->
		  <div class="col-md-6 col-sm-6">
			  <?php
							  /** This filter is documented in wp-includes/default-widgets.php */


						$bg_image_default = medicaldirectory_IMAGE.'footer-map-bg.png';
						$title = 	(isset($medicaldirectory_option_data['medicaldirectory-title-contact']) ? $medicaldirectory_option_data['medicaldirectory-title-contact'] :'');
						$logo = 	(isset($medicaldirectory_option_data['medicaldirectory-footer-icon']) ? $medicaldirectory_option_data['medicaldirectory-footer-icon']['url'] : '' );
						$address = (isset($medicaldirectory_option_data['medicaldirectory-address-contact']) ? $medicaldirectory_option_data['medicaldirectory-address-contact'] :'');
						$phone_no = (isset($medicaldirectory_option_data['medicaldirectory-phone-contact']) ? $medicaldirectory_option_data['medicaldirectory-phone-contact'] :'' );
						$email = 	(isset($medicaldirectory_option_data['medicaldirectory-email-contact']) ? $medicaldirectory_option_data['medicaldirectory-email-contact']:'' );
						$bg_image = (isset($medicaldirectory_option_data['medicaldirectory-contact-bg-image']['url']) ? $medicaldirectory_option_data['medicaldirectory-contact-bg-image']['url']: $bg_image_default);
						if($bg_image==''){$bg_image=$bg_image_default;}

						?>

						<!-- <div class="col-md-3 col-sm-6"> -->

						<h5>Information</h5>

						<?php if ( ! empty( $logo ) ) { ?>

							<!-- <a href="#" class="logo footer-logo"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_html_e( 'image', 'medical-directory' ); ?>"></a> -->

						<?php } ?>



							<?php



							if ( ! empty( $bg_image ) ) {

								echo '<ul class="contact-info has-bg-image contain" data-bg-image="'.$bg_image.'">';

							}

							else{

								echo '<ul class="contact-info">';

							}



							if ( ! empty( $address ) ) {

								echo '<li><i class="fa fa-map-marker"></i><address>'.$address.'</address></li>';

							}



							if ( ! empty( $phone_no ) ) {

								echo '<li><i class="fa fa-phone"></i><a href="tel:#">'.$phone_no.'</a></li>';

							}



							if ( ! empty( $email ) ) {

								echo '<li><i class="fa fa-envelope"></i><a href="mailto:">'.$email.'</a></li>';

							}


							?>

						</ul>
		  </div>

         <!-- Start left footer sidebar -->

    <?php   if(isset($medicaldirectory_option_data['medicaldirectory-left-footer-switch'])){ ?>

            <?php

            if(is_active_sidebar('medicaldirectory_footer_left_sidebar')):

				dynamic_sidebar('medicaldirectory_footer_left_sidebar');

            endif;

            ?>

    <?php } ?>

      </div>
    </div>
  </div> <!-- end .uou-block-4e -->
  <?php } ?>


<?php } ?>

<!-- Start Bottom 7 -->
<?php if(isset($medicaldirectory_option_data['medicaldirectory-multi-bottom-image'])&&($medicaldirectory_option_data['medicaldirectory-multi-bottom-image']==1)){?>
  <!-- uou block 4b -->
  <!-- test-->
   <?php  
$site_url_dental= substr(site_url(),0,strpos(site_url(),"/",10));
?>
<section class="service-help clearfix">
   <div class="container">
     <div class="row clearfix">
        <div class="col-sm-12 col-md-12 clearfix">
          <ul>
           <li> <a href="mailto:Service@dentalchat.com" target="_top"> Need a Service Help? </a>  </li> <li> <a href="mailto:Service@dentalchat.com" target="_top"> Service@dentalchat.com </a> </li>
          </ul>   
         </div>
      </div>
   </div>
 </section>
  <div class="footer_top">
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-3  col-sm-6 col-xs-12  clearfix footer-block">
		  <h4>Dental Chat</h4>
              <ul class="link-ft">
                <li><a href="<?php echo $site_url_dental; ?>/">Home</a></li>
                <li><a href="<?php echo $site_url_dental; ?>/about">About us</a></li>
                <li><a href="<?php echo $site_url_dental; ?>/contact-us">Contact</a></li>
                <li><a href="http://dentalchat.com/blog" target="_blank">Blog</a></li>
                <li><a href="<?php echo $site_url_dental; ?>/faq">FAQ</a></li>
               </ul>
              <div class="clearfix"></div>

             
              <div class="clearfix"></div>
			  		
          </div><!--end col-->
		  
		<div class="col-md-3  col-sm-6 col-xs-12  clearfix footer-block">

		<h4>Common Dental Issues</h4>
        
		<ul class="link-ft term_list_common_dental">
		
		<li><a href="<?php echo $site_url_dental; ?>/tooth-abscess" >Tooth  Abscess</a></li>		
		<li><a href="<?php echo $site_url_dental; ?>/tooth-pain-decay">Tooth Pain / Decay</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/broken-filling">Broken Filling</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/broken-bonding">Broken Bonding</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/denture-repair">Denture Repair</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/root-canal">Root Canal</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/painful-gums">Painful Gum</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/tooth-ache">Tooth Ache</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/avulsed-teeth">Avulsed Teeth</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/losse-crowns">Loose Crowns</a></li>
        <li class="term-show"><a href="<?php echo $site_url_dental; ?>/dental-implant-failure">Dental Implant Failure</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/wisdom-teeth-pain">Wisdom Teeth Pain</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/teeth-sensitivity">Teeth Sensitivity</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/bad-breath">Bad Breath / Halitosis</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/grinding-teeth">Grinding Teeth / Bruxism</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/dental-dry-socket">Dental Dry Socket Problems</a></li>

		</ul>
		<div class="more_show_common_dental">+ More</div>
		<div class="less_show_common_dental" style="display:none;">- Less</div>

		</div><!--end col-->
		  
		<div class="col-md-3  col-sm-6 col-xs-12  clearfix footer-block">

		<h4>Dental&nbsp;Specialties</h4>

		<ul class="link-ft term_list_specialist">
		<li ><a href="<?php echo $site_url_dental; ?>/cosmetic-dentistry">Cosmetic Dentistry</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/dental-implant">Dental Implants</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/dental-public-health">Dental Public Health</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/endodontics">Endodontics</a></li>
		<li><a href="<?php echo $site_url_dental; ?>/general-dentistry">General Dentistry</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/periodontics">Periodontics</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/pediatrics-dentistry">Pediatric Dentistry</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/prosthodontics">Prosthdontics</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/orthodontics-orthopedics">Orthodontics and Dentofacial Orthopedics</a></li>
		<li class="term-show"><a href="<?php echo $site_url_dental; ?>/oral-surgery">Oral and Maxillofacial Surgery</a></li>
		</ul>
        <div class="more_show_specialist">+ More</div>
        <div class="less_show_specialist" style="display:none;">- Less</div>
		</div><!--end col-->
		
		<div class="col-md-3  col-sm-6 col-xs-12  clearfix footer-block">

        <h4>Are you a top Dentist ?</h4>

		<div class="footer-block-cnt">
		<a class="ft-btn" href="<?php echo $site_url_dental; ?>/dentist-signup">Join DentalChat</a>
		</div>

		<h4>Have a Dental Question ?</h4>

		<div class="footer-block-cnt">
        <!-- ngIf: is_front_logged==2 --><a ng-if="is_front_logged==2" href="<?php echo $site_url_dental; ?>/patient-profile/my-message/" class="ft-btn ng-scope" style="">Chat with a Dentist</a><!-- end ngIf: is_front_logged==2 -->
        <!-- ngIf: is_front_logged==1 -->
        <!-- ngIf: is_front_logged==0 || !is_front_logged -->
		
		</div>
		

		</div><!--end col-->
		  
		  
        </div>
      </div>
    </div>
    <div class="footer_mid">
        <div class="container">
            <h4>Other</h4>
                <ul class="link-ft">
                 <li><a href="<?php echo $site_url_dental; ?>/cosmetic-dentistry">Cosmetic Dentistry Procedures</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/oral-pathology">Common Oral Diseases / Oral Pathology Problems</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/sansitivity">Dental Pain from Teeth Sensitivity</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/emergency-dentist-question">Emergency Dentist Questions</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/emergency-dental-care-question">Emergency Dental Care Question / Ask a Local Dentist</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/getting-braces-orthodontist-treatment-info">Getting Braces – Orthodontist Treatment Info</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/local-dentists-communication">Local Dentists Communications</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/local-emergency-dental-questions-online">Local Emergency Dental Questions Online</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/online-emergency-dentist-question">Online Emergency Dentist Question</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/local-emergency">Local Emergency Dentist Chat</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/local-dental-chatting-online">Local Dental Implants / Dental Implant Failures</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/local-root-canal-chat">Local root Canal Chat</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/online-dental-communication">Online Dental Communication</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/online-dentists-chat-blog">Online Dentists Chat Blog</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/online-dentists-marketing">Online Dentists Marketing</a></li>
                 <li><a href="<?php echo $site_url_dental; ?>/painful-gums">Periodontal Gum Problems</a></li>
              
               </ul>
        </div>
        
    </div>
  <!--End Test-->
  <div class="uou-block-4a secondary">
    <div class="container">

      <?php if(isset($medicaldirectory_option_data['medicaldirectory-show-footer-copyrights'])){?>
      <p>
          
        <?php if(isset($medicaldirectory_option_data['medicaldirectory-show-footer-credits'])) {?>
		<a href="http://dentalchat.com/privacy-policy/" target="_blank">Privacy Policy</a> 
		<a href="http://dentalchat.com/terms-and-conditions/" target="_blank">Terms and condition</a>
        <?php } ?>
        <?php if(isset($medicaldirectory_option_data['medicaldirectory-copyright-text'])&&!empty($medicaldirectory_option_data['medicaldirectory-copyright-text'])) {?>
        <?php
				if(isset($medicaldirectory_option_data['medicaldirectory-copyright-link']) && $medicaldirectory_option_data['medicaldirectory-copyright-link']!='' ){?>

					<a  href="http://<?php echo esc_html($medicaldirectory_option_data['medicaldirectory-copyright-link']);?> "><?php echo esc_html($medicaldirectory_option_data['medicaldirectory-copyright-text']);?></i></a>  
					<?php
				}else{
					echo esc_html($medicaldirectory_option_data['medicaldirectory-copyright-text']);
				}



				?>
        <?php } ?>
        <?php bloginfo('name'); ?>.
        
        <?php if(isset($medicaldirectory_option_data['medicaldirectory-after-copyright-text'])&&!empty($medicaldirectory_option_data['medicaldirectory-after-copyright-text'])) {?>
        <?php echo esc_html($medicaldirectory_option_data['medicaldirectory-after-copyright-text']); ?>
        <?php } ?>

      </p>
      <?php } ?>


    <!-- Start sccial Profile -->

   <!-- <?php if(isset($medicaldirectory_option_data['medicaldirectory-social-profile'])){?>

      <ul class="social-icons">

        <?php if(isset($medicaldirectory_option_data['medicaldirectory-facebook-profile']) && !empty($medicaldirectory_option_data['medicaldirectory-facebook-profile'])) : ?>
        <li><a  href="http://<?php echo esc_url($medicaldirectory_option_data['medicaldirectory-facebook-profile']);?> "><i class="fa fa-facebook"></i></a></li>
        <?php endif; ?>

        <?php if(isset($medicaldirectory_option_data['medicaldirectory-twitter-profile']) && !empty($medicaldirectory_option_data['medicaldirectory-twitter-profile'])) : ?>
        <li><a  href="http://<?php echo esc_url($medicaldirectory_option_data['medicaldirectory-twitter-profile']);?> "><i class="fa fa-twitter"></i></a></li>
        <?php endif; ?>

        <?php if(isset($medicaldirectory_option_data['medicaldirectory-google-profile']) && !empty($medicaldirectory_option_data['medicaldirectory-google-profile'])) : ?>
        <li><a  href="http://<?php echo esc_url($medicaldirectory_option_data['medicaldirectory-google-profile']);?> "><i class="fa fa-google"></i></a></li>
        <?php endif; ?>

        <?php if(isset($medicaldirectory_option_data['medicaldirectory-linkedin-profile']) && !empty($medicaldirectory_option_data['medicaldirectory-linkedin-profile'])) : ?>
        <li><a  href="http://<?php echo esc_url($medicaldirectory_option_data['medicaldirectory-linkedin-profile']);?> "><i class="fa fa-linkedin"></i></a></li>
        <?php endif; ?>

        <?php if(isset($medicaldirectory_option_data['medicaldirectory-pinterest-profile']) && !empty($medicaldirectory_option_data['medicaldirectory-pinterest-profile'])) : ?>
        <li><a  href="http://<?php echo esc_url($medicaldirectory_option_data['medicaldirectory-pinterest-profile']);?> "><i class="fa fa-pinterest"></i></a></li>
        <?php endif; ?>

      </ul>

    <?php }?>-->
    <div class="footer_socialBox">
				<ul ng-init="socailLink('8,9,10,11,12')">
				<li><a href="https://www.facebook.com/dentalchat/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="https://twitter.com/dentalchat" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="https://www.instagram.com/getdentalchat/?hl=en" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="https://www.youtube.com/channel/UCU4cLVgp9i-2hDvzCewiIxw" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
				<li><a href="https://plus.google.com/115274381124451985510" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				</ul>
				</div>
    <!-- end of social profile -->

    </div>
  </div>
  <!-- end .uou-block-4a -->
    <?php } ?>

<div class="close-chat">X</div>
 <script>
  jQuery(function(){
    jQuery('.blog .breadcrumbs li:first-child a').attr('href','https://dentalchat.com');
  });
 </script>
 <script>
    /*Audio Video Image*/
jQuery("#profile_image").on('change', function () {
    //alert('sdfs');
 
    var imgPath = jQuery(this)[0].value;
    var file_size = jQuery(this)[0].files[0].size;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    var image_holder = jQuery("#profile-image-show");
	if(file_size>=2097152) { 
	 alert("Audio/Video/Image size should be less then 2MB");
	 this.value = null;
	  image_holder.empty();
	 return false;
	}else if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" ||  extn == "mp4" ||  extn == "mp3" ) {
        if (typeof (FileReader) != "undefined") {
            image_holder.empty();
            var reader = new FileReader();
            reader.onload = function (e) {
                if(extn == "mp4"){ 
                      
                jQuery(image_holder).append('<video width="320" height="240" controls><source src="'+e.target.result+'" type="video/mp4"></video>');
              
                }else if(extn == "mp3"){
                     jQuery(image_holder).append(' <audio controls><source src="'+e.target.result+'" type="audio/mp3"></audio>');
                  
                }else{
               jQuery(image_holder).append('<img src="'+e.target.result+'" height="100px;" width="100px;"/>');
}
            }
            image_holder.show();
            reader.readAsDataURL(jQuery(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    } else {
        alert("Please select file format or size");
    }
});
/*Thumbnail Image*/
jQuery("#thumbnail_image").on('change', function () {
    //alert('sdfs');

    var imgPath = jQuery(this)[0].value;
    var file_size = jQuery(this)[0].files[0].size;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    var thumb_holder = jQuery("#thumbnail-image-show");
    
    if(file_size>=2097152) { 
	 alert("Image size should be less then 2MB");
	 this.value = null;
	 thumb_holder.empty();
	 return false;
	}if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
        if (typeof (FileReader) != "undefined") {
            
            thumb_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                jQuery("<img />", {
                    "src": e.target.result,
                        "class": "thumb-image",
                        "height":"120px",
                        "width":"auto"
                }).appendTo(thumb_holder);
 
            }
            thumb_holder.show();
            reader.readAsDataURL(jQuery(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    } else {
       var thumb_holder1 = jQuery("#thumbnail-image-show");
       thumb_holder1.empty();
        alert("Please select format like JPG,JPEG,GIF,PNG");
        
    }
});
/*  jQuery('.post_form_submit_btn').prop('disabled', true);
     jQuery('input[id="title"]').blur(function() {
        if(jQuery(this).val().trim() != '') {
           jQuery('.post_form_submit_btn').prop('disabled', false);
        }else{
             jQuery('.post_form_submit_btn').prop('disabled', true);
        }
     });*/
     
$( document ).ready(function() {
 $('.more_show_common_dental').on('click',function(){
     //alert('hello');
      $('.term_list_common_dental .term-show').toggleClass('show' );
     $('.less_show_common_dental').show();
     $(this).hide();
      
     
 });  
  $('.less_show_common_dental').on('click',function(){
    $('.term_list_common_dental .term-show').toggleClass('show' );
     $(this).hide();
     $('.more_show_common_dental').show();
  });  
  
$('.more_show_specialist').on('click',function(){
      $('.term_list_specialist .term-show').toggleClass('show' );
     $('.less_show_specialist').show();
     $(this).hide();
 }); 
 
   $('.less_show_specialist').on('click',function(){
    $('.term_list_specialist .term-show').toggleClass('show' );
     $(this).hide();
     $('.more_show_specialist').show();
  });  
/*$('ul.term-list').each(function(){
  var LiN = $(this).find('li').length;
  if( LiN >5){    
    $('li', this).eq(2).nextAll().hide().addClass('toggleable');
    $(this).append('<li class="more">+ More</li>');    
  }
});
$('ul.term-list').on('click','.more', function(){
  if( $(this).hasClass('less') ){    
    $(this).text('+ More').removeClass('less');    
  }else{
    $(this).text('- Less').addClass('less'); 
  }
  $(this).siblings('li.toggleable').slideToggle();
}); */
}); 


</script>  
<?php wp_footer(); ?>

<!-- Include all compiled plugins (below), or include individual files as needed -->
</body>
</html>