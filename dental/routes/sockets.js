var Chat = require('../models/chat');

var lookups = [];
var Setlookup = function (storable, check) {
    var returnval = {};
    returnval.status = 0;
    returnval.buddy_socket_id = '';
    for (var d in lookups) {
        //---checking the global object if this connection exists or not----//////
        //----then just update self socket id-------------///////////////////
        if (lookups[d].from_username == storable.from_username && lookups[d].to_username == storable.to_username && lookups[d].post_id == storable.post_id) {
            if (check == 'push') {
                returnval.status = 1;
                lookups[d].socket_id = storable.socket_id;
                break;
            }
            if (check == 'get') {
                returnval.status = 1;
                returnval.buddy_socket_id = lookups[d].socket_id;
                break;
            }
        }
    }
    return returnval;
}




exports.initialize = function (server) {

    var io = require('socket.io')(server);
    var chatObj = [];
    ///////////////////------start Of connection-------//////////////////  
    io.on("connection", function (socket) {
        ///////////////////------start SOcket On set name-------//////////////////
        socket.on("set_name", function (data) {

            console.log("data", data);
            socket.user = data.from;
            socket.fromName = data.senderName;
            socket.receiverName = data.receiverName;
            socket.post_id = data.post_id;
             //Comment By Shailendra - 5 May 2018
         if (data.from > data.buddy) 
          {
             socket.uniquecode = data.from + '_' + data.buddy;
          } 
          else 
          {
               socket.uniquecode = data.buddy + '_' + data.from;
          }
        
        var uniquecode_fb = data.from + '_' + data.buddy;
        var uniquecode_bd = data.buddy + '_' + data.from;
        //Add By Shailendra - 5 May 2018
 //    socket.uniquecode = data.buddy + '_' + data.from;
//console.log("UserType", data.usertype);
//if(data.usertype == "Patient")
//socket.uniquecode = data.from + '_' + data.buddy;
//else
//socket.uniquecode = data.buddy + '_' + data.from;
//alert(socket.uniquecode);
//console.log(socket.uniquecode);
            //socket.receiver = data.buddy;
            socket.Room = data.roomName;
            console.log("set room:" + socket.Room);

            socket.emit('name_set', data);
            var chatHolder = [];
            Chat.find({
                    $or: [{
                        'uniquecode': uniquecode_fb
                    }, {
                        'uniquecode': uniquecode_bd
                    }],
                  //  uniquecode: socket.uniquecode,
                  //  post_id: socket.post_id
                    post_id: socket.post_id

                }, function (err, chat) {
                    if (err) {
                        console.log(err);
                    } else {

                        socket.send(chat);

                    }
                })
                .limit(50)
                .sort({
                    $natural: -1
                });

            //console.log(socket);
            if (socket.Room == "") {
                //---storing self data in a object--////
                socket.receiver = data.buddy;
                var storable = {};
                storable.from_username = socket.user;
                storable.to_username = socket.receiver;
                storable.socket_id = socket.id;
                storable.post_id = socket.post_id;

                console.log(JSON.stringify(storable));
                //-----setting the lookup in Setlookup function above--------/////////////

                var check = "push";
                var status = Setlookup(storable, check);
                //--------else store the full data of connection in global object--------///////////
                if (!status.status)
                    lookups.push(storable);
                console.log(lookups);
            } else {
                socket.receiver = socket.Room;
                console.log("set Room name:" + socket.Room);
                socket.join(socket.Room);
            }

        });
        ///////////////////------end SOcket On set name-------//////////////////
        ///////////////////------start of SOcket On send message-------//////////////////
        socket.on('message', function (message) {
            //JSON.stringify(message);
     //       alert(socket.uniquecode);
//console.log(socket.uniquecode);
            console.log("message", message);
            message = JSON.parse(message);
            //console.log(message.type);
            console.log("get user:" + socket.user);
            console.log(message.type);
            if (message.type == "userMessage") {
                console.log('s');
                Chat.create({
                    from: socket.user,
                    to: socket.receiver,
                    message: message.message,
                    uniquecode: socket.uniquecode,
                    post_id: socket.post_id,
                    chat_file: message.file_name,
                    file_ext: message.file_type,
                    senderName: socket.fromName,
                    receiverName: socket.receiverName,
                    status: message.status,
                    createdAt: new Date()

                }, function (err, chat) {
                    console.log(err);
                    console.log(chat);
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(chat);
                        message.username = socket.user;
                        message.from = socket.user;
                        message.to = socket.receiver;
                        message.senderName = socket.fromName;
                        message.receiverName = socket.receiverName;
                        message.post_id = socket.post_id;
                        message._id = chat._id;
                       // message.createdAt = new Date() .toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                         message.createdAt = new Date();
                        //message.username = socket.user;
                        //message.username = socket.user;
                        if (socket.Room == "") {
                            console.log("single Chat");
                            //-----setting the clients data---to know clients socket id-----///    
                            var storable = {};
                            storable.from_username = socket.receiver;
                            storable.to_username = socket.user;
                            storable.post_id = socket.post_id;
                            var check = "get";
                            var status = Setlookup(storable, check);
                            if (io.sockets.connected[status.buddy_socket_id] && status.status == 1) {
                                console.log(message);
                                io.sockets.connected[status.buddy_socket_id].emit('message', JSON.stringify(message));
                                io.sockets.connected[status.buddy_socket_id].emit('send_message', JSON.stringify(message));
                            }
                            //if (status.buddy_socket_id != socket.id) {
                            //  message.type = "myMessage";
                            // io.sockets.connected[socket.id].emit('message', JSON.stringify(message));
                            //}
                        } else {
                            console.log("get Room name:" + socket.Room);
                             console.log(message);
                            io.sockets.in(socket.Room).emit('message', JSON.stringify(message));
                            io.sockets.in(socket.Room).emit('send_message', JSON.stringify(message));
                        }
                    }
                });

            }
        });
        ///////////////////------end SOcket On send message-------//////////////////

        ///////////////////------start of SOcket On send message covetus-------//////////////////
        socket.on('send_message', function (message) { console.log('send_message'); });
        ///////////////////------end SOcket On send message covetus-------//////////////////
        /*file upload socketing*/
        socket.on('file-uploaded', function (uploadedData) {
            //var type = JSON.parse(typeData);
            var from = uploadedData.from;
            var to = uploadedData.to;
            var storable = {};
            storable.from_username = to;
            storable.to_username = from;
            storable.post_id = uploadedData.post_id;
            var check = "get";
            uploadedData.createdAt = new Date(uploadedData.createdAt)
                .toLocaleTimeString()
                .replace(/(.*)\d\d+/, '$1');
            var status = Setlookup(storable, check);
            io.sockets.connected[socket.id].emit('file-uploaded', JSON.stringify(uploadedData));
            io.sockets.connected[socket.id].emit('send_message', JSON.stringify(uploadedData));
            if (io.sockets.connected[status.buddy_socket_id] && status.status == 1) {
                io.sockets.connected[status.buddy_socket_id].emit('file-uploaded', JSON.stringify(uploadedData));
                io.sockets.connected[status.buddy_socket_id].emit('send_message', JSON.stringify(uploadedData));
            }
        });


        /*Chat close*/

        socket.on('close_message', function (uploadedData) {
            var chatData = JSON.parse(uploadedData);
            var p_id = chatData.patient_id;
            var d_id = chatData.doctor_id;
            var storable = {};
            storable.from_username = d_id;
            storable.to_username = p_id;
            storable.post_id = uploadedData.post_id;
            var check = "get";
            var status = Setlookup(storable, check);

            if (io.sockets.connected[status.buddy_socket_id] && status.status == 1) {
                io.sockets.connected[status.buddy_socket_id].emit('close_message', JSON.stringify(chatData));
            }
        });
    });
    ///////////////////------end Of connection-------//////////////////  
    server.listen(8005, function () {
        console.log('listening on *:8005');
    });

    /*httpsServer.listen(8005, function() {
        console.log('listening on *:8005');
    });*/
};




//socket.broadcast.to(Room).emit('message',JSON.stringify(message));
//socket.in(socket.room).broadcast.send(JSON.stringify(message));
//socket.broadcast.to('room1').emit('message', JSON.stringify(message));
//io.sockets.connected['room1'].emit('message', JSON.stringify(message));
//io.to('room1').emit('message', JSON.stringify(message));  
//var clients = io.sockets.clients();
// var clients_in_the_room = io.sockets.adapter.rooms[roomId]; 
//  for (var clientId in clients_in_the_room ) {
//    console.log('client: %s', clientId); //Seeing is believing 
//    var client_socket = io.sockets.connected[clientId];//Do whatever you want with this
// }
//console.log(io.sockets.adapter.rooms['room1']);



// socket.on('create', function(room) {

//          socket.Room = room.roomName;
//          console.log("set Room name:"+socket.Room);
//          socket.join(socket.Room);
//      });