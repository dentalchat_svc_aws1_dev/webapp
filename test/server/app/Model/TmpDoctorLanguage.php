<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TmpDoctorLanguage extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'tmp_doctor_languages';
}

