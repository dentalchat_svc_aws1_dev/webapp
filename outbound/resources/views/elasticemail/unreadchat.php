<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Dental Chat</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!--<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">-->
		<!-- <link href="https://getpiggybank.com/wp-content/themes/twentythirteen/include/css/font.css" rel="stylesheet"> -->
		<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Lato:300,400,700,900);
body {
	overflow-y: scroll;
	overflow-x: hidden;
}
body {
	/**/
	font-family: 'Lato', sans-serif;
}
* {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: none;
	-webkit-text-resize: 100%;
	text-resize: 100%;
}
a {
	outline: none;
	color: #40aceb;
	text-decoration: underline;
}
a:hover {
	text-decoration: none !important;
}
.title a:hover {
	text-decoration: underline !important;
}
.title-2 a:hover {
	text-decoration: underline !important;
}
.btn:hover {
	opacity: 0.8;
}
.btn a:hover {
	text-decoration: none !important;
}
.btn {
	-webkit-transition: all 0.3s ease;
	-moz-transition: all 0.3s ease;
	-ms-transition: all 0.3s ease;
	transition: all 0.3s ease;
}
table td {
	border-collapse: collapse !important;
}
.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div {
	line-height: inherit;
}
.logo_img img {
	width: 79%;
	padding: 5px 0;
	text-align: center;
	margin: 0 auto;
	display: inline-block;
	float: none;
}
td .phone_img {
	width: 299px !important;
}
td.portfolio_sec {
	background-image: url("http://outbound.dentalchat.com/public/images/footer-bg.jpg");
}
.custom_title:after {
    content: "";
    display: block;
    background: linear-gradient(to right, rgb(255, 255, 255), rgb(46, 49, 146), rgb(255, 255, 255));
    width: 100%;
    height: 1px;
    margin: 10px 0 0 0;
}
hr {
    display: block;
    background: linear-gradient(to right, rgb(255, 255, 255), rgb(46, 49, 146), rgb(255, 255, 255));
    width: 100%;
    height: 1px;
    margin: 10px 0;
    border: 0;
}			
td.footer td {
    text-align: center;
}
.footer a {
    color: #fff;
    line-height: 22px;
}
			
@media only screen and (max-width:500px) {
table[class="flexible"] {
	width: 100% !important;
}
.module2_section img, .module3_section img, .module4_section img, .module5_section img, .module6_section img {
	width: 100% !important;
	height: auto !important;
}
.portfolio_sec {
	padding: 50px 5px 0 5px !important;
}
table[class="center"] {
	float: none !important;
	margin: 0 auto !important;
}
.logo_img {
	padding-left: 0 !important;
}
*[class="hide"] {
	display: none !important;
	width: 0 !important;
	height: 0 !important;
	padding: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important;
}
td[class="img-flex"] img {
	width: 100% !important;
	height: auto !important;
}
td[class="aligncenter"] {
	text-align: center !important;
}
th[class="flex"] {
	display: block !important;
	width: 100% !important;
}
td[class="wrapper"] {
	padding: 0 !important;
}
td[class="nav"] {
	padding: 20px 0 0 !important;
	text-align: center !important;
}
td[class="h-auto"] {
	height: auto !important;
}
td[class="description"] {
	padding: 30px 20px !important;
}
td[class="i-120"] img {
	width: 135px !important;
	height: auto !important;
}
td[class="footer"] {
	padding: 5px 20px 20px !important;
}
td[class="footer"] td[class="aligncenter"] {
	line-height: 25px !important;
	padding: 20px 0 0 !important;
}
tr[class="table-holder"] {
	display: table !important;
	width: 100% !important;
}
th[class="thead"] {
	display: table-header-group !important;
	width: 100% !important;
}
th[class="tfoot"] {
	display: table-footer-group !important;
	width: 100% !important;
}
td .phone_img {
	width: 209px !important;
}
}
</style>
		</head>
		<body style="margin:0; padding:0;" bgcolor="#eaeced" >
        <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
          <!-- fix for gmail -->
          <tr>
            <td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                <tr>
                <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
              </tr>
              </table></td>
          </tr>
            <tr>
          
            <td class="wrapper" style="padding:0 10px;">
          
          <!-- module 1 -->
          <table class="module1_section" data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                  <tr>
                  <td style="padding:0px; background-color: #2e3192;border-bottom: 1px solid #eaeced;"><table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                      <th class="flex" width="113" align="center" style="padding:0;"> <table class="center" cellpadding="0" cellspacing="0">
                          <tr>
                          <td class="logo_img" style="line-height:0; padding-left: 0px; text-align: center;"><a target="_blank" style="text-decoration:none;" href="index.html"><img src="<?php echo url()?>/public/images/logo.png" border="0" color:#606060;" align="left" vspace="0" hspace="0" alt="" /></a></td>
                        </tr>
                        </table>
                        </th>
                    </tr>
                    </table></td>
                </tr>
                </table></td>
            </tr>
          </table>
          
          <!-- module 4 -->
          <table class="module3_section" data-module="module-4" data-thumb="thumbnails/04.png" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                  <tr>
                  <td data-bgcolor="bg-block" class="holder" style="padding:25px 60px 13px; background: #fff;" bgcolor="#fff"><table width="100%" cellpadding="0" cellspacing="0">
                      
					<tr>
              <td id="top_part" height="65" background="<?php echo url()?>/public/images/top_image.png" style="background-color: #fff;background-repeat:no-repeat;background-size:100% 100%;background-position:right 0;"></td>
            </tr>
							  <tr style="background-color:#f8f8f8;">
                      <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title custom_title" align="center" style="font-size:21px; color:#292c34; padding:0 0 23px; text-transform: capitalize;">  You have unread messages about the post <?php echo $name;?> 
 </td>
                    </tr>
                    

							  <tr style="background-color:#f8f8f8;"><td><hr></td></tr>
					<tr>
              <td id="top_part" height="65" background="<?php echo url()?>/public/images/bot_image.png" style="background-color: #fff;background-repeat:no-repeat;background-size:100% 100%;background-position:right 0;"></td>
            </tr>		  
							  <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td align="center" style="font-weight: bold;"><strong>Thanks,</strong></td>
                    </tr>
                      <tr style="height: 10px;">
                      <td align="center"></td>
                    </tr>
                      <tr>
                      <td align="center">Your DentalChat Team </td>
                    </tr>
                    </table></td>
                </tr>
                </table></td>
            </tr>
          </table>
          <!-- module 5 -->
          <table class="module4_section" data-module="module-5" width="100%" cellpadding="0" cellspacing="0">
              <tr>
            
              <td data-bgcolor="bg-module" bgcolor="#eaeced">
            
            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
              <tr>
                  <td class="portfolio_sec" data-bgcolor="bg-block" class="holder" style="padding:17px 0 0 0;position:relative;" bgcolor="#B1584A">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#f9f9f9;" class="title" align="center" style="font-size:29px; color:#fff; padding:0 0 23px;"><a href="mailto:Service@dentalchat.com" style="font-size: 19px;color: #0f0f0f;text-decoration: none;font-weight: 700;
" target="_top">Need Help?  service@dentalchat.com</a></td>
                  </tr>
                  
                  <!-- Get Piggy -->
                  
                </table>
                </td>
              
                </tr>
              
            </table>
              </td>
            
              </tr>
            
          </table>
          <!-- module 7 -->
          <table class="module6_section" data-module="module-7" data-thumb="thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td data-bgcolor="bg-module"><table class="flexible" width="600" align="center" style="margin:0 auto; background:#2e3192;" cellpadding="0" cellspacing="0">
                  <tr>
                  <td class="footer" style="padding:18px 10px 9px 10px;"><table width="100%" cellpadding="0" cellspacing="0">
                      <tr class="table-holder">
                      
                      <th class="thead" width="" align="center" style="vertical-align:top; padding:0;"> <table class="center" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                          <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;"><a target="_blank" style="text-decoration:none;" href="https://www.facebook.com/covetus"><img src="<?php echo url()?>/public/images/ico-facebook.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="6" height="13" alt="fb" /></a></td>
                          <td width="20"></td>
                          <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;"><a target="_blank" style="text-decoration:none;" href="https://twitter.com/CovetusLLC"><img src="<?php echo url()?>/public/images/ico-twitter.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="tw" /></a></td>
                          <td width="19"></td>
                          <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;"><a target="_blank" style="text-decoration:none;" href="https://plus.google.com/+Covetus"><img src="<?php echo url()?>/public/images/ico-google-plus.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="19" height="15" alt="g+" /></a></td>
                          <td width="20"></td>
                          <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;"><a target="_blank" style="text-decoration:none;" href="https://www.linkedin.com/company/covetus-llc"><img src="<?php echo url()?>/public/images/ico-linkedin.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="in" /></a></td>
                        </tr>
                        </table>
                        </th>
                       </tr> 
                       <tr>
                         <td><hr></td>
                       </tr>
                       <tr class="table-holder">
                        <th class="tfoot" width="" align="center" style="vertical-align:top; padding:0;"> <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 10px;">
                              You are receiving Digest emails. <a href="https://dentalchat.com/dentistemailunsubscribe/<?php echo $user_id;?>">Unsubscribe</a><br>
                              This email was intended for <?php echo $firstname.' '.$lastname;?>. <a href="#">Learn why we included this.</a><br>
                              If you need assistance or have questions, please contact Dentalchat <a href="#">Customer Service.</a><br>
                              @<?php echo date('Y'); ?> DentalChat Corporation, 12231 Newport Ave, Santa Ana, CA 92705.
                          </td>
                          </tr>
                        </table>
                        </th>
                    </tr>
                    </table></td>
                </tr>
                </table></td>
            </tr>
          </table>
            </td>
          
            </tr>
          
          <!-- fix for gmail -->
          <tr>
            <td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
          </tr>
        </table>
</body>
</html>