@extends('admin/layout/admin_template')
 
@section('content')
<style>
.country input{margin-right:0px;}
#search_key{
	margin-right:10px;
}
.country select{margin-right:10px;}

.country .btn{
	    position: relative;
    top: -5px;
}

.country tr{
	float:right;
	width:100%;

}

.country select {
    margin-right: 10px;
    width: 190px;
}


.country button, .country input[type="button"], .country input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer;
    width: 90px;
}
.tractive td{
    background-color: #ffe7dc !important;
}
.searchtr{
    /*margin-bottom: 17px;border-bottom: 1px solid #ccc;padding-bottom:9px;*/
}
</style>
@if(Session::has('success_message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success_message') }}</p>
@endif

@if(Session::has('failure_message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('failure_message') }}</p>
@endif

<script type="text/javascript">
function add_blog(){

    window.location = '<?php echo url(); ?>/admin/add/testimonial';
}
</script>


<table cellpadding="0" cellspacing="0" border="0" width="100%"> 
        <tbody><tr class="searchtr">
           
            <td width="50%" style="padding:0px 1px; text-align:right;">
                <input type="button" onclick="add_blog();" class="btn" name="btn_search_hotel" id="btn_search_hotel" value="Add New Testimonial">
				
            </td>
            </tr>
		<tr>
             
        </tr>
    </tbody></table>


{!! Form::open(['url' => 'admin/hotel-list','method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'']) !!}
<div class="module">
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped  display user-datatable" width="100%">
    <thead>
        <tr style="color:#FFFFFF; background:#141b27;">
            <th>Id</th>
			<!-- <th widht="5%" style="align:center;"><input type="checkbox" name="check_all_records" id="check_all_records" onclick="selectAllRecords();" /></th> -->
			<th width="25%">Name</th>
            <th width="25%">Occupation</th>
            <th width="25%">Feedback</th>
            <th width="20%">Posted Date</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
</table>    
</div>
</div>
{!! Form::close() !!}

<script>
$(document).ready(function($) {
    var job_cat = $('#job_cat').val();
    var status_value = $("#is_active").val();
     $(".user-datatable").dataTable({
                   "iDisplayLength": 10,
                   "displayStart": 0,
                   "searching": false,
                   "paging": true,
                    "bPaginate": true,
                    "sPaginationType": "full_numbers",
                    "bFilter": false,
                    "bInfo": false,
                    "pagingType": "full_numbers",
                    language : {
                      sLengthMenu: "Show _MENU_",
                      emptyTable: "No record found",
                      processing: ""
                    },
                    processing: true,
                    serverSide: true,
                    ajax: '<?php echo url();?>/admin/testimonial/ajax-testimonial/',
                    columns: [
                        { data: 'id', name: 'id', "bVisible": false},
                      //  { data: 'checkbox_td', name: 'checkbox_td',"bSortable": false},
                        { data: 'reviewer_name', name: 'reviewer_name' },
                        { data: 'occupation', name: 'occupation' },
                        { data: 'feedback', name: 'feedback' },

						{ data: 'posted_date', name: 'posted_date' },
                        { data: 'action', name: 'action',"bSortable": false,'sClass' : 'action-btns'},
                    ],
                    aaSorting: [[0, 'DESC']],
                    // "fnDrawCallback": function(oSettings) {
                    //     if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                    //         $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                    //         $("#DataTables_Table_0_length").hide();
                    //     }
                    // }
            });
     $("th").removeClass("action-btns");
});
function changeJobStatus(status,id){
    var confrim_msg = confirm("Are you sure you want to change status of this Job?");
    if(confrim_msg){
        $('#success_status_span_'+id).html('wait....');
        $('#success_status_span_'+id).show();
        $.ajax({
            url: base_url+'/admin/jobs/status',
            type: "get",
            data: { status:status,id:id},
            success: function(data){
                if(data == '1'){
                    $('.alert-success').html('');
                    $('#success_status_span_'+id).html('Status updated.');
                    $('#success_status_span_'+id).fadeIn('slow');
                    $('#success_status_span_'+id).fadeOut('slow');
                    $("#user_active_"+id).attr('disabled',true);
                }
            }
        });
    }else{
        $("#user_active_"+id).val(0);
    }

}

function removetestimonial(id){
    var confirm_msg = confirm("Are you sure you want to delete this Testimonial?");
    if(confirm_msg){
        window.location.href = site_url+"/admin/deletetestimonial/"+id;
    }
}
</script>
{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
@endsection
