<div class="navbar navbar-fixed-top admin-header">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
				<i class="icon-reorder shaded"></i></a><a class="brand admin-logo" href="<?php echo url();?>/admin/home"><img src="<?php echo url();?>/public/backend/images/logo.png" alt="logo"></a>
			<a href="javascript:void(0);" class="makeleftcollapse pull-left"><i class="fa fa-bars" aria-hidden="true"></i></a>	
			<div class="nav-collapse collapse navbar-inverse-collapse">
			   	
				<ul class="nav pull-right">
					
					<!-- <li><a href="#">{{ Auth::user()->name }}  </a></li> -->
					<li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  	 <!--  <?php if(Auth::user()->admin_icon != ''){ ?>
						<img src="<?php echo url();?>/uploads/admin_profile/{!! Auth::user()->admin_icon !!}" class="nav-avatar" />
						<?php }else{ ?>
						<img src="<?php echo url();?>/public/backend/images/icons/NoAvatar_member.png" class="nav-avatar" />
				  	<?php } ?> -->
				  	<strong>{{ Auth::user()->name }}  </strong>
						<b class="caret"></b></a>
						<ul class="dropdown-menu">
						   
							<li><a href="<?php echo url();?>/admin/admin-profile">Edit Profile</a></li>
							<li><a href="<?php echo url();?>/admin/change-password">Change password</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo url();?>/auth/logout">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- /.nav-collapse -->
		</div>
	</div>
	<!-- /navbar-inner -->
</div>
