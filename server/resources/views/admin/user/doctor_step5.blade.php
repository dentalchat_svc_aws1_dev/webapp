@extends('admin/layout/admin_template')

@section('content')
	{!! Form::open(['url' => 'admin/dentist/dentist-step5/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step5']) !!}
		
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}
		{!! Form::hidden('data_count',count($doc_exp),['class'=>'span8','id'=>'data_count']) !!}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCECNx6YKAjaYfP9Eq7FXAMB1QmjUKvMZk&libraries=places"></script>
		<?php 
		$i =0;
		if(!empty($doc_exp)){
			foreach($doc_exp as $eachdoc){	
		?>
		<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
        
        <?php if($i>0){?>
        <div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeExp(<?php echo $i;?>,<?php echo $eachdoc['id'];?>);">Remove</a></div>
        <?php } ?>
					
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Professional Experience </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][prof_exp]" class="span8" id="prof_exp_<?php echo $i;?>" value="{{$eachdoc['prof_exp']}}">
				</div>
			</div>	
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Dental Business Name </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][dental_business]" class="span8" id="dental_business_<?php echo $i;?>" value="{{$eachdoc['dental_business']}}">
				</div>
			</div>	
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Location </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][location]" class="span8" id="location_<?php echo $i;?>" value="{{$eachdoc['location']}}">
				</div>
			</div>
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">From </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][exp_from]" class="span8 cal_date" id="exp_from_<?php echo $i;?>" value="{{$eachdoc['exp_from']}}">
				</div>
			</div>
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">To </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][exp_to]" class="span8 cal_date pull-left importantleftpull" id="exp_to_<?php echo $i;?>" value="{{$eachdoc['exp_to']}}"  <?php echo ($eachdoc['present']==1)?'disabled':''?>>
					<div class="to_field">
						<label class="pull-left present">
						<input type="checkbox" name="data[{{$i}}][present]" id="present_<?php echo $i;?>"  onclick="checkPresent(<?php echo $i;?>);" value="1" <?php echo ($eachdoc['present']==1)?'checked=checked':''?> >
						</label>Present
					</div>
				</div>
			</div>			

			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Document</label>
				<div class="controls">
					<input type="file" id="exp_photo_<?php echo $i;?>" name="data[{{$i}}][exp_photo]"/>
					<input type="hidden" id="hid_exp_photo" name="data[{{$i}}][hid_exp_photo]" value="{{$eachdoc['exp_photo']}}"/>
					<input type="hidden" id="hid_exp_photo_name" name="data[{{$i}}][hid_exp_photo_name]" value="{{$eachdoc['org_exp']}}"/>
				</div>	
                <?php if($eachdoc['exp_photo']!=''){?>			
					<p class="new_avatar" style="padding-left: 180px;padding-top: 10px;">
                     <a href="<?php echo url();?>/uploads/exp_document/{{$eachdoc['exp_photo']}}" target="_blank">{{$eachdoc['org_exp']}}</a>
                     <!--<img src="<?php echo url();?>/uploads/exp_document/{{$eachdoc['exp_photo']}}" class="nav-avatar">-->
                     </p>	
                    <?php } ?>			
			</div>			
		</div>	
		<?php 
		$i++;
			} 
		}
		else
		{
		?>
			<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
						
				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Professional Experience </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][prof_exp]" class="span8" id="prof_exp_<?php echo $i;?>">
					</div>
				</div>	

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Dental Business Name </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][dental_business]" class="span8" id="dental_business_<?php echo $i;?>">
					</div>
				</div>	

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Location </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][location]" class="span8" id="location_<?php echo $i;?>">
					</div>
				</div>
				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">From </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][exp_from]" class="span8 cal_date" id="exp_from_<?php echo $i;?>">
					</div>
				</div>

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">To </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][exp_to]" class="span8 cal_date pull-left importantleftpull" id="exp_to_<?php echo $i;?>">
  						<div class="to_field">
							<label class="pull-left present">
							<input type="checkbox" name="data[{{$i}}][present]" id="present_<?php echo $i;?>" onclick="checkPresent(<?php echo $i;?>);" value="1">
							</label>Present
						</div>
					</div>
                </div>
				

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Document</label>
					<div class="controls">
						<input type="file" id="exp_photo" name="data[{{$i}}][exp_photo]"/>	
						<input type="hidden" id="hid_exp_photo" name="data[{{$i}}][hid_exp_photo]"/>
						<input type="hidden" id="hid_exp_photo_name" name="data[{{$i}}][hid_exp_photo_name]"/>
											
					</div>	
				</div>		
			</div>
		<?php
		}
		?>

		<input type="hidden" name="rowcount" id="rowcount" value="<?php echo $doc_exp_count;?>">
		<input type="button" onclick="addExp();" class="btn" name="add_exp" id="add_exp" value="Add More">

		
			
		<div class="control-group">
		
			<div class="controls">
				<a href="{!! url('admin/dentist/dentist-step4/'.$user_id)!!}" class="btn">Prev</a>
				{!! Form::submit('Next',array('class'=>'btn','name'=>'action','value'=>'Next')) !!}				
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}


<script>
	
	function addExp(){
		var rowcountval = $("#rowcount").val();
		var rowcount =  parseInt(rowcountval)+1;
		$("#rowcount").val(rowcount);
		var addonhtml = '';
		
		var html = '<div class="control-group" style="display:block;" id="div_id_'+rowcount+'"><div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeExp('+rowcount+');">Remove</a></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Professional Experience </label><div class="controls"><input type="text" name="data['+rowcount+'][prof_exp]" class="span8"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Dental Business Name </label><div class="controls"><input type="text" name="data['+rowcount+'][dental_business]" class="span8"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Location </label><div class="controls"><input type="text" name="data['+rowcount+'][location]" class="span8" id="location_'+rowcount+'"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">From </label><div class="controls"><input type="text" name="data['+rowcount+'][exp_from]" id="exp_from_'+rowcount+'" class="span8"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">To </label><div class="controls"><input type="text" name="data['+rowcount+'][exp_to]" id="exp_to_'+rowcount+'" class="span8 pull-left importantleftpull" id="exp_to_'+rowcount+'"><div class="to_field"><label class="pull-left present"><input type="checkbox" name="data['+rowcount+'][present]" id="present_'+rowcount+'" onclick="checkPresent('+rowcount+');" value="1"></label>Present</div></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Document</label><div class="controls"><input type="file" id="exp_photo_'+rowcount+'" name="data['+rowcount+'][exp_photo]"/><input type="hidden" id="hid_exp_photo" name="data['+rowcount+'][hid_exp_photo]"/><input type="hidden" id="hid_exp_photo_name" name="data['+rowcount+'][hid_exp_photo_name]"/></div></div></div>';

		$("#add_exp").before(html);
		initdatepickerfrom('#'+'exp_from_'+rowcount);
		initdatepickerto('#'+'exp_to_'+rowcount);
		initialize('location_'+rowcount);
		
		$("input[name='data["+rowcount+"][exp_photo]']").rules("add", { extension: "png|pdf|jpg|jpeg" });
	}


	function removeExp(row,licenseitemid){
      	var response = confirm('Do you really want to remove this Item?');
		if(response==1){
	      	if(licenseitemid){
	      		$("#div_id_"+row).remove();	   
	      		$.ajax({
				  url: site_url+"/admin/item/remove-item",
				  type: 'POST',
				  data: {'itemid':licenseitemid,'model':'DoctorExperience'},
				  success: function(html){
				  }
				});

	      	}else{
	      		$("#div_id_"+row).remove();	   
	      	}	      	
        }
      }

    function checkPresent(id)
    {
    	$("#exp_to_"+id).val('');	

    	var isDisabled = $("#exp_to_"+id).is(':disabled');

	    if (isDisabled) 
	    {
	        $("#exp_to_"+id).prop('disabled', false);
	    } 
	    else 
	    {
	        $("#exp_to_"+id).prop("disabled",true);		
	    }
    }
</script>

 <script type="text/javascript">

    $(document).ready(function(){
     /****** Fure date restriction *****/
       var FromEndDate = new Date();
	   $('.cal_date').datepicker({
	      endDate: FromEndDate, 
	      autoclose: true
	    });
	   /****** Fure date restriction *****/
	   
    	initdatepickerfrom('.cal_date');
    	initdatepickerto('.cal_date');

    	var data_count = $('#data_count').val(); // Already saved data count

    	for(var i=0;i<=data_count;i++) // For Add More location auto complete
    	{
	    	var id = 'location_'+i;
	    	initialize(id);
	    }
		
		var rowcount = $("#rowcount").val();
		for(i=0;i<rowcount;i++)
		{
			$("input[name='data["+i+"][exp_photo]']").rules("add", { extension: "xls|csv|png|pdf|jpg|jpeg|doc|docx|bmp|odt" });
			console.log("input[name='data["+i+"][exp_photo]']");
		}
    })

    function initdatepickerfrom(selector){

     	$(selector).datepicker({
	       format: 'dd/mm/yyyy',
	       maxDate : 'now'  
	     });     	
    }

    function initdatepickerto(selector){
     	$(selector).datepicker({
	       format: 'dd/mm/yyyy'  
	     });     	
    }


    /******** Google Auto complete Address Start **********/
	
    function initialize(selector) {
        var input = document.getElementById(selector);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            
        });

    }

    /******** Google Auto complete Address Start **********/

</script>  


	
@stop