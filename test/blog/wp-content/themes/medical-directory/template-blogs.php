<?php
/**
 * Template Name: Blog Template
 */

get_header(); 
?>
<div class="blog-content">
   <div class="container">
      <div class="col-md-12">
    <div class="row">
         <?php 
         $query_args = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'post_status'=>'publish',
            'orderby'=>'DESC'
         );
         $the_query = new WP_Query( $query_args ); 
          if ( $the_query->have_posts() ) { 
          while ( $the_query->have_posts() ) : $the_query->the_post();       
         ?>      
         <div class="col-sm-4">
            <article class="uou-block-7g blog post-679 post type-post status-publish format-standard has-post-thumbnail hentry category-dental-chat-dentalchat category-featured category-online-dental-chat" data-badge-color="ff0099">
                <div class="mystory_images">
                <?php 
                if ( has_post_thumbnail() ) { echo get_the_post_thumbnail( $post_id, 'large' ); } else { echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
        . '/assets/img/teeth_default.jpg" />'; }
                //echo get_the_post_thumbnail( $post_id, array( 360, 220) );
                ?>
                </div>
                  <div class="content">
                    <span class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
                     <h1><a href="<?php echo get_the_permalink(); ?>">  <?php the_title(); ?>  </a></h1>
                     <p><?php echo wp_trim_words( get_the_content(), 50, '..' ); ?></p>
                     <a href="<?php echo get_the_permalink(); ?>" class="btn btn-small btn-primary">Read More</a>
                  </div>
            </article>  
            </div>
         <?php endwhile;
          }
        wp_reset_postdata();
            wp_reset_query();
            ?>
            <?php
    /*       if ( is_user_logged_in() ) {
     echo do_shortcode('[ajax_load_more post_type="post" scroll="false" offset="" posts_per_page="3" transition="fade" button_label="More Blog"]');
           
} else {
   echo do_shortcode('[ajax_load_more post_type="post" offset="3" posts_per_page="3" transition="fade" button_label="More Blog"]');
}
        */    
            ?>
         </div>          
      </div>
   </div>
</div>
<?php get_footer(); ?>