<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $guarded=[];
	public $timestamps = false;

    protected $table = 'users';

    public function user_details()
    {
        return $this->belongsTo('App\Model\User','subadmin_id')->select(array('id','name','email'));
    }

}
