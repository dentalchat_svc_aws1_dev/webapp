<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PatientAuthToken extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'patient_auth_tokens';
    /*public function getUserDetail()
	{
		return $this->belongsTo('App\Model\SiteUser','user_id');
	}*/
}
