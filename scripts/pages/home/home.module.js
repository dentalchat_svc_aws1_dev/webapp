/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.home', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('home', {
				url: '/',
                templateUrl: 'scripts/pages/home/home.html',
                controller: 'HomePageCtrl',
                resolve:{
                            factory: function($q,$rootScope,$location,$state)
                            {
                                setTimeout(function(){
                                    if ($rootScope.is_front_logged==1)
                                    {
                                        $rootScope.checkLastServieCallTimeAfterLogin();
                                    }
                                    $rootScope.$apply();
                                },500);
                            }
                        }
            });
        $stateProvider.state('logout', {
                            url: '/logout',
                            controller: 'indexcontroller',                              
                        });
      $stateProvider
            .state('unverifiedcron', {
				url: '/unverifiedcron',
                templateUrl: 'scripts/pages/home/unverifiedcron.html',
                controller: 'HomePageCtrl',
                resolve:{
              
                        }
            });
    $stateProvider
            .state('uncomprofile', {
				url: '/uncomprofile',
                templateUrl: 'scripts/pages/home/uncompleteprofile.html',
                controller: 'HomePageCtrl',
                resolve:{
                           
                        }
            });   
            
    $stateProvider
            .state('unreadmsgcronpatient', {
				url: '/unreadmsgcronpatient',
                templateUrl: 'scripts/pages/home/unreadmsgcronpatient.html',
                controller: 'HomePageCtrl',
                resolve:{
                           
                        }
            });
    $stateProvider
            .state('unreadmsgcrondoctor', {
				url: '/unreadmsgcrondoctor',
                templateUrl: 'scripts/pages/home/unreadmsgcrondoctor.html',
                controller: 'HomePageCtrl',
                resolve:{
                           
                        }
            }); 
     $stateProvider
            .state('pateintemailunsubscribe', {
				url: '/pateintemailunsubscribe/:parameter',
                templateUrl: 'scripts/pages/home/emailunsubscribe.html',
                controller: 'HomePageCtrl',
                resolve:{
                           
                        }
            });
     $stateProvider 
            .state('dentistemailunsubscribe', {
				url: '/dentistemailunsubscribe/:parameter',
                templateUrl: 'scripts/pages/home/emailunsubscribe.html',
                controller: 'HomePageCtrl',
                resolve:{
                           
                        }
            }); 
        /*$stateProvider.state('patient-logout', {
                            url: '/patient-logout',
                            controller: 'indexcontroller',                              
                        });*/
    }
	
})();
