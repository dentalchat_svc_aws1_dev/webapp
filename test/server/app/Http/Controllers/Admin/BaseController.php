<?php namespace App\Http\Controllers\Admin; /* path of this controller*/

use App\Book;
use App\Model\Mobile;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Notifications;
use App\Model\SiteUser;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use Illuminate\Pagination\Paginator;
use DB;
use Auth;
use Redirect;

use App\Helper\helpers;



class BaseController extends Controller {

	public function __construct() 
    {
    	//echo "hello";exit;
		/* All Site Settings List */
        $sitesettings = DB::table('sitesettings')->get();
        $all_sitesetting = array();
        foreach($sitesettings as $each_sitesetting)
        {
            $all_sitesetting[$each_sitesetting->name] = $each_sitesetting->value; 
        }
        $notficationcount = Notifications::where('read','=',0)->get();
        Session::put('admin_notificatin_count', count($notficationcount));
	    view()->share('all_sitesetting',$all_sitesetting);

    }

	public function pr($array) //to print array
	{
		echo "<pre>";
		print_r($array);
	}
   
   

}
