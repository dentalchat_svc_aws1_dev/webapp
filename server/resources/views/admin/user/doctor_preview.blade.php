@extends('admin/layout/admin_template')

@section('content')
           
    @if(Session::has('success_message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success_message') }}</p>
    @endif  
    <div class="">    
        <div class="module maincontent">
            <div class="module-head">
            <h3>Basic Profile</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                  <div class="border-line">
                    <div class="module-left">First Name :</div> <div class="module-right"> <?php echo $dentist_details['docs_details']['first_name']; ?></div>
                     </div> 
                    <div class="border-line">   
                    <div class="module-left">Last Name :</div> <div class="module-right"> <?php echo $dentist_details['docs_details']['last_name']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">Date Of Birth :</div> <div class="module-right"> <?php echo date('m-d-Y',strtotime($dentist_details['docs_details']['date_of_birth'])); ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">Gender :</div> <div class="module-right"> <?php echo ($dentist_details['docs_details']['gender']==1)?'Male':'Female'; ?></div>
                     </div>
                    <div class="border-line">
                    <div class="module-left">Email :</div> <div class="module-right"> <?php echo $dentist_details['docs_details']['email']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">Contact Number :</div> <div class="module-right"> <?php echo $dentist_details['docs_details']['contact_number']; ?></div>
                    </div>

                    <div class="admin-profile-img">
                    <?php if($dentist_details['docs_details']['profile_pics']!=''){ ?>
                    <p class="new_avatar"><img src="<?php echo url();?>/uploads/dentist_profile_image/{{$dentist_details['docs_details']['profile_pics']}}"></p>
                    <?php } ?> 
                    </div> 

                </div>  
            </div>
        </div> 

        <div class="module maincontent">
            <div class="module-head">
            <h3>Business Details</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                  <div class="border-line">
                    <div class="module-left">Business Name : </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['business_name']; ?></div>
                  </div> 
                  <div class="border-line">
                    <div class="module-left">Business Address :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['address']; ?></div>
                  </div>
                  <div class="border-line">
                    <div class="module-left">Zip Code :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['zip_code']; ?></div>
                  </div>
                    <?php if($dentist_details['docs_clinics']['sun_opening_hours_from']!='' && $dentist_details['docs_clinics']['sun_opening_hours_to']!='')
                    { ?>
                        <div class="border-line">
                    <div class="module-left">Sun : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['sun_opening_hours_from'].' - '.$dentist_details['docs_clinics']['sun_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['mon_opening_hours_from']!='' && $dentist_details['docs_clinics']['mon_opening_hours_to']!='')
                    { ?>

                    </div>
                    </div>
                         <div class="border-line">
                    <div class="module-left"> Mon : </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['mon_opening_hours_from'].' - '.$dentist_details['docs_clinics']['mon_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['tue_opening_hours_from']!='' && $dentist_details['docs_clinics']['tue_opening_hours_to']!='')
                    { ?>
                    </div>
                    </div>
                         <div class="border-line">
                    <div class="module-left">Tues : </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['tue_opening_hours_from'].' - '.$dentist_details['docs_clinics']['tue_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['wed_opening_hours_from']!='' && $dentist_details['docs_clinics']['wed_opening_hours_to']!='')
                    { ?>
                    </div>
                    </div>
                        <div class="border-line">
                    <div class="module-left"> Wed : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['wed_opening_hours_from'].' - '.$dentist_details['docs_clinics']['wed_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['thu_opening_hours_from']!='' && $dentist_details['docs_clinics']['thu_opening_hours_to']!='')
                    { ?>
                    </div>
                    </div>
                        <div class="border-line">
                    <div class="module-left">Thur : </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['thu_opening_hours_from'].' - '.$dentist_details['docs_clinics']['thu_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['fri_opening_hours_from']!='' && $dentist_details['docs_clinics']['fri_opening_hours_to']!='')
                    { ?>
                    </div>
                    </div>
                   <div class="border-line">
                    <div class="module-left">Fri </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['fri_opening_hours_from'].' - '.$dentist_details['docs_clinics']['fri_opening_hours_to'];?>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['sat_opening_hours_from']!='' && $dentist_details['docs_clinics']['sat_opening_hours_to']!='')
                    { ?>
                    </div>
                    </div>
                  <div class="border-line">
                    <div class="module-left">Sat </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['sat_opening_hours_from'].' - '.$dentist_details['docs_clinics']['sat_opening_hours_to'];?>
                    <?php } ?>
                     </div>
                     </div>
                    <div class="admin-profile-img"> 
                    <?php if($dentist_details['docs_clinics']['clinic_picture']!=''){ ?>
                    <p class="new_avatar"><img src="<?php echo url();?>/uploads/clinic_picture/{{$dentist_details['docs_clinics']['clinic_picture']}}"></p>
                    <?php } ?>   
                    </div>

                </div>  
            </div>
        </div>

        <div class="module maincontent">
            <div class="module-head">
            <h3>Education & Training</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <?php if(!empty($dentist_details['docs_education']))
                   {
                    foreach($dentist_details['docs_education'] as $c=>$eachdeg)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">Degree : </div> <div class="module-right">  <?php echo $eachdeg['degree']; ?></div>
                    </div>
                    <div class="border-line">
                     <div class="module-left">Year Completed : </div> <div class="module-right"> <?php echo $eachdeg['completed_year']; ?></div>
                    </div>
                    <div class="border-line">
                     <div class="module-left">School Name : </div> <div class="module-right"> <?php echo $eachdeg['medical_school']; ?></div>
                    </div> 
                    <div class="border-line">
                     <div class="module-left">School Location : </div> <div class="module-right">  <?php echo $eachdeg['location']; ?></div>
                    </div> 
                    <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/certificate/{{$eachdeg['certificate']}}">View Certificate</a> </h3> </div>

                   <?php } 
                   }?>             
                </div>  
            </div>
            <div class="module-body">
                 <div class="control-group" style="display:block;">
                    <?php if(!empty($dentist_details['docs_award']))
                    {
                    ?>
                        <div class="border-line">
                     <div class="module-left">Award : </div>
                     <div class="module-right">
                    <?php
                        foreach($dentist_details['docs_award'] as $a=>$eachaward)
                        {
                    ?>
                            <p><?php echo $eachaward['award']; ?></p>
                    <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>

                 </div>
             </div>
             <div class="module-body">
                 <div class="control-group" style="display:block;">
                    <?php if(!empty($dentist_details['docs_insurance']))
                    {
                    ?>
                        <div class="border-line">
                     <div class="module-left">Insurance :</div>
                     <div class="module-right">
                    <?php 
                        foreach($dentist_details['docs_insurance'] as $in=>$eachinsurance)
                        {
                    ?>
                            <p><?php echo $eachinsurance['insurance']; ?></p>
                     <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                 </div>
             </div>
            
        </div>

        <div class="module maincontent">
            <div class="module-head">
            <h3>License</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <?php if(!empty($dentist_details['docs_license']))
                   {
                    foreach($dentist_details['docs_license'] as $l=>$eachlicense)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">License : </div>
                     <div class="module-right"> <?php echo $eachlicense['license_details']; ?></div>
                     </div>
                    <?php  if($eachlicense['license_photo']!='') {    ?>               
                    <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/license_photo/{{$eachlicense['license_photo']}}">View License</a> </h3></div>
                   <?php }
                     } 
                   }?>             
                </div>
            </div>
        </div>

        <div class="module maincontent">
            <div class="module-head">
            <h3>Professional Experience</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <?php if(!empty($dentist_details['docs_experiece']))
                   {
                    foreach($dentist_details['docs_experiece'] as $e=>$eachexperiece)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">Professional Experience :  </div> <div class="module-right"> <?php echo $eachexperiece['prof_exp']; ?></div>
                    </div>
                    <div class="border-line">
                     <div class="module-left">Dental Business Name : </div> <div class="module-right"> <?php echo $eachexperiece['dental_business']; ?></div>
                    </div> 
                    <div class="border-line">
                     <div class="module-left">Location :</div> <div class="module-right">  <?php echo $eachexperiece['location']; ?></div>
                    </div>
                    <?php if($eachexperiece['exp_from']!='')
                    { 
                        if($eachexperiece['exp_to']!=''){?>
                        
                        <div class="border-line">
                     <div class="module-left">Experience Duration : </div> <div class="module-right"><?php echo $eachexperiece['exp_from'].' - '.$eachexperiece['exp_to'];?>
                     </div></div>
                    
                    <?php } else { ?>

                        <div class="border-line">
                     <div class="module-left">Experience Duration :</div> <div class="module-right"> <?php echo $eachexperiece['exp_from'].' - Till Date';?></div></div>
                    <?php 
                        }
                    }
                    ?>

                    <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/exp_document/{{$eachexperiece['exp_photo']}}">View Experience Certificate</a> </h3></div>
                   <?php } 
                   }?>             
                </div>
            </div>
        </div>       

        <div class="module maincontent">
            <div class="module-head">
            <h3>Skill Highlights / Specialties</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <?php if(!empty($dentist_details['docs_skill']))
                    {
                    ?>
                    <div class="border-line">
                     <div class="module-left">Specialties : </div> 
                     <div class="module-right"> 
                    <?php
                    foreach($dentist_details['docs_skill'] as $s=>$eachskill)
                    {
                    ?> 
                      <p><?php echo $eachskill['skills']; ?></p>
                    <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>             
                </div> 
            </div>
        </div>

        <div class="module maincontent">
            <div class="module-head">
            <h3>Language Spoken / Personal Statement</h3>
            </div>
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <?php if(!empty($dentist_details['docs_language']))
                    {
                    ?>
                   <div class="border-line">
                     <div class="module-left">language Spoken : </div> 
                     <div class="module-right"> <?php echo $dentist_details['docs_language']['language_name'];?></div>
                     </div>

                    <div class="border-line">
                     <div class="module-left">Personal Statement : </div> 
                     <div class="module-right">
                        <div class="controls">
                        <?php echo ($dentist_details['docs_language']['personal_statement']!='')?$dentist_details['docs_language']['personal_statement']:''?>
                        </div>
                        </div>
                   <?php 
                    } 
                   ?>             
                </div>  
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <a href="{!! url('admin/dentist/dentist-step7/'.$user_id)!!}" class="btn">Back</a>
                <?php if($dentist_details['publish_status']>0){?>
                <a href="javascript:void(0)"  class="btn" onclick="publishApproved(<?php echo $user_id; ?>)">Publish</a>  
                <?php } ?>          
            </div>
        </div>
    </div>
    
    <script>
    function publishApproved(id)
    {
        $.ajax({
                  url: site_url+"/admin/item/publish-profile",
                  type: 'POST',
                  data: {'dentist_id':id},
                  success: function(html)
                  {
                    window.location.href=site_url+"/admin/dentist/dentist-preview/"+id;
                  }
                });
    }    
        
    </script>

@stop