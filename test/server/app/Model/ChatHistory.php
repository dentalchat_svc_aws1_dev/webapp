<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChatHistory extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'chat_histories';
    public function get_doctor_detail()
	{
		return $this->belongsTo('App\Model\DoctorDetails','doctor_id','doctor_id');
	}
}
