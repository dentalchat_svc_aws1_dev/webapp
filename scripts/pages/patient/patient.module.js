/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.patient', [])
        .config(routeConfig)
        .constant('POSTCONFIG',{       
        AssetsUrl       : (location.hostname=='localhost') ? 'http://'+(location.hostname)+'/cleardoc-com/assets/' : 'http://'+(location.hostname)+'/server/uploads/patient_post_attachments',
        
    })



    /** @ngInject */
    function routeConfig($stateProvider,$compileProvider) 
	{	
		 	$compileProvider.debugInfoEnabled(true);
		    $stateProvider.state('patient-registration', {
								url: '/patient-registration',
								cache:false,
								templateUrl: 'scripts/pages/patient/patient_registration.html',
								controller: 'patientPageCtrl',
								activename:'step',
														
			})
		    $stateProvider.state('patient-login', {
								url: '/patient-login',
								templateUrl: 'scripts/pages/patient/patient_login.html',
								controller: 'patientPageCtrl',
								activename:'step',
								/*resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                       
			                                       $location.path('/dentist-messageboard');
			                                    }
			                                    if($rootScope.is_front_logged==1)
			                                    {
                    								$location.path('/patient-profile/my-post');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }*/
			});

		    $stateProvider.state('patient-login-story', {
								url: '/patient-login-story',
								templateUrl: 'scripts/pages/patient/patient_login_story.html',
								controller: 'patientPageCtrl',
								activename:'step',
								/*resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                       
			                                       $location.path('/dentist-messageboard');
			                                    }
			                                    if($rootScope.is_front_logged==1)
			                                    {
                    								$location.path('/patient-profile/my-post');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }*/
			});
			$stateProvider.state('patient-registration-success', {								
								url: '/patient-registration-success',
								templateUrl: 'scripts/pages/register/register_success.html',
								controller: 'patientPageCtrl',
			});
	$stateProvider.state('special', {								
				url: '/special',
				templateUrl: 'scripts/pages/patient/business.html',
				controller: 'patientPageCtrl',
});
/*$stateProvider.state('offer', {								
	url: '/offer',
	templateUrl: 'scripts/pages/patient/exhaustive.html',
	controller: 'patientPageCtrl',
});*/
$stateProvider.state('freeoffer', {								
	url: '/freeoffer',
	templateUrl: 'scripts/pages/patient/idealistic.html',
	controller: 'patientPageCtrl',
});
$stateProvider.state('now', {								
	url: '/now',
	templateUrl: 'scripts/pages/patient/posh.html',
	controller: 'patientPageCtrl',
});
			$stateProvider.state('activatepatient', {
								url: '/activate-patient/:parameter',
								//templateUrl: 'scripts/pages/register/dentist_signin.html',
								controller: 'patientPageCtrl',								
							});

			$stateProvider.state('patient-forgotpassword', {
									url: '/patient-forgotpassword',
									templateUrl: 'scripts/pages/patient/patient_forgot_password.html',
									controller: 'patientPageCtrl',
								});
			$stateProvider.state('patient-reset-password', {
									url: '/patient-reset-password/:parameter',
									templateUrl: 'scripts/pages/patient/patient_reset_password.html',
									controller: 'patientPageCtrl',
								});

			$stateProvider.state('patient-change-password', {
								url: '/patient-change-password',
								templateUrl: 'scripts/pages/patient/patient_change_password.html',
								controller: 'patientPageCtrl',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }
							});

			$stateProvider.state('patientProfile', {
								url: '/patient-profile',
								templateUrl: 'scripts/pages/patient/patient_profile.html',
								controller: 'patientPageCtrl',
								resolve:{
				                            factory: function($q,$rootScope,$location)
				                            {
				                                setTimeout(function(){
				                                    if ($rootScope.is_front_logged==2){		
				                                    	//$location.path('/dentist-messageboard');		                                    	
				                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
				                                    }
				                                    else
				                                    {
                        								$location.path('/patient-login');
				                                    }
				                                    $rootScope.$apply();
				                                },500);
				                            }
				                        }					
							})
						

			$stateProvider.state('create-post', {
								url: '/patient-profile/post/:title',
								templateUrl: 'scripts/pages/patient/create_post.html',
								controller: 'patientPageCtrl',
								activename:'step',
								/*resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                            	$rootScope.mypost=false;
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                        //$location.path('/dentist-messageboard');
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }*/						
							});

			$stateProvider.state('my-post', {
								url: '/patient-profile/my-post',
								templateUrl: 'scripts/pages/patient/my_post.html',
								controller: 'patientPageCtrl',
								activename:'step',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                            	$rootScope.mypost=true;
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                        //$location.path('/dentist-messageboard');
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }						
							});

			$stateProvider.state('my-message', {
								url: '/patient-profile/my-message/:postId/:act',
								templateUrl: 'scripts/pages/patient/my_message.html',
								controller: 'patientPageCtrl',
								activename:'step',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                            	$rootScope.mypost=true;
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                        //$location.path('/dentist-messageboard');
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }						
							});

			$stateProvider.state('post-details', {
								url: '/post-details/:postId',
								templateUrl: 'scripts/pages/patient/post_details.html',
								controller: 'patientPageCtrl',
								activename:'step',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                            	$rootScope.mypost=true;
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                       //$location.path('/dentist-messageboard');
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }						
							});

			$stateProvider.state('viewdoctorprofile', {
								url: '/doctorprofile/:doctorId/:doctorName',
								templateUrl: 'scripts/pages/patient/view_doctor_profile.html',
								controller: 'patientPageCtrl',
								activename:'step',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                       $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                        //$location.path('/dentist-messageboard');			                                        
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }						
							});
						
			$stateProvider.state('backgroundmailsend', {
								url: '/doctorprofile/:doctorId',
								templateUrl: 'scripts/pages/patient/view_doctor_profile.html',
								controller: 'patientPageCtrl',
								activename:'step',
								resolve:{
			                            factory: function($q,$rootScope,$location)
			                            {
			                                setTimeout(function(){
			                                    if ($rootScope.is_front_logged==2){				                                    	
			                                        $rootScope.checkLastServieCallTimeAfterPatientLogin();
			                                        //$location.path('/dentist-messageboard');
			                                    }
			                                    else
			                                    {
                    								$location.path('/patient-login');
			                                    }
			                                    $rootScope.$apply();
			                                },500);
			                            }
			                        }						
							});




    }
	
})();
