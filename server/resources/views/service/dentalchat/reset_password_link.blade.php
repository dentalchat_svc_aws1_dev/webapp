<!doctype html>
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta charset="utf-8">
<title>Reset Password</title>

</head>

<body>
<center>
  <style>
@import url('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
</style>
  <table width="620" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td style="background-repeat:no-repeat;background-position:0 0;background-size:cover;padding-left:30px;padding-top:30px;padding-right:30px;"><table width="560" border="0" cellpadding="0" cellspacing="0" style="">
          <thead>
            <tr>
              <td align="center" colspan="2" style="padding-bottom:22px; padding-top:22px;" bgcolor="#eef3fa">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td>
                        <a href="" target="_blank"><img src="<?php echo url();?>/public/backend/images/logo.png" alt=""  style="width: 120px; height: 120px; display: block; object-fit: cover;"></a>
                      </td>
                    </tr>
                </table>
              
              </td>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#eef3fa">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td id="top_part" height="70" background="<?php echo url();?>/public/backend/images/top_image.png" style="background-repeat:no-repeat;background-size:cover;background-position:right 0;"></td>
            </tr>
            <tr>
              <td style="padding-left:30px;padding-right:30px; padding-top: 15px;" bgcolor="#f8f8f8"><table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                 <tr>
                  <td align="left">
                  <p style="font-size:20px;line-height:22px;color:#2e3192;font-family:'Lato', sans-serif;font-weight:700;display:block;text-align:left;margin:0;margin-bottom:20px;display:block;">Hi {!! $name !!},</p>
                  </td>
                  </tr> 
                  <tr>
                    <td align="center">
                      <p style="font-size:15px; line-height:20px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:400;margin:0;margin-bottom:25px;display:block;text-align:left">We received a request to change the password for the Dentalchat.com account associated with this email address. To change your password, follow this link.</p>
                      <div style="width:96%; font-size:16px;line-height:22px;color:#2e3192;font-family:'Lato', sans-serif;font-weight:700;border:1px solid #2e3192;border-radius:25px;  display: inline-block;margin-bottom:30px;">
                        <p style="margin:0;margin-top:13px;margin-bottom:13px;margin-left:25px;margin-right:25px;">
                        <span><a href="{!! $reset_password_link !!}" target="_blank">{!! $reset_password_link !!}</a></span>
                        </p>
                      </div>
                      <p style="font-size:15px; line-height:20px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:400;margin:0;margin-bottom:25px;display:block;text-align:left"><b>Please note that this link is Only valid for 4 hours.</b></p>  
                      <p style="font-size:15px; line-height:20px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:400;margin:0;margin-bottom:25px;display:block;text-align:left">If you didn't request a password reset or Link not working? Please contact us service@dentalchat.com.</p> 
                      </td>
                  </tr>
                  
                  
                </table></td>
            </tr>
            <tr>
              <td id="bot_part" background="<?php echo url();?>/public/backend/images/bot_image.png" height="71" style="background-size:cover;background-position:right;background-repeat:no-repeat;">
                  <p style="font-size:15px; line-height:20px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:400;margin:0;margin-bottom:25px;display:block;text-align:left">Sincerely, <br/>DentalChat Team</p>
              </td>
            </tr>
              </table>
              
              </td>
            </tr>
          </tbody>
          <tfoot>
           <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#2e3192">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td align="center" style="font-size: 18px;color: #ffffff;line-height: 22px;font-weight: 300;font-family: 'Lato', sans-serif;padding-top: 17px;padding-bottom: 17px;">Thank you, <a href="{!! $perm_domain !!}" target="_blank" style="font-weight:700;color:#fff;text-decoration:none;">DentalChat</a></td>
            </tr>
            </table>
            </td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</center>
</body>
</html>