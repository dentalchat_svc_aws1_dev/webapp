'use strict';
var app = angular.module('doctorchat', ['ngAnimate', 'ngCookies', 'ngResource', 'ui.router', 'ngSanitize', 'ngMessages', 'validation.match', 'angular-loading-bar', 'app.config', 'config.service', 'ngMaterial', 'angularjs-dropdown-multiselect', 'hljs', 'ui.bootstrap', 'ngIdle', 'tableSort', 'angularValidator', 'oitozero.ngSweetAlert', '720kb.datepicker', 'moment-picker', 'doctorchat.pages', 'doctorchat.components', 'ui.bootstrap.datetimepicker', 'ngStorage', 'ngMap', 'socialLogin', 'vcRecaptcha', 'google.places', 'ngMeta', 'btford.socket-io']).config(['$httpProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider', 'momentPickerProvider', 'socialProvider', 'ngMetaProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, momentPickerProvider, socialProvider, ngMetaProvider) {
    ngMetaProvider.useTitleSuffix(true);
    ngMetaProvider.setDefaultTitle('Dental chat');
    ngMetaProvider.setDefaultTitleSuffix(' | Dental chat');
    ngMetaProvider.setDefaultTag('description', 'Dental chat');
    ngMetaProvider.setDefaultTag('keywords', 'Dental chat');
    momentPickerProvider.options({
        locale: 'en',
        format: 'L LTS',
        maxView: 'minute',
        autoclose: true,
        today: false,
        keyboard: false,
        leftArrow: '',
        rightArrow: '',
        yearsFormat: 'YYYY',
        monthsFormat: '',
        daysFormat: '',
        hoursFormat: 'hh:mm',
        minutesFormat: 'hh:mm a',
        minutesStep: 30,
        secondsStep: 1,
        showHeader: false,
    });
    socialProvider.setGoogleKey("290388409241-vqor8q5tsk5fo01jud50adf6t111nfuq.apps.googleusercontent.com");
    socialProvider.setFbKey({
        appId: "176163339558719",
        apiVersion: "v2.8"
    });
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
}]).run(function($rootScope, $location, $http, $state, configService, ngMeta) {
    ngMeta.init();
    $rootScope.is_front_logged = 0;
    $rootScope.base_url = configService.getEnvConfig().apiURL;
    var a_token = localStorage.getItem('access_token');
    var auth_token = localStorage.getItem('auth_token');
    if (a_token) {
        $rootScope.is_front_logged = 1;
    } else if (auth_token) {
        $rootScope.is_front_logged = 2;
    }
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.showLoader = 1;
        $rootScope.curr_state = toState.name;
        $rootScope.activename = toState.activename;
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.showLoader = 0;
        $rootScope.promoPage = 0;
        if (toState.name == 'promopage' || toState.name == 'specialpromo' || toState.name == 'promotion' || toState.name == 'pro') {
            $rootScope.promoPage = 1;
        }
        
    });
    $rootScope.$on('event:social-sign-in-success', function(event, userDetails) {
        console.log(userDetails);
        $rootScope.showLoader = 1;
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            }
        };
        $http.post(configService.getEnvConfig().apiURL + "patient-social-login", userDetails, config).success(function(data, status, headers, config) {
            $rootScope.showLoader = 0;
            if (data.status == 0) {
                $rootScope.show_message = 'Wrong email id or password.';
                $rootScope.showErrMsg = 1;
            } else if (data.status == 1) {
                $rootScope.is_front_logged = 2;
                localStorage.setItem("auth_token", data.auth_token);
                localStorage.setItem("patient_id", data.patient_id);
                localStorage.setItem("patient_id_raw", data.patient_id_raw);
                localStorage.setItem("patient_name", data.patient_name);
                localStorage.setItem('patientImage', data.profile_pic);
                localStorage.setItem('patientName', data.patient_name);
                localStorage.setItem('loginType', data.login_type);
                $rootScope.loginType = data.login_type;
                $rootScope.logged_user_name = data.patient_name;
                $rootScope.user_profile_pics = data.profile_pic;
                if (data.is_any_post > 0) {
                    $state.go('my-post', {});
                } else {
                    $state.go('create-post', {});
                }
            } else {
                $rootScope.show_message = 'Something Wrong';
            }
        }).error(function(data, status, header, config) {
            $scope.show_message = 'Something is Wrong';
        });
    })
});
app.directive('onError', function() {
	  return {
		restrict:'A',
		link: function(scope, element, attr) {
		  element.on('error', function() {
			element.attr('src', attr.onError);
		  })
		}
	  }
})
app.filter('titleCase', function() {
    return function(input) {
        input = input || '';
        return input.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
})
var checkisLogin = function(configService) {
    var user_auth_token = localStorage.getItem("user_auth_token");
    if (typeof user_auth_token == 'undefined' || user_auth_token == "" || user_auth_token == null) {
        window.location.href = configService.getEnvConfig().frontApiURL;
    }
}
app.controller('indexcontroller', function(Idle, $state, $compile, $interval, $rootScope, $location, $http, SweetAlert, configService, $mdDialog, $scope, $filter, $timeout) {
    $rootScope.logged_user_name = (localStorage.getItem('patientName')) ? localStorage.getItem('patientName') : $rootScope.logged_user_name;
    $rootScope.loginType = (localStorage.getItem('loginType')) ? localStorage.getItem('loginType') : $rootScope.loginType;
    $rootScope.user_profile_pics = (localStorage.getItem('patientImage')) ? localStorage.getItem('patientImage') : $rootScope.user_profile_pics;
    $rootScope.patientLogout = function() {
        var token = localStorage.getItem("auth_token");
        var patientId = localStorage.getItem("patient_id");
        var data = {
            patientId: patientId
        };
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Token': token,
            }
        };
        $rootScope.showLoader = 1;
        $http.post(configService.getEnvConfig().apiURL + "service/patient/patient-logout", data, config).success(function(data, status, headers, config) {
            $rootScope.showLoader = 0;
            if (data.status == 1) {
                sessionStorage.clear();
                localStorage.clear();
                localStorage.removeItem("auth_token");
                localStorage.removeItem("patient_id");
                localStorage.removeItem('patientName');
                localStorage.removeItem('loginType');
                localStorage.removeItem('patientImage');
                $rootScope.is_front_logged = 0;
                $scope.redirectPage('/');
            } else {
                $rootScope.show_message = 'Something Wrong';
            }
        }).error(function(data, status, header, config) {
            $scope.show_message = 'Something is Wrong';
        });
    };
    $scope.redirectPage = function(page_name) {
        if (page_name == '/') $location.path('/');
        else
            $location.path('/' + page_name);
    };
    var currentState = $state.current.name;
    if (currentState == 'logout') $rootScope.logout();
    $rootScope.logout = function() {
        var token = localStorage.getItem("access_token");
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Token': token,
            }
        };
        $http.post(configService.getEnvConfig().apiURL + "service/logout", {}, config).success(function(data, status, headers, config) {
            if (data.status == 1) {
                sessionStorage.clear();
                localStorage.clear();
                localStorage.removeItem("access_token");
                localStorage.removeItem("usr_info");
                localStorage.removeItem('patientName');
                $rootScope.is_front_logged = 0;
                localStorage.removeItem("patient_id");
                localStorage.removeItem("patient_id_raw");
                localStorage.removeItem("patient_name");
                localStorage.removeItem('patientImage');
                $scope.redirectPage('/');
            }
        }).error(function(data, status, header, config) {
            $scope.show_message = 'Something is Wrong';
        });
    }
    $rootScope.getUserSpecificInfo = function() {
        var user_info = atob(localStorage.getItem("usr_info"));
        var user_array = new Array();
        var user_array = user_info.split('|@|');
        var user_id = user_array[0];
        var user_first_name = user_array[1];
        var user_email = user_array[2];
        var user_profile_pics = user_array[4];
        var user_temp_pics = user_array[6];
        var make_profile_url = user_array[5];
        return [user_id, user_first_name, user_email, user_profile_pics, make_profile_url, user_temp_pics];
    }
    $rootScope.profile_image = '';
    $rootScope.checkLastServieCallTimeAfterLogin = function(specific_info) {
    	
        var token = localStorage.getItem("access_token");
        var client_details = [];
        client_details = $scope.getUserSpecificInfo();
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Token': token,
            }
        };
        $http.get(configService.getEnvConfig().apiURL + "service/check-last-servie-call-time-after-login", config).success(function(data, status, headers, config) {
            $rootScope.showLoader = 0;
            if (data.status == 0) {
                sessionStorage.clear();
                localStorage.removeItem("access_token");
                localStorage.removeItem("usr_info");
                $rootScope.is_front_logged = 0;
                $scope.redirectPage('dentist-signin');
            } else {
                $rootScope.logged_user_id = client_details[0];

                if(localStorage.getItem("currentLoggedUname")){
                 $rootScope.logged_user_name = localStorage.getItem("currentLoggedUname");
                }else{
                  $rootScope.logged_user_name = client_details[1];	
                }
               
                
                $rootScope.logged_user_email = client_details[2];
                var final_img = localStorage.getItem('dentist_profile_pic');
                if (final_img != '') {
                    $rootScope.user_profile_pics = final_img;
                } else {
                    $rootScope.user_profile_pics = client_details[3];
                }
                $rootScope.user_temp_pics = client_details[5];
                $rootScope.make_profile_url = client_details[4];
                console.log($rootScope.user_profile_pics);
                window.scroll({
                     top: 0, 
                     left: 0, 
                     behavior: 'smooth' 
                    });
            }
        }).error(function(data, status, header, config) {
            console.log(data);
            $scope.show_message = 'Something is Wrong';
        });
    };

    $rootScope.checkLastServieCallTimeAfterPatientLogin = function(specific_info) {
        var token = localStorage.getItem("auth_token");
        
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Token': token,
            }
        };
        $http.get(configService.getEnvConfig().apiURL + "service/check-last-servie-call-time-after-patient-login", config).success(function(data, status, headers, config) {
            $rootScope.showLoader = 0;
            if (data.status == 0) {
                sessionStorage.clear();
                localStorage.removeItem("access_token");
                $rootScope.is_front_logged = 0;
                $scope.redirectPage('patient-login');
            } else {
                $rootScope.logged_user_id = localStorage.getItem("patient_id");
                $rootScope.logged_user_name = localStorage.getItem('patientName');
                $rootScope.user_profile_pics = localStorage.getItem('patientImage');
                $rootScope.is_front_logged = 2;
            }
        }).error(function(data, status, header, config) {
            console.log(data);
            $scope.show_message = 'Something is Wrong';
        });
    };
    
    if (localStorage.getItem('patientName')) {
        $rootScope.logged_user_name = localStorage.getItem('patientName');
    }
    $rootScope.stepAlreadyFill = function() {
        var token = localStorage.getItem("access_token");
        var config = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Access-Token': token,
            }
        };
        $http.get(configService.getEnvConfig().apiURL + "service/dentistservice/empty-step-check", config).success(function(data, status, headers, config) {
            $rootScope.showLoader = 0;
            console.log(data);
            if (data.status == 1) {
                $rootScope.step2 = data.step2;
                $rootScope.step3 = data.step3;
                $rootScope.step4 = data.step4;
                $rootScope.step5 = data.step5;
                $rootScope.step6 = data.step6;
                $rootScope.step7 = data.step7;
            }
        }).error(function(data, status, header, config) {
            console.log(data);
            $scope.show_message = 'Something is Wrong';
        });
    }
});
app.directive('pwCheck', [function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function() {
                scope.$apply(function() {
                    var v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('pwmatch', v);
                });
            });
        }
    }
}]);