/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages', [
            'doctorchat.pages.home',
			'doctorchat.pages.register',
            'doctorchat.pages.cms',
            'doctorchat.pages.profile',
            'doctorchat.pages.patient',
            'doctorchat.pages.promo'
        ]).config(routeConfig); 

		  /** @ngInject */
		  function routeConfig($stateProvider, $urlRouterProvider) {
		    $urlRouterProvider.otherwise('/404');
		    $stateProvider.state("otherwise", { url: '/404',
                templateUrl: 'scripts/pages/home/404.html',
                controller: 'HomePageCtrl',}); 
		  }
        
    
})();
