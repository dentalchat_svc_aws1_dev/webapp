<?php namespace App\Http\Controllers\Service;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
/*************** Model name ***************/
use App\Model\HomePage; 
use App\Model\Testimonial;
use App\Model\Sitesetting; /* Model name*/

use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;

class HomeSettingController extends BaseController {

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
        if(file_exists('../blog/wp-load.php')){
            require_once '../blog/wp-load.php';
        }
  		parent::__construct();
    }
    
    function getTest(){
        echo 1;
    }
	
	function postHomeSettings()
	{
		$data =Request::all();        
		$page_id = $data['page_id'];
		$all_settings = HomePage::where('id',1)->first()->toArray();
		/*start code by covetus 11-aug-2017*/
		$args = array(
          'post_type' => 'mystory',
          'post_status' => 'publish',
          'numberposts' => 3
        );
        $wp_blog = array();
        $wp_blogs = get_posts( $args );
        if($wp_blogs){
            foreach($wp_blogs as $i => $blog){
                $wp_blog[$i]['permalink'] = get_permalink($blog->ID);
                $temp_img = wp_get_attachment_url( get_post_thumbnail_id($blog->ID));
                $wp_blog[$i]['img_url'] = ($temp_img) ? $temp_img :url('public/teeth_default.jpg');
                $wp_blog[$i]['post_title'] = isset( $blog->post_title ) ? $blog->post_title : '';
                $wp_blog[$i]['post_content'] = mb_substr(strip_tags($blog->post_content), 0, 100);
            }
        }
        wp_reset_query();
        /* end code by covetus*/
		echo json_encode(array('status'=>1,'home_setting'=>$all_settings,'wp_blogs'=>$wp_blog));
	}

    function postTestimonialDetails()
    {
        //$data =Request::all();        
        //$page_id = $data['page_id'];
        $testimonialcount = Testimonial::get()->count();
        if($testimonialcount){
           $all_testimonial = Testimonial::orderBy('id','DESC')->get()->toArray(); 
        }else{
           $all_testimonial =''; 
        }
        //print_r($all_testimonial);exit;
        echo json_encode(array('status'=>1,'all_testimonial'=>$all_testimonial));
    }

    /*** Socila Media Link ****/
    function postSocialMediaLink()
    {
        $data =Request::all();
       //print_r($data);exit;
        $array_ids = explode(',', $data['link_id']);
        $all_link = Sitesetting::whereIn('id',$array_ids)->get();
        //print_r($all_link);exit;
        echo json_encode(array('status'=>1,'home_link'=>$all_link));
    }
    /*** Socila Media Link ****/

}
