<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dentchat_main_test');

/** MySQL database username */
define('DB_USER', 'dentchat_admin');

/** MySQL database password */
define('DB_PASSWORD', '@!dentchat!@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'AUTOMATIC_UPDATER_DISABLED', true );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',8&1jd0=sx:[R*O1X-O44:p9EODUUIxZ`S!ic9fC+I<wUoZzMvxpZ[du]3eF)cT7');
define('SECURE_AUTH_KEY',  '692N=eOTtw@NmMCaOly0 -s!|*g8fbWQM%%+[>xKNnj**H uz40RF_}C<z2)XBeQ');
define('LOGGED_IN_KEY',    'f{=u]OGlHT7m z-+j:6n$Pr09/{&-~vu)/Ht$$[fg&-JO%2,qR[$v4c-@t+y6KxC');
define('NONCE_KEY',        'F~>$$zf^l7mDgtD: &s?|(RqP`k5bR,;<qx*?{xSs-}*`L!!<iAk&gnet++vRSFx');
define('AUTH_SALT',        'smp(;&+U;9TPa{7StFq>f`TM|Pfgw*[el7k:!#s~;j&+r,*H$}<vKO1<q$L#gIF;');
define('SECURE_AUTH_SALT', 'q@3wCw}+g |?2Q7t0z=jiiEx{r3OkU0b$3F2/OUT99PtmS8uVQH{?,PK>q#x2Yrq');
define('LOGGED_IN_SALT',   ':JL8av2R1F+:xE}e:{]<E_ECjLvCEW+;PeE!VD%Hk@&i>Zsr:j01%r)E[@#X*Jz{');
define('NONCE_SALT',       '9&akQ7qk1,^nde6b1Us<bGWD}V{X?%X5cgc[vT)W<]-RH8qG2y,Xuf/H_]&y|w}=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
