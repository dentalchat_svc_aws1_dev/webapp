var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Chat = new Schema({
    from: Number,
    to: Number,
    uniquecode: String,
    post_id: Number,
    message: String,
    chat_file:String,
    file_ext:String,
    status:String,
    senderName:String,
    receiverName:String,
    createdAt:Date
    
});

module.exports = mongoose.model('dentalchats', Chat);
