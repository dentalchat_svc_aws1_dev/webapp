(function() {
    'use strict';
    angular.module('doctorchat.pages.register').controller('RegisterPageCtrl', RegisterPageCtrl);

    function RegisterPageCtrl($scope, $timeout, $window, $http, configService, $rootScope, $location, homeService, $stateParams, $state, $localStorage, $sessionStorage) {
        $scope.plan = $location.search().plan;
        $scope.showLoader = 0;
        $window.scrollTo(0, 0);
        $scope.user = {};
        $rootScope.obj = {};
        $scope.expiredlink = 0;
        $rootScope.showSuccessMsg = 0;
        $rootScope.hideChangePasswordFB = 0;
        $rootScope.showErrMsg = 0;
        $rootScope.showErrMsgFrgt = 0;
        $rootScope.show_message = '';
        $rootScope.is_front_logged = 0;
        $scope.checkEmail = function(email) {
            var patt = /^$|^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;
            var res = patt.test(email);
            if (res) {
                return 1;
            } else {
                if (typeof email == 'undefined') {
                    return 1;
                }
                return 0;
            }
        };
        //console.log(configService.getEnvConfig().apiURL + "service/register");
        $scope.registerDentist = function(user) {
            var plan = $scope.plan;
                
            if(plan==undefined || (plan!=0 && plan!=99 && plan!=189)){
                plan=0;
            }
            
            $rootScope.showLoader = 1;
            $scope.succ_msg = '';
            var website = 0;
            var amount = 0;
            var data = {
                user: user,
                plan: plan
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/register", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 2) {
                    $rootScope.change_color = "color:red";
                    $rootScope.show_message_email = 'Email already exists.';
                    $rootScope.showSuccMsg = 1;
                } else {
                    $rootScope.reg_success_msg1 = "Thank you for registering your Dental Practice with us.";
                    $rootScope.reg_success_msg2 = "A verification link has been sent to email id  " + data.useremail + "";
                    
                    $location.path('/registration-success');
                }
            }).error(function(data, status, header, config) {});
        }
        $scope.activatedUser = function() {
            var base64email = $stateParams.parameter;

            var data = {
                email: atob(base64email)
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/check-activation", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                //console.log(data);
                if (data.status == 1) {
                    $rootScope.showLoader = 0;
                    $rootScope.showErrMsg = 1;
                    $rootScope.show_class = "alert alert-success";
                    $rootScope.show_message = 'Email Id verification – Success!!!';
                    //console.log($rootScope.showErrMsg);
                    //console.log($rootScope.show_message);
                    //console.log($rootScope.show_class);
                   // $location.path('/dentist-signin');
                } else if (data.status == 2) {
                    $rootScope.showErrMsg = 1;
                    $rootScope.show_class = "alert alert-danger";
                    $rootScope.show_message = 'Your account is already activated.';
                    //$location.path('/dentist-signin');
                } else {
                    $rootScope.showErrMsg = 1;
                    $rootScope.show_class = "alert alert-danger";
                    $rootScope.show_message = 'Some problem occured.';
                   // $location.path('/dentist-signin');
                }
            }).error(function(data, status, header, config) {
               // console.log(data);
            });
        }
        var currentState = $state.current.name;
        if (currentState == 'activate-user') $scope.activatedUser();
        $scope.dentistLogin = function(user) {
            $rootScope.show_my_message = '';
            $rootScope.showSuccessMsg = 0;
            var data = {
                user: user
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentist-login", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
               
                if (data.status == 0) {
                    $rootScope.show_message = 'Wrong email id or password.';
                    $rootScope.showErrMsg = 1;
                } else if (data.status == 2) {
                    $rootScope.show_message = 'Your account is not verified, Please check your email and click the verification link';
                    $rootScope.showErrMsg = 1;
                } else if (data.status == 3) {
                    $rootScope.is_front_logged = 1;
                    
                    localStorage.setItem("access_token", data.data.token);
                    
                    localStorage.setItem("usr_info", data.data.usr_info);
                    
                    localStorage.setItem("dentist_id", data.data.user_id);
                    
                    localStorage.setItem("dentist_name", data.data.name);
                    
                    localStorage.setItem("plan", data.data.plan);
                    
                    localStorage.setItem("dentist_payment_status", data.data.payment_status);
                    
                    localStorage.setItem('dentist_profile_pic', data.data.final_profile_image);
                    $location.path('/dentist-checkout');
                } 
                else if (data.status == 4) {
                    $rootScope.is_front_logged = 1;
                    localStorage.setItem("access_token", data.data.token);
                    localStorage.setItem("usr_info", data.data.usr_info);
                    localStorage.setItem("dentist_id", data.data.user_id);
                    localStorage.setItem("dentist_name", data.data.name);
                    localStorage.setItem('dentist_profile_pic', data.data.final_profile_image);
                    $location.path('/dentist-messageboard');
                }else {
                    $rootScope.is_front_logged = 1;
                    if(data.popup_status==1){
                        localStorage.setItem("pop_open", 1);
                    }
                    localStorage.setItem("access_token", data.data.token);
                    localStorage.setItem("usr_info", data.data.usr_info);
                    localStorage.setItem("dentist_id", data.data.user_id);
                    localStorage.setItem("dentist_name", data.data.name);
                    localStorage.setItem('dentist_profile_pic', data.data.final_profile_image);
                    $location.path('/dentist-messageboard');
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        };
        $scope.ForgotPassword = function(user) {
            $scope.showErrMsg = 0;
            var data = {
                email: user.email,
                user_type: user.user_type
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/forgot-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $scope.show_class = "iferror_mess alert alert-danger";
                    $scope.show_message = 'Email Id does not exist.';
                    $scope.showErrMsg = 1;
                } else if (data.status == 2) {
                    $scope.show_class = "iferror_mess alert alert-danger";
                    $scope.show_message = 'You are unable to retrieve your password as your account is inactive.';
                    $scope.showErrMsg = 1;
                } else {
                    $scope.showErrMsg = 1;
                    $scope.show_class = "ifsucc_mess alert alert-success";
                    $scope.show_message = 'Change password link is sent to your email.';
                     $timeout(function() {
                        $location.path('/dentist-signin');
                    }, 45000);
                    $scope.forgotForm.reset();
                   
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        };
        $scope.checkValidUrl = function() {
            var url = $location.url();
            var param = url.split('/').pop();
            var data = {
                param: param
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/check-link", data, config).success(function(data, status, headers, config) {
                //console.log(data);
                if (data.status == 0) {
                    $scope.show_class = "alert alert-danger";
                    $scope.show_message = 'This link is expired';
                    $scope.showErrMsg = 1;
                    $scope.expiredlink = 1;
                } else {
                    $scope.user.email = data.data.email;
                }
            }).error(function(data, status, header, config) {
                //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.ResetPassword = function(user) {
            var data = {
                password: user.password,
                email: user.email
            };
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/change-forgot-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $scope.show_class = "ifsucc_mess alert alert-success";
                $scope.show_message = 'Your Password has been changed. Please logged in with new password.';
                $scope.showErrMsg = 1;
                 $timeout(function() {
                        $location.path('/dentist-signin');
                    }, 1000);
            }).error(function(data, status, header, config) {
                //console.log(data);
                $scope.show_message = 'Something is Wrong';
            });
        };
    }
})();