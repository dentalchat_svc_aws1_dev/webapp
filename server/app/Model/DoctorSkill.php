<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorSkill extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_skills';
     
}

