<!doctype html>
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta charset="utf-8">
<title>Activation Email</title>

</head>
 
<body>
<center>
  <style>
@import url('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
</style>
  <table width="620" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td style="background-repeat:no-repeat;background-position:0 0;background-size:cover;padding-left:30px;padding-top:30px;padding-right:30px;"><table width="560" border="0" cellpadding="0" cellspacing="0" style="">
          <thead>
            <tr>
              <td align="center" colspan="2" style="padding-bottom:22px; padding-top:22px;" bgcolor="#222">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td>
                        <a href="" target="_blank"><img src="<?php echo url();?>/public/backend/images/logo.png" alt=""></a>
                      </td>
                    </tr>
                </table>
              
              </td>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#222">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td id="top_part" height="70" background="<?php echo url();?>/public/backend/images/top_image.png" style="background-repeat:no-repeat;background-size:cover;background-position:right 0;"></td>
            </tr>
            <tr>
              <td style="padding-left:30px;padding-right:30px; padding-top: 15px;" bgcolor="#f8f8f8"><table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                 
                  <tr>
                    <td align="center">
                      <div style="width:96%; font-size:16px;line-height:22px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:700;background:#fff;border:1px solid #c59d5f;border-radius:25px;  display: inline-block;margin-bottom:30px;">
                     
                       
                        <p><?php echo $name ?> sends a message.</p>
                        <p>Message : <?php echo $message_content; ?></p>
                      </div>
                      </td>
                  </tr>
                  
                  
                </table></td>
            </tr>
            <tr>
              <td id="bot_part" background="<?php echo url();?>/public/backend/images/bot_image.png" height="71" style="background-size:cover;background-position:right;background-repeat:no-repeat;"></td>
            </tr>
              </table>
              
              </td>
            </tr>
          </tbody>
          <tfoot>
           <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#c59d5f">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td align="center" style="font-size: 18px;color: #ffffff;line-height: 22px;font-weight: 300;font-family: 'Lato', sans-serif;padding-top: 17px;padding-bottom: 17px;">Thank you</td>
            </tr>
            </table>
            </td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</center>
</body>
</html>