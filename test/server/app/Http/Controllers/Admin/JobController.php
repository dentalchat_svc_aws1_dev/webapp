<?php namespace App\Http\Controllers\Admin;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Country; /* Model name*/
//use App\Model\Restaurant; /* Model name*/
use App\Model\Category; /* Model name*/
use App\Model\SiteUser; /* Model name*/
use App\Model\Sitesetting; /* Model name*/
use App\Model\Language; /* Model name*/
use App\Model\City; /* Model name*/
use App\Model\Job; /* Model name*/
use App\Model\DocJob;

use App\Model\DocCategory; /* Model name*/
use App\Model\DocPractice; /* Model name*/
use App\Model\DocQualification; /* Model name*/
use App\Model\DocResearch; /* Model name*/
use App\Model\DocSpecialist; /* Model name*/



use App\Book;
use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;
use Cookie;
use Yajra\Datatables\Datatables;


class JobController extends BaseController {

  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
    	parent::__construct();
    }
    
	/********************************************************************************
	 *								GET USER LIST									*
	 *******************************************************************************/
    function jobList()
    {

    	$module_head 		= "Job List";
        $job_class 		    = "active";
        $job_cat 		    = Request::query('job_cat');
        $active 			= Request::query('active');
        $all_category       = Category::where('status','=','1')->get();
		$title				= "Job List";
        return view('admin.job.job_list',compact('job_class',
												'module_head',
												'active','title','all_category',
												'job_cat'
													));
    }
	
	/********************************************************************************
	 *									EDIT USER 									*
	 *******************************************************************************/
	function ajaxJobsList(){
		$job_cat 		   = Request::query('job_cat');
        $active 			= Request::query('active');
        $where = "";
		 if($job_cat != ''){
	            $where 			.= "`category_id`= '".$job_cat."' AND ";
	     }
        if($active != ''){
            $where 		   .= "`status`= '".$active."' AND ";
        }
        $where 		   .= '1';
        $all_jobs =  Job::with('users_details','category_details')->whereRaw($where)->select('jobs.*');

		return Datatables::of($all_jobs)
		->addColumn('checkbox_td', function ($all_jobs) {
                return '<input type="checkbox"  recordType="multipleRecord" multipleRecord="'.$all_jobs->id.'" />';
            })
		 ->addColumn('action', function ($all_jobs) {
                return '<a href="'.url().'/admin/job/details/'.$all_jobs->id.'" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;<a href="javascript:void(0);" onclick="removejob('.$all_jobs->id.')" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            })
		 ->editColumn('status', function ($all_jobs) {
		 	if($all_jobs->status == 1){
             	$status_html = '<select disabled style="width:110px;" name="status" id="user_active_'.$all_jobs->id.'" onchange="changeJobStatus(this.value,'.$all_jobs->id.')"><option value="1" selected>Approve</option><option value="0">Unapprove</option></select><br /><span class="alert-success" id="success_status_span_'.$all_jobs->id.'" style="display:none;"></span>';
		 	}else{
		 		$status_html = '<select style="width:110px;" name="status" id="user_active_'.$all_jobs->id.'" onchange="changeJobStatus(this.value,'.$all_jobs->id.')"><option value="1">Approve</option><option value="0" selected>Unapprove</option></select><span class="alert-success" id="success_status_span_'.$all_jobs->id.'" style="display:none;"></span>';

		 	}
		 	return $status_html;
            })
		 ->editColumn('name', function ($all_jobs) {
		 	return $all_jobs->users_details->name." ".$all_jobs->users_details->last_name;
		  })
		->make(true);
	}

	function viewJob($id=""){
		$module_head 		= "Job Details";
        $job_class 		    = "active";
		$job_details =  Job::with('users_details','category_details','city_details')->where('id','=',$id)->first();
		$travel_choice_arry = array("0"=>"City","1"=>"State","2"=>"Country","3"=>"Abroad");
		return view('admin.job.view_job_details',compact('job_class',
												'module_head','job_details','travel_choice_arry'
													));
	}

	function changestatus(){
		$status = Request::query('status');
		$job_id = Request::query('id');
		$job_details = Job::where('id','=',$job_id)->first();
		$job_category = $job_details->category_id;
		$job_city = $job_details->city;
		$city_details = City::where('id','=',$job_city)->first();
		$job_state = $city_details->state_id;
		$job_country = $city_details->country_id;
		$job_travel = $job_details->travel_choice;

		$fetchdoctor = SiteUser::with('docs_Category','docs_location')->where('status','=',1)->where('user_type','=','d')
					->whereHas('docs_Category', function($catquery) use ($job_category){
							$catquery->where('category_id', $job_category);
					});
		if($job_travel != 3){
			$fetchdoctor =	$fetchdoctor->whereHas('docs_location', function($locquery) use ($job_city,$job_travel,$job_state,$job_country){
						if($job_travel == 0) $locquery->where('city_id', $job_city);
						else if($job_travel == 1) $locquery->where('state_id', $job_state);
						else if($job_travel == 2) $locquery->where('country_id', $job_country);

					});
		}
		$fetchdoctor = $fetchdoctor->get();

		$sitesettings = DB::table('sitesettings')->where('id',1)->first();
		if(!empty($sitesettings))
		{
			$admin_users_email = $sitesettings->value;
		}
		$docjob = array();
		$docs_id = array();
		foreach ($fetchdoctor as $doctor_details) {
				$docjob[] = array('job_id'=>$job_id,'doc_id'=>$doctor_details->id);
				$docs_id[] = $doctor_details->id;
		}
		if(!empty($docs_id)){
			$doc_id_str = implode(",",$docs_id);
		}
		$cmd = "wget -bq --spider ".url()."/admin/send-approve-mail/".base64_encode($doc_id_str);
		shell_exec(escapeshellcmd($cmd));
		DocJob::insert($docjob);
		$update_status = Job::where('id','=',$job_id)->update([
												'status' => 1,
												]);
		echo "1";exit;
	}
	function deleteJob($id = ""){
		$delete_job = Job::where('id','=',$id)->delete();
		$delete_doctor_job  = DocJob::where('job_id','=',$id)->delete();
		Session::flash('success_message', 'Job deleted successfully.'); 
		Session::flash('alert-class', 'alert alert-success'); 
		return redirect('admin/job/list');
	}

	
	
}
