@extends('admin/layout/admin_template')

@section('content')
	{!! Form::open(['url' => 'admin/dentist/dentist-step7/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step7','name'=>'doc_step7']) !!}
		
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}
		{!! Form::hidden('pub_prev',0,['class'=>'span8','id'=>'pub_prev']) !!}
		
		<script type="text/javascript" src="<?php echo url();?>/public/backend/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="<?php echo url();?>/public/backend/css/bootstrap-multiselect.css" type="text/css"/>
		
		<div class="control-group" style="display:block;">						
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Language Spoken </label>
				<div class="controls">

				{!! Form::select('languages[]', $languages,$selected_lang_array, array('class' => 'span3', 'id'=>'example-getting-started','multiple'=>true)) !!}

				</div>
			</div>	

			<!-- <div class="control-group" style="display:block;">
				<?php //for($i= 0; $i<count($selected_lang_array_name);$i++)
				{
				?>
				<p id="rcorners1"><?php //echo $selected_lang_array_name[$i]?></p>
				<?php 
				}	
				?>		
			</div> -->

			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Personal Statement </label>
				<div class="controls">
					<textarea rows="2" class="span8" name="personal_statement" id="personal_statement"><?php echo (isset($doc_language[0]['personal_statement']))?$doc_language[0]['personal_statement']:''?></textarea>
				</div>
			</div>
		</div>		
		
			
		<div class="control-group">
		
			<div class="controls">
				<a href="{!! url('admin/dentist/dentist-step6/'.$user_id)!!}" class="btn">Prev</a>
				{!! Form::submit('Preview',array('class'=>'btn','name'=>'action','value'=>'Preview')) !!}	
				{!! Form::button('Publish',array('class'=>'btn','name'=>'action','value'=>'Publish','onClick' => 'previewAdmin()')) !!}				
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
	
<script type="text/javascript">
    $(document).ready(function() {
        $('#example-getting-started').multiselect({
        	maxHeight: 140
        });
    });
    function previewAdmin()
    {
    	$("#pub_prev").val(1);
    	$("#doc_step7").submit();
    }
</script>

@stop