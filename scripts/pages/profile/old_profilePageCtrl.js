(function() {
    'use strict';
    angular.module('doctorchat.pages.profile').factory('chatSocket', function(socketFactory) {

        var myIoSocket = io.connect('https://dentalchat.com:8005/');
        var socket = socketFactory({
            ioSocket: myIoSocket
        });
        myIoSocket.on('connect', function(){
            //console.log('connected');
        });
        return socket;
    }).filter('ageFilter', function() {
        function calculateAge(birthday) {
            var date = new Date(birthday);
            var ageDifMs = Date.now() - date.getTime();
            var ageDate = new Date(ageDifMs);
            var age = Math.abs(ageDate.getUTCFullYear() - 1970);
            if (isNaN(age)) {
                return 0;
            } else {
                return age;
            }
        }
        return function(birthdate) {
            return calculateAge(birthdate);
        };
    }).value('messageFormatter', function(date, nick, message) {
        return date.toLocaleTimeString() + ' - ' + nick + ' - ' + message + '\n';
    }).value('chatImageApiPath', 'https://dentalchat.com:8005/').value('chatImagePath', 'https://dentalchat.com:8005/').directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    }).controller('profilePageCtrl', profilePageCtrl);

    function profilePageCtrl($scope, $timeout, $window, $http, configService, $rootScope, $location, homeService, $stateParams, $state, $log, chatSocket, messageFormatter, chatImagePath, chatImageApiPath, SweetAlert) {
        var currentState = $state.current.name;
        $scope.apiUrl = configService.getEnvConfig().apiURL;
        $scope.startDateBeforeRender = function ($dates) {
            const todaySinceMidnight = new Date();
            todaySinceMidnight.setUTCHours(0,0,0,0);
            $dates.filter(function (date) {
              return date.utcDateValue < todaySinceMidnight.getTime();
            }).forEach(function (date) {
              date.selectable = false;
            });
        }
        $scope.showLoader = 0;
        $window.scrollTo(0, 0);
        $scope.dentist = {};
        $rootScope.obj = {};
        $scope.expiredlink = 0;
        $rootScope.showSuccessMsg = 0;
        $rootScope.showErrMsg = 0;
        $rootScope.showErrMsgFrgt = 0;
        $rootScope.show_message = '';
        $scope.dentist = {};
        $scope.showimgprev = 0;
        $scope.show_message_old_password = '';
        $scope.show_color = "";
        $scope.showSuccMsg = 0;
        $scope.old_password_class = '';
        $scope.dentistLanguage = {};
        $scope.allLanguage = [];
        $scope.dentistSkill = {};
        $scope.docsMore = [];
        $scope.docfiles = [];
        $scope.docsExp = [];
        $scope.docexpfiles = [];
        $scope.docsEdu = [];
        $scope.docedufiles = [];
        $scope.dentistAwards = {};
        $scope.dentistInsurance = {};
        $scope.currentDate = new Date().toString();
        $scope.messageLogPatient = [];
        $scope.searchSelectAllSettings = {
            enableSearch: true,
            showSelectAll: true,
            keyboardControls: true,
            smartButtonMaxItems: 5,
            smartButtonTextConverter: function(itemText, originalItem) {
                return itemText;
            }
        };
        $scope.resetSearch = function(){
            $scope.searchKeyword = '';
        }
        $scope.imageUploadMy = function(files) {
            $scope.dentist.profile_image_data = files[0];
        }
        $scope.imageUploadClinic = function(files) {
            $scope.dentistClient.clinic_picture_data = files[0];
        }
        $scope.showImage = function() {
            $scope.showimgprev = 1;
        }
        $scope.delete_image = function() {
            $scope.showimgprev = 0;
        }
        $scope.checkEmail = function(email) {
            var patt = /^$|^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;
            var res = patt.test(email);
            if (res) {
                return 1;
            } else {
                if (typeof email == 'undefined') {
                    return 1;
                }
                return 0;
            }
        };



        if ($rootScope.curr_state == 'dentistProfileStep1') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step1", {}, config).success(function(data, status, headers, config) {
              
                if (data.status == 1) {
                    $scope.dentist = data.dentistdetails.docs_details;
                    $rootScope.dentist_profile_pic = data.dentistdetails.docs_details.profile_pics;
                    localStorage.setItem('dentist_profile_pic', data.dentistdetails.docs_details.profile_pics);
                    
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $rootScope.dentist_profile_pic = localStorage.getItem('dentist_profile_pic');
        $scope.updateDentistStep1 = function(dentist) {
            
            var fd = new FormData();
            for (var key in $scope.dentist) {
                if (typeof($scope.dentist[key]) == "object") {
                    fd.append(key, JSON.stringify($scope.dentist[key]));
                } else {
                    fd.append(key, $scope.dentist[key]);
                }
            }
            fd.append('profile_pics', $scope.dentist.profile_image_data);
            fd.append('profile_pics_exists', $scope.showimgprev);
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step1", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                localStorage.setItem('currentLoggedUname', $scope.dentist.first_name);
               
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.dentist = data.update_doc_details.docs_details;
                    $rootScope.dentist_profile_pic = data.update_doc_details.docs_details.profile_pics;
                    localStorage.setItem('dentist_profile_pic', data.update_doc_details.docs_details.profile_pics);
                    $location.path('/dentist-profile-step2');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message_email = 'Email already exists..';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        if ($rootScope.curr_state == 'dentistProfileStep2') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step2", {}, config).success(function(data, status, headers, config) {
                
                if (data.status == 1) {
                    $scope.dentistClient = data.dentistdetails.docs_clinics;
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.updateDentistStep2 = function(dentistClient) {
            var fd = new FormData();
            for (var key in $scope.dentistClient) {
                if (typeof($scope.dentistClient[key]) == "object") {
                    fd.append(key, JSON.stringify($scope.dentistClient[key]));
                } else {
                    fd.append(key, $scope.dentistClient[key]);
                }
            }
            fd.append('clinic_picture', $scope.dentistClient.clinic_picture_data);
            fd.append('clinic_picture_exists', $scope.showimgprev);
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step2", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.dentistClient = data.update_clinic_det.docs_clinics;
                    $location.path('/dentist-profile-step3');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.dentist_awards = [];
        $scope.dentist_insurance = [];
        $scope.addNewAward = function() {
            var newItemNo = $scope.dentist_awards.length + 1;
            $scope.dentist_awards.push({
                'id': 'choice' + newItemNo
            });
        }
        $scope.removeAward = function(index, award_id) {

         swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {

            var lastItem = $scope.dentist_awards.length - 1;
            $scope.dentist_awards.splice(index, 1);
            var data = {
                award_id: award_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-award", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {} else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          
          } 

        });
    };
    $scope.setAppointment = function(aptVal) {
        var data = {
            'patient_id': $scope.patientDetails.get_patient.id,
            'doctor_id': $scope.dentistId,
            'post_id': $scope.postId,
            'is_status': aptVal,
            'proposedate':$scope.proposeDate||''
        }
        if(aptVal==2){
            swal({
              title: "Are you sure?",
              text: "Are you sure you want to reset the post for appointment!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, reset it!",
              cancelButtonText: "No, plx!",
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm){
              if(isConfirm){
                $rootScope.showLoader = 1;
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "set-appointment-doctors",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.appointment = aptVal;
                        $scope.appointment_time = response.proposedate;
                        SweetAlert.swal("Done!", response.msg, "success");
                        $scope.message = 'You can reschedule appointment'
                        $scope.sendMessage();
                        
                    } else {
                        SweetAlert.swal("Oops!", "There is some problem", "error");
                    }
                }).error(function(response) {
                    SweetAlert.swal("Oops!", "There is some problem", "error");
                });
              }
            }); 
        }else{
            $rootScope.showLoader = 1;
            $http({
                method: 'post',
                dataType: "jsonp",
                crossDomain: true,
                url: configService.getEnvConfig().apiURL + "set-appointment-doctors",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(response, status, headers, config) {
                if (response.status == 1) {
                    $rootScope.showLoader = 0;
                    $scope.patientDetails.appointment = aptVal;
                    $scope.patientDetails.is_status = response.is_status;
                    $scope.patientDetails.appointment_time = response.proposedate;
                    SweetAlert.swal("Done!", response.msg, "success");
                    $scope.message = 'Appointment Proposed for '+$scope.patientDetails.appointment_time;
                    $scope.sendMessage();
                    
                } else {
                    SweetAlert.swal("Oops!", "There is some problem", "error");
                }
            }).error(function(response) {
                SweetAlert.swal("Oops!", "There is some problem", "error");
            });
        }
    }

        $scope.delectEducationCertificate = function(index, remove_id) {

          swal({
              title: "Are you sure?",
              text: "Are you sure you want to delete!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: true,
             closeOnCancel: true
            },
            function(isConfirm){
              if (isConfirm) {

                var data = {
                    remove_id: remove_id
                };
                $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-certificate", data, config).success(function(data, status, headers, config) {
                    $rootScope.showLoader = 0;
                    $rootScope.checkLastServieCallTimeAfterLogin();
                    if (data.status == 1) {
                        $scope.docsEdu[index].certificate = '';
                    } else {
                        $rootScope.showLoader = 0;
                        $scope.show_color = "color:red";
                        $scope.show_message = 'Something is Wrong';
                    }
                }).error(function(data, status, header, config) {
                    $scope.show_message = 'Something is Wrong';
                });

              }
         });
 
        }



        $scope.addNewInsurance = function() {
            var newItemNo = $scope.dentist_insurance.length + 1;
            $scope.dentist_insurance.push({
                'id': 'choice' + newItemNo
            });
        }
        $scope.removeInsurance = function(index, insurance_id) {

           swal({
              title: "Are you sure?",
              text: "Are you sure you want to delete!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: true,
             closeOnCancel: true
            },
            function(isConfirm){
              if (isConfirm) {

            var lastItem = $scope.dentist_insurance.length - 1;
            $scope.dentist_insurance.splice(index, 1);
            var data = {
                insurance_id: insurance_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-insurance", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {} else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          }
       });

        };
        $scope.docsEdu = [];
        $scope.addNewEducation = function() {
            var newItemNo = $scope.docsEdu.length + 1;
            $scope.docsEdu.push({
                'id': 'moredoc' + newItemNo,
                'docname': ''
            });
        };
        $scope.removeEducation = function(index, exp_id) {

          swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
             $scope.docedufiles.splice(index, 1);
                $scope.docsEdu.splice(index, 1);
                var data = {
                    exp_id: exp_id
                };
                $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-education", data, config).success(function(data, status, headers, config) {
                    $rootScope.showLoader = 0;
                    $rootScope.checkLastServieCallTimeAfterLogin();
                    if (data.status == 1) {
                      //swal("Deleted!", "Delete.", "success");
                    } else {
                        $rootScope.showLoader = 0;
                        $scope.show_color = "color:red";
                        $scope.show_message = 'Something is Wrong';
                    }
                }).error(function(data, status, header, config) {
                    $scope.show_message = 'Something is Wrong';
                });
          } 

        });
            



        };
        $scope.docEduUpload = function(files, i) {
            $scope.docedufiles[i] = files[0];
        };
        $scope.changeTheEdu = function(files, i) {
            $scope.show_loader = 1;
            $scope.$digest();
            var i_arr = i.split('_');
            i = i_arr[i_arr.length - 1];
            var reader = new FileReader();
            $scope.docEduUpload(files, i);
            if (typeof files[0] != 'undefined') {
                reader.readAsDataURL(files[0]);
                reader.onload = function(e) {
                    var t = files[0].type,
                        n = files[0].name,
                        s = ~~(files[0].size / 1024);
                    var image_type_arr = n.split(".");
                    var image_type = image_type_arr[image_type_arr.length - 1].toLowerCase();
                    if (!(image_type === 'pdf' || image_type === 'jpg' || image_type === 'jpeg' || image_type === 'png' || image_type === 'gif')) {
                        sweetAlert("Oops...", "Not a valid document type.Please use pdf/jpeg/jpg/png type file.", "error");
                        $scope.show_loader = 0;
                        $scope.$digest();
                        return;
                    } else if (s > 10 * 1024) {
                        $scope.show_loader = 0;
                        sweetAlert("Oops...", "File size should be less than 10MB", "error");
                        $scope.$digest();
                        return;
                    } else {
                        if (typeof $scope.docsEdu[i] == 'undefined') {
                            $scope.docsEdu[i] = {};
                        }
                        $scope.docsEdu[i].doc_file_name = n;
                        $scope.show_loader = 0;
                        $scope.$digest();
                    }
                };
            } else {
                sweetAlert("Oops...", "No file selected", "error");
                $scope.show_loader = 0;
                $scope.$digest();
            }
        };
        if ($rootScope.curr_state == 'dentistProfileStep3') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step3", {}, config).success(function(data, status, headers, config) {
              
                if (data.status == 1) {
                    if (data.dentist_award.length == 0) {
                        $scope.addNewAward();
                    } else {
                        for (var i in data.dentist_award) {
                            $scope.dentist_awards.push(data.dentist_award[i]);
                        }
                    }
                    if (data.dentist_insurance.length == 0) {
                        $scope.addNewInsurance();
                    } else {
                        for (var i in data.dentist_insurance) {
                            $scope.dentist_insurance.push(data.dentist_insurance[i]);
                        }
                    }
                }
                if (data.dentist_education_data.length == 0) {
                    $scope.addNewEducation();
                } else {
                    $scope.docedufiles = [];
                    for (var key in data.dentist_education_data) {
                         var ext = data.dentist_education_data[key].certificate.substr(data.dentist_education_data[key].certificate.lastIndexOf('.')+1);
                         data.dentist_education_data[key]['ext'] = ext;
                         $scope.docsEdu.push(data.dentist_education_data[key]);
                        if (data.dentist_education_data[key].certificate == '' || data.dentist_education_data[key].certificate == null) $scope.docedufiles.push('');
                        else
                            $scope.docedufiles.push(data.dentist_education_data[key].certificate);
                            
                    }
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.updateDentistStep3 = function() {
            $rootScope.showLoader = 1;
            var fd = new FormData();
            for (var key2 in $scope.docedufiles) {
                fd.append('documents[' + key2 + ']', $scope.docedufiles[key2]);
            }
            fd.append('dentist_awards', JSON.stringify($scope.dentist_awards));
            fd.append('dentist_insurance', JSON.stringify($scope.dentist_insurance));
            fd.append('documentName', JSON.stringify($scope.docsEdu));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step3", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $location.path('/dentist-profile-step4');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.docsMore = [];
        $scope.addNewLicense = function() {
            var newItemNo = $scope.docsMore.length + 1;
            $scope.docsMore.push({
                'id': 'moredoc' + newItemNo,
                'docname': ''
            });
        };


        $scope.removeLicense = function(index, license_id) {
 
        swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $scope.docfiles.splice(index, 1);
            $scope.docsMore.splice(index, 1);
            var data = {
                license_id: license_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-license", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {} else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';

                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          
          }

        });


        };
        $scope.delectLicense = function(index, remove_id) {

         swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            
            var data = {
                remove_id: remove_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-license-image", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.docsMore[index].license_doc = '';
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });

        }
      });


        }
        $scope.docUpload = function(files, i) {
            $scope.docfiles[i] = files[0];
        };
        $scope.changeTheDoc = function(files, i) {
            $scope.show_loader = 1;
            $scope.$digest();
            var i_arr = i.split('_');
            i = i_arr[i_arr.length - 1];
            var reader = new FileReader();
            $scope.docUpload(files, i);
            if (typeof files[0] != 'undefined') {
                reader.readAsDataURL(files[0]);
                reader.onload = function(e) {
                    var t = files[0].type,
                        n = files[0].name,
                        s = ~~(files[0].size / 1024);
                    var image_type_arr = n.split(".");
                    var image_type = image_type_arr[image_type_arr.length - 1].toLowerCase();
                    if (!(image_type === 'pdf' || image_type === 'jpg' || image_type === 'jpeg' || image_type === 'png' || image_type === 'gif')) {
                        sweetAlert("Oops...", "Not a valid document type.Please use pdf/jpeg/jpg/png type file.", "error");
                        $scope.show_loader = 0;
                        $scope.$digest();
                        return;
                    } else if (s > 10 * 1024) {
                        $scope.show_loader = 0;
                        sweetAlert("Oops...", "File size should be less than 10MB", "error");
                        $scope.$digest();
                        return;
                    } else {
                        if (typeof $scope.docsMore[i] == 'undefined') {
                            $scope.docsMore[i] = {};
                        }
                        $scope.docsMore[i].doc_file_name = n;
                        $scope.show_loader = 0;
                        $scope.$digest();
                    }
                };
            } else {
                sweetAlert("Oops...", "No file selected", "error");
                $scope.show_loader = 0;
                $scope.$digest();
            }
        };
        if ($rootScope.curr_state == 'dentistProfileStep4') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step4", {}, config).success(function(data, status, headers, config) {
                if (data.status == 1) {
                    $scope.license = data.dentist_license_data;
                    $scope.docfiles = [];
                    for (var key in data.dentist_license_data) {
                        if (typeof(data.dentist_license_data[key].license_photo) != 'undefined' || typeof(data.dentist_license_data[key].license_details) != 'undefined') {
                            
                           var ext = data.dentist_license_data[key].license_photo.substr(data.dentist_license_data[key].license_photo.lastIndexOf('.')+1);
                           data.dentist_license_data[key]['ext'] = ext;
                            $scope.docsMore.push({
                                'id': data.dentist_license_data[key].id,
                                'license': data.dentist_license_data[key].license_details,
                                'license_doc': data.dentist_license_data[key].license_photo,
                                'org_license': data.dentist_license_data[key].org_license,
                                'ext': ext
                            });
                            $scope.docfiles.push(data.dentist_license_data[key].license_photo);
                        }
                    }
                }
                if (!$scope.docsMore.length) {
                    $scope.addNewLicense();
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.updateDentistStep4 = function() {
            $rootScope.showLoader = 1;
            var fd = new FormData();
            for (var key2 in $scope.docfiles) {
                fd.append('documents[' + key2 + ']', $scope.docfiles[key2]);
            }
            fd.append('documentName', JSON.stringify($scope.docsMore));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step4", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $location.path('/dentist-profile-step5');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.docsExp = [];
        $scope.addNewExp = function() {
            var newItemNo = $scope.docsExp.length + 1;
            $scope.docsExp.push({
                'id': 'moredoc' + newItemNo,
                'docname': ''
            });
        };
     
        $scope.removeExperience = function(index, exp_id) {


         swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {

            $scope.docexpfiles.splice(index, 1);
            $scope.docsExp.splice(index, 1);
            var data = {
                exp_id: exp_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-experience", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {} else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          }
        });

        };


        $scope.delectExprience = function(index, remove_id) {

         swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {

            var data = {
                remove_id: remove_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-experience-image", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.docsExp[index].experience_doc = '';
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          }
         });
       }

        $scope.docExpUpload = function(files, i) {
            $scope.docexpfiles[i] = files[0];
        };
        $scope.changeTheExp = function(files, i) {
            $scope.show_loader = 1;
            $scope.$digest();
            var i_arr = i.split('_');
            i = i_arr[i_arr.length - 1];
            var reader = new FileReader();
            $scope.docExpUpload(files, i);
            if (typeof files[0] != 'undefined') {
                reader.readAsDataURL(files[0]);
                reader.onload = function(e) {
                    var t = files[0].type,
                        n = files[0].name,
                        s = ~~(files[0].size / 1024);
                    var image_type_arr = n.split(".");
                    var image_type = image_type_arr[image_type_arr.length - 1].toLowerCase();
                    if (!(image_type === 'pdf' || image_type === 'jpg' || image_type === 'jpeg' || image_type === 'png' || image_type === 'gif')) {
                        sweetAlert("Oops...", "Not a valid document type.Please use pdf/jpeg/jpg/png/gif type file.", "error");
                        $scope.show_loader = 0;
                        $scope.$digest();
                        return;
                    } else if (s > 10 * 1024) {
                        $scope.show_loader = 0;
                        sweetAlert("Oops...", "File size should be less than 10MB", "error");
                        $scope.$digest();
                        return;
                    } else {
                        if (typeof $scope.docsExp[i] == 'undefined') {
                            $scope.docsExp[i] = {};
                        }
                        $scope.docsExp[i].doc_file_name = n;
                        $scope.show_loader = 0;
                        $scope.$digest();
                    }
                };
            } else {
                sweetAlert("Oops...", "No file selected", "error");
                $scope.show_loader = 0;
                $scope.$digest();
            }
        };
        if ($rootScope.curr_state == 'dentistProfileStep5') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step5", {}, config).success(function(data, status, headers, config) {
                if (data.status == 1) {
                    $scope.license = data.dentist_experience_data;
                    $scope.docexpfiles = [];
                    for (var key in data.dentist_experience_data) {
                        var ext = data.dentist_experience_data[key].exp_photo.substr(data.dentist_experience_data[key].exp_photo.lastIndexOf('.')+1);
                           data.dentist_experience_data[key]['ext'] = ext;
                          
                        $scope.docsExp.push({
                            'id': data.dentist_experience_data[key].id,
                            'prof_exp': data.dentist_experience_data[key].prof_exp,
                            'dental_business': data.dentist_experience_data[key].dental_business,
                            'location': data.dentist_experience_data[key].location,
                            'exp_from': data.dentist_experience_data[key].exp_from,
                            'exp_to': data.dentist_experience_data[key].exp_to,
                            'experience_doc': data.dentist_experience_data[key].exp_photo,
                            'org_exp': data.dentist_experience_data[key].org_exp,
                            'ext':ext
                        });
                        $scope.docexpfiles.push(data.dentist_experience_data[key].exp_photo);
                    }
                }
                if (!$scope.docsExp.length) {
                    $scope.addNewExp();
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.updateDentistStep5 = function() {
            $rootScope.showLoader = 1;
            var fd = new FormData();
            for (var key2 in $scope.docexpfiles) {
                fd.append('documents[' + key2 + ']', $scope.docexpfiles[key2]);
            }
            fd.append('documentName', JSON.stringify($scope.docsExp));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step5", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $location.path('/dentist-profile-step6');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        if ($rootScope.curr_state == 'dentistProfileStep6') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step6", {}, config).success(function(data, status, headers, config) {
                if (data.status == 1) {
                    $scope.dentist_skills = data.dentist_skills;
                }
                if (!$scope.dentist_skills.length) {
                    $scope.addNewSkill();
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.dentist_skills = [{
            id: 'choice1'
        }];
        $scope.addNewSkill = function() {
            var newItemNo = $scope.dentist_skills.length + 1;
            $scope.dentist_skills.push({
                'id': 'choice' + newItemNo
            });
        }


        $scope.removeChoice = function(index, skill_id) {
 
          swal({
              title: "Are you sure?",
              text: "Are you sure you want to delete!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: true,
              closeOnCancel: true
          },
        function(isConfirm){
          if (isConfirm) {
           
            var lastItem = $scope.dentist_skills.length - 1;
            $scope.dentist_skills.splice(index, 1);
            var data = {
                skill_id: skill_id
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/remove-skill", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {} else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
          }
       });

        };
        $scope.updateDentistStep6 = function() {
            $rootScope.showLoader = 1;
            var fd = new FormData();
            for (var key in $scope.dentistSkill) {
                fd.append(key, $scope.dentistSkill[key]);
            }
            fd.append('dentist_skills', JSON.stringify($scope.dentist_skills));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step6", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $location.path('/dentist-profile-step7');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        if ($rootScope.curr_state == 'dentistProfileStep7') {
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token,
                }
            };
            $scope.messageLogPatient = [];
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-step7", {}, config).success(function(data, status, headers, config) {
                if (data.status == 1) {
                    $scope.dentistLanguage = data.dentist_lang_details.docs_language;
                    $scope.language_option = data.languages;
                    if (typeof data.dentist_lang_details.docs_language != 'undefined') {
                        if (data.dentist_lang_details.docs_language.languages != '') {
                            var lang_arr = data.dentist_lang_details.docs_language.languages.split(',');
                            for (var key in lang_arr) {
                                $scope.allLanguage.push({
                                    'id': parseInt(lang_arr[key])
                                });
                            }

                            $scope.$broadcast('chckedget', {
                                data: $scope.allLanguage
                            });
                        }
                        
                    }
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }

        function fd_append(fd, obj) {
            for (var key in obj) {
                if (typeof obj[key] == 'object') {
                    fd = fd_append(fd, obj[key]);
                } else
                    fd.append(key, obj[key]);
            }
            return fd;
        }
        $scope.updateDentistStep7 = function(dentistLanguage) {
            var fd = new FormData();
            fd = fd_append(fd, $scope.dentistLanguage)
            fd.append('allLanguage', JSON.stringify($scope.allLanguage));
            fd.append('pagetype', JSON.stringify($scope.allLanguage));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/update-dentist-profile-step7", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.dentistLanguage = data.dentist_lang_details.docs_language;
                    swal({
                        title: "All Set !!!",
                        text: "Your Profile will be activated in few minutes. Please check your email for further instructions",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                        imageUrl: "images/thumbs-up.jpg"
                    }, function(isConfirm) {
                        if (isConfirm) {
                            $location.path('/dentist-preview');
                        }
                    });
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });

            
            
        }
        $scope.savePreviewShow = function() {
            var fd = new FormData();
            fd = fd_append(fd, $scope.dentistLanguage)
            fd.append('allLanguage', JSON.stringify($scope.allLanguage));
            fd.append('pagetype', JSON.stringify($scope.allLanguage));
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': undefined,
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/save-show-preview", fd, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                $rootScope.checkLastServieCallTimeAfterLogin();
                if (data.status == 1) {
                    $scope.dentistLanguage = data.dentist_lang_details.docs_language;
                    $location.path('/dentist-preview');
                    $window.scrollTo(0, 0);
                } else {
                    $rootScope.showLoader = 0;
                    $scope.show_color = "color:red";
                    $scope.show_message = 'Something is Wrong';
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.changeCustomerPassword = function(dentist) {
            var data = {
                dentist: dentist
            };
            var token = localStorage.getItem("access_token");
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $rootScope.showLoader = 1;
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/change-password", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $scope.show_color = "color:red";
                    $scope.show_message_old_password = 'Invalid old password..';
                    $scope.old_password_class = 'has-error';
                } else {
                    $scope.change_class = "alert alert-success";
                    $scope.show_message = 'Password has been updated successfully.';
                    $scope.showSuccMsg = 1;
                    $window.scrollTo(0, 0);
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        
        $scope.$watch('dentist.old_password', function(value, oldValue) {
            if (oldValue != value) {
                $scope.show_message_old_password = '';
                $scope.old_password_class = '';
            }
        });

        $scope.getAllDentistDetails = function() {
            $rootScope.showLoader = 1;
            var data = {
                params: $stateParams.params
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-profile", data).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $location.path('/');
                } else {
                    $scope.dentist_details = data.dentist_details;
                    $scope.degree = '';
                    $scope.clinic_address = data.dentist_details.tmp_docs_clinics.address;
                    var tmp = '';
                    if (typeof data.dentist_details.tmp_docs_education != 'undefined') {
                        for (var i = 0; i < data.dentist_details.tmp_docs_education.length; i++) {
                            tmp += data.dentist_details.tmp_docs_education[i].degree + ', ';
                        }
                        $scope.degree = tmp.replace(/,\s*$/, "");
                    }
                    if (typeof $scope.dentist_details.tmp_docs_experiece != 'undefined') {
                        for (var i = 0; i < $scope.dentist_details.tmp_docs_experiece.length; i++) {
                            $scope.jobtitle = $scope.dentist_details.tmp_docs_experiece[$scope.dentist_details.tmp_docs_experiece.length - 1].prof_exp;
                        }
                    }
                    $timeout(function() {
                        initialize(data.dentist_details.tmp_docs_clinics.lat, data.dentist_details.tmp_docs_clinics.lang);
                    }, 2000);
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });

        }

        $scope.changeStatus = function(id,status) { 
           $rootScope.showLoader = 1;
            var data = {review_id:id,status:status};
            
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/change-status", data).success(function(data, status) {
                $rootScope.showLoader = 0;
                if (data == 1) {
                    $window.location.reload();
                    //$location.path('/');
                } 
            }).error(function(data, status) {
                $scope.show_message = 'Something is Wrong';
            }); 
        }

        $scope.getAllDentistPreviewData = function() {
            $rootScope.showLoader = 1;
            var token = localStorage.getItem("access_token");
            var data = {};
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-preview", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 0) {
                    $location.path('/');
                } else {
                    $scope.dentist_details = data.dentist_details;
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }
        $scope.approvedPublish = function() {
            $rootScope.showLoader = 1;
            var token = localStorage.getItem("access_token");
            var data = {};
            var config = {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Access-Token': token
                }
            };
            $http.post(configService.getEnvConfig().apiURL + "service/dentistservice/dentist-publish", data, config).success(function(data, status, headers, config) {
                $rootScope.showLoader = 0;
                if (data.status == 1) {
                    swal({
                        title: "All Set !!!",
                        text: "Your Profile will be activated in few minutes. Please check your email for further instructions",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                        imageUrl: "images/thumbs-up.jpg"
                    }, function(isConfirm) {
                        if (isConfirm) {
                            $location.path('/dentist-preview');
                        }
                    });
                }
            }).error(function(data, status, header, config) {
                $scope.show_message = 'Something is Wrong';
            });
        }

        if (currentState == 'dentistprofile') {
            $scope.getAllDentistDetails()
        }
        if (currentState == 'dentistpreview') {
            $scope.getAllDentistPreviewData()
        }
        $scope.openChatHeadWindow = false;
        $scope.clickFirstPost =function()
        {
            $timeout(function(){
            var post_trigger= $(".message-left-panel .tab-pane ul li:first-child").attr('data-post');
            var post_trigger_obj = JSON.parse(post_trigger); 
            $scope.openChatHead(post_trigger_obj,0);
             },2000); 
        }
        
        if(currentState == 'dentistmydashboard' || currentState == 'dentistmessageboard'){
            $scope.fetchAttendentPostCount = function() {
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id')
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "tot-attendant-post-count",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $scope.attendentPostCount = response.tot_attendant_post;
                    } else {
                        $scope.attendentPostCount = 0;
                    }
                }).error(function(response) {
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.fetchUnAttendentPostCount = function() {
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id')
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "tot-unattendant-post-count",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $scope.unattendentPostCount = response.tot_unattendant_post;
                    } else {
                        $scope.unattendentPostCount = 0;
                    }
                }).error(function(response) {
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
        }
        if(currentState == 'dentistmydashboard'){
            $scope.emergency_total = 100;
            $scope.emergency_attended_total = 0;
            $scope.emergency_unattended_total = 100;
            $scope.emergency_attended_per = parseFloat(($scope.emergency_attended_total*100)/$scope.emergency_total).toFixed(2);
            $scope.emergency_unattended_per = parseFloat(100-$scope.emergency_attended_per).toFixed(2);
            $scope.nonemergency_total = 100;
            $scope.nonemergency_attended_total = 0;
            $scope.nonemergency_unattended_total = 100;
            $scope.nonemergency_attended_per = parseFloat(($scope.nonemergency_attended_total*100)/$scope.nonemergency_total).toFixed(2);
            $scope.nonemergency_unattended_per = parseFloat(100-$scope.nonemergency_attended_per).toFixed(2);
            
            $scope.chartsItem =  '';
            $scope.piechartsdata = $scope.piechartslabels = [];
            $scope.fetchAttendentPostCount();
            $scope.fetchUnAttendentPostCount();
            
            $scope.changePeriodcharts = function () {
                $http.post(configService.getEnvConfig().apiURL + "get-barcharts-data", {type: $scope.chartsItem,'doctor_id': localStorage.getItem('dentist_id')}, config).success(function(res, status, headers, config) {
                    $rootScope.checkLastServieCallTimeAfterLogin();
                    if (res.status == 1) {
                        $scope.piechartsdata = res.chartdata.data;
                        $scope.piechartslabels = res.chartdata.labels;
                        $scope.emergency_total = res.progressbar.emergency_total;
                        $scope.emergency_attended_total = res.progressbar.emergency_attended_total;
                        $scope.emergency_unattended_total = res.progressbar.emergency_unattended_total;
                        $scope.emergency_attended_per = parseFloat(($scope.emergency_attended_total*100)/$scope.emergency_total).toFixed(2);
                        $scope.emergency_unattended_per = parseFloat(100-$scope.emergency_attended_per).toFixed(2);
                        $scope.nonemergency_total = res.progressbar.nonemergency_total;
                        $scope.nonemergency_attended_total = res.progressbar.nonemergency_attended_total;
                        $scope.nonemergency_unattended_total = res.progressbar.nonemergency_unattended_total;
                        $scope.nonemergency_attended_per = parseFloat(($scope.nonemergency_attended_total*100)/$scope.nonemergency_total).toFixed(2);
                        $scope.nonemergency_unattended_per = parseFloat(100-$scope.nonemergency_attended_per).toFixed(2);
                    } 
                }).error(function(res, status, header, config) {
                    $scope.show_message = 'Something is Wrong';
                });
                
            };
            
            
            $scope.changePeriodcharts();

        }
        
        if (currentState == 'dentistmessageboard') {
            $scope.postList = [];
            $scope.postListAttended = [];
            $scope.postListUnAttended = [];
            $scope.clickFirstPost();
            $rootScope.showLoader = 1;

             
               $scope.currentPage = 15;
               $scope.currentPaged = 0;
               $scope.currentPagedatt=0;
               $scope.currentunPagedatt=0;
               $scope.totalItems = 0;
               $scope.pageSize = 15;
               $scope.totalallPage=0;
               $scope.totalattndPage=0;
               $scope.totalunattndPage=0;

            $scope.fetchAllPost = function(pagesize) {
                $scope.openChatHeadWindow = false;
                $scope.searchKeyword = '';
                $scope.selectedTab = 2;
                $scope.selectedTab = 1;
                $rootScope.showLoader = 1;
                     if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+15; 
                    $scope.currentPaged-=1;
                    //$scope.currentPaged-=1;
                }
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  '0',
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-patients-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }, 
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postList = response.patient_posts;
                         $scope.totalItems = response.total_count;
                        $scope.totalallPage= Math.ceil($scope.totalItems/$scope.pageSize);
                         $scope.currentPaged=1;
                        angular.forEach($scope.postList , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time*1000));
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postList[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                //var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postList[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                            chatSocket.emit("set_name", {
                                from: Number(localStorage.getItem('dentist_id')),
                                senderName: localStorage.getItem('dentist_name'),
                                buddy: Number(value.get_patient.id),
                                receiverName: value.get_patient.name + ' ' + value.get_patient.last_name,
                                roomName: '',
                                post_id: Number(value.post_id)
                            });
                        });
                        
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
         
        $scope.fetchAllPostPagi = function(pagesize) {
                
        
                $scope.openChatHeadWindow = false;
                $scope.searchKeyword = '';
                $scope.selectedTab = 1;
                $rootScope.showLoader = 1;
                if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+15; 
                    $scope.currentPaged-=1;
                }
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  $scope.currentPage,
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-patients-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }, 
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postList = response.patient_posts;
                        $scope.totalItems = response.total_count;
                        $scope.currentPage+=$scope.pageSize;
                         if(pagesize!=1){
                        $scope.currentPaged+=1;
                         }
                        $scope.totalallPage= Math.ceil($scope.totalItems/$scope.pageSize);
                       /* $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
                        $scope.endItem = $scope.currentPage * $scope.pageSize;
                        $scope.totalPage= Math.ceil($scope.totalItems/$scope.pageSize);                

                         if ($scope.endItem > $scope.totalItems) {$scope.endItem = $scope.totalItems;}*/
                        
                        angular.forEach($scope.postList , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time*1000));
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postList[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                //var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postList[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                            chatSocket.emit("set_name", {
                                from: Number(localStorage.getItem('dentist_id')),
                                senderName: localStorage.getItem('dentist_name'),
                                buddy: Number(value.get_patient.id),
                                receiverName: value.get_patient.name + ' ' + value.get_patient.last_name,
                                roomName: '',
                                post_id: Number(value.post_id)
                            });
                        });
                       $scope.buttonText = "Load More";
     

                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
                 //$scope.fetchAllPost();
            }      
            $scope.openPostDetails = function(postId) {
                $rootScope.showLoader = 1;
                var post_ids = postId;
                $scope.postAttachmentPath = configService.getEnvConfig().apiURL;
                var data = {
                    'post_id': post_ids,
                    'auth_token': token
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-patient-post-details",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    if (response.status == 1) {
                        $rootScope.showLoader = 0;
                        $scope.pDetails = response.patient_post_detls;
                        $("#post_details_cont").modal('show');
                    } else {
                        //SweetAlert.swal("Oops!", "Please try again", "error"); 
                    }
                }).error(function(response) {
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.fetchAttendedPost = function() {
                //var token = localStorage.getItem("access_token");
               // $scope.clickFirstPost();
                $scope.selectedTab = 2;
                //$scope.searchKeyword = '';
                $rootScope.showLoader = 1;
                $scope.openChatHeadWindow = false;

                 var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  '0',
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-attendant-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postListAttended = response.patient_posts;
                        
                        //$scope.attendCount = response.patient_posts.length;
                         $scope.attendCount = response.tot_attendant_post;
                         $scope.postList = response.patient_posts;
                        $scope.totalItems = response.tot_attendant_post;
                        $scope.totalattndPage= Math.ceil($scope.totalItems/$scope.pageSize);
                      $scope.currentPagedatt=1;
                        angular.forEach($scope.postListAttended , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time)*1000);
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListAttended[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListAttended[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        $rootScope.showLoader = 0;
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
       $scope.fetchAttendedPostpagi = function(pagesize) {
                //var token = localStorage.getItem("access_token");
               // $scope.clickFirstPost();
                $scope.selectedTab = 2;
                //$scope.searchKeyword = '';
                $rootScope.showLoader = 1;
                $scope.openChatHeadWindow = false;
         
                 if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+15; 
                    $scope.currentPagedatt-=1;
                }
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  $scope.currentPage,
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-attendant-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postListAttended = response.patient_posts;
                        $scope.attendCount = response.patient_posts.length;
                        
                     $scope.totalItems = response.tot_attendant_post;
                        $scope.currentPage+=$scope.pageSize;
                        if(pagesize!=1){
                        $scope.currentPagedatt+=1;
                        }
                        $scope.totalattndPage= Math.ceil($scope.totalItems/$scope.pageSize);
            
                        angular.forEach($scope.postListAttended , function(value, key) {
                            if(typeof value.chat_history_arr.sent_time !='undefined')
                            {
                                var postodate = new Date(parseInt(value.chat_history_arr.sent_time)*1000);
                                var posyr  = postodate.getFullYear();
                                var posmon = postodate.getMonth()+1;
                                var posDt = postodate.getDate();
                                var poshr = postodate.getHours();
                                var posmn = postodate.getMinutes();
                                var possec = postodate.getSeconds();
                                //var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));
                                var localTimeConvert = postodate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListAttended[key].chat_history_arr.sent_time = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                            }
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListAttended[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        $rootScope.showLoader = 0;
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
           // $scope.fetchAttendedPost();
           $scope.fetchUnAttendedPost = function() {
                //$scope.clickFirstPost();
                $scope.openChatHeadWindow = false;
                //$scope.searchKeyword = '';
                $scope.selectedTab = 3;
                $rootScope.showLoader = 1;
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  '0',
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-unattendant-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postListUnattended = response.patient_posts;
                       
                        $scope.unAttendCount = response.tot_unattendant_post;
                       // $scope.unAttendCount = $scope.postListUnattended.length;
                           $scope.totalItems = response.tot_unattendant_post;
                          $scope.totalunattndPage= Math.ceil($scope.totalItems/$scope.pageSize);
                         $scope.currentunPagedatt=1;
                        angular.forEach($scope.postListUnattended , function(value, key) {
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListUnattended[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        $rootScope.showLoader = 0;
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
            $scope.fetchUnAttendedPostpagi = function(pagesize) {
                //$scope.clickFirstPost();
                $scope.openChatHeadWindow = false;
                //$scope.searchKeyword = '';
                $scope.selectedTab = 3;
                $rootScope.showLoader = 1;
                if(pagesize==1){
                   $scope.currentPage-=$scope.pageSize+15; 
                    $scope.currentunPagedatt-=1;
                }
                var data = {
                    'auth_token': token,
                    'doctor_id': localStorage.getItem('dentist_id'),
                    'page':  $scope.currentPage,
                    'perpage' : $scope.pageSize
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "get-unattendant-post",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(data)
                }).success(function(response, status, headers, config) {
                    $rootScope.showLoader = 0;
                    if (response.status == 1) {
                        $scope.postListUnattended = response.patient_posts;
                       
                        $scope.unAttendCount = response.tot_unattendant_post;
                        $scope.totalItems = response.tot_unattendant_post;
                        $scope.currentPage+=$scope.pageSize;
                        if(pagesize!=1){
                        $scope.currentunPagedatt+=1;
                        }
                        $scope.totalunattndPage= Math.ceil($scope.totalItems/$scope.pageSize);

                        angular.forEach($scope.postListUnattended , function(value, key) {
                            if(typeof value.posted_datetime!='undefined')
                            {
                                var postodate_1 = new Date(parseInt(value.posted_datetime)*1000);
                                var posyr_1  = postodate_1.getFullYear();
                                var posmon_1 = postodate_1.getMonth() + 1;
                                var posDt_1 = postodate_1.getDate();
                                var poshr_1 = postodate_1.getHours();
                                var posmn_1 = postodate_1.getMinutes();
                                var possec_1 = postodate_1.getSeconds();
                                var utc_date_1 = new Date(Date.UTC(posyr_1, posmon_1, posDt_1, poshr_1, posmn_1, possec_1));
                                var localTimeConvert_1 = postodate_1.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                                $scope.postListUnattended[key].posted_date = posyr_1+'-'+posmon_1+'-'+posDt_1+' '+localTimeConvert_1;
                            }
                        });
                        $timeout(function() {
                            $(".scrollBox_left").mCustomScrollbar({
                                setHeight: 694,
                                theme: "dark"
                            });
                            $(".scrollBox").mCustomScrollbar({
                                setHeight: 410,
                                theme: "dark"
                            });
                        })
                    } else {
                        $rootScope.showLoader = 0;
                        //SweetAlert.swal("Oops!", "Please try again", "error");
                    }
                }).error(function(response) {
                    $rootScope.showLoader = 0;
                    //SweetAlert.swal("Oops!", "Please try again", "error");
                });
            }
          //  $scope.fetchUnAttendedPost();
             $scope.fetchAllPost();
            $scope.fetchAttendentPostCount();
            $scope.fetchUnAttendentPostCount();
            $scope.messageLogPatient = [];
            $scope.readChat = function(){
                var datas = {
                    auth_token: token,
                    doctor_id : $scope.dentistId, 
                    post_id: $scope.postId
                }
                $http({
                    method: 'post',
                    dataType: "jsonp",
                    crossDomain: true,
                    url: configService.getEnvConfig().apiURL + "read-chat-dentist",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param(datas)
                }).success(function(response, status, headers, config) {
                    //console.log(response);
                }).error(function(response) {
                    
                });
            }
            chatSocket.emit("send_message",{});
            $scope.openChatHead = function(postDetails, index, $listindex) {
                $rootScope.checkLastServieCallTimeAfterLogin();
              //  $scope.fetchAttendedPost();
               // $scope.fetchUnAttendedPost();
                $scope.openChatHeadWindow = true;
                
                $scope.patientDetails = postDetails;
                var date = new Date();
                $scope.currentTime = date.toLocaleTimeString();


                if(typeof postDetails.posted_date!='undefined')
                {
                    var postodate = new Date(postDetails.posted_date);
                    var posyr  = postodate.getFullYear();
                    var posmon = postodate.getMonth() + 1;
                    var posDt = postodate.getDate();
                    var poshr = postodate.getHours();
                    var posmn = postodate.getMinutes();
                    var possec = postodate.getSeconds();

                    var utc_date = new Date(Date.UTC(posyr, posmon, posDt, poshr, posmn, possec));

                    var localTimeConvert = utc_date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');

                    
                    $scope.patientDetails.change_posted_date = posyr+'-'+posmon+'-'+posDt+' '+localTimeConvert;
                }

                $scope.dentistId = localStorage.getItem('dentist_id');
                $scope.dentistName = localStorage.getItem('dentist_name');
                $scope.postId = postDetails.post_id;
                
                $scope.readChat();
                
                if($listindex==0){
                    //console.log($scope.postList[index].unread_chat_history_count);
                    if($scope.postList[index].unread_chat_history_count){
                        
                       $scope.postList[index].unread_chat_history_count = 0; 
                    }
                }else if($listindex==1){
                    if($scope.postListAttended[index].unread_chat_history_count){
                       $scope.postListAttended[index].unread_chat_history_count=0; 
                    }
                }else if($listindex==2){
                    if($scope.postListUnattended[index].unread_chat_history_count){
                       $scope.postListUnattended[index].unread_chat_history_count=0; 
                    }
                }
                $scope.chatIndex = index;
                $scope.receiverName = postDetails.get_patient.name + ' ' + postDetails.get_patient.last_name;
                chatSocket.emit("set_name", {
                    from: Number($scope.dentistId),
                    senderName: $scope.dentistName,
                    buddy: Number($scope.patientDetails.get_patient.id),
                    receiverName: $scope.receiverName, 
                    roomName: '',
                    post_id: Number($scope.postId)
                });
                $scope.messageLogPatient = [];
            }
            $scope.chatImagePath = chatImagePath;
            $scope.message = '';
            $scope.chatImageApiPath = chatImageApiPath;

            /*// If state is this then select the first post ACTIVE //
            if($state.current.name == "dentistmessageboard")
            {                
                   $scope.clickFirstPost();           
            } */           
            
            $scope.sendMessage = function() {
                $scope.attendCount = '';
                $scope.unAttendCount = '';
                $scope.fetchAttendedPost();
                $scope.fetchUnAttendedPost();
                $scope.openChatHeadWindow = true;
                var curtime = new Date().toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
               
                $log.debug('sending message', $scope.message);
                if (!angular.isUndefined($scope.filename) && $scope.filename != '') {
                    var formData = new FormData();
                    formData.append('image', $scope.filename);
                    formData.append('from', Number($scope.dentistId));
                    formData.append('senderName', $scope.dentistName);
                    formData.append('buddy', Number($scope.patientDetails.get_patient.id));
                    formData.append('receiverName', $scope.receiverName);
                    formData.append('message', (!angular.isUndefined($scope.message)) ? $scope.message : '');
                    formData.append('status', 'unread');
                    formData.append('type', 'userMessage');
                    formData.append('post_id', Number($scope.postId));
                    formData.append('createdAt', curtime);
                    $http({
                        method: 'post',
                        url: chatImageApiPath + "image-uploaded",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: formData,
                        transformRequest: angular.identity
                    }).success(function(response, status, headers, config) {
                        if (response.status == 1) {
                            chatSocket.emit('file-uploaded', response.result);
                            $scope.filename = '';
                            var data = {
                                'auth_token': token,
                                'patient_id': $scope.patientDetails.get_patient.id,
                                'doctor_id': $scope.dentistId,
                                'post_id': $scope.postId,
                                'patient_content': '',
                                'doctor_content': (!angular.isUndefined($scope.message)) ? $scope.message : 'File',
                            }
                            $http({
                                method: 'post',
                                dataType: "jsonp",
                                crossDomain: true,
                                url: configService.getEnvConfig().apiURL + "save-chat-history",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: $.param(data)
                            }).success(function(response, status, headers, config) {
                                if (response.status == 1) {}
                            }).error(function(response) {});
                        }
                    }).error(function(response) {
                        $scope.show_message = 'Something is Wrong';
                    });
                } else {
                    if ($scope.message != '') {
                        var data = {
                            message: (!angular.isUndefined($scope.message)) ? $scope.message : '',
                            file_name: '',
                            file_type: '',
                            status: 'unread',
                            type: 'userMessage'
                        };
                        var message = {};
                        message.username = $scope.dentistId;
                        message.from = $scope.dentistId;
                        message.to = $scope.patientDetails.get_patient.id;
                        message.senderName = $scope.dentistName;
                        message.receiverName = $scope.receiverName;
                        message.post_id = $scope.postId;
                        message.message = $scope.message;
                        message.createdAt = curtime;
                        $scope.messageLogPatient.push(message);
                        if ($scope.selectedTab == 1) {

                          if($scope.postListUnattended[$scope.chatIndex] != undefined)
                             {
                                $scope.postList[$scope.chatIndex].chat_history_count = ($scope.postList[$scope.chatIndex].chat_history_count + 1);
                             }
                        }
                        if ($scope.selectedTab == 2) {
                          if($scope.postListUnattended[$scope.chatIndex] != undefined)
                             {
                               $scope.postListAttended[$scope.chatIndex].chat_history_count = ($scope.postListAttended[$scope.chatIndex].chat_history_count + 1);
                             }
                        }
                        if ($scope.selectedTab == 3) {

                            if($scope.postListUnattended[$scope.chatIndex] != undefined)
                             {
                                $scope.postListUnattended[$scope.chatIndex].chat_history_count = ($scope.postListUnattended[$scope.chatIndex].chat_history_count + 1);
                             }
                            
                        }
                        $(".scrollBox").mCustomScrollbar("update");
                        $timeout(function() {
                            $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                        }, 1000);
                        chatSocket.emit('message', JSON.stringify(data));
                        var data = {
                            'auth_token': token,
                            'patient_id': $scope.patientDetails.get_patient.id,
                            'doctor_id': $scope.dentistId,
                            'post_id': $scope.postId,
                            'patient_content': '',
                            'createdAt': curtime,
                            'doctor_content': (!angular.isUndefined($scope.message)) ? $scope.message : '',
                        }
                        $http({
                            method: 'post',
                            dataType: "jsonp",
                            crossDomain: true,
                            url: configService.getEnvConfig().apiURL + "save-chat-history",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: $.param(data)
                        }).success(function(response, status, headers, config) {
                            if (response.status == 1) {}
                        }).error(function(response) {});
                    } else {}
                }
                if ($scope.patientDetails.is_attendant == 0) {
                    $scope.unattendentPostCount = ($scope.unattendentPostCount - 1);
                    $scope.attendentPostCount = ($scope.attendentPostCount + 1);
                    if ($scope.unattendentPostCount <= 0) {
                        $scope.unattendentPostCount = 0;
                    }
                }
                $scope.message = '';
            };
            $scope.uploadAttachment = function(fileData) {
                $scope.filename = fileData[0];
                $timeout(function() {
                    $scope.sendMessage();
                }, 1);
            }
            var current_dentist_msg_id = '';
            if ($rootScope.is_front_logged == 1) {
                chatSocket.on('send_message', function(msg){
                   
                  /* console.log('send_message');
                    console.log(msg);*/
                    var data = JSON.parse(msg);
                    var localtime = dateFormatter(data.createdAt);
                    angular.forEach($scope.postList, function(val, key) {
                        if(val.post_id == data.post_id){
                            $scope.postList[key].unread_chat_history_count = $scope.postList[key].unread_chat_history_count+1;
                            $scope.postList[key].chat_history_arr.doctor_content = '';
                            $scope.postList[key].chat_history_arr.patient_content = data.message;
                            if(data.createdAt.length>9)
                            $scope.postList[key].chat_history_arr.sent_time  = localtime;
                            if(data.post_id == $scope.postId){
                                $scope.postList[key].unread_chat_history_count = 0;
                            }
                        }
                    });
                    angular.forEach($scope.postListAttended, function(val, key) {
                        if(val.post_id == data.post_id){
                            $scope.postListAttended[key].unread_chat_history_count = $scope.postListAttended[key].unread_chat_history_count+1;
                            $scope.postListAttended[key].chat_history_arr.doctor_content = '';
                            $scope.postListAttended[key].chat_history_arr.patient_content = data.message;
                            if(data.createdAt.length>9)
                            $scope.postListAttended[key].chat_history_arr.sent_time  = localtime;
                            if(data.post_id == $scope.postId){
                                $scope.postListAttended[key].unread_chat_history_count = 0;
                            }
                        }
                    });
                    angular.forEach($scope.postListUnattended, function(val, key) {
                        if(val.post_id == data.post_id){
                            $scope.postListUnattended[key].unread_chat_history_count = $scope.postListUnattended[key].unread_chat_history_count+1;
                            $scope.postListUnattended[key].chat_history_arr.doctor_content = '';
                            $scope.postListUnattended[key].chat_history_arr.patient_content = data.message;
                            $scope.postListUnattended[key].chat_history_arr.sent_time  = localtime;
                            if(data.post_id == $scope.postId){
                                $scope.postListUnattended[key].unread_chat_history_count = 0;
                            }
                        }
                    });
                });
                chatSocket.on('message', function(data) {
                    if($scope.messageLogPatient.length == 0 &&  Array.isArray(data)){
                        $scope.messageLogPatient = [];
                        if(data.length>0){
                            angular.forEach(data, function(val, key) {
                                if(val.createdAt.length>9){
                                 val.createdAt = dateFormatter(val.createdAt);
               
                                }
                            });
                            $scope.messageLogPatient = data.reverse();
                        }
                        
 
                    }else{
                        if(data.trim() != ''){
                            data = JSON.parse(data);
                            var curPost = data.post_id || 0;
                            if(curPost !== 0 && curPost == $scope.postId){
                                if($scope.messageLogPatient.length > 0){
                                    $scope.messageLogPatient.push(data);
                                }else{
                                    
                                }
                            }
                            $scope.messageLogPatient.forEach(function(item, index) {
                                if ($scope.messageLogPatient[index].createdAt.indexOf("AM") != -1 || $scope.messageLogPatient[index].createdAt.indexOf("PM") != -1) {
                                    var d = new Date();
                                    var n = d.toISOString();
                                    $scope.messageLogPatient[index].createdAt = dateFormatter(n);
                         
                                }else{
                                    
                                    $scope.messageLogPatient[index].createdAt = dateFormatter($scope.messageLogPatient[index].createdAt);
                                }
                                //$scope.messageLogPatient[index].createdAt = dateFormatter($scope.messageLogPatient[index].createdAt);
                            });
                            
                            
                   
                            if ($scope.selectedTab == 1) {
                                if($scope.postList[$scope.chatIndex] !='undefined')
                                    $scope.postList[$scope.chatIndex].chat_history_count = ($scope.postList[$scope.chatIndex].chat_history_count + 1);
                            }
                            if ($scope.selectedTab == 2) {
                                if($scope.postListAttended[$scope.chatIndex] !='undefined')
                                    $scope.postListAttended[$scope.chatIndex].chat_history_count = ($scope.postListAttended[$scope.chatIndex].chat_history_count + 1);
                            }
                            if ($scope.selectedTab == 3) {
                                if($scope.postListUnattended[$scope.chatIndex] !='undefined')
                                    $scope.postListUnattended[$scope.chatIndex].chat_history_count = ($scope.postListUnattended[$scope.chatIndex].chat_history_count + 1);
                            }
                        }
                    }
                    $(".scrollBox").mCustomScrollbar("update");
                    $timeout(function() {
                        $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                    }, 100);
                });
                var current_id = '';
                chatSocket.on('file-uploaded', function(data) {
                    if ($rootScope.is_front_logged == 1) {
                        if (!Array.isArray(data)) {
                            data = JSON.parse(data);
                            if (data.from != '') {
                                if (data._id != current_id) {
                                    $scope.messageLogPatient.push(data);
                                    current_id = data._id;
                                    if ($scope.selectedTab == 1) {
                                        $scope.postList[$scope.chatIndex].chat_history_count = ($scope.postList[$scope.chatIndex].chat_history_count + 1);
                                    }
                                    if ($scope.selectedTab == 2) {
                                        $scope.postListAttended[$scope.chatIndex].chat_history_count = ($scope.postListAttended[$scope.chatIndex].chat_history_count + 1);
                                    }
                                    if ($scope.selectedTab == 3) {
                                        $scope.postListUnattended[$scope.chatIndex].chat_history_count = ($scope.postListUnattended[$scope.chatIndex].chat_history_count + 1);
                                    }
                                }
                            }
                        }
                        $(".scrollBox").mCustomScrollbar("update");
                        setTimeout(function() {
                            $(".scrollBox").mCustomScrollbar("scrollTo", "bottom");
                        }, 100);
                    }
                });
                chatSocket.on('close_message', function(data) {
                    
                    if ($rootScope.is_front_logged == 1) {
                        if (!Array.isArray(data)) {
                            data = JSON.parse(data);
                            if (data.post_id != '') {
                                if ($scope.selectedTab == 1) {
                                    var index = $scope.postList.findIndex(x >= x.post_id == data.post_id);
                                    $scope.postList[index].is_closed_chat = 1;
                                }
                                if ($scope.selectedTab == 2) {
                                    var index = $scope.postListAttended.findIndex(x >= x.post_id == data.post_id);
                                    $scope.postListAttended[index].is_closed_chat = 1;
                                }
                                if ($scope.selectedTab == 3) {
                                    var index = $scope.postListUnattended.findIndex(x >= x.post_id == data.post_id);
                                    $scope.postListUnattended[index].is_closed_chat = 1;
                                }
                                $scope.patientDetails.is_closed_chat = 1;
                            }
                        }
                    }
                });
            }
            var dateFormatter = function(date) {
                var curd = new Date(),
                    curmonth = '' + (curd.getMonth() + 1),
                    curday = '' + curd.getDate(),
                    curyear = curd.getFullYear();
                if (curmonth.length < 2) curmonth = '0' + curmonth;
                if (curday.length < 2) curday = '0' + curday;
                var curdate = [curyear, curmonth, curday].join('-');
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                var postDate = [year, month, day].join('-');
                if (postDate < curdate) {
                    return postDate+' '+new Date(date).toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                } else {
                    return new Date(date).toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
                }
            }
            
            
        }
        var map;

        $scope.checkErr = function(startDate,endDate) {

            $scope.errMessage = '';
            var curDate = new Date();
            if(new Date(startDate) <= new Date(endDate)){
                $scope.errMessage = 'To date must not be lease than from date';
                document.getElementById("date_validation_error").style.display ="block";
                angular.element(document.getElementById('step5Next'))[0].disabled = true;
                return false;
            }
            else
            {
                angular.element(document.getElementById('step5Next'))[0].disabled = false;
            }

        };

        $scope.DateErroHide = function(startDate,endDate) {
           document.getElementById("date_validation_error").style.display ="none";
           angular.element(document.getElementById('step5Next'))[0].disabled = false;
           return false;
         };
 


        function initialize(lat, lang) {
            var myLatlng;
            if (lat != '' && lang != '') {
                myLatlng = new google.maps.LatLng(lat, lang);
            } else {
                myLatlng = new google.maps.LatLng(40.73776640000001, -73.9896235);
            }
            var myOptions = {
                zoom: 20,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var marker = new google.maps.Marker({
                draggable: false,
                position: myLatlng,
                map: map,
                title: "Your location"
            });
            var infoWindow = new google.maps.InfoWindow({
                content: $scope.clinic_address
            });
            google.maps.event.addListener(marker, 'click', function() {
                infoWindow.open(map, marker);
            });
        }
    }
    
})();