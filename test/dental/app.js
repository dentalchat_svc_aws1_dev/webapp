var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var http = require('http');
var https = require('https');
var routes = require('./routes/index');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var fs = require("fs");
// var users = require('./routes/users');

var app = express();

// var server = http.createServer(app).listen(app.get('port'),
// function(){
// console.log("Express server listening on port " + app.
// get('port'));
// });
//var server = http.Server(app);

//var io = require('socket.io')(http);

 //var privateKey  = fs.readFileSync('/var/cpanel/ssl/installed/keys/c1c0c_d9e59_4265c4eb21b84f3c656a6d15b564e95e.key', 'utf8');
 var privateKey  = fs.readFileSync('/var/cpanel/ssl/installed/keys/c1c0c_d9e59_4265c4eb21b84f3c656a6d15b564e95e.key', 'utf8');
 var certificate = fs.readFileSync('/var/cpanel/ssl/installed/certs/dentalchat_com_c1c0c_d9e59_1521331199_4e2c83ded72267db017c748a3bedc8c9.crt', 'utf8');
 var credentials = {key: privateKey, cert: certificate};
 var httpsServer = https.createServer(credentials, app);

// server.listen(3000, function(){
//   console.log('listening on *:3000');
// });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// app.get('/room', routes.room);
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));

app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));
// app.use(function(req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
//     next();
// });
app.use('/', routes);

// app.get('/chat',function(req,res){
//   res.sendFile(path.join(__dirname, 'app_client','chatroom.html'));
// });

//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers  .initialize(server,httpsServer)
require('./routes/sockets.js').initialize(httpsServer)//.initialize(server);

process.on("uncaughtException", function(error) {
    console.log(error);
    console.log("The exception was caught!")
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
//-------------------------
//mongoose.connect('mongodb://localhost/dental');

// var MongoDBprotocol = 'mongodb';

// var serverAddress = 'dentalchat.com';
// var Port = '27017';
// var dbName = 'dentalchat';
// mongoose.connect('mongodb://root:123456@localhost/test',{auth:{authdb:"admin"}});
mongoose.set('debug', true); // turn on debug

var dbUrl = 'mongodb://142.4.27.236:27017/dentalchat'; 
//var dbUrl = 'mongodb://root:salt%40%23123@dentalchat.com:27017/dentalchat';
//var dbUrl = 'mongodb://localhost:27017/dentalchat';
//dbUrl,{auth:{authdb:"admin"}
var connection = mongoose.connect(dbUrl,function(err){
    if(err)
    {
        console.log(err);
    }
    else
    {
        console.log('success');
    }

});

//-----------------
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
