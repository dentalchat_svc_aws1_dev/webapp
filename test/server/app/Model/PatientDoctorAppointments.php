<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PatientDoctorAppointments extends Model
{

    protected $table = 'patient_doctor_appointments';
    protected $fillable = ['*'];
    
}
