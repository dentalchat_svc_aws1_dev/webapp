
@extends('admin/layout/admin_template')

@section('content')

	{!! Form::open(['url' => 'admin/add/testimonial','method'=>'POST','class'=>'form-horizontal row-fluid','id'=>'testimonial_form','onsubmit'=>'userJs.checkUserEmail()']) !!}

	
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Name *</label>
				
			<div class="controls">
				{!! Form::text('reviewer_name',null,['class'=>'span8','id'=>'reviewer_name','required'=>true]) !!}

			</div>
		</div>
			
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Occupation *</label>
				
			<div class="controls">
				{!! Form::text('occupation',null,['class'=>'span8','id'=>'occupation']) !!}
				<div style="color:#F00;font-size:13px;clear:both;" id="slug_msg"></div>
			</div>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Feedback *</label>
				
			<div class="controls">
				 {!! Form::textarea('feedback',null,['class'=>'span8 ','id'=>'feedback','keyup'=>'check_blog_name(this.value);']) !!}

			</div>
		</div>
		<div class="control-group">
		
			<div class="controls">
				{!! Form::submit('Save',array('class'=>'btn','name'=>'action','value'=>'save')) !!}
				<a href="{!! url('admin/testimonial/list')!!}" class="btn">Back</a>
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
	
	<!-- <script type="text/javascript">
		$(document).ready(function(){
			//add  testimonial_form Start //
			if ($("#testimonial_form").length){
				$("#testimonial_form").validate({

					ignore: [],
					// Specify the validation rules
					rules: {
						reviewer_name: {
							required: true
						},
						occupation:{
							required: true
						},
						feedback: {
							required: true,
							maxlength: 1600
						}
					
					},
				
				 // Specify the validation error messages
					messages: {
						
						
					},         

					submitHandler: function(form) {
						form.submit();
					}
				});
			}
		});

	</script> -->
	
    @stop
    
    