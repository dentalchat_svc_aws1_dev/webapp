<div class="col-sm-4">
            <article class="uou-block-7g blog post-679 post type-post status-publish format-standard has-post-thumbnail hentry category-dental-chat-dentalchat category-featured category-online-dental-chat" data-badge-color="ff0099">
            <div class="mystory_images">    
            <?php if ( has_post_thumbnail() ) { echo get_the_post_thumbnail( $post_id, 'large' ); } else { echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
        . '/assets/img/teeth_default.jpg" />'; }
            //echo get_the_post_thumbnail( $post_id, array( 360, 220) );
            ?></div><div class="content">
                    <span class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
                     <h1><a href="<?php echo get_the_permalink(); ?>">  <?php the_title(); ?>  </a></h1>
                     <p><?php echo wp_trim_words( get_the_content(), 50, '..' ); ?></p>
                     <a href="<?php echo get_the_permalink(); ?>" class="btn btn-small btn-primary">Read More</a>
                  </div>
            </article>  
            </div>