<?php namespace App\Http\Controllers\Service;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
/*************** Model name ***************/
///*use App\Model\Country; 
//use App\Model\Category; 
//use App\Model\SiteUser; */
//use App\Model\Doctor; 	
//use App\Model\DoctorDetails;
//use App\Model\DoctorClinic;
//use App\Model\DoctorEducation;
//use App\Model\DoctorAward;
//use App\Model\DoctorInsurance;
//use App\Model\DoctorExperience;
//use App\Model\DoctorLicense;
//use App\Model\DoctorSkill;
//use App\Model\DoctorLanguage; 
//use App\Model\DoctorLoginDetails;
//
//use App\Model\TmpDoctorDetails;
//use App\Model\TmpDoctorClinic;
//use App\Model\TmpDoctorEducation;
//use App\Model\TmpDoctorAward;
//use App\Model\TmpDoctorInsurance;
//use App\Model\TmpDoctorExperience;
//use App\Model\TmpDoctorLicense;
//use App\Model\TmpDoctorSkill;
use App\Model\Doctor;
use App\Model\DoctorDetails;
use App\Model\Cmspage; 

use App\User;
use App\Http\Requests;
use App\Helper\helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;

class CmsController extends BaseController {

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
  		parent::__construct();
    }
    function getTest(){
        echo 1;
    }
	
	function postAllCms()
	{
		$data =Request::all();
		$page_id = $data['page_id'];
		$all_cms = Cmspage::where('id',$page_id)->first()->toArray();
		$all_cms['description'] = html_entity_decode($all_cms['description']);
        //$all_cms['description'] = $all_cms['description'];
		echo json_encode(array('status'=>1,'all_cms'=>$all_cms));
	}
	
   public function add_contact(Request $request) {
      
           
        $input = Input::all();
        $dataarr = array();
                   
                   
		$check_user_email 	= DoctorDetails::where('email',$input['user']['email'])->count();
		if($check_user_email>0){
		   echo json_encode(array('status'=>2));
		}else {
                   
                    /* create doctor id */
                   	$src_sys = isset($data['user']['src_sys']) ? $data['user']['src_sys'] : 'special';
			      // $hear_about_us = isset($data['user']['hear_about_us']) ? $data['user']['hear_about_us'] : '';
			    $hear_about_us ='';
		    	$create_doctor		= Doctor::create([
									'email_status'     => 0,
									'admin_status'     => 0,
									'publish_status'   => 0,
									'hear_about_us'   => $hear_about_us,
									'referral_code' => $input['user']['referral_code'],
									'src_sys'   => $src_sys,
									'complete_profile_status' => 0,								
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
			
			    $dataarr['doctor_id'] = $create_doctor->id;    // last  inserted id
			
			/* close here */
                  
                   $dataarr['first_name'] = $input['user']['first_name'];
                   $dataarr['last_name'] = $input['user']['last_name'];
                   $dataarr['business_name'] = $input['user']['business_name'];
                   $dataarr['contact_number'] = $input['user']['contact_number'];
                   $dataarr['conuntry_code'] = $input['user']['country_code'];
                   $dataarr['zipcode'] = $input['user']['zipcode'];
                   $dataarr['streetaddress'] = $input['user']['streetaddress'];
                   $dataarr['city'] = $input['user']['city'];
                   $dataarr['state'] = $input['user']['state'];
                   $dataarr['email'] = $input['user']['email'];
                   $password = trim($input['user']['password']) ? trim($input['user']['password']) : '';
                   //$dataarr['type'] ='business';
                   $dataarr['password'] = bcrypt($password);
                    $dataarr['created_at']	= date('Y-m-d H:i:s');
				   $dataarr['updated_at']	= date('Y-m-d H:i:s');
                  
                   $gen_id = DB::table('doctor_details')->insertGetId($dataarr);
                   if ($gen_id) {
                       
                /* send mail to user */
                       
				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$user_email = $input['user']['email'];
				$user_name = $input['user']['first_name'];

				
				$length = strrpos(url(), "/");
				$activation_url = substr(url(),0,$length);
				$activation_password_link = $activation_url.'/activate-user/'.base64_encode($user_email);
				//echo $activation_password_link; exit;
				$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('DentalChat :: Email verification link!');
	            });

                /********** Welcome Email *********************************/
                $this->mailsend($create_doctor->id,"Welcome to DentalChat", $user_email,$user_name.' '.$input['user']['last_name'], $user_name,$input['user']['last_name'], "dentalwelcome", $activation_password_link,1);

                       
                       
                       
                       echo json_encode(array('status'=>1, 'useremail'=> $dataarr['email']));
                   } else {
                       echo json_encode(array('status'=>0));
                   }
        }           
               
           
       }
       public function add_exclusive(Request $request) {
      
           
        $input = Input::all();
                   $dataarr = array();
                   
                   	$check_user_email 	= DoctorDetails::where('email',$input['user']['email'])->count();
		if($check_user_email>0){
		   echo json_encode(array('status'=>2));
		}else {
                   
                   /* create doctor id */
                   	$src_sys = isset($data['user']['src_sys']) ? $data['user']['src_sys'] : 'offer';
			      // $hear_about_us = isset($data['user']['hear_about_us']) ? $data['user']['hear_about_us'] : '';
			    $hear_about_us ='';
		    	$create_doctor  	= Doctor::create([
									'email_status'     => 0,
									'admin_status'     => 0,
									'publish_status'   => 0,
									'referral_code'    => $input['user']['referral_code'],
									'hear_about_us'   => $hear_about_us,
									'src_sys'   => $src_sys,
									'complete_profile_status' => 0,								
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
			
			    $dataarr['doctor_id'] = $create_doctor->id;    // last  inserted id
			
			/* close here */
			
                   $dataarr['first_name'] = $input['user']['first_name'];
                   $dataarr['last_name'] = $input['user']['last_name'];
                   $dataarr['business_name'] = $input['user']['business_name'];
                   $dataarr['contact_number'] = $input['user']['contact_number'];
                   $dataarr['conuntry_code'] = $input['user']['country_code'];
                   $dataarr['zipcode'] = $input['user']['zipcode'];
                   $dataarr['streetaddress'] = $input['user']['streetaddress'];
                   $dataarr['city'] = $input['user']['city'];
                   $dataarr['state'] = $input['user']['state'];
                   $dataarr['email'] = $input['user']['email'];
                   $password = trim($input['user']['password']) ? trim($input['user']['password']) : '';
                   $dataarr['password'] = bcrypt($password);
                   $dataarr['created_at']	= date('Y-m-d H:i:s');
				   $dataarr['updated_at']	= date('Y-m-d H:i:s');
                  // $dataarr['type'] ='exclusive';
                   
                   $gen_id = DB::table('doctor_details')->insertGetId($dataarr);
                   if ($gen_id) {
                       
                       
                /* send mail to user */
				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$user_email = $input['user']['email'];
				$user_name = $input['user']['first_name'];

				
				$length = strrpos(url(), "/");
				$activation_url = substr(url(),0,$length);
				$activation_password_link = $activation_url.'/activate-user/'.base64_encode($user_email);
				//echo $activation_password_link; exit;
				$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('DentalChat :: Email verification link!');
	            });

                /********** Welcome Email *********************************/
                $this->mailsend($create_doctor->id,"Welcome to DentalChat", $user_email,$user_name.' '.$input['user']['last_name'], $user_name,$input['user']['last_name'], "dentalwelcome", $activation_password_link,1);
                       
                       echo json_encode(array('status'=>1, 'useremail'=> $dataarr['email']));
                   } else {
                       echo json_encode(array('status'=>0));
                   }
                   
		}       
               
           
       }
       
        public function add_idealistic(Request $request) {
      
           
        $input = Input::all();
                   $dataarr = array();
                   
                   	$check_user_email 	= DoctorDetails::where('email',$input['user']['email'])->count();
		if($check_user_email>0){
		   echo json_encode(array('status'=>2));
		}else {
                   
                    /* create doctor id */
                   	$src_sys = isset($data['user']['src_sys']) ? $data['user']['src_sys'] : 'freeoffer';
			      // $hear_about_us = isset($data['user']['hear_about_us']) ? $data['user']['hear_about_us'] : '';
			    $hear_about_us ='';
		    	$create_doctor		= Doctor::create([
									'email_status'     => 0,
									'admin_status'     => 0,
									'publish_status'   => 0,
									'hear_about_us'   => $hear_about_us,
									'referral_code' => $input['user']['referral_code'],
									'src_sys'   => $src_sys,
									'complete_profile_status' => 0,								
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
			
			    $dataarr['doctor_id'] = $create_doctor->id;    // last  inserted id
			
		    	/* close here */
                  
                   $dataarr['first_name'] = $input['user']['first_name'];
                   $dataarr['last_name'] = $input['user']['last_name'];
                   $dataarr['business_name'] = $input['user']['business_name'];
                   $dataarr['contact_number'] = $input['user']['contact_number'];
                   $dataarr['conuntry_code'] = $input['user']['country_code'];
                   $dataarr['zipcode'] = $input['user']['zipcode'];
                   $dataarr['streetaddress'] = $input['user']['streetaddress'];
                   $dataarr['city'] = $input['user']['city'];
                   $dataarr['state'] = $input['user']['state'];
                   $dataarr['email'] = $input['user']['email'];
                   $password = trim($input['user']['password']) ? trim($input['user']['password']) : '';
                   $dataarr['password'] = bcrypt($password);
                  // $dataarr['type'] ='idealistic';
                   $dataarr['created_at']	= date('Y-m-d H:i:s');
				   $dataarr['updated_at']	= date('Y-m-d H:i:s');
                   $gen_id = DB::table('doctor_details')->insertGetId($dataarr);
                   if ($gen_id) {
                     
                /* send mail to user */
                       
				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$user_email = $input['user']['email'];
				$user_name = $input['user']['first_name'];

				
				$length = strrpos(url(), "/");
				$activation_url = substr(url(),0,$length);
				$activation_password_link = $activation_url.'/activate-user/'.base64_encode($user_email);
				//echo $activation_password_link; exit;
				$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('DentalChat :: Email verification link!');
	            });

                /********** Welcome Email *********************************/
                $this->mailsend($create_doctor->id,"Welcome to DentalChat", $user_email,$user_name.' '.$input['user']['last_name'], $user_name,$input['user']['last_name'], "dentalwelcome", $activation_password_link);

                       echo json_encode(array('status'=>1, 'useremail'=> $dataarr['email']));
                       
                   } else {
                       echo json_encode(array('status'=>0));
                   }
		}   
           
       }
       
       public function add_posh(Request $request) {
      
           
        $input = Input::all();
                   $dataarr = array();
                   
                   	$check_user_email 	= DoctorDetails::where('email',$input['user']['email'])->count();
		if($check_user_email>0){
		   echo json_encode(array('status'=>2));
		}else {
                   
                    /* create doctor id */
                   	$src_sys = isset($data['user']['src_sys']) ? $data['user']['src_sys'] : 'now';
			      // $hear_about_us = isset($data['user']['hear_about_us']) ? $data['user']['hear_about_us'] : '';
			    $hear_about_us ='';
		    	$create_doctor		= Doctor::create([
									'email_status'     => 0,
									'admin_status'     => 0,
									'publish_status'   => 0,
									'hear_about_us'   => $hear_about_us,
									'referral_code' => $input['user']['referral_code'],
									'src_sys'   => $src_sys,
									'complete_profile_status' => 0,								
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
			
			    $dataarr['doctor_id'] = $create_doctor->id;    // last  inserted id
			
		    	/* close here */
                   $dataarr['first_name'] = $input['user']['first_name'];
                   $dataarr['last_name'] = $input['user']['last_name'];
                   $dataarr['business_name'] = $input['user']['business_name'];
                   $dataarr['contact_number'] = $input['user']['contact_number'];
                   $dataarr['conuntry_code'] = $input['user']['country_code'];
                   $dataarr['zipcode'] = $input['user']['zipcode'];
                   $dataarr['streetaddress'] = $input['user']['streetaddress'];
                   $dataarr['city'] = $input['user']['city'];
                   $dataarr['state'] = $input['user']['state'];
                   $dataarr['email'] = $input['user']['email'];
                   $password = trim($input['user']['password']) ? trim($input['user']['password']) : '';
                   $dataarr['password'] = bcrypt($password);
                   $dataarr['created_at']	= date('Y-m-d H:i:s');
				   $dataarr['updated_at']	= date('Y-m-d H:i:s');
                   //$dataarr['type'] ='posh';
                   $gen_id = DB::table('doctor_details')->insertGetId($dataarr);
                   if ($gen_id) {
                       
                /* send mail to user */
                       
				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$user_email = $input['user']['email'];
				$user_name = $input['user']['first_name'];

				
				$length = strrpos(url(), "/");
				$activation_url = substr(url(),0,$length);
				$activation_password_link = $activation_url.'/activate-user/'.base64_encode($user_email);
				//echo $activation_password_link; exit;
				$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('DentalChat :: Email verification link!');
	            });

                /********** Welcome Email *********************************/
                $this->mailsend($create_doctor->id,"Welcome to DentalChat", $user_email,$user_name.' '.$input['user']['last_name'], $user_name,$input['user']['last_name'], "dentalwelcome", $activation_password_link,1);

                       echo json_encode(array('status'=>1, 'useremail'=> $dataarr['email']));
                   } else {
                       echo json_encode(array('status'=>0));
                   }
		}   
           
       }
       
       /* for send mail */
private function mailsend($user_id=false,$subject= false, $email = false,$name=false, $fname = false,$lname = false, $mailtemplate = false, $business_name= false,$cc = false){
             
  $url = 'http://outbound.dentalchat.com/mailsend';
  
   try{
        $post = array(
        'user_id'=>$user_id,    
        'name' => $name,
        'firstname'=>$fname,
        'lastname'=>$lname,
        'business_name'=>$business_name,
        'template_name'=>$mailtemplate,
        'email'=>$email,
        'subject'=>$subject
        );
    if($cc){
        $post['cc']=1;
    }   
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false
        ));
       
        $result=curl_exec ($ch);
        curl_close ($ch);
         }
         catch(Exception $ex){
             //echo $ex->getMessage();
         }
    }
    
    /**** end here */
       
    /**** Contact Us Function ***/
    function postSubmitContact()
    {
        $data = Request::all();
        
                ##############Captcha validation --STARTS###############
        $captcha = $data['g_recaptcha_response'];
        $postdata = http_build_query(
          array(
            'secret' => '6LehIR8UAAAAAB7zIctT4uiWYd9ruxzD7Q50ksPH', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );

        //Build options for the post request
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );

        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

        /* Send request to Googles siteVerify API */
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
        $response = json_decode($response, true);
        if($response["success"]===false)
        {
            $response_arr = array(
                'status'    =>  '3',
                'msg'       =>  "Captcha mismatch error."
            );
            echo json_encode($response_arr);exit();
        }
        ##############Captcha validation --ENDS###############
        //print_r($data);exit;
        /*$access_token   = Request::header('Access-Token');
        $user_dtls      = UserLoginDetail::where('token',$access_token)->first();
        
        $insertdata     = array('email'=>$data['user']['email'],
                            'name'=>$data['user']['firstname'],
                            'message'=>$data['user']['message'],
                            'subject'=>$data['user']['subject'],
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s'));
        $create_question = ContactMessage::create($insertdata);*/
        
        /***** SEND MAIL -start ********/
        
        $sitesettings = DB::table('sitesettings')->where('id',1)->first();
        if(!empty($sitesettings))
        {
            $admin_users_email = $sitesettings->value;
            
        }
        
        $user_email = $data['user']['email'];
        $user_name  = $data['user']['firstname'];
        $subject    = "An new message from DentalChat contact page"; //$data['user']['subject'];
        $message_content    = $data['user']['message'];
        
        $sent = Mail::send('service.dentalchat.contact_mail', array('name'=>$user_name,'email'=>$user_email,'admin_users_email'=>$admin_users_email,'message_content'=>$message_content), 
        function($message) use ($admin_users_email, $user_email,$user_name,$subject)
        {
            $message->from($user_email,$user_name);
            $message->to($admin_users_email, $user_name)->subject($subject);
        });
        echo json_encode(array('status'=>1));
        /***** SEND MAIL - end ********/
        
        /*if($create_question)
        {
            echo json_encode(array('status'=>1));
        }
        else{
            echo json_encode(array('status'=>0));
        }*/
    }
    /**** Contact Us Function ***/


}
