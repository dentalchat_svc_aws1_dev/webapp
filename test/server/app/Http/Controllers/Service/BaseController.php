<?php 
namespace App\Http\Controllers\Service; /* path of this controller*/

use App\Http\Requests;
use App\Model\Doctor; 	
use App\Model\DoctorDetails;
use App\Model\DoctorClinic;
use App\Model\DoctorEducation;
use App\Model\DoctorAward;
use App\Model\DoctorInsurance;
use App\Model\DoctorExperience;
use App\Model\DoctorLicense;
use App\Model\DoctorSkill;
use App\Model\DoctorLanguage; 
use App\Model\DoctorLoginDetails;
use App\Model\Patient; 
use App\Model\PatientAuthToken; 

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use Illuminate\Pagination\Paginator;
use DB;

use App\Helper\helpers;



class BaseController extends Controller {

	public function __construct() 
    {		

    }
	
	/*	Fetch user_id from  "DoctorLoginDetails" table	*/
	public function getUserId($token=""){
		$user_token_info 		= DoctorLoginDetails::where('token',$token)->first();
		return $user_token_info['user_id'];
	}

	public function getUserDetailById($user_id=""){
		$user_det 		= DoctorDetails::where('id',$user_id)->first();
		return $user_det;
	}
	
	public function getUserDetail($token=""){
		$user_details = DoctorLoginDetails::with('getUserDetail')->where('token',$token)->first();
		return $user_details;
	}
	
	public function fetchStripSecretAPI(){
		$stripe_info  = Sitesetting::select('value')->where('name','STRIPE_SECRET_API')->first()->toArray();
		return $stripe_info['value'];
	}

	public function fetchStripPublicAPI(){
		$stripe_info  = Sitesetting::select('value')->where('name','STRIPE_PUBLIC_API')->first()->toArray();
		return $stripe_info['value'];
	}
	public function generateStrongPassword($length = 8, $available_sets = 'luds'){
		$sets = array();
		if(strpos($available_sets, 'l') !== false)
			$sets[] = 'abcdefghjkmnpqrstuvwxyz';
		if(strpos($available_sets, 'u') !== false)
			$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		if(strpos($available_sets, 'd') !== false)
			$sets[] = '23456789';
		if(strpos($available_sets, 's') !== false)
			$sets[] = '!@#$%&*?';
		$all = '';
		$password = '';
		foreach($sets as $set)
		{
			$password .= $set[array_rand(str_split($set))];
			$all .= $set;
		}
		$all = str_split($all);
		for($i = 0; $i < $length - count($sets); $i++)
			$password .= $all[array_rand($all)];
		$password = str_shuffle($password);
		return $password;
	}

	public function get_unique_alphanumeric_no( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
  	}

  	  public function simple_encrypt($text)
	  {
	    /*$salt = "earlysandwich.co";
	    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));*/

	    $output = false;
	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';
	    // hash
	    $key = hash('sha256', $secret_key);

	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	    $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
	      $output = base64_encode($output);

	      return $output;
	  }

	  // This function will be used to decrypt data.
	  public function simple_decrypt($text)
	  {
	    /*$salt = "earlysandwich.co";
	    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));*/

	    $output = false;
	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    // hash
	    $key = hash('sha256', $secret_key);
	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	    $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
	    return $output;
	  }


	  public function is_authenticate_user($patient_id = null,$auth_token = null)
	  {
	  	$is_auth_token = PatientAuthToken::where('patient_id','=',$patient_id)
	  										->where('auth_token','=',$auth_token)
	  										->where('status','=','1')
	  										->count();

	  	$is_patient_exists = Patient::where('id','=',$patient_id)
	  									->count();

	  	if($is_auth_token > 0 && $is_patient_exists > 0)
	  	{
	  		return "1";
	  	}
	  	else
	  	{
	  		return "0";
	  	}
	  }

	  public function get_image_ext($image_name = null)
	  {
	  	$image_name_arr = explode(".",$image_name);
	  	$arr_len = count($image_name_arr);
	  	$ext_index = (int)$arr_len - 1;
	  	$ext = $image_name_arr[$ext_index];
	  	return $ext;
	  }

}
