<?php namespace App\Http\Controllers\Admin; /* path of this controller*/
// Define Model
use App\Model\Sitesetting;
use App\Http\Requests;
use App\Http\Controllers\Controller;    
use Illuminate\Support\Facades\Request;
//use App\Model\SiteUser;
use App\Model\Notifications;

use Mail;
use Input; /* For input */
use Validator;
use Session;
use Imagine\Image\Box;
use Image\Image\ImageInterface;
use Illuminate\Pagination\Paginator;
use DB;
use Hash;
use Auth;
use Cookie;
use App\Helper\helpers;
use Cart;
use Redirect;
use Yajra\Datatables\Datatables;

class NotificationController extends BaseController {
   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function getallnotificatios(){
      $module_head    = "Notifications";
      $noti_class     = "active";
      $title          =   "Notifications";
      return view('admin.notification.notification_list',compact('noti_class',
              'module_head','title'
      )
    );
       
   }

   public function ajaxnotificationList(){
      $all_notifications = Notifications::with('from_details')->select('notifications.*')->where('hide_status','=','0');
      $rowdatatable =  Datatables::of($all_notifications)

      ->setRowClass(function ($all_notifications) {
                if($all_notifications->read == 0){
                  $update_notification = Notifications::where('id','=',$all_notifications->id)->update([
                                             'read' => 1
                                         ]);
                }
                return $all_notifications->read  == 0 ? 'unread' : 'read';
        })
      ->setRowAttr(['onclick' => function ($all_notifications) {

                if($all_notifications->link == ""){
                    $link = url().'/admin/dentist/dentist-compare/'.$all_notifications->from_details['doctor_id'];
                }else{
                   $link = url().$all_notifications->link;
                }
                return "gonotificationLink('".$link."')";
        }])
      ->editColumn('from_details.first_name', function ($all_patients) {
        $fullname = $all_patients->from_details->first_name." ".$all_patients->from_details->last_name;
              return $fullname;

            })
      ->make(true);
      return $rowdatatable;
   }
   
}