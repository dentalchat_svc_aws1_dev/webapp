﻿// When the browser is ready...
$(function() {
	// Setup form validation on the #register-form element
	
if ($("#doc_step1").length){

	jQuery.validator.addMethod("phoneUSCustom", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                  phone_number.match(/^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$/);
        }, "Invalid phone number");

	jQuery.validator.addMethod("CountyCodeUSCustom", function (country_code, element) {
            return this.optional(element) || country_code.match(/^\+?([\d]+)$/);
        }, "Invalid country code");

	$("#doc_step1").validate({

		ignore: [],
		// Specify the validation rules
		rules: {
			hid_frm_submit_res:{
				equalTo: "#hid_validate_res"
			},
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			country_code: {
				required: true,
                CountyCodeUSCustom: true,
            },
			contact_number: {
				required: true,
                phoneUSCustom: true,
            },
			password: {
				required: true
			},
			profile_img:{
				accept: "image/*"
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			hid_frm_submit_res: "",
			email: {
					email:"Please enter valid email."
			},
			profile_img:{
			   accept: 'Selected File should be an image',
			}
		},         

		submitHandler: function(form) {
			//return false;
			form.submit();
		}
	});
}

// 2nd Step Start //

if ($("#doc_step2").length){
	$("#doc_step2").validate({
		// Specify the validation rules
		rules: {
			zip_code: {
				required: true
			},
			clinic_picture:{
				accept: "image/*",
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			
			clinic_picture:{
			   accept: 'Selected File should be an image',
			}
		},         

		submitHandler: function(form) {
			form.submit();
		}
	});
}

// 2nd Step End //


// 3rd Step Start //

if ($("#doc_step3").length){
	$("#doc_step3").validate({
		ignore: [],
		// Specify the validation rules
		rules: {
			"data[0][certificate]":{
				extension: "png|pdf|jpg|jpeg",
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			
			 certificate:{
			    accept: 'Selected File should be jpg,jpeg,png,pdf',
			 }
		},         

		submitHandler: function(form) {
			form.submit();
		}
	});
}

// 3rd Step End //

// 4th Step Start //

if ($("#doc_step4").length){
	$("#doc_step4").validate({
		ignore: [],
		// Specify the validation rules
		rules: {
			"data[0][license_photo]":{
				extension: "png|pdf|jpg|jpeg",
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			
			license_photo:{
			   accept: 'Selected File should be jpg,jpeg,png,pdf',
			}
		},         

		submitHandler: function(form) {
			form.submit();
		}
	});
}

// 4th Step End //

// 5th Step Start //

if ($("#doc_step5").length){
	$("#doc_step5").validate({
		ignore: [],
		// Specify the validation rules
		rules: {
			"data[0][exp_photo]":{
				extension: "png|pdf|jpg|jpeg",
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			
			exp_photo:{
			   accept: 'Selected File should be jpg,jpeg,png,pdf',
			}
		},         

		submitHandler: function(form) {
			form.submit();
		}
	});
}

// 5th Step End //
//7th step//
    // $('#doc_step7').find('[name="lang_multiple"]')
    //         .multiselect({
    //             enableFiltering: true,
    //             includeSelectAllOption: true,
    //             // Re-validate the multiselect field when it is changed
    //             onChange: function(element, checked) {
    //                 $('#doc_step7').formValidation('revalidateField', 'lang_multiple');

    //                 adjustByScrollHeight();
    //             },
    //             onDropdownShown: function(e) {
    //                 adjustByScrollHeight();
    //             },
    //             onDropdownHidden: function(e) {
    //                 adjustByHeight();
    //             }
    //         })
    //         .end();
$("#doc_step7").validate({


	rules: {

    "languages[]": {
                        required : true,
                        minlength: 1
                    }
    }

});



if ($("#note_form").length){
	
	$("#note_form").validate({
		rules: {
			
			note: {
				required: true
			},
			
		
		},
	
	 // Specify the validation error messages
		messages: {
			hid_frm_submit_res: "",
			note: {
					required:"Please enter note details"
			},
			
		},               

		submitHandler: function(form) {
			form.submit();
		}
	});
	userJs.addValidationRules();
}

//subadmin
if ($("#cms_form_sub").length){
	$("#cms_form_sub").validate({
		ignore: [],
		// Specify the validation rules
		rules: {
			hid_frm_submit_res:{
				equalTo:"#hid_validate_res",
			},
			user_type: {
				required: true
			},
			name: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			contact: {
				required: true,
				number: true,
				maxlength: 10
			},
			subadmin_id: {
				required: true
			},
			gender: {
				required: true
			},
			address: {
				required: true
			},
			city: {
				required: true
			},
			zip_code: {
				required: true,
				number: true
			},
			video_link: {
				required: true,
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			hid_frm_submit_res: "",
			subadmin_id: {
					required:"Please type subadmin name and choose from suggested admin"
			},
			email: {
					email:"Please enter valid email."
			},
			url: {
					email:"Please enter valid url."
			},
			contact: {
				number: "Please enter number only.",
			},
		},               

		submitHandler: function(form) {
			form.submit();
		}
	});
	userJs.addValidationRules();
}

if ($("#product_form").length){
	$("#product_form").validate({
		ignore: [],
		rules: {
			name: {
				required: true
			},
			price: {
				required: true,
				number: true
			}
		
		},
	
	 // Specify the validation error messages
		messages: {
			hid_frm_submit_res: "",
			name: {
					email:"Please enter valid email."
			},
			contact: {
				number: "Please enter number only.",
			},
		},               

		submitHandler: function(form) {
			form.submit();
		}
	});
}




// add testimonial_form End //
});

/****************************************************************************************
 *				All functions related with user section		 							*
 ***************************************************************************************/
var userJs = {
	
	/***************	Check same email exists or not for subadmin. 	************************/
	checkSubAdminEmail: function (operationMode) {
		var hid_user_id = "";
		if (operationMode=='EDIT') {
			hid_user_id	= $('#hid_user_id').val();
		}
		else{
			hid_user_id = "";
		}
		var email	= $('#email').val();
		$.ajax({
			type 	: 'get',
			data 	: {hid_user_id: hid_user_id,email: email},
			url 	: base_url+'/admin/subadmin/check',
			async	: false,
			success	: function(response){
				//alert(response);
				if (response==1){
					$('#user_email_msg').text('Email already exists.');
					$('#user_email_msg').css('display','block');
					$('#hid_validate_res').val(0);
				}
				else{
					$('#user_email_msg').css('display','none');
					$('#hid_validate_res').val(1);
				}
			}
		});
    },
	
	
	/***************	Check same email exists or not. 	************************/
	checkUserEmail: function () {
		var hid_user_id = "";		
			hid_user_id	= $('#hid_user_id').val();

		var email	= $('#email').val();
		$.ajax({
			type 	: 'get',
			data 	: {hid_user_id: hid_user_id,email: email},
			url 	: base_url+'/admin/patient/check',
			async	: false,
			success	: function(response){
				//alert(response);
				if (response==1){
					$('#user_email_msg').text('Email already exists.');
					$('#user_email_msg').css('display','block');
					$('#hid_validate_res').val(0);
				}
				else{
					$('#user_email_msg').css('display','none');
					$('#hid_validate_res').val(1);
				}
			}
		});
    },
	
	changeDentistStatus: function(value,id) // Dentist Status
	{
		$.ajax({
			url: base_url+'/admin/patient/dentist-status',
			type: "get",
			data: { this_val : value,this_id : id},
			success: function(data){
				if(data == '1'){
					$('.alert-success').html('');
					$('#success_status_span_'+id).html('Status updated.');
					$('#success_status_span_'+id).fadeIn('slow');
					$('#success_status_span_'+id).fadeOut('slow');
				}
			}
		});
		
	},
	changeStatus: function(value,id)  // patient Status
	{
		$.ajax({
			url: base_url+'/admin/patient/status',
			type: "get",
			data: { this_val : value,this_id : id},
			success: function(data){
				if(data == '1'){
					$('.alert-success').html('');
					$('#success_status_span_'+id).html('Status updated.');
					$('#success_status_span_'+id).fadeIn('slow');
					$('#success_status_span_'+id).fadeOut('slow');
				}
			}
		});
		
	},
	
	addValidationRules: function(){
		var form_type = $('#form_type').val();
		var pwd_field = $('#password').val();
		if(form_type=='ADD' || pwd_field!='')
		{	
			$("#cms_form").validate({
				rules: {
					password: {
						minlength:7,
						pattern: /^$|^(?=.*?[A-Z]).{7,}$/,
						messages: {
							minlength:"Please enter minimum 7 character",
							pattern: "Password should have at least 7 characters and at least one uppercase letter. example: Passport34.",
						}
					},
					conf_password: {
						equalTo:"#password",
						messages:"Please enter the same value again"
					}
				}
			});
		
			$( "#password" ).rules( "add", {
				minlength:7,
				pattern: /^$|^(?=.*?[A-Z]).{7,}$/,
				messages: {
					minlength:"Please enter minimum 7 character",
					pattern: "Password should have at least 7 characters and at least one uppercase letter. example: Passport34.",
				}
			});
			$( "#conf_password" ).rules( "add", {
				equalTo:"#password",
				messages:"Please enter the same value again"
			});
		}
	},
	
	/*
	 *	During editing user, if password field is not empty then add validation
	 *	rules otherwise remove validation rules. 
	 */
	rulesWhileEditingUser: function(){
		var password = $('#password').val();
		if (password!='')
		{
			userJs.addValidationRules();
		}
		else
		{
			$( "#password, #conf_password" ).rules( "remove" );
		}
	},
	
	remove: function(id){
		var response = confirm('Do you really want to remove this dentist?');
		if(response==1){
			window.location.href = base_url+"/admin/dentist/remove/"+id;
		}
	},

	//used for patient delete
	removePatient: function(id){
		var response = confirm('Do you really want to remove this patient?');
		if(response==1){
			window.location.href = base_url+"/admin/patient/remove/"+id;
		}
	},


	/*-----------------------------------------------------------------------------*/
}

/*--------------------------------------------------------------------------------------*/
var productJs = {
	changeStatus: function(id){
		
		var this_val = $('#user_active_'+id).val();
		var this_id = $('#record_id_'+id).val();
		//alert(this_val)
		$.ajax({
			url: base_url+'/admin/products/status',
			type: "get",
			data: { this_val : this_val,this_id : this_id},
			success: function(data){
				if(data == '1'){
					$('.alert-success').html('');
					$('#success_status_span_'+id).html('Status updated.');
					$('#success_status_span_'+id).fadeIn('slow');
					$('#success_status_span_'+id).fadeOut('slow');
				}
			}
		});
		
	},
	/*
	 *	During editing user, if password field is not empty then add validation
	 *	rules otherwise remove validation rules. 
	 */
	
	remove: function(id){
		var response = confirm('Do you really want to remove this user?');
		if(response==1){
			window.location.href = base_url+"/admin/products/remove/"+id;
		}
	},
	/*-----------------------------------------------------------------------------*/
}

/****************************************************************************************/
/*			Function Name : dateRangeOverlaps											*/
/*			Uses	: Check date range overlap betwwen two date ranges					*/
/****************************************************************************************/
function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
	if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
	if (a_start <= b_end   && b_end   <= a_end) return true; // b ends in a
	if (b_start <  a_start && a_end   <  b_end) return true; // a in b
	return false;
}
/*--------------------------------------------------------------------------------------*/