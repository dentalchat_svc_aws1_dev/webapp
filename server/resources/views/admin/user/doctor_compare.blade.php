@extends('admin/layout/admin_template')

@section('content')
           
    @if(Session::has('success_message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success_message') }}</p>
    @endif  
    <div class="">    
        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Basic Profile</h3>
            </div>
            <div class="module-body">
                <!-- Prev --> 
                <div class="control-group" style="display:block;">
                    <div class="border-line">
                     <h4 class="pre-cur">previous step</h4>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">First Name :</div> <div class="module-right"> <?php echo ($tmp_dentist_details['tmp_docs_details']['first_name']) ? $tmp_dentist_details['tmp_docs_details']['first_name'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Last Name :</div> <div class="module-right">  <?php echo ($tmp_dentist_details['tmp_docs_details']['last_name']) ? $tmp_dentist_details['tmp_docs_details']['last_name'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Date Of Birth :</div> <div class="module-right"> <?php echo ($tmp_dentist_details['tmp_docs_details']['date_of_birth'] != '0000-00-00' && $tmp_dentist_details['tmp_docs_details']['date_of_birth'] != '') ? date('m-d-Y',strtotime($tmp_dentist_details['tmp_docs_details']['date_of_birth'])) : 'Not Applicable'; ?></div>
                    </div>
                    <?php
                        if($tmp_dentist_details['tmp_docs_details']['gender']=='1'){
                            $Tempgender ='Male';
                        }elseif ($tmp_dentist_details['tmp_docs_details']['gender']=='2') {
                            $Tempgender ='Female';
                        }else{
                            $Tempgender ='';
                        }

                    ?> 
                    <div class="border-line">   
                    <div class="module-left">Gender : </div> <div class="module-right"> <?php echo $Tempgender; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Email : </div> <div class="module-right"> <?php echo ($tmp_dentist_details['tmp_docs_details']['email']) ? $tmp_dentist_details['tmp_docs_details']['email'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Contact Number : </div> <div class="module-right"><?php echo ($tmp_dentist_details['tmp_docs_details']['contact_number']) ? $tmp_dentist_details['tmp_docs_details']['contact_number'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="admin-profile-img">
                    <?php if($tmp_dentist_details['tmp_docs_details']['profile_pics']){ ?>
                    <p class="new_avatar"><img src="<?php echo url();?>/uploads/tmp_dentist_profile_image/{{$tmp_dentist_details['tmp_docs_details']['profile_pics']}}"></p>
                    <?php }else { ?>
                        <p class="new_avatar">Not Applicable</p>
                    <?php }?>  
                    </div>                           
                </div> 
                <!-- Curr --> 
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">First Name : </div> <div class="module-right"> <?php echo $dentist_details['docs_details']['first_name']; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Last Name :</div> <div class="module-right"> <?php echo $dentist_details['docs_details']['last_name']; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Date Of Birth : </div> <div class="module-right"><?php echo ($dentist_details['docs_details']['date_of_birth'] != '0000-00-00') ? date('m-d-Y',strtotime($dentist_details['docs_details']['date_of_birth'])) : ''; ?></div>
                    </div>
                    <div class="border-line">
                    <?php
                        if($dentist_details['docs_details']['gender']=='1'){
                            $gender ='Male';
                        }elseif ($dentist_details['docs_details']['gender']=='2') {
                            $gender ='Female';
                        }else{
                            $gender ='';
                        }

                    ?>   
                    <div class="module-left">Gender : </div> <div class="module-right"> <?php echo $gender; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Email : </div> <div class="module-right"> <?php echo $dentist_details['docs_details']['email']; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Contact Number : </div> <div class="module-right"> <?php echo $dentist_details['docs_details']['contact_number']; ?></div>
                    </div>

                    <div class="admin-profile-img">
                    <?php if($dentist_details['docs_details']['profile_pics']!==''){ ?>
                    <p class="new_avatar"><img src="<?php echo url();?>/uploads/dentist_profile_image/{{$dentist_details['docs_details']['profile_pics']}}"></p>
                    <?php } ?>  
                    </div>                           
                </div>                  
            </div>
        </div> 

        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Business Details</h3>
            </div> 
            <!-- Prev -->            
            <div class="module-body">
                <div class="control-group" style="display:block;">
                    <div class="border-line">
                     <h4 class="pre-cur">previous step</h4>
                    </div>                
                    <div class="border-line">   
                    <div class="module-left">Business Name :</div> <div class="module-right"> <?php echo ($tmp_dentist_details['tmp_docs_clinics']['business_name']) ? $tmp_dentist_details['tmp_docs_clinics']['business_name'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Business Address : </div> <div class="module-right"><?php echo ($tmp_dentist_details['tmp_docs_clinics']['address']) ? $tmp_dentist_details['tmp_docs_clinics']['address'] : 'Not Applicable'; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Zip Code :</div> <div class="module-right"> <?php echo ($tmp_dentist_details['tmp_docs_clinics']['zip_code']) ? $tmp_dentist_details['tmp_docs_clinics']['zip_code'] : 'Not Applicable'; ?></div>
                    </div>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['sun_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['sun_opening_hours_to']!='')
                    { ?>
                    <div class="border-line">   
                    <div class="module-left">Sun :</div> <div class="module-right"> <?php echo $tmp_dentist_details['tmp_docs_clinics']['sun_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['sun_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['mon_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['mon_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Mon : </div> <div class="module-right"><?php echo $tmp_dentist_details['tmp_docs_clinics']['mon_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['mon_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['tue_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['tue_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Tues :</div> <div class="module-right"> <?php echo $tmp_dentist_details['tmp_docs_clinics']['tue_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['tue_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['wed_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['wed_opening_hours_to']!='')
                    { ?>
                    
                    <div class="border-line">   
                    <div class="module-left"> Wed : </div> <div class="module-right"><?php echo $tmp_dentist_details['tmp_docs_clinics']['wed_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['wed_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['thu_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['thu_opening_hours_to']!='')
                    { ?>
                    
                    <div class="border-line">   
                    <div class="module-left">Thur :</div> <div class="module-right"> <?php echo $tmp_dentist_details['tmp_docs_clinics']['thu_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['thu_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['fri_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['fri_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Fri : </div> <div class="module-right"><?php echo $tmp_dentist_details['tmp_docs_clinics']['fri_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['fri_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($tmp_dentist_details['tmp_docs_clinics']['sat_opening_hours_from']!='' && $tmp_dentist_details['tmp_docs_clinics']['sat_opening_hours_to']!='')
                    { ?>
                    
                    <div class="border-line">   
                    <div class="module-left">Sat :</div> <div class="module-right"> <?php echo $tmp_dentist_details['tmp_docs_clinics']['sat_opening_hours_from'].' - '.$tmp_dentist_details['tmp_docs_clinics']['sat_opening_hours_to'];?>
                    </div>
                      </div>
                    <?php } ?>
                      
                    <div class="admin-profile-img">
                    <?php if($tmp_dentist_details['tmp_docs_clinics']['clinic_picture']){ ?>
                    <p class="new_avatar" style=""><img src="<?php echo url();?>/uploads/tmp_clinic_picture/{{$tmp_dentist_details['tmp_docs_clinics']['clinic_picture']}}"></p>
                    <?php } ?>   
                    </div>                          
                </div>  
            <!-- Curr --> 

                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div>                
                    <div class="border-line">   
                    <div class="module-left">Business Name : </div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['business_name']; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Business Address :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['address']; ?></div>
                    </div>
                    <div class="border-line">   
                    <div class="module-left">Zip Code :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['zip_code']; ?></div>
                    </div>

                    <?php if($dentist_details['docs_clinics']['sun_opening_hours_from']!='' && $dentist_details['docs_clinics']['sun_opening_hours_to']!='')
                    { ?>
                        <div class="border-line">   
                    <div class="module-left">Sun : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['sun_opening_hours_from'].' - '.$dentist_details['docs_clinics']['sun_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['mon_opening_hours_from']!='' && $dentist_details['docs_clinics']['mon_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Mon : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['mon_opening_hours_from'].' - '.$dentist_details['docs_clinics']['mon_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['tue_opening_hours_from']!='' && $dentist_details['docs_clinics']['tue_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Tues :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['tue_opening_hours_from'].' - '.$dentist_details['docs_clinics']['tue_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['wed_opening_hours_from']!='' && $dentist_details['docs_clinics']['wed_opening_hours_to']!='')
                    { ?>
                    
                        <div class="border-line">   
                    <div class="module-left">Wed :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['wed_opening_hours_from'].' - '.$dentist_details['docs_clinics']['wed_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['thu_opening_hours_from']!='' && $dentist_details['docs_clinics']['thu_opening_hours_to']!='')
                    { ?>
                        
                        <div class="border-line">   
                    <div class="module-left">Thur :</div> <div class="module-right"> <?php echo $dentist_details['docs_clinics']['thu_opening_hours_from'].' - '.$dentist_details['docs_clinics']['thu_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['fri_opening_hours_from']!='' && $dentist_details['docs_clinics']['fri_opening_hours_to']!='')
                    { ?>
                        
                        <div class="border-line">   
                    <div class="module-left">Fri : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['fri_opening_hours_from'].' - '.$dentist_details['docs_clinics']['fri_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>

                    <?php if($dentist_details['docs_clinics']['sat_opening_hours_from']!='' && $dentist_details['docs_clinics']['sat_opening_hours_to']!='')
                    { ?>
                        
                        <div class="border-line">   
                    <div class="module-left">Sat : </div> <div class="module-right"><?php echo $dentist_details['docs_clinics']['sat_opening_hours_from'].' - '.$dentist_details['docs_clinics']['sat_opening_hours_to'];?>
                    </div>
                    </div>
                    <?php } ?>
                    

                   <div class="admin-profile-img">
                    <?php if($dentist_details['docs_clinics']['clinic_picture']){ ?>
                    <p class="new_avatar" ><img src="<?php echo url();?>/uploads/clinic_picture/{{$dentist_details['docs_clinics']['clinic_picture']}}"></p>
                    <?php } ?>
                    </div>                             
                </div>  
            </div>            
            
        </div>

        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Education & Training</h3>
            </div>

             <!-- Prev -->            
            <div class="module-body">
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">previous step</h4>
                    </div>                
                   <?php if(!empty($tmp_dentist_details['tmp_docs_education']))
                   {
                    foreach($tmp_dentist_details['tmp_docs_education'] as $c=>$tmp_eachdeg)
                    {
                    ?> 
                    <div class="border-line">
                    <div class="module-left">Degree : </div><div class="module-right"><?php echo $tmp_eachdeg['degree']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">Year Completed :</div><div class="module-right"><?php echo $tmp_eachdeg['completed_year']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">School Name :</div> <div class="module-right"> <?php echo $tmp_eachdeg['medical_school']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">School Location :</div> <div class="module-right"> <?php echo $tmp_eachdeg['location']; ?></div></div>

                    <?php if($tmp_eachdeg['certificate']){ ?>
                     <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/tmp_certificate/{{$tmp_eachdeg['certificate']}}">View Certificate</a> </h3> </div>
                   <?php }
                      } 
                   }?>             
                </div>   
            <!-- Curr --> 
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div>
                   <?php if(!empty($dentist_details['docs_education']))
                   {
                    foreach($dentist_details['docs_education'] as $c=>$eachdeg)
                    {
                    ?> 
                    <div class="border-line">
                    <div class="module-left">Degree : </div> <div class="module-right"> <?php echo $eachdeg['degree']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">Year Completed :</div> <div class="module-right">  <?php echo $eachdeg['completed_year']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">School Name :</div> <div class="module-right">  <?php echo $eachdeg['medical_school']; ?></div>
                    </div>
                    <div class="border-line">
                    <div class="module-left">School Location :</div> <div class="module-right">  <?php echo $eachdeg['location']; ?></div></div>
                    
                    <?php if($eachdeg['certificate']){ ?>
                        <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/certificate/{{$eachdeg['certificate']}}">View Certificate</a> </h3></div>

                   <?php } 
                       }
                   }?>             
                </div>  
            </div> 

            <!-- Prev -->
            <div class="module-body">
                 <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">previous step</h4>
                    </div> 
                    <?php if(!empty($tmp_dentist_details['tmp_docs_award']))
                    {
                    ?>
                        <div class="border-line">
                     <div class="module-left">Award : </div>
                     <div class="module-right">
                    <?php
                        foreach($tmp_dentist_details['tmp_docs_award'] as $a=>$tmp_eachaward)
                        {
                    ?>
                            <p><?php echo $tmp_eachaward['award']; ?></p>
                     <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                 </div>
             
            <!-- Curr --> 
                 <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div> 
                    <?php if(!empty($dentist_details['docs_award']))
                    {
                    ?>
                        <div class="border-line">
                     <div class="module-left">Award :</div>
                      <div class="module-right">
                    <?php
                        foreach($dentist_details['docs_award'] as $a=>$eachaward)
                        {
                    ?>
                            <p><?php echo $eachaward['award']; ?></p>
                    <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                 </div>
            </div>            

            <!-- Prev -->
            <div class="module-body">
                 <div class="control-group" style="display:block;">
                     <div class="border-line">   
                     <h4 class="pre-cur">previous step</h4>
                    </div>                 
                    <?php if(!empty($tmp_dentist_details['tmp_docs_insurance']))
                    {
                    ?>
                         <div class="border-line">
                     <div class="module-left">Insurance : </div>
                     <div class="module-right">
                    <?php 
                        foreach($tmp_dentist_details['tmp_docs_insurance'] as $in=>$tmp_eachinsurance)
                        {
                    ?>
                            <p><?php echo $tmp_eachinsurance['insurance']; ?></p>
                     <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                 </div>

            <!-- Curr --> 
                 <div class="control-group" style="display:block;">
                    <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div>   
                    <?php if(!empty($dentist_details['docs_insurance']))
                    {
                    ?>
                    <div class="border-line">
                     <div class="module-left">Insurance :</div>
                        <div class="module-right">
                    <?php 
                        foreach($dentist_details['docs_insurance'] as $in=>$eachinsurance)
                        {
                    ?>
                            <p><?php echo $eachinsurance['insurance']; ?></p>
                    <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                 </div>
            </div>            
                       
        </div>

         <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>License</h3>
            </div>
            <!-- Prev-->
            <div class="module-body">           
                    <div class="control-group" style="display:block;">
                        <div class="border-line">   
                         <h4 class="pre-cur">previous step</h4>
                        </div>
                       <?php if(!empty($tmp_dentist_details['tmp_docs_license']))
                       {
                        foreach($tmp_dentist_details['tmp_docs_license'] as $l=>$tmp_eachlicense)
                        {
                        ?> 
                        <div class="border-line">
                     <div class="module-left">License : </div> <div class="module-right"><?php echo $tmp_eachlicense['license_details']; ?></div> 
                     </div>
                     <?php if($tmp_eachlicense['license_details']){ ?>                 
                        <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/tmp_license_photo/{{$tmp_eachlicense['license_photo']}}">View License</a> </h3></div>
                       <?php } 
                        }
                       }?>             
                    </div>
            <!-- Curr -->   
                <div class="control-group" style="display:block;">
                      <div class="border-line">   
                     <h4 class="pre-cur">current step</h4>
                    </div>                
                   <?php if(!empty($dentist_details['docs_license']))
                   {
                    foreach($dentist_details['docs_license'] as $l=>$eachlicense)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">License : </div> <div class="module-right"> <?php echo $eachlicense['license_details']; ?></div> </div>
                     <?php if($eachlicense['license_details']){ ?>                  
                        <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/license_photo/{{$eachlicense['license_photo']}}">View License</a> </h3></div>
                   <?php }
                      }
                   }?>             
                </div>
            </div>            
            
        </div>

        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Professional Experience</h3>
            </div>
            <!-- Prev -->
            <div class="module-body">
                <div class="control-group" style="display:block;">
                        <div class="border-line">   
                         <h4 class="pre-cur">previous step</h4>
                        </div>
                   <?php if(!empty($tmp_dentist_details['tmp_docs_experiece']))
                   {
                    foreach($tmp_dentist_details['tmp_docs_experiece'] as $e=>$tmp_eachexperiece)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">Professional Experience : </div> <div class="module-right"> <?php echo $tmp_eachexperiece['prof_exp']; ?></div>
                     </div>
                    <div class="border-line">
                     <div class="module-left">Dental Business Name : </div> <div class="module-right"> <?php echo $tmp_eachexperiece['dental_business']; ?></div>
                     </div>
                    <div class="border-line">
                     <div class="module-left">Location : </div> <div class="module-right"> <?php echo $tmp_eachexperiece['location']; ?></div>
                    </div>
                    
                    <?php if($tmp_eachexperiece['exp_from']!='')
                    { 
                        if($tmp_eachexperiece['exp_to']!=''){?>
                        
                     <div class="border-line">
                     <div class="module-left">Experience Duration :</div> 
                     <div class="module-right"><?php echo $tmp_eachexperiece['exp_from'].' - '.$tmp_eachexperiece['exp_to'];?></div>
                     </div>
                    
                    <?php } else { ?>

                    <div class="border-line">
                     <div class="module-left">Experience Duration :</div> 
                      <div class="module-right"><?php echo $tmp_eachexperiece['exp_from'].' - Till Date';?></div>
                      </div>
                    <?php 
                        }
                    }
                    ?>

                    <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/tmp_docs_experiece/{{$tmp_eachexperiece['exp_photo']}}">View Experience</a> </h3> </div>
                   <?php } 
                   }?>             
                </div>
            <!-- Curr -->   
                <div class="control-group" style="display:block;">
                     <div class="border-line">   
                        <h4 class="pre-cur">current step</h4>
                      </div>               
                   <?php if(!empty($dentist_details['docs_experiece']))
                   {
                    foreach($dentist_details['docs_experiece'] as $e=>$eachexperiece)
                    {
                    ?> 
                    <div class="border-line">
                     <div class="module-left">Professional Experience :</div> <div class="module-right">  <?php echo $eachexperiece['prof_exp']; ?></div></div>
                    <div class="border-line">
                     <div class="module-left">Dental Business Name : </div> <div class="module-right"> <?php echo $eachexperiece['dental_business']; ?></div></div>
                    <div class="border-line">
                     <div class="module-left">Location : </div> <div class="module-right"> <?php echo $eachexperiece['location']; ?></div></div>
                    
                    <?php if($eachexperiece['exp_from']!='')
                    { 
                        if($eachexperiece['exp_to']!=''){?>
                        
                        <div class="border-line">
                     <div class="module-left">Experience Duration : </div> <div class="module-right"><?php echo $eachexperiece['exp_from'].' - '.$eachexperiece['exp_to'];?></div></div>
                    
                    <?php } else { ?>

                        <div class="border-line">
                     <div class="module-left">Experience Duration : </div> <div class="module-right"><?php echo $eachexperiece['exp_from'].' - Till Date';?></div></div>
                    <?php 
                        }
                    }
                    ?>
                    <?php if($eachexperiece['exp_photo']!='') { ?>
                    <div> <h3 class="a-hdline"> <a href="<?php echo url();?>/uploads/exp_document/{{$eachexperiece['exp_photo']}}">View Experience</a> </h3></div>
                    <?php } ?>
                   <?php } 
                   }?>             
                </div>
            </div>
            
            
        </div>   


        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Skill Highlights / Specialties</h3>
            </div>
            <!-- Prev -->
            <div class="module-body">
                <div class="control-group" style="display:block;">
                   <div class="border-line">   
                     <h4 class="pre-cur">previous step</h4>
                    </div> 
                   <?php if(!empty($tmp_dentist_details['tmp_docs_skill']))
                    {
                    ?>
                    <div class="border-line">
                     <div class="module-left">License :</div> 
                     <div class="module-right">
                    <?php
                    foreach($tmp_dentist_details['tmp_docs_skill'] as $s=>$tmp_eachskill)
                    {
                    ?> 
                      <p><?php echo $tmp_eachskill['skills']; ?></p>
                   <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>             
                </div> 
            <!-- Curr -->
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                      <h4 class="pre-cur">current step</h4>
                    </div> 
                   <?php if(!empty($dentist_details['docs_skill']))
                    {
                    ?>
                    <div class="border-line">
                     <div class="module-left">License :</div>
                     <div class="module-right">
                    <?php
                    foreach($dentist_details['docs_skill'] as $s=>$eachskill)
                    {
                    ?> 
                      <p><?php echo $eachskill['skills']; ?></p>
                   <?php 
                        }
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                    ?>            
                </div> 
            </div>            
            
        </div>

        <div class="module maincontent module-cur-pre">
            <div class="module-head">
            <h3>Language Spoken </h3>
            </div>
            <!--Prev -->
            <div class="module-body">
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                      <h4 class="pre-cur">previous step</h4>
                    </div>                
                   <?php if(!empty($tmp_dentist_details['tmp_docs_language']))
                    {
                    ?>
                    <div class="border-line">
                     <div class="module-left">language Spoken : </div> 
                     <div class="module-right"> <?php echo $tmp_dentist_details['tmp_docs_language']['language_name'];?></div>
                     </div>
                        <div class="border-line">
                     <div class="module-left">Personal Statement : </div> 
                     <div class="module-right">
                        <div class="controls">
                        <?php echo ($tmp_dentist_details['tmp_docs_language']['personal_statement']!='')?$tmp_dentist_details['tmp_docs_language']['personal_statement']:''?>
                        </div>
                     </div>
                     </div> 
                   <?php 
                    } 
                   ?> 
                </div>
            <!--Curr -->
                <div class="control-group" style="display:block;">
                    <div class="border-line">   
                      <h4 class="pre-cur">current step</h4>
                    </div> 
                   <?php if(!empty($dentist_details['docs_language']))
                    {
                    ?>
                   <div class="border-line">
                     <div class="module-left">language Spoken : </div> 
                     <div class="module-right"> <?php echo $dentist_details['docs_language']['language_name'];?></div>
                     </div>

                    <div class="border-line">
                     <div class="module-left">Personal Statement : </div> 
                     <div class="module-right">
                        <div class="controls">
                        <?php echo ($dentist_details['docs_language']['personal_statement']!='')?$dentist_details['docs_language']['personal_statement']:''?>
                        </div>
                        </div>
                        </div>
                   <?php 
                    } 
                   ?>             
                 </div>
                    
            </div>
        </div>

            
        </div>

        

        <div class="control-group">
            <div class="controls">
                <a href="{!! url('admin/dentist/dentist-step7/'.$user_id)!!}" class="btn">Back</a>
                <a href="javascript:void(0)" onclick="approved(<?php echo $user_id; ?>)" class="btn">Approved & Publish</a>            
            </div>
        </div>
    </div>
    
    <script>
    function approved(id)
    {
        $.ajax({
                  url: site_url+"/admin/item/approved-profile",
                  type: 'POST',
                  data: {'dentist_id':id},
                  success: function(html)
                  {
                    window.location.href=site_url+"/admin/dentist/dentist-preview/"+id;
                  }
                });
    }    
        
    </script>

@stop