/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.register', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) 
	{
		$stateProvider.state('dentist-signup', {
								url: '/dentist-signup',
								templateUrl: 'scripts/pages/register/dentist_signup.html',
								controller: 'RegisterPageCtrl',								
							});
		
		$stateProvider.state('registration-success', {
								url: '/registration-success',
								templateUrl: 'scripts/pages/register/register_success.html',
								controller: 'RegisterPageCtrl',
							});

		$stateProvider.state('activate-user', {
								url: '/activate-user/:parameter',
								templateUrl: 'scripts/pages/register/dentist_activate.html',
								controller: 'RegisterPageCtrl',								
							});
		
		$stateProvider.state('dentist-signin', {
								url: '/dentist-signin',
								templateUrl: 'scripts/pages/register/dentist_signin.html',
								controller: 'RegisterPageCtrl',
							});
		$stateProvider.state('dentist-forgotpassword', {
								url: '/dentist-forgotpassword',
								templateUrl: 'scripts/pages/register/dentist_forgot_password.html',
								controller: 'RegisterPageCtrl',
							});
		$stateProvider.state('reset-password', {
								url: '/reset-password/:parameter',
								templateUrl: 'scripts/pages/register/dentist_reset_password.html',
								controller: 'RegisterPageCtrl',
							});

		$stateProvider.state('dentist-change-password', {
								url: '/dentist-signin',
								templateUrl: 'scripts/pages/register/dentist_change_password.html',
								controller: 'RegisterPageCtrl',
							});




    }
	
})();
