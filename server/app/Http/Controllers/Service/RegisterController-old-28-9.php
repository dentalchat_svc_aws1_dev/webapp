<?php namespace App\Http\Controllers\Service;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Model\Doctor; 	
use App\Model\DoctorDetails;
use App\Model\DoctorClinic;
use App\Model\DoctorEducation;
use App\Model\DoctorAward;
use App\Model\DoctorInsurance;
use App\Model\DoctorExperience;
use App\Model\DoctorLicense;
use App\Model\DoctorSkill;
use App\Model\DoctorLanguage; 
use App\Model\DoctorLoginDetails;
use App\Model\Sitesetting;
use App\Model\SiteUser;
use App\Model\PatientAuthToken;
use App\Model\Patient;

use App\Model\TmpDoctorDetails;

use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;



class RegisterController extends BaseController {


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
        if(file_exists('../blog/wp-load.php')){
            require_once '../blog/wp-load.php';
        }
  		//parent::__construct();
    }


	
	
	/* User registration function */
	function postRegister(){
		
		$data = Request::all();
		
		$check_user_email 	= DoctorDetails::where('email',$data['user']['email'])->count();
		if($check_user_email>0){
			echo json_encode(array('status'=>2));
		}
		else{
			$src_sys = isset($data['user']['src_sys']) ? $data['user']['src_sys'] : 'dentist-signin';
			$hear_about_us = isset($data['user']['hear_about_us']) ? $data['user']['hear_about_us'] : '';
			$create_doctor		= Doctor::create([
									'email_status'     => 0,
									'admin_status'     => 0,
									'publish_status'   => 0,
									'hear_about_us'   => $hear_about_us,
									'src_sys'   => $src_sys,
									'complete_profile_status' => 0,								
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
			
			$doctor_id = $create_doctor->id;    // last  inserted id
			
			$doctor_details		= DoctorDetails::create([
									'doctor_id'		 =>	$doctor_id,		 
									'first_name'     => $data['user']['first_name'],
									'last_name'     => $data['user']['last_name'],
									'email'       	=> $data['user']['email'],
									'contact_number' => $data['user']['contact_number'],
									'conuntry_code' => $data['user']['country_code'],
									'password'      => Hash::make($data['user']['password']),									
									'created_at'	=> date('Y-m-d H:i:s'),
									'updated_at'	=> date('Y-m-d H:i:s')
								]);
            if($doctor_details){
                /* wp function start here to check user exists or not*/
                if ( email_exists( $data['user']['email'] ) ) {
                    $wp_user = get_user_by( 'email', $data['user']['email'] );
                    if($wp_user){
                        $wp_user_id = $wp_user->ID;
                        wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $password ) );
                        wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                    }
                }else{
                    $userdata = array(
                        'user_email' => $data['user']['email'],
                        'user_login' => $data['user']['email'],
                        'display_name'=>$data['user']['first_name'].' '.trim($data['user']['last_name']),
                        'nickname'=>$data['user']['first_name'],
                        'first_name'=>$data['user']['first_name'],
                        'last_name' => $data['user']['last_name'],
                        'user_pass'  => $data['user']['password']
                    );
    
                    $wp_user_id = wp_insert_user( $userdata ) ;
                    if($wp_user_id){
                        wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                        update_user_meta($wp_user_id, 'contact_number', $data['user']['contact_number']);
                        update_user_meta($wp_user_id, 'conuntry_code',$data['user']['country_code']);
                    }
                }
                /*wp function end here*/
			}
			/*start code by covetus*/
			$cur_date = date('Y-m-d H:i:s');
			$clinic_data = ['doctor_id'     => $doctor_id, 'business_name' => $data['user']['business_name'],
			                'zip_code'     => $data['user']['zipcode'], 'created_at'	=> $cur_date, 'updated_at'	=> $cur_date];
			if($src_sys == 'special-promo'){
			   $clinic_data = ['doctor_id' => $doctor_id, 'business_name' => $data['user']['business_name'],
			                    'zip_code' => $data['user']['zipcode'],
                                'address' => isset($data['user']['business_address'])?trim($data['user']['business_address']): '',
                                'lat' => isset($data['user']['lat'])?trim($data['user']['lat']): '',
                                'lang' => isset($data['user']['lang'])?trim($data['user']['lang']): '',
                                'city' => isset($data['user']['city'])?trim($data['user']['city']): '',
                                'state' => isset($data['user']['state'])?trim($data['user']['state']): '',
                                'country' => isset($data['user']['country'])?trim($data['user']['country']): '', 
                                'created_at' => $cur_date, 'updated_at' => $cur_date]; 
                if(isset($data['user']['dental_license'])&&!empty($data['user']['dental_license'])){
                    DoctorLicense::create(['doctor_id'=>$doctor_id, 'license_details'=>$data['user']['dental_license'], 'created_at'=>$cur_date, 'updated_at'=>$cur_date]);
                }
			    if(isset($data['user']['dental_services_offered'])&&!empty($data['user']['dental_services_offered'])){
			        DoctorSkill::create(['doctor_id' => $doctor_id,'skills'=> trim($data['user']['dental_services_offered']),'created_at' => $cur_date,'updated_at' => $cur_date]);	
                }
                if(isset($data['user']['short_description'])&&!empty($data['user']['short_description'])){
                    DoctorLanguage::create([ 'doctor_id' => $doctor_id, 'personal_statement'=> $data['user']['short_description'], 'created_at' => $cur_date,'updated_at' => $cur_date]);
                }
			}
			$doctor_clinic		= DoctorClinic::create($clinic_data);
            /*end code by covetus*/

			############Generating XML --STARTS#########################
	        /*$cmd = "wget -bq --spider http://demo.uiplonline.com/dentist-amar/dev/server/save-doctors-xml";
            shell_exec(escapeshellcmd($cmd));*/
            $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
	        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	            xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
	            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	        $doctor_xml = DoctorDetails::get();
	        foreach($doctor_xml as $v)
	        {

	        	$doc_clinic_dtls = DoctorClinic::where('doctor_id',$v->doctor_id)->first();

	            $xmlString .= '<url>';
	            $xmlString .= '<Doctor_Name>'.$v->first_name.' '.$v->last_name.'</Doctor_Name>';
	            $xmlString .= '<Doctor_Business_Name>'.$doc_clinic_dtls->business_name.'</Doctor_Business_Name>';
	            $xmlString .= '<Doctor_zipcode>'.$doc_clinic_dtls->zip_code.'</Doctor_zipcode>';
	            //$xmlString .= '<Date_Of_Birth>'.$v->date_of_birth.'</Date_Of_Birth>';
	            //$xmlString .= '<Email>'.$v->email .'</Email>';
	           // $xmlString .= '<Contact_No>'.$v->conuntry_code.' '.$v->contact_number .'</Contact_No>';
	           /* if($v->profile_pics != '')
	            {
	            	$xmlString .= '<Image>'.url('/').'server/uploads/dentist_profile_image/'.$v->profile_pics.'</Image>';
	            }
	            else
	            {
	            	$xmlString .= '<Image>'.$v->profile_pics.'</Image>';
	            }*/
	            
	            $xmlString .= '</url>';
	        }

	        $xmlString .= '</urlset>';

	        $dom = new \DOMDocument;
	        $dom->preserveWhiteSpace = FALSE;
	        $dom->loadXML($xmlString);
	        //$dom->save('xml/doctors.xml');
	        $dom->save('../sitemap.xml');

			############Generating XML --ENDS#########################
			
			
			if($doctor_details){

				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$user_email = $data['user']['email'];
				$user_name = $data['user']['first_name'];

				
				$length = strrpos(url(), "/");
				$activation_url = substr(url(),0,$length);
				$activation_password_link = $activation_url.'/activate-user/'.base64_encode($user_email);
				//echo $activation_password_link; exit;
				$sent = Mail::send('service.dentalchat.dentist_activation_mail', array('name'=>$user_name,'email'=>$user_email,'activation_password_link'=>$activation_password_link,'admin_users_email'=>$admin_users_email,'sitename'=>env('SITENAME'),'activation_url'=>$activation_url), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('DentalChat :: Email verification link!');
	            });

				echo json_encode(array('status'=>1,'useremail'=>$user_email));
			}
			else{
				echo json_encode(array('status'=>0));
			}
		}
		
		
		exit;
	}


	/* Registration Verified Link */
	function postCheckActivation(){
	
		$data 		= Request::all();
		$email 		= $data['email'];

		// check with email and user status
		$doc_table = DoctorDetails::where('email',$email)->first();
		$doctor_id = $doc_table->doctor_id;

		if($doctor_id!='')
		{
			$check_user_query	= Doctor::where('id',$doctor_id)->where('email_status',0);
			$check_user = $check_user_query->first();

			if($check_user!=""){
	               
				$user_id_with_time 	= $check_user['id'].':'.date("Y-m-d H:i:s");
				$token 				= Hash::make($user_id_with_time);

				// Update status for register user

				Doctor::where('id',$doctor_id)->update(array('email_status' => 1));
				
				$wp_user = get_user_by( 'email', $email );
		        if($wp_user){
		            $wp_user_id = $wp_user->ID;
                    wp_update_user( array( 'ID' => $wp_user_id, 'user_status' => 1 ) );  
		        }
		        
		
				echo json_encode(
							array(
								'status'	=> 1,
								'data' => array()
							)
						);  
	                
	        }
	        else{
	                
	        	echo json_encode(
							array(
								'status'	=> 2,
								'data' => array()
							)
				);
	        }
		}
		else
		{
			echo json_encode(
							array(
								'status'	=> 3,
								'data' => array()
							)
				);
		}
		

		exit;
	}

	
	
	
	/* Dentist Login */
	function postDentistLogin(){

		$data 		= Request::all();
		$email 		= $data['user']['email'];
		$password 	= $data['user']['password'];
		$encrypt_pass = Hash::make($password);

		$login_arr = array('email' => $email, 'password' => $encrypt_pass);

		// check with email and user type
		$tmp_check_user	= TmpDoctorDetails::where('email',$email)->first();
		if($tmp_check_user !=NULL)
		{
			$check_user = $tmp_check_user;
			$temp_check = 1;
		}
		else
		{
			$check_user	= DoctorDetails::where('email',$email)->first();
			$temp_check = 0;
		}
		 
		 
         
//print_r($check_user); exit;
		if($check_user!=""){

                $user_pass = $check_user->password;

                // check for password
                if(Hash::check($password, $user_pass)){

                    // Check for active                 
                    $user_cnt = Doctor::where('email_status', 1)->where('id',$check_user->doctor_id)->count();
//print_r($check_user);exit;
                    if($user_cnt){
                        
                    	$user_id_with_time 	= $check_user['id'].':'.date("Y-m-d H:i:s");
						$token 				= Hash::make($user_id_with_time);
				
						$create_user		= DoctorLoginDetails::create([
												'doctor_id'       => $check_user['doctor_id'],
												'token'       	=> $token,
												'login_time'    => date("Y-m-d H:i:s")
											]);
						
						$make_profile_url = strtolower($check_user['first_name']).'-'.strtolower($check_user['last_name']).'-'.$check_user['doctor_id'];

						if($temp_check==1){
							$temp_profile_pic = $check_user['profile_pics'];
						}else{
							$temp_profile_pic = '';
						}

						$encripted_usr_info	= base64_encode($check_user['id']."|@|".$check_user['first_name']."|@|".$check_user['email']."|@|".$token."|@|".$check_user['profile_pics']."|@|".$make_profile_url."|@|".$temp_profile_pic);

						//print(base64_decode($encripted_usr_info));exit;
						/*echo json_encode(
									array(
										'status'	=> 1,
										'data' => array('usr_info'=>$encripted_usr_info,'user_id'=>$check_user['id'],'name'=>$check_user['first_name'],'email'=>$check_user['email'],'token'=>$token)
									)
								);  */ 
						//$doctor_profile_img_detls	= DoctorDetails::where('email',$email)->first();
						/* wp function start here to check user exists or not*/
                        if ( email_exists( $email ) ) {
                            $wp_user = get_user_by( 'email', $email );
                            if($wp_user){
                                $wp_user_id = $wp_user->ID;
                                wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $password ) );
                                wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                            }
                        }else{
                            $userdata = array(
                                'user_email' => $email,
                                'user_login' => $email,
                                'display_name'=>$check_user['first_name'].' '.trim($check_user['last_name']),
                                'nickname'=>$check_user['first_name'],
                                'first_name'=>$check_user['first_name'],
                                'last_name' => $check_user['last_name'],
                                'user_pass'  => $password 
                            );
            
                            $wp_user_id = wp_insert_user( $userdata ) ;
                            if($wp_user_id){
                                wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                                update_user_meta($wp_user_id, 'contact_number', $check_user['contact_number']);
                                update_user_meta($wp_user_id, 'conuntry_code',$check_user['country_code']);
                            }
                        }
                        if($wp_user_id){
                            wp_set_current_user( $wp_user_id, $email );
                            wp_set_auth_cookie( $wp_user_id );
                            do_action( 'wp_login', $email );
                        }
                        /*wp function end here*/



						echo json_encode(
									array(
										'status'	=> 1,
										'data' => array(
											'usr_info'=>$encripted_usr_info,
											'user_id'=>$check_user['doctor_id'],
											'name'=>$check_user['first_name'],
											'email'=>$check_user['email'],
											'token'=>$token,
											'final_profile_image'=>$check_user['profile_pics']
										)
									)
								);
					  			 
					  Session::put('user_pass_for_chaking', $password);
					  return;                     
                    }
                    else{
						
						echo json_encode(
									array(
										'status'	=> 2,
										'data' => array()
									)
								);     
                    }
                }
                else{
                        
                	echo json_encode(
									array(
										'status'	=> 0,
										'data' => array()
									)
								);                   
                }
        }
        else{
                
        	echo json_encode(
							array(
								'status'	=> 0,
								'data' => array()
							)
						); 

        }

	exit;
	}
	
	
		
	/* Forget password */
	public function postForgotPassword(){

		$email 		= Request::input('email');
		//$user_type 	= Request::input('user_type');

		// Check If email exist in DB
		$email_query =  	DoctorDetails::where('email',$email);
		$email_exist_cnt = 	$email_query->count();

		if($email_exist_cnt==0){

			echo json_encode(
							array(
								'status'	=> 0
							)
						); 
		}
		else{

			$user_details = $email_query->first();
			$user_name = $user_details->last_name;
			$user_email = $user_details->email;
			$user_cnt = Doctor::where('email_status', 1)->where('admin_status', 1)->where('id',$user_details->doctor_id)->count();
			
			if($user_cnt){
			// Get randon number
				$random_code = mt_rand();
	 
				// Update the random code in DB
				$email_query->update(array('code_number' => $random_code));
				$length = strrpos(url(), "/");
				$forgot_mail_url = substr(url(),0,$length);
				
				$reset_password_link = $forgot_mail_url.'/reset-password/'.base64_encode($user_email).'@@'.base64_encode($random_code);

				$perm_domain	= $forgot_mail_url;
				$sitesettings = DB::table('sitesettings')->where('id',1)->first();
				if(!empty($sitesettings))
				{
					$admin_users_email = $sitesettings->value;
				}

				$sent = Mail::send('service.dentalchat.reset_password_link', array('name'=>$user_name,'email'=>$user_email,'reset_password_link'=>$reset_password_link,'admin_users_email'=>$admin_users_email,'perm_domain'=>$perm_domain), 
	            function($message) use ($admin_users_email, $user_email,$user_name)
	            {
	                $message->from($admin_users_email,'Dental Chat');
	                $message->to($user_email, $user_name)->subject('Dental Chat::Reset your dentalchat.com password!');
	            });

				echo json_encode(
								array(
									'status'	=> 1
								)
							); 
			}else{
				echo json_encode(
							array(
								'status'	=> 2
							)
						);
			}  

		}

      
	}

	
	/* Check forget password link */
	public function postCheckLink(){

		$param 		= Request::input('param');

		//echo $param;exit;
		
		// Explaode the parameter to get email id and random number
		list($email,$random_code) = explode('@@', $param);
		$email 			= base64_decode($email);
		$random_code 	= base64_decode($random_code);

		// Check If random code is deleted
		$email_query =  	DoctorDetails::where('email',$email)->where('code_number',$random_code);
		$email_exist_cnt = 	$email_query->count();

		
		if($email_exist_cnt==0){

			echo json_encode(
							array(
								'status'	=> 0,
								'data' 		=> array()
							)
						); 
		}
		else{

            echo json_encode(
							array(
								'status'	=> 1,
								'data' 		=> array('email'=>$email)
							)
						);   

		}      
	}

	
	/* user and restaurant owner forgot password */
	public function postChangeForgotPassword(){

		$password 	= Request::input('password');
		$email 		= Request::input('email');
		//echo $password; exit;
		// Update Password
		$email_query = DoctorDetails::where('email',$email);
		$email_query->update(array('code_number' => '','password' => Hash::make($password)));

		$check_email = TmpDoctorDetails::where('email',$email)->count();

		if($check_email>0)
		{
			$email_query1 = TmpDoctorDetails::where('email',$email);
			$email_query1->update(array('code_number' => '','password' => Hash::make($password)));	
		}
		/* wp function start here for password update */
		$userdata = DoctorDetails::where('email',$email)->first();
		if($userdata){
		    if ( email_exists( $email ) ) {
                $wp_user = get_user_by( 'email', $email );
                if($wp_user){
                    $wp_user_id = $wp_user->ID;
                    wp_update_user( array( 'ID' => $wp_user_id, 'user_pass' => $password ) );
                    wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                }
            }else{
                $userdata = array(
                    'user_email' => $email,
                    'user_login' => $email,
                    'display_name'=>$userdata->first_name.' '.trim($userdata->last_name),
                    'nickname'=>$userdata->first_name,
                    'first_name'=>$userdata->first_name,
                    'last_name' => $userdata->last_name,
                    'user_pass'  => $password 
                );
            
                $wp_user_id = wp_insert_user( $userdata ) ;
                if($wp_user_id){
                    wp_update_user( array ('ID' => $wp_user_id, 'role' => 'author') ) ;
                    update_user_meta($wp_user_id, 'contact_number', $userdata->contact_number);
                    update_user_meta($wp_user_id, 'conuntry_code',$userdata->country_code);
                }
            }
		}

        /* wp function end here */
		echo json_encode(
							array(
								'status'	=> 1
							)
						); 

      
	}
	
	
	/* Logout function */
	function postLogout(){		
		$access_token = Request::header('Access-Token');
		$logout = DoctorLoginDetails::where('token', $access_token)->delete();
		/* wp function start here to check user exists or not*/
        wp_logout();
        /*wp function end here*/
		echo json_encode(
					array(
						'status'	=> 1
					)
				); 
		exit;
	}
	
	/*	User Profile Info */
	public function postUserProfile(){
		$data = Request::all();
		$access_token 	= Request::header('Access-Token');
		$user_dtls 		= UserLoginDetail::where('token',$access_token)->first();
		$user_id		= $user_dtls['user_id'];
		$user_type      = $data['usertype'];
		if($user_type == 'p'){
			$user_info 	= SiteUser::with('city_details')->where('id',$user_id)->first();
			echo json_encode(
					array(
						'status'	=> 1,
						'data' => $user_info
					)
				);
		}else if($user_type == 'd'){
			$all_active_category    = Category::select(['id','name as label'])->where('status','=',1)->get();
        	$all_active_lang = Language::select(['name as id','name as label'])->where('status','=',1)->get();
			$user_info 	= SiteUser::with('docs_qualification','docs_speciality','docs_research')->where('id',$user_id)->first();
			$doctor_category = DocCategory::select(['category_id as id'])->where('doctor_id','=',$user_id)->get();
			
			$docs_location = DocPractice::with('city_name')->where('doctor_id','=',$user_id)->get();
			$doc_practice = array();
			foreach ($docs_location as $practices) {
				if(!empty($practices->city_name)){
					$doc_practice[] = $practices->city_name->name;
				}
			}
			$all_city = City::select(['name'])->where('status','=',1)->get();
			$cityarr = array();
			foreach ($all_city as $city) {
				$cityarr[] = $city->name;
			}
			$doc_languages = explode(",", $user_info->languages);
			$doc_language_arr = array();
			foreach ($doc_languages as $lang) {
				if($lang != '') $doc_language_arr[]['id'] = $lang;
			}
			echo json_encode(
					array(
						'status'	=> 1,
						'data' => $user_info,
						'categories' => $all_active_category,
						'doc_category' => $doctor_category,
						'languages'  => $all_active_lang,
						'doc_languages' => $doc_language_arr,
						'city' => $cityarr,
						'doc_practice' => $doc_practice
					)
				);
		}
		
		exit;
	}
	
	
	
	
	/*	Checking Last service call time after login */
	public function getCheckLastServieCallTimeAfterLogin(){
		
		$access_token 			= Request::header('Access-Token');	
		$user_token_info 		= DoctorLoginDetails::where('token',$access_token)->first();
        $session_pass = Hash::make(Session::get('user_pass_for_chaking')); 
      
		$check_user = 0;
        //$check_pass = 0;
		if(count($user_token_info)>0){
			$check_user 			= Doctor::where('id',$user_token_info->doctor_id)->where('email_status',1)->count();
			//$check_pass = DoctorDetails::where('id',$user_token_info->doctor_id)->where('password',$session_pass)->count();
			
		}
		//echo $check_user; exit;
      
        

		if($check_user==0){
			echo json_encode(array('status'=>0));
		}
		else{
			$current_date_time		= date("Y-m-d H:i:s");
			$last_login_time		= $user_token_info['login_time'];
			$to_time 				= strtotime($last_login_time);
			$from_time 				= strtotime($current_date_time);
			$last_service_call_time = round(abs($to_time - $from_time) / 60,2);

			$user_type_info 		= $this->getUserDetail($access_token);
			
			if($last_service_call_time>floatval(15)){
				$logout = DoctorLoginDetails::where('token', $access_token)->delete();
				echo json_encode(array('status'=>0));
			}
			else{
				$user_pre_login_dtls= $user_token_info['login_time'];
				$user_token_info->update(array('login_time'    => date("Y-m-d H:i:s")));
				echo json_encode(array('status'=>1));
			}
		}
		
	}

	/*  Checking Last service call time after login */
    public function getCheckLastServieCallTimeAfterPatientLogin(){
        
        $access_token           = Request::header('Access-Token');  
        $user_token_info        = PatientAuthToken::where('auth_token',$access_token)->first();
      
        $check_user = 0;
        if(count($user_token_info)>0){
            $check_user             = Patient::where('id',$user_token_info->patient_id)->where('status',1)->count();
        }
        //echo $check_user; exit;
        if($check_user==0){
            echo json_encode(array('status'=>0));
        }
        else{
            $current_date_time      = date("Y-m-d H:i:s");
            $last_login_time        = $user_token_info['login_time'];
            $to_time                = strtotime($last_login_time);
            $from_time              = strtotime($current_date_time);
            $last_service_call_time = round(abs($to_time - $from_time) / 60,2);

            $user_type_info         = $this->getUserDetail($access_token);
            exit;
            if($last_service_call_time>floatval(15)){
                $logout = PatientAuthToken::where('auth_token', $access_token)->delete();
                echo json_encode(array('status'=>0));
            }
            else{
                $user_pre_login_dtls= $user_token_info['login_time'];
                $user_token_info->update(array('login_time'    => date("Y-m-d H:i:s")));
                echo json_encode(array('status'=>1));
            }
        }
        
    }
	
	
	/*	Updating user profile */
	public function postUpdateUserProfile(){
		
		$data				= Request::all();
		// echo "<pre>";
		// print_r($data);
		// exit;
		$access_token 		= Request::header('Access-Token');
		$user_dtls 			= UserLoginDetail::where('token',$access_token)->first();
		$user_id			= $user_dtls['user_id'];
		
		$user_info 	= SiteUser::where('id','!=',$user_id)->where('email',$data['email'])->first();
		if(count($user_info)>0){
			echo json_encode(array('status'=>0));
		}
		else{

			$city_details = City::select(['id'])->where('name','=',$data['city'])->first();

			$create_user		= SiteUser::where('id', $user_id)	
									  ->update([
										'name'          => $data['first_name'],
										'last_name'     => $data['last_name'],
										'email'       	=> $data['email'],
										'contact'     	=> $data['mobile'],
										'title'			=> $data['titlePtnt'],
										'gender'		=> $data['gender'],
										'age'			=> $data['age'],
										'city'			=> $city_details->id,
										'postcode'		=> $data['postcode'],
										'updated_at'	=> date('Y-m-d H:i:s')
									]);

			/************	Upload restaurant logo	**********/
			if (Input::hasFile('user_image'))
			{
				$obj 				= new helpers();
				$profile_image 		= Input::file('user_image');
				$destinationPath 	= 'uploads/profile_image/'; // upload path                
				$extension 			= $profile_image->getClientOriginalExtension(); // getting image extension                
				$profile_image_name = rand(111111111,999999999).'.'.$extension; // renameing image
				
				$profile_image->move($destinationPath, $profile_image_name);                    
				$rupload_usr_image	= SiteUser::where('id', $user_id)
											  ->update([
													'image'  => $profile_image_name
											]);

			}
			echo json_encode(array('status'=>1));
		}
		
			
	}

	

	public function postUpdateDoctorProfile(){
		$data	= Request::all();
		$access_token 	= Request::header('Access-Token');
		$user_dtls 		= UserLoginDetail::where('token',$access_token)->first();
		$user_id		= $user_dtls['user_id'];
		
		$user_info 	= SiteUser::where('id','!=',$user_id)->where('email',$data['email'])->first();
		if(count($user_info)>0){
			echo json_encode(array('status'=>0));
		}
		else{
		 $languages = "";
		 $language_arr = json_decode($data['language_model'],true);
		 if(!empty($language_arr)){
			 foreach ($language_arr as $lang) {
			 	$langarr[] = $lang['id'];
			 }
			$languages = implode(",", $langarr);
		}
		$update_user		= SiteUser::where('id', $user_id)
								->update([
									'title'         => $data['title'],
									'name'          => trim($data['name']),
									'last_name'     => trim($data['last_name']),
									'email'       	=> $data['email'],
									'contact'       => $data['contact'],
									'profile_title' => $data['profile_title'],
									'about'        => $data['about'],
									'languages'   => $languages,
									'registered_with' => $data['registered_with'],
								]);
		$educations = DocQualification::where('doctor_id','=',$user_id)->delete();
		$specialist = DocSpecialist::where('doctor_id','=',$user_id)->delete();
		$research = DocResearch::where('doctor_id','=',$user_id)->delete();
		$practice = DocPractice::where('doctor_id','=',$user_id)->delete();
		$category = DocCategory::where('doctor_id','=',$user_id)->delete();


		$data['doc_education'] = json_decode($data['edu'],true);
		if(!empty($data['doc_education'])){
			foreach ($data['doc_education'] as $education) {
				if($education['education'] != ''){
					DocQualification::create([
						'doctor_id' => $user_id,
						'education' => $education['education']
						]);
				}
			}
		}
		$data['doc_specialist'] = json_decode($data['spec'],true);
		if(!empty($data['doc_specialist'])){
			foreach ($data['doc_specialist'] as $specialist) {
				if($specialist['specialist'] != ''){
					DocSpecialist::create([
						'doctor_id' => $user_id,
						'specialist' => $specialist['specialist']
						]);
				}
			}
		}

		$data['doc_resrch'] = json_decode($data['resrch'],true);
		if(!empty($data['doc_resrch'])){
			foreach ($data['doc_resrch'] as $research) {
				if($research['research'] != ''){
					DocResearch::create([
						'doctor_id' => $user_id,
						'research' => $research['research']
						]);
				}
			}
		}
		$data['category_model'] = json_decode($data['category_model'],true);
		if(!empty($data['category_model'])){
			foreach ($data['category_model'] as $category) {
				if($category['id'] != ''){
					DocCategory::create([
						'doctor_id' => $user_id,
						'category_id' => $category['id']
						]);
				}
			}
		}
		$data['location'] = json_decode($data['location'],true);
		if(!empty($data['location'])){
			foreach ($data['location'] as $practices_city) {
				$fetch_city = City::where('name','=',$practices_city)->first();
				$city_id = $fetch_city->id;
				$state_id = $fetch_city->state_id;
				$country_id = $fetch_city->country_id;
				DocPractice::create([
					'doctor_id' => $user_id,
					'city_id' => $city_id,
					'state_id' => $state_id,
					'country_id'=>$country_id
					]);
			}
		}
			/************	Upload restaurant logo	**********/
			if (Input::hasFile('user_image') && $data['user_image_exist'] == 1)
			{
				$profile_image 		= Input::file('user_image');
				$destinationPath 	= 'uploads/profile_image/'; // upload path                
				$extension 			= $profile_image->getClientOriginalExtension(); // getting image extension                
				$profile_image_name = rand(111111111,999999999).'.'.$extension; // renameing image
				
				$profile_image->move($destinationPath, $profile_image_name);                    
				$rupload_usr_image	= SiteUser::where('id', $user_id)
											  ->update([
													'image'  => $profile_image_name
											]);

		 }else if($data['user_image_exist'] == 0){
		 	$old_image_data = SiteUser::where('id', $user_id)->first();
		 	$old_image = $old_image_data->image;
		 	if($old_image != ''){
		 		unlink('uploads/profile_image/'.$old_image);
		 	}
		 	$rupload_usr_image	= SiteUser::where('id', $user_id)
											  ->update([
													'image'  => ''
											]);
		 }
			echo json_encode(array('status'=>1));
		}

	}
	
	/*	Save Card in Stripe and add card-id in DB */
	public function postCustSaveCard(){

		// Get Access token
		$access_token = Request::header('Access-Token');	
		
		// Get Stripe token
		$stripe_token = Request::input('stripe_token');

		//Stripe::setApiKey("sk_test_O8Q98yrLmapZDzheBUYMVouj");
		Stripe::setApiKey($this->fetchStripSecretAPI());

		try
      	{
			// Get Logged user id
			$user_id = $this->getUserId($access_token);
			
			// check if the user has atleast one card saved
			$user_card_query = SiteUserCard::select('cust_id','card_name')->where('site_user_id',$user_id);
			$save_card_cnt = $user_card_query->count();
			
			if($save_card_cnt==0){
				$primary_card = 1;
				
				// Create Customer and get details for that customer
				$customer = Customer::create(array(
					'card'  => $stripe_token
				));
				
				$customer_id 	= $customer->id;
				$card_id 		= $customer->default_source;
				$card_type 		= $customer->sources->data[0]->brand;
				$last4 			= $customer->sources->data[0]->last4;
				$exp_month 		= $customer->sources->data[0]->exp_month;
				$card_name 		= $customer->sources->data[0]->name;
				$exp_year 		= $customer->sources->data[0]->exp_year;
				
			}
			else{
				$primary_card = 0;
				
				// fetch customer id
				$user_card_details = $user_card_query->first()->toArray();
				
				try
      			{

					$customer = \Stripe\Customer::retrieve($user_card_details['cust_id']);
					$stripe_card_details = $customer->sources->create(array("source" => $stripe_token));
					//print_r($stripe_card_details->name);exit;
					
					$customer_id 	= $user_card_details['cust_id'];
					$card_id 		= $stripe_card_details->id;
					$card_type 		= $stripe_card_details->brand;
					$last4 			= $stripe_card_details->last4;
					$exp_month 		= $stripe_card_details->exp_month;
					$card_name 		= $stripe_card_details->name;
					$exp_year 		= $stripe_card_details->exp_year;
				}
				catch (\Stripe\Error\InvalidRequest $e) 
				{
				// Invalid parameters were supplied to Stripe's API
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				} 
				catch (\Stripe\Error\Authentication $e) 
				{
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				} 
				catch (\Stripe\Error\ApiConnection $e) 
				{
				// Network communication with Stripe failed
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				} 
				catch (\Stripe\Error\Base $e)
				{
				// Display a very generic error to the user, and maybe send yourself an email
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				} 
				catch (\Stripe\Error\Api $e) 
				{
				// Stripe's servers are down!
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				}
				catch (\Stripe\Error\Card $e)
				{
				// Card Was declined
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				} 
				catch (Exception $e) 
				{
				$error = $e->getMessage();
				return response()->json(['status'=>0,'msg' => $error]); 
				}
				
			}


			
			//print_r($customer);exit;

			
			// Add card details 
			SiteUserCard::create([
									'site_user_id'  => $user_id,
									'cust_id'       => $customer_id,
									'card_id'       => $card_id,
									'primary_card'  => $primary_card,
									'card_type'     => $card_type,
									'last4'      	=> $last4,
									'exp_month'     => $exp_month,
									'card_name'     => $card_name,
									'exp_year'		=> $exp_year,
									'post_date'		=> date('Y-m-d H:i:s')
								]);
			
			echo json_encode(array('status'=>1,'msg'=>'Your card is saved sucessfully'));

		}
		catch (\Stripe\Error\InvalidRequest $e) 
		{
		// Invalid parameters were supplied to Stripe's API
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		} 
		catch (\Stripe\Error\Authentication $e) 
		{
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		} 
		catch (\Stripe\Error\ApiConnection $e) 
		{
		// Network communication with Stripe failed
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		} 
		catch (\Stripe\Error\Base $e)
		{
		// Display a very generic error to the user, and maybe send yourself an email
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		} 
		catch (\Stripe\Error\Api $e) 
		{
		// Stripe's servers are down!
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		}
		catch (\Stripe\Error\Card $e)
		{
		// Card Was declined
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		} 
		catch (Exception $e) 
		{
		$error = $e->getMessage();
		return response()->json(['status'=>0,'msg' => $error]); 
		}


	}
	
	


	/*	Get All Cards details */
	public function postCardDetails(){
		
		$access_token = Request::header('Access-Token');
		
		// Get Logged user id
		$user_id = $this->getUserId($access_token);

		// Get cards details
		$card_dtls = SiteUserCard::where('site_user_id',$user_id)->orderBy('primary_card', 'desc')->orderBy('post_date', 'desc')->get();
		$card_dtls->toArray();
		
		echo json_encode(array('status'=>1,'data'=>$card_dtls,'cnt'=>count($card_dtls)));
		
	}
	
	/* Delete card */
	public function postDeleteCard(){
		try
      	{
			//Stripe::setApiKey("sk_test_O8Q98yrLmapZDzheBUYMVouj");
			Stripe::setApiKey($this->fetchStripSecretAPI());
			
			$id = Request::input('id');
			$user_card_dtls = SiteUserCard::select('cust_id','card_id')->where('id',$id)->first()->toArray();
			
			// Delete Card from stripe
			try {
				  // Use a Stripe PHP library method that may throw an exception....
				$customer = \Stripe\Customer::retrieve($user_card_dtls['cust_id']);
				$customer->sources->retrieve($user_card_dtls['card_id'])->delete();
				
				// Delete Card from DB
				SiteUserCard::where('id', $id)->delete();
				
				return response()->json(['status'=>1,'msg' => 'Your Card is deleted successfully']);

			} 
			catch (\Stripe\Error\InvalidRequest $e) 
			{
			// Invalid parameters were supplied to Stripe's API
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Authentication $e) 
			{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\ApiConnection $e) 
			{
			// Network communication with Stripe failed
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Base $e)
			{
			// Display a very generic error to the user, and maybe send yourself an email
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Api $e) 
			{
			// Stripe's servers are down!
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			}
			catch (\Stripe\Error\Card $e)
			{
			// Card Was declined
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (Exception $e) 
			{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			}
		}
		catch (Exception $e) 
		{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
		}
		
	}
	
	/* Update primary card */
	public function postUpdatePrimaryCard(){
		try
      	{
			//Stripe::setApiKey("sk_test_O8Q98yrLmapZDzheBUYMVouj");
			Stripe::setApiKey($this->fetchStripSecretAPI());

			$access_token = Request::header('Access-Token');
			// Get Logged user id
			$user_id = $this->getUserId($access_token);

			$id = Request::input('id');
			$user_card_dtls = SiteUserCard::select('cust_id','card_id')->where('id',$id)->first()->toArray();
			
			try {
				// Update primary Card in stripe
				$customer = \Stripe\Customer::retrieve($user_card_dtls['cust_id']);
				$customer->default_source=$user_card_dtls['card_id'];
				$customer->save(); 
				
				// Update primary Card in DB			
				SiteUserCard::where('site_user_id', $user_id)->update(array('primary_card' => 0));		// Update all primary card to 0 for logged in user
				
				SiteUserCard::where('id', $id)->update(array('primary_card' => 1));
				
				return response()->json(['status'=>1,'msg' => 'Primary card has been changed successfully']);
			}
			catch (\Stripe\Error\InvalidRequest $e) 
			{
			// Invalid parameters were supplied to Stripe's API
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Authentication $e) 
			{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\ApiConnection $e) 
			{
			// Network communication with Stripe failed
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Base $e)
			{
			// Display a very generic error to the user, and maybe send yourself an email
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (\Stripe\Error\Api $e) 
			{
			// Stripe's servers are down!
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			}
			catch (\Stripe\Error\Card $e)
			{
			// Card Was declined
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			} 
			catch (Exception $e) 
			{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
			}
			
		}
		catch (Exception $e) 
		{
			$error = $e->getMessage();
			return response()->json(['status'=>0,'msg' => $error]); 
		}
		
	}
	
	
	
	public function getDeafult(){
		Stripe::setApiKey("sk_test_O8Q98yrLmapZDzheBUYMVouj");
		
		$customer = \Stripe\Customer::retrieve("cus_9VajIBSi36Iq22");
		$customer->default_source='card_19Cly227lWkgHMvKbE6Gz69F';
		$customer->save(); 

		
		
	}

	public function getStripePublickey(){
		
		return response()->json(['status'=>1,'data' => $this->fetchStripPublicAPI()]);
	}
}
