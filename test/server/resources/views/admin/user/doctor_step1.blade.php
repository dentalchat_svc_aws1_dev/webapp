@extends('admin/layout/admin_template')

@section('content')
	<script>
		$(document).ready(function(){
			var _URL = window.URL || window.webkitURL;
			$("#profile_img").change(function (e) {

				$('#img_msg').html('');

			    var file, img;
			    var allowed_coverimage_width = parseInt(300);
				var allowed_coverimage_height = parseInt(300);
				var file, img;
			    if ((file = this.files[0])) {
			        img = new Image();
			        img.onload = function () {
			            var wid = this.width;
			            var ht = this.height;

			            if(wid < allowed_coverimage_width)
						{
							$('#img_msg').html("Profile Image width should be minimum "+allowed_coverimage_width+" px");
							$('#blah').attr('src', '');
							$('#profile_img').val('');
							$('#subbtn').attr('type','button');
							return false;

						}else{
							$('#subbtn').attr('type','submit');
						}
						if(ht < allowed_coverimage_height)
						{
							$('#img_msg').html("Profile Image height should be minimum "+allowed_coverimage_height+" px");
							$('#blah').attr('src', '');
							$('#profile_img').val('');
							$('#subbtn').attr('type','button');
							return false;
						}else{
							$('#subbtn').attr('type','submit');
						}
						
			        };

			        img.src = _URL.createObjectURL(file);
			    }
			});
		});
	</script>
	{!! Form::open(['url' => 'admin/dentist/dentist-step1/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step1','onsubmit'=>'userJs.checkUserEmail()']) !!}
				
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}
		{!! Form::hidden('hid_frm_submit_res',1,['class'=>'span8','id'=>'hid_frm_submit_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}

		
		<div class="control-group" style="display:block;">
		
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">First Name *</label>
			<div class="controls">
				{!! Form::text('first_name',(count($doc_details)>0)?$doc_details->docs_details['first_name']:'',['class'=>'span8','id'=>'first_name']) !!}
			</div>
		</div>
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Last Name *</label>
			<div class="controls">
				{!! Form::text('last_name',(count($doc_details)>0)?$doc_details->docs_details['last_name']:'',['class'=>'span8','id'=>'last_name']) !!}
			</div>
		</div>
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Date Of Birth</label>
				
			<div class="controls">
			
			{!! Form::text('date_of_birth',(count($doc_details)>0)?$doc_details->docs_details['date_of_birth']:'',['class'=>'span8','id'=>'date_of_birth']) !!}
			</div>
				
		</div>
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Gender</label>
				
			<div class="controls">
			<?php 
			if(count($doc_details)>0)
				$gender = $doc_details->docs_details['gender'];
			else
				$gender='';
			?>
				{!! Form::select('gender', array(''=>'Gender','1' => 'Male', '2' => 'Female'),$gender, array('class' => 'span3')) !!}
			</div>
				
		</div>
		
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Email *</label>				
			<div class="controls">
				{!! Form::email('email',(count($doc_details)>0)?$doc_details->docs_details['email']:'',['class'=>'span8','id'=>'email',(count($doc_details)>0)?'disabled':'']) !!}
				<div style="color:#F00;font-size:13px;clear:both;display:none;" id="user_email_msg"></div>
			</div>				
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Country Code *</label>
			<div class="controls">
				<input type="text" name="country_code" id="country_code" class="span8"  value="<?php echo (count($doc_details)>0)?$doc_details->docs_details['conuntry_code']:'' ?>">
			</div>
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Contact Number *</label>
			<div class="controls">
				<input type="text" name="contact_number" id="contact_number" class="span8"  value="<?php echo (count($doc_details)>0)?$doc_details->docs_details['contact_number']:'' ?>">
			</div>
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Password *</label>
				
			<div class="controls">
				<input type="password" name="password" id="password" class="span8" onblur="userJs.rulesWhileEditingUser();" value="<?php echo (count($doc_details)>0 && $doc_details->docs_details['password']!='')?'password':'' ?>" <?php echo (count($doc_details)>0 && $doc_details->docs_details['password']!='')?'disabled':'' ?> required="required"/>
			</div>
				
		</div>
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Image</label>
				<div class="controls">
				<div class="file-upload upload-btn">
				<div class="upload">Upload</div>
				<input type="file" id="profile_img" name="profile_img"/>
				</div>
				<span id="img_msg" style="color:#F00;font-size:12px;clear:both;"></span>
			</div>
			<?php if(count($doc_details)>0 && $doc_details->docs_details['profile_pics']!=''){?>
				<p class="new_avatar" style="padding-left: 180px;padding-top: 10px;"><img src="<?php echo url();?>/uploads/dentist_profile_image/{{ $doc_details->docs_details['profile_pics'] }}" class="nav-avatar"></p>
			<?php } ?>
		</div>	

		
			
		<div class="control-group">
		
			<div class="controls">
				<a href="{!! url('admin/dentist/list')!!}" class="btn">Back</a>
				{!! Form::submit('Next',array('class'=>'btn','name'=>'action','value'=>'Next','id'=>'subbtn')) !!}				
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}

<script type="text/javascript">  
    $(document).ready(function(){
    	/****** Fure date restriction *****/
       var FromEndDate = new Date();
	   $('#date_of_birth').datepicker({
	      endDate: FromEndDate, 
	      autoclose: true
	    });
	   /****** Fure date restriction *****/
    	dobpicker('#date_of_birth')
    })
     function dobpicker(selector){
     	$(selector).datepicker({  	        
	       format: 'mm-dd-yyyy'
	     });     	
     }  
</script>  


	
@stop