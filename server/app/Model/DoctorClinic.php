<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorClinic extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_clinics';

     public function docDetails(){
    	return $this->belongsTo('App\Model\DoctorDetails', 'doctor_id','id');
    }
}

