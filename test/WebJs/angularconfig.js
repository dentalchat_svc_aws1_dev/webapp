/*angular.module('app.config', []).constant('env', 'development_admin')
    .value('config', {

        development_admin: {
            frontApiURL: window.location.origin + '/HTML-DES/angular_app', // Set your angular base path here for front End
            backApiURL: window.location.origin + '/HTML-DES/angular_app', // Set your angular base path here for admin end
            partnerApiURL: window.location.origin + '/HTML-DES/angular_app', // Set your angular base path here for partner end
            siteurl: '',
            mobileWeburl: '',
            serverURL: window.location.origin + "HTML-DES/angular_app/", // this is the api base url
            imageURL: window.location.origin + 'HTML-DES/angular_app/server/uploads/'
        }
    });
*/

angular.module('app.config',[]).constant('env','live')
.value('config', {

	dev: {
	    apiURL:  'https://test.dentalchat.com/server/' ,// Set your base path here
	    siteurl:  '',
	    mobileWeburl : ''
	},

	staging: {
	    apiURL:  'https://test.dentalchat.com/server/' ,// Set your base path here
	    siteurl:  '',
	    mobileWeburl : '',
	    OauthUrl : 'https://epic-staging.easyparksystem.net/epic-rest/oauth/token',
	    username: 'nevaventures',
	    password: 'm3AeSvqdF9xa',
	},

	development: {
	    apiURL:  'https://test.dentalchat.com/server/',		// Set your base path here
	    siteurl:  'https://test.dentalchat.com/server/',
	    mobileWeburl : '',
	    OauthUrl : 'https://epic-staging.easyparksystem.net/epic-rest/oauth/token',
	    username: 'nevaventures',
	    password: 'm3AeSvqdF9xa',
	},

	live: {
	    apiURL:  'https://test.dentalchat.com/server/',		// Set your base path here
	    siteurl:  'https://test.dentalchat.com/',
	    mobileWeburl : '',
	    OauthUrl : 'https://epic-staging.easyparksystem.net/epic-rest/oauth/token',
	    username: 'nevaventures',
	    password: 'm3AeSvqdF9xa',
	}
});


