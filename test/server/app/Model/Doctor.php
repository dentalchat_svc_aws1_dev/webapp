<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctors';
    
    public function docs_details(){
    	return $this->hasOne('App\Model\DoctorDetails', 'doctor_id','id');
    }

    public function docs_clinics(){
        return $this->hasOne('App\Model\DoctorClinic', 'doctor_id','id');
    }

     public function docs_education(){
    	return $this->hasMany('App\Model\DoctorEducation', 'doctor_id','id');
    }
    public function docs_award(){
    	return $this->hasMany('App\Model\DoctorAward', 'doctor_id','id');
    }
    public function docs_insurance(){
    	return $this->hasMany('App\Model\DoctorInsurance', 'doctor_id','id');
    }
    public function docs_experiece(){
    	return $this->hasMany('App\Model\DoctorExperience', 'doctor_id','id');

    }
    public function docs_license(){
        return $this->hasMany('App\Model\DoctorLicense', 'doctor_id','id');

    }
    public function docs_skill(){
        return $this->hasMany('App\Model\DoctorSkill', 'doctor_id','id');

    }
    public function docs_language(){
        return $this->hasOne('App\Model\DoctorLanguage', 'doctor_id','id');

    }

/**************** TEMPORARY DENTIST PROFILE START ***************************/

    public function tmp_docs_details(){
        return $this->hasOne('App\Model\TmpDoctorDetails', 'doctor_id','id');
    }

    public function tmp_docs_clinics(){
        return $this->hasOne('App\Model\TmpDoctorClinic', 'doctor_id','id');
    }

     public function tmp_docs_education(){
        return $this->hasMany('App\Model\TmpDoctorEducation', 'doctor_id','id');
    }
    public function tmp_docs_award(){
        return $this->hasMany('App\Model\TmpDoctorAward', 'doctor_id','id');
    }
    public function tmp_docs_insurance(){
        return $this->hasMany('App\Model\TmpDoctorInsurance', 'doctor_id','id');
    }
    public function tmp_docs_experiece(){
        return $this->hasMany('App\Model\TmpDoctorExperience', 'doctor_id','id');

    }
    public function tmp_docs_license(){
        return $this->hasMany('App\Model\TmpDoctorLicense', 'doctor_id','id');

    }
    public function tmp_docs_skill(){
        return $this->hasMany('App\Model\TmpDoctorSkill', 'doctor_id','id');

    }
    public function tmp_docs_language(){
        return $this->hasOne('App\Model\TmpDoctorLanguage', 'doctor_id','id');

    }
    public function review_rating(){
        return $this->hasMany('App\Model\DoctorReviewRating', 'doctor_id','id');

    }

}
