<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorReviewRating extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_review_ratings';

    public function get_patient_details()
	{
	    return $this->belongsTo('App\Model\Patient', 'patient_id');
	}
     
}

