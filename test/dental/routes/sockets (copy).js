var Chat = require('../models/chat');
var lookups=[];

var Setlookup = function(storable,check){
	var returnval={};
	returnval.status=0;
	returnval.buddy_socket_id='';
			for(var d in lookups)
			{
				//---checking the global object if this connection exists or not----//////
				//----then just update self socket id-------------///////////////////
				if(lookups[d].from_username==storable.from_username&&lookups[d].to_username==storable.to_username)
				{
					if(check == 'push'){
						returnval.status=1;
						lookups[d].socket_id=storable.socket_id;
						break;
					}
					if(check == 'get'){
						returnval.status=1;
						returnval.buddy_socket_id=lookups[d].socket_id;
						break;
					}
				}
			}	
	return 	returnval; 	
}


exports.initialize = function(server) {
	var io = require('socket.io')(server);
    ///////////////////------start Of connection-------//////////////////  
    io.on("connection", function(socket){ 
	    ///////////////////------start SOcket On set name-------//////////////////
	    socket.on("set_name", function(data){
	    	socket.user = data.name;
	    	socket.receiver = data.buddy;
			socket.emit('name_set', data);
			socket.send(JSON.stringify({type:'serverMessage',
			message: 'Welcome to the most interesting chat room on earth!'}));
			//console.log(socket);
			//---storing self data in a object--////
			var storable={};
			storable.from_username=socket.user;
			storable.to_username=socket.receiver;
			storable.socket_id=socket.id;

			console.log(JSON.stringify(storable));
			//-----setting the lookup in Setlookup function above--------/////////////
			
			var check = "push";
			var status = Setlookup(storable,check);
			//--------else store the full data of connection in global object--------///////////
			if(!status.status)
				lookups.push(storable);
			console.log(lookups);
		});
		///////////////////------end SOcket On set name-------//////////////////
		socket.on('create', function(room) {
    		socket.join(room);
    		socket.room = room;
    	});
		///////////////////------start of SOcket On send message-------//////////////////
	    socket.on('message', function(message){
			message= JSON.parse(message);
			if(message.type == "userMessage"){
				Chat.create({
	                from: socket.user,
	                to : socket.receiver,
	                message:message.message

	            }, function(err, chat) {
				    if (err) {
				      console.log(err);
				    }
			    });
			//-----setting the clients data---to know clients socket id-----///    
			var storable={};
			storable.from_username=socket.receiver;
			storable.to_username=socket.user;
			var check = "get";
			var status = Setlookup(storable,check);

				message.username=socket.user;
				//socket.broadcast.to('room1').emit('message', JSON.stringify(message));
				//io.sockets.connected['room1'].emit('message', JSON.stringify(message));
				//io.to('room1').emit('message', JSON.stringify(message));	
				io.sockets.in('room1').emit('message',JSON.stringify(message));
				if (io.sockets.connected[status.buddy_socket_id]&&status.status==1) {
				    io.sockets.connected[status.buddy_socket_id].emit('message', JSON.stringify(message));
				}
					message.type = "myMessage";
					io.sockets.connected[socket.id].emit('message', JSON.stringify(message));
			}
		});
		///////////////////------end SOcket On send message-------//////////////////
		 
		
		});
        ///////////////////------end Of connection-------//////////////////  
            server.listen(8000, function(){
          console.log('listening on *:8000');
});


	};
