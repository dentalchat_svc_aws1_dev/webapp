@extends('admin/layout/admin_template')

@section('content')
	{!! Form::open(['url' => 'admin/dentist/dentist-step3/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step3']) !!}
		
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}
		{!! Form::hidden('data_count',count($doc_education),['class'=>'span8','id'=>'data_count']) !!}

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCECNx6YKAjaYfP9Eq7FXAMB1QmjUKvMZk&libraries=places"></script>
		
		<?php 
		$i =0;
		if(!empty($doc_education)){
			foreach($doc_education as $eachdoc){	
		?>
		<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
        
        <?php if($i>0){?>
        <div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeEducation(<?php echo $i;?>,<?php echo $eachdoc['id'];?>);">Remove</a></div>
        <?php } ?>
					
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Degree </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][degree]" class="span8" id="degree_<?php echo $i;?>" value="{{$eachdoc['degree']}}">
				</div>
			</div>

			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Year Completed </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][completed_year]" class="span8 year_completed" id="completed_year_<?php echo $i;?>" value="{{$eachdoc['completed_year']}}">
				</div>
			</div>

			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">School Name </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][medical_school]" class="span8" id="medical_school_<?php echo $i;?>" value="{{$eachdoc['medical_school']}}" />
				</div>
			</div>
			
			
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">School Location </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][location]" class="span8 sc_loc" id="location_<?php echo $i;?>" value="{{$eachdoc['location']}}">
				</div>
			</div>	

			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Upload Certificate </label>
				<div class="controls">
					<input type="file" id="certificate_<?php echo $i;?>" name="data[{{$i}}][certificate]"/>
					<input type="hidden" id="hid_certificate" name="data[{{$i}}][hid_certificate]" value="{{$eachdoc['certificate']}}"/>
					<input type="hidden" id="hid_certificate_name" name="data[{{$i}}][hid_certificate_name]" value="{{$eachdoc['org_certificate']}}"/>
				</div>	
                <?php if($eachdoc['certificate']!=''){?>			
					<p class="new_avatar" style="padding-left: 180px;padding-top: 10px;"><a href="<?php echo url();?>/uploads/certificate/{{$eachdoc['certificate']}}"  target="_blank">{{$eachdoc['org_certificate']}}</a></p>
                    <!--<img src="<?php //echo url();?>/uploads/certificate/{{$eachdoc['certificate']}}" class="nav-avatar"> -->	
                    <?php } ?>			
			</div>			
		</div>	
		<?php 
		$i++;
			} 
		}
		else
		{
		?>
			<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
						
				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Degree </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][degree]" class="span8" id="degree_<?php echo $i;?>">
					</div>
				</div>

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Year Completed </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][completed_year]" class="span8 year_completed" id="completed_year_<?php echo $i;?>">
					</div>
				</div>

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">School Name </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][medical_school]" class="span8" id="medical_school_<?php echo $i;?>" />
					</div>
				</div>
			
				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">School Location </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][location]" class="span8 sc_loc" id="location_<?php echo $i;?>">
					</div>
				</div>	

				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Certificate </label>
					<div class="controls">
						<input type="file" id="certificate" name="data[{{$i}}][certificate]"/>
						<input type="hidden" id="hid_certificate" name="data[{{$i}}][hid_certificate]"/>
						<input type="hidden" id="hid_certificate_name" name="data[{{$i}}][hid_certificate_name]"/>					
					</div>				
				</div>			
			</div>
		<?php
		}
		?>

		<input type="hidden" name="rowcount" id="rowcount" value="<?php echo $doc_education_count;?>">
		<input type="hidden" name="rowcount_award" id="rowcount_award" value="<?php echo $doc_award_count;?>">
		<input type="hidden" name="rowcount_insurance" id="rowcount_insurance" value="<?php echo $doc_insurance_count;?>">

		<input type="button" onclick="addEducation();" class="btn" name="add_education" id="add_education" value="Add More">

		<!-- Add Award Section Start -->
		<div><hr for="basicinput">Award & Publication </hr></div>
		<?php 
		$j =0;		
		if(!empty($doc_award)){

			foreach($doc_award as $eachaward){	
		?>
		<div class="control-group" style="display:block;" id="award_div_id_<?php echo $j;?>">
        
        <?php if($j>0){?>
        <div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeAward(<?php echo $j;?>,<?php echo $eachaward['id'];?>);">Remove</a></div>
        <?php } ?>
					
			<div class="control-group" style="display:block;">				
				<div class="controls">
					<input type="text" name="data_award[{{$j}}][award]" class="span8" id="degree_<?php echo $j;?>" value="{{$eachaward['award']}}">
				</div>
			</div>
		</div>
		<?php  
		$j++;
			}
		}
		else
		{
		?>
			<div class="control-group" style="display:block;" id="award_div_id_<?php echo $j;?>">					
				<div class="control-group" style="display:block;">				
					<div class="controls">
						<input type="text" name="data_award[{{$j}}][award]" class="span8" id="award_<?php echo $j;?>">
					</div>
				</div>
			</div>
		<?php 
		} 
		?>
		
		<input type="button" onclick="addAward();" class="btn" name="add_award" id="add_award" value="Add More">

		<!-- Add Award Section End -->
		

	<!-- Add Insurance Section Start -->
		<div><hr for="basicinput">In Network Insurance </hr></div>
		<?php 
		$k =0;		
		if(!empty($doc_insurance)){

			foreach($doc_insurance as $eachinsurance){	
		?>
		<div class="control-group" style="display:block;" id="insurance_div_id_<?php echo $k;?>">
        
        <?php if($k>0){?>
        <div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeInsurance(<?php echo $k;?>,<?php echo $eachinsurance['id'] ?>);">Remove</a></div>
        <?php } ?>
					
			<div class="control-group" style="display:block;">				
				<div class="controls">
					<input type="text" name="data_insurance[{{$k}}][insurance]" class="span8" id="insurance_<?php echo $k;?>" value="{{$eachinsurance['insurance']}}">
				</div>
			</div>
		</div>
		<?php  
		$k++;
			}
		}
		else
		{
		?>
			<div class="control-group" style="display:block;" id="insurance_div_id_<?php echo $k;?>">					
				<div class="control-group" style="display:block;">				
					<div class="controls">
						<input type="text" name="data_insurance[{{$k}}][insurance]" class="span8" id="insurance_<?php echo $k;?>">
					</div>
				</div>
			</div>
		<?php 
		} 
		?>
		
		<input type="button" onclick="addInsurance();" class="btn" name="add_insurance" id="add_insurance" value="Add More">

	<!-- Add Insurance Section End -->
		
			
		<div class="control-group">		
			<div class="controls">
				<a href="{!! url('admin/dentist/dentist-step2/'.$user_id)!!}" class="btn">Prev</a>
				{!! Form::submit('Next',array('class'=>'btn','name'=>'action','value'=>'Next')) !!}				
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}

<script>
	
	function addEducation(){
		var rowcountval = $("#rowcount").val();
		var rowcount =  parseInt(rowcountval)+1;
		$("#rowcount").val(rowcount);
		var addonhtml = '';
		
		var html = '<div class="control-group" style="display:block;" id="div_id_'+rowcount+'"><div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeEducation('+rowcount+');">Remove</a></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Degree </label><div class="controls"><input type="text" name="data['+rowcount+'][degree]" class="span8" id="degree_'+rowcount+'"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Year Completed </label><div class="controls"><input type="text" name="data['+rowcount+'][completed_year]" id="completed_year_'+rowcount+'" class="span8"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">School Name </label><div class="controls"><input type="text" name="data['+rowcount+'][medical_school]" class="span8" id="medical_school_'+rowcount+'" /></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">School Location </label><div class="controls"><input type="text" name="data['+rowcount+'][location]" class="span8" id="location_'+rowcount+'"></div></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Certificate</label><div class="controls"><input type="file" id="certificate_'+rowcount+'" name="data['+rowcount+'][certificate]"/><input type="hidden" id="hid_certificate" name="data['+rowcount+'][hid_certificate]"/><input type="hidden" id="hid_certificate_name" name="data['+rowcount+'][hid_certificate_name]"/></div></div></div>';
		$("#add_education").before(html);
		initdatepicker('#'+'completed_year_'+rowcount);
		initialize('location_'+rowcount);
		$("input[name='data["+rowcount+"][certificate]']").rules("add", { extension: "png|pdf|jpg|jpeg" });
	}

	function addAward(){
		var rowcountval_award = $("#rowcount_award").val();
		var rowcount_award =  parseInt(rowcountval_award)+1;
		$("#rowcount_award").val(rowcount_award);
		var addonhtml = '';
		
		var html = '<div class="control-group" style="display:block;" id="award_div_id_'+rowcount_award+'"><div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeAward('+rowcount_award+');">Remove</a></div><div class="controls"><input type="text" name="data_award['+rowcount_award+'][award]" class="span8" id="award_'+rowcount_award+'"></div></div></div>';
		$("#add_award").before(html);
	}

	function addInsurance(){
		var rowcountval_insurance = $("#rowcount_insurance").val();
		var rowcount_insurance =  parseInt(rowcountval_insurance)+1;
		$("#rowcount_insurance").val(rowcount_insurance);
		var addonhtml = '';
		
		var html = '<div class="control-group" style="display:block;" id="insurance_div_id_'+rowcount_insurance+'"><div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeInsurance('+rowcount_insurance+');">Remove</a></div><div class="controls"><input type="text" name="data_insurance['+rowcount_insurance+'][insurance]" class="span8" id="insurance_'+rowcount_insurance+'"></div></div></div>';
		$("#add_insurance").before(html);
	}

	function removeEducation(row,licenseitemid)
	{
      	var response = confirm('Do you really want to remove this Item?');
		if(response==1)
		{
	      	if(licenseitemid){
	      		$("#div_id_"+row).remove();	   
	      		$.ajax({
				  url: site_url+"/admin/item/remove-item",
				  type: 'POST',
				  data: {'itemid':licenseitemid,'model':'DoctorEducation'},
				  success: function(html){
				  }
				});

	    	}
		    else
		    {
		      	$("#div_id_"+row).remove();	   
		    }	      	
        }

    }

    function removeAward(row,licenseitemid){
      	var response = confirm('Do you really want to remove this Item?');
		if(response==1)
		{
	      	if(licenseitemid){
	      		$("#award_div_id_"+row).remove();	   
	      		$.ajax({
				  url: site_url+"/admin/item/remove-item",
				  type: 'POST',
				  data: {'itemid':licenseitemid,'model':'DoctorAward'},
				  success: function(html){
				  }
				});
	    }
		    else
		    {
		      	$("#award_div_id_"+row).remove();	   
		    }	      	
        }
      }

    function removeInsurance(row,licenseitemid)
      {
      	var response = confirm('Do you really want to remove this Item?');
		if(response==1)
		{
	      	if(licenseitemid){
	      		$("#insurance_div_id_"+row).remove();	   
	      		$.ajax({
				  url: site_url+"/admin/item/remove-item",
				  type: 'POST',
				  data: {'itemid':licenseitemid,'model':'DoctorInsurance'},
				  success: function(html){
				  }
				});
		    }
		    else
		    {
		      	$("#insurance_div_id_"+row).remove();	   
		    }	      	
        }
      }
      
</script>

<script type="text/javascript">  
    $(document).ready(function(){
    	initdatepicker('.year_completed');

    	var data_count = $('#data_count').val(); // Already saved data count

    	for(var i=0;i<=data_count;i++)   // For Add More location auto complete
    	{
	    	var id = 'location_'+i;
	    	initialize(id);
	    }
		
		var rowcount = $("#rowcount").val();
		for(i=0;i<rowcount;i++)
		{
			$("input[name='data["+i+"][certificate]']").rules("add", { extension: "xls|csv|png|pdf|jpg|jpeg|doc|docx|bmp|odt" });
			console.log("input[name='data["+i+"][certificate]']");
		}
      	
		
    })
	
	
     function initdatepicker(selector){
     	$(selector).datepicker({  
	        minViewMode: 2,
	       format: 'yyyy'  
	     });     	
    }  

    /******** Google Auto complete Address Start **********/
	
    function initialize(selector) {
        var input = document.getElementById(selector);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            
        });

    }

    /******** Google Auto complete Address Start **********/
</script>  

@stop