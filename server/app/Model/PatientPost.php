<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PatientPost extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'patient_posts';

    public function get_attachments()
    {
        return $this->hasMany('App\Model\PatientPostAttachment','patient_post_id');
    }

    public function get_patient()
    {
        return $this->belongsTo('App\Model\Patient','patient_id');
    }

    public function chat_details()
    {
        return $this->hasMany('App\Model\ChatHistory','post_id');
    }
}
