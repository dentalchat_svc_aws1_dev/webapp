@extends('admin/layout/admin_template')

@section('content')
	{!! Form::open(['url' => 'admin/dentist/dentist-step6/'.$user_id,'method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'doc_step6']) !!}
		
		{!! Form::hidden('hid_validate_res',1,['class'=>'span8','id'=>'hid_validate_res']) !!}		
		{!! Form::hidden('hid_user_id',$user_id,['class'=>'span8','id'=>'hid_user_id']) !!}

		
		<?php 
		$i =0;
		if(!empty($doc_skill)){
			foreach($doc_skill as $eachdoc){	
		?>
		<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
        
        <?php if($i>0){?>
        <div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeSkill(<?php echo $i;?>,<?php echo $eachdoc['id'];?>);">Remove</a></div>
        <?php } ?>
					
			<div class="control-group" style="display:block;">
				<label class="control-label" for="basicinput">Skill Highlights/Specialties </label>
				<div class="controls">
					<input type="text" name="data[{{$i}}][skills]" class="span8" id="skills_<?php echo $i;?>" value="{{$eachdoc['skills']}}">
				</div>
			</div>					
		</div>	
		<?php 
		$i++;
			} 
		}
		else
		{
		?>
			<div class="control-group" style="display:block;" id="div_id_<?php echo $i;?>">
						
				<div class="control-group" style="display:block;">
					<label class="control-label" for="basicinput">Skill Highlights/Specialties </label>
					<div class="controls">
						<input type="text" name="data[{{$i}}][skills]" class="span8" id="skills_<?php echo $i;?>">
					</div>
				</div>	
			</div>
		<?php
		}
		?>

		<input type="hidden" name="rowcount" id="rowcount" value="<?php echo $doc_skill_count;?>">
		<input type="button" onclick="addSkill();" class="btn" name="add_skill" id="add_skill" value="Add More">

		
			
		<div class="control-group">
		
			<div class="controls">
				<a href="{!! url('admin/dentist/dentist-step5/'.$user_id)!!}" class="btn">Prev</a>
				{!! Form::submit('Next',array('class'=>'btn','name'=>'action','value'=>'Next')) !!}				
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}
	

<script>
	
	function addSkill(){
		var rowcountval = $("#rowcount").val();
		var rowcount =  parseInt(rowcountval)+1;
		$("#rowcount").val(rowcount);
		var addonhtml = '';
		
		var html = '<div class="control-group" style="display:block;" id="div_id_'+rowcount+'"><div class="price_field"><a class="remove-btn" href="javascript:void(0);" onclick="removeSkill('+rowcount+');">Remove</a></div><div class="control-group" style="display:block;"><label class="control-label" for="basicinput">Skill Highlights/Specialties </label><div class="controls"><input type="text" name="data['+rowcount+'][skills]" class="span8"></div></div></div>';

		$("#add_skill").before(html);
		
		
		//$("input[name='data["+rowcountval+"][prof_exp]']").rules("add", { required: true });
	}


	function removeSkill(row,licenseitemid){
      	var response = confirm('Do you really want to remove this Item?');
		if(response==1){
	      	if(licenseitemid){
	      		$("#div_id_"+row).remove();	   
	      		$.ajax({
				  url: site_url+"/admin/item/remove-item",
				  type: 'POST',
				  data: {'itemid':licenseitemid,'model':'DoctorExperience'},
				  success: function(html){
				  }
				});

	      	}else{
	      		$("#div_id_"+row).remove();	   
	      	}	      	
        }

      }
</script>

@stop