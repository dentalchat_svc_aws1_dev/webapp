<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
	protected $guarded=[];
	public $timestamps = false;

    protected $table = 'testimonial';
    
    public function get_blog_images(){
        return $this->hasOne('App\Model\BlogImage', 'blog_id','id');
        
    }
   
}
