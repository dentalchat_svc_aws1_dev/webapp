
var userJs = {
	
	/***************	Check same email exists or not for subadmin. 	************************/
	
	
	changeStatus: function(value,id){
		$.ajax({
			url: base_url+'/admin/question/status',
			type: "get",
			data: { this_val : value,this_id : id},
			success: function(data){
				if(data == '1'){
					$('.alert-success').html('');
					$('#success_status_span_'+id).html('Status updated.');
					$('#success_status_span_'+id).fadeIn('slow');
					$('#success_status_span_'+id).fadeOut('slow');
				}
			}
		});
		
	},
	changeAnsStatus: function(value,id){
		$.ajax({
			url: base_url+'/admin/answers/status',
			type: "get",
			data: { this_val : value,this_id : id},
			success: function(data){
				if(data == '1'){
					$('.alert-success').html('');
					$('#success_status_span_'+id).html('Status updated.');
					$('#success_status_span_'+id).fadeIn('slow');
					$('#success_status_span_'+id).fadeOut('slow');
				}
			}
		});
		
	},
	remove: function(id){
		var response = confirm('Do you really want to remove this user?');
		if(response==1){
			window.location.href = base_url+"/admin/question/remove/"+id;
		}
	},
	removeAnswer: function(id,ques_id){
		var response = confirm('Do you really want to remove this user?');
		if(response==1){
			window.location.href = base_url+"/admin/answers/remove/"+id+"/"+ques_id;
		}
	},
	/*-----------------------------------------------------------------------------*/
}