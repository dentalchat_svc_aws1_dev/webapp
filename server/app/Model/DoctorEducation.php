<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorEducation extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'doctor_educations';
     
}

