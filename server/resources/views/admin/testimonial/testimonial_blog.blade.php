{!! HTML::script('resources/assets/js/ckeditor/ckeditor.js') !!} 

@extends('admin/layout/admin_template')

@section('content')

	{!! Form::open(['url' => 'admin/add/edit','method'=>'POST', 'files'=>true,'class'=>'form-horizontal row-fluid','id'=>'edit_blog_form']) !!}
	
		{!! Form::hidden('blog_id',$blog_arr->id,['class'=>'span8','id'=>'blog_id']) !!}
		
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Name *</label>
				
			<div class="controls">
				{!! Form::text('reviewer_name',$blog_arr->reviewer_name,['class'=>'span8','id'=>'reviewer_name','required'=>true]) !!}

			</div>
		</div>
			
		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Occupation *</label>
				
			<div class="controls">
				{!! Form::text('occupation',$blog_arr->occupation,['class'=>'span8','id'=>'occupation','required'=>true]) !!}

				<div style="color:#F00;font-size:13px;clear:both;" id="slug_msg"></div>
			</div>
		</div>

		<div class="control-group" style="display:block;">
			<label class="control-label" for="basicinput">Feedback *</label>
				
			<div class="controls">
				 {!! Form::textarea('feedback',$blog_arr->feedback,['class'=>'span8','id'=>'feedback','required'=>true]) !!}
				
			</div>
		</div>

			
		<div class="control-group">
		
			<div class="controls">
				{!! Form::submit('Save',array('class'=>'btn','name'=>'action','value'=>'save')) !!}
				<a href="{!! url('admin/testimonial/list')!!}" class="btn">Back</a>
			</div>
				
		</div>
	
	{!! Form::close() !!}
	{!! HTML::script(url().'/public/backend/scripts/user.js') !!}

    @stop
    
    