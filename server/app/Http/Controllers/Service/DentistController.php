<?php namespace App\Http\Controllers\Service;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
/*************** Model name ***************/

use App\Model\Doctor; 	
use App\Model\DoctorDetails;
use App\Model\DoctorClinic;
use App\Model\DoctorEducation;
use App\Model\DoctorAward;
use App\Model\DoctorInsurance;
use App\Model\DoctorExperience;
use App\Model\DoctorLicense;
use App\Model\DoctorSkill;
use App\Model\DoctorLanguage; 
use App\Model\DoctorLoginDetails;
use App\Model\Notifications;
use App\Model\DoctorNotification;


use App\Model\Sitesetting;
use App\Model\TmpDoctorDetails;
use App\Model\TmpDoctorClinic;
use App\Model\TmpDoctorEducation;
use App\Model\TmpDoctorAward;
use App\Model\TmpDoctorInsurance;
use App\Model\TmpDoctorExperience;
use App\Model\TmpDoctorLicense;
use App\Model\TmpDoctorSkill;
use App\Model\TmpDoctorLanguage; 
use App\Model\PatientPost; 
use App\Model\PatientPostAttachment; 
use App\Model\PatientDoctorAppointments; 
use App\Model\ChatHistory; 
use App\Model\DoctorReviewRating; 
use App\Model\ClosedPostChat; 
use App\Model\Patient;
use App\Model\Language; 

use App\User;
use App\Http\Requests;
use App\Helper\helpers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;
use Cache;

class DentistController extends BaseController {

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function __construct() {
  		parent::__construct();
    }
    function getTest(){
        echo 1;
    }
	
    /*On Profile Update By Dentist, Admin Will Receive A Notification Email About The Update- Only If He Had Approved The Previous Notification*/
    public function SendNotificationEmail($dentist_id)
    {
        $site_name = "Dental Chat";
        $site = Sitesetting::where(['name' => 'Email'])->first();
        $to_email = $site->value;
    
        $doc_det = DoctorDetails::where(['doctor_id' => $dentist_id])->first();
        //$from_email = $doc_det['email'];
        $user_name = $doc_det['first_name'].' '.$doc_det['last_name'];
        $subject = "Profile Update Notification ".$user_name;
        $body = "Hye Admin! Profile is updated by dentist ".$user_name.", please review and approve.";
        $from_email = 'notification@dentalchat.com';
        $today = date("d-m-Y");

        $mail = Mail::send('service.dentalchat.notification_message_mail', array('name'=>'','post_title'=>'Test Title','profile_image'=>'','content'=>$body,'date'=>$today,'reply_url'=>''), 
                       function($message) use ($to_email, $from_email,$subject,$site_name)
                        {
                            $message->from($from_email, $site_name);
                            $message->to($to_email)->subject($subject);
                        });
    }


    /*****************************************************************/
             /***************** STEP- 1 START **************/
    /*****************************************************************/

	function postDentistStep1(){
    $access_token   = Request::header('Access-Token');
		$user_dtls 		= DoctorLoginDetails::where('token',$access_token)->first();
		$dentist_id		= $user_dtls['doctor_id'];
		
		$path = 'uploads/dentist_profile_image/';
        $dentistdetails = Doctor::with('docs_details')->where('id',$dentist_id)->first();
		
   //print_r($dentistdetails->docs_details['date_of_birth']); exit;
    if($dentistdetails->docs_details['date_of_birth']=='00-00-0000' || $dentistdetails->docs_details['date_of_birth']=='0000-00-00' || $dentistdetails->docs_details['date_of_birth']=='')
    {
      $dentistdetails->docs_details['date_of_birth'] = '';
    }else{
      $dentistdetails->docs_details['date_of_birth'] = date('d-M-Y',strtotime($dentistdetails->docs_details['date_of_birth']));
    }  
//print_r($dentistdetails->docs_details['date_of_birth']); exit;
		if($dentistdetails->docs_details['profile_pics']=='' || (!file_exists($path.$dentistdetails->docs_details['profile_pics'])))
		{
			$dentistdetails->docs_details['profile_pics'] = 'no_image.jpg';
		} 


		  
        if($dentistdetails){
             echo json_encode(
                    array(
                        'status'    => 1,
                        'dentistdetails' => $dentistdetails,
                    )
                );

        }else{
             echo json_encode(
                    array(
                        'status'    => 0,
                        'dentistdetails' => array(),
                    )
                );

        }
        

    }
	
	function postUpdateDentistProfileStep1()
	{
  	  $data = Request::all();
  		
      $dentist_id =  $data['doctor_id'];

        $doc_details = Doctor::with('docs_details')->where('id',$data['doctor_id'])->first();
   
        if (Input::hasFile('profile_pics'))
        {
          $destinationPath = 'uploads/dentist_profile_image/'; 
          $extension = Input::file('profile_pics')->getClientOriginalExtension(); // getting image extension
          $fileName = time().'.'.$extension; // renaming image
          Input::file('profile_pics')->move($destinationPath, $fileName); // uploading file to given path
          
          $data['image']=$fileName;
          // unlink old photo
          @unlink('uploads/dentist_profile_image/'.$doc_details->docs_details['profile_pics']);
        }
        else
        {
           $data['image'] = $doc_details->docs_details['profile_pics'];           
        }

        
        $main_doctor['updated_at'] = date('Y-m-d h:i:s');

        $doctor = Doctor::where('id',$data['doctor_id'])->update([
                                            'publish_status' => 0,
                                            'complete_profile_status' => 0,
                                            'updated_at'      => date('Y-m-d h:i:s')
                                        ]);

        if($data['date_of_birth'] == '00-00-0000' || $data['date_of_birth'] == '0000-00-00' || $data['date_of_birth'] =='')  // Age calculation
        {
            $age ='';
            
        }
        else
        {
            //echo $data['date_of_birth'];
            $age = (date('Y') - date('Y',strtotime($data['date_of_birth'])));
            //echo $age; exit;
        }

        $doc_details = DoctorDetails::where('doctor_id',$data['doctor_id'])->update([
                                        'first_name'    => trim($data['first_name']),
                                        'last_name'     => trim($data['last_name']),
                                        'age'           => $age,
                                        'contact_number'=> preg_replace('/[^0-9]+/', '',$data['contact_number']),
                                        'conuntry_code' => $data['conuntry_code'],
                                        'date_of_birth' => $data['date_of_birth']?date('Y-m-d',strtotime($data['date_of_birth'])):'0000-00-00',
                                        'gender'        => $data['gender'],
                                        'profile_pics'  => $data['image'],
                                        'updated_at'      => date('Y-m-d h:i:s')
                                    ]);

        $update_doc_details = Doctor::with('docs_details')->where('id',$data['doctor_id'])->first();

        //Notification change after  any change if already not sent
          $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
          if($dentist_lang_details->sent_notification ==0)
            {
              $send_noti =  Notifications::create([
                                            'from' =>$dentist_id,
                                            'msg' => 'Changed on some steps.',
                                            'type' => 'profile_update',
                                            'link' => '',
                                            'read' => 0,
                                            'hide_status' =>0,
                                            'added_date' =>date('Y-m-d')]); 

              $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
              $this->sendNotificationEmail($dentist_id);
            } 

        if($doc_details)
        {
    /******************************** Elastic email send ***********************************/
      $dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
	  $bussinessname  = $dr_bussiness['business_name'];  
      $this->mailsend($dentist_id,'Your DentalChat.com profile has been updated', $data['email'], trim($data['first_name']).' '.trim($data['last_name']) ,$data['first_name'],$data['last_name'], 'updatestep1' , $bussinessname,1);
     /******************************** Email End here*******************************/

            echo json_encode(array('status'=>1,'update_doc_details'=>$update_doc_details));
        } 
        else
        {
            echo json_encode(array('status'=>0,'update_doc_details'=>$update_doc_details));
        }
	}
	
    /*****************************************************************/
             /***************** STEP- 1 END **************/
    /*****************************************************************/

    /*****************************************************************/
             /***************** STEP- 2 START **************/
    /*****************************************************************/

    function postDentistStep2(){
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];       
        
        $path = 'uploads/clinic_picture/';
        $dentistdetails = Doctor::with('docs_clinics')->where('id',$dentist_id)->first();
        
        if($dentistdetails->docs_clinics['clinic_picture']=='' || (!file_exists($path.$dentistdetails->docs_clinics['clinic_picture'])))
        {
            $dentistdetails->docs_clinics['clinic_picture'] = 'no_image.jpg';
        }       

        if($dentistdetails){
             echo json_encode(
                    array(
                        'status'    => 1,
                        'dentistdetails' => $dentistdetails,
                    )
                );

        }else{
             echo json_encode(
                    array(
                        'status'    => 0,
                        'dentistdetails' => array(),
                    )
                );

        }       

    }

    function postUpdateDentistProfileStep2()
    {
        $data = Request::all();
       // print_r($data);exit;
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];

        $clinic_det = Doctor::with('docs_clinics')->where('id',$dentist_id)->first();
                

        if (Input::hasFile('clinic_picture'))
        {
          $destinationPath = 'uploads/clinic_picture/'; 
          $extension = Input::file('clinic_picture')->getClientOriginalExtension(); // getting image extension
          $fileName = time().'.'.$extension; // renaming image
          Input::file('clinic_picture')->move($destinationPath, $fileName); // uploading file to given path
          // $this->create_thumbnail($thumb_path,$fileName,$extension); 
          $data['image']=$fileName;

          // unlink old photo
          @unlink('uploads/clinic_picture/'.$clinic_det->docs_clinics['clinic_picture']);
        }
        else
        {
            $data['image'] = $clinic_det->docs_clinics['clinic_picture'];           
        }
           
        $check_clicnicid = DoctorClinic::where('doctor_id',$dentist_id)->count();

        if($check_clicnicid>0){

            $clinic_det    = DoctorClinic::where('doctor_id',$dentist_id)->update([

                    'business_name'         => isset($data['business_name'])?trim($data['business_name']): '',
                    'web_address'               => isset($data['web_address'])?trim($data['web_address']): '',
                    'address'               => isset($data['address'])?trim($data['address']): '',
                    'lat'                   => isset($data['lat'])?trim($data['lat']): '',
                    'lang'                  => isset($data['lang'])?trim($data['lang']): '',
                    'zip_code'              => isset($data['zip_code'])?trim($data['zip_code']): '',
                    'city'                   => isset($data['city'])?trim($data['city']): '',
                    'state'                  => isset($data['state'])?trim($data['state']): '',
                    'country'              => isset($data['country'])?trim($data['country']): '',
                    'sun_opening_hours_from'=> isset($data['sun_opening_hours_from'])?trim($data['sun_opening_hours_from']): '',
                    'sun_opening_hours_to'  => isset($data['sun_opening_hours_to'])?trim($data['sun_opening_hours_to']): '',

                    'mon_opening_hours_from'=> isset($data['mon_opening_hours_from'])?trim($data['mon_opening_hours_from']): '',
                    'mon_opening_hours_to'  => isset($data['mon_opening_hours_to'])?trim($data['mon_opening_hours_to']): '',

                    'tue_opening_hours_from'=> isset($data['tue_opening_hours_from'])?trim($data['tue_opening_hours_from']): '',
                    'tue_opening_hours_to'  => isset($data['tue_opening_hours_to'])?trim($data['tue_opening_hours_to']): '',

                    'wed_opening_hours_from'=> isset($data['wed_opening_hours_from'])?trim($data['wed_opening_hours_from']): '',
                    'wed_opening_hours_to'  => isset($data['wed_opening_hours_to'])?trim($data['wed_opening_hours_to']): '',

                    'thu_opening_hours_from'=> isset($data['thu_opening_hours_from'])?trim($data['thu_opening_hours_from']): '',
                    'thu_opening_hours_to'  => isset($data['thu_opening_hours_to'])?trim($data['thu_opening_hours_to']): '',

                    'fri_opening_hours_from'=> isset($data['fri_opening_hours_from'])?trim($data['fri_opening_hours_from']): '',
                    'fri_opening_hours_to'  => isset($data['fri_opening_hours_to'])?trim($data['fri_opening_hours_to']): '',

                    'sat_opening_hours_from'=> isset($data['sat_opening_hours_from'])?trim($data['sat_opening_hours_from']): '',
                    'sat_opening_hours_to'  => isset($data['sat_opening_hours_to'])?trim($data['sat_opening_hours_to']): '',

                    'clinic_picture'        => isset($data['image'])?$data['image']: '',
                    'updated_at'            => date('Y-m-d h:i:s')
                ]);
        }else{
          $clinic_det    = DoctorClinic::create([
                    'doctor_id'         => $dentist_id,
                    'business_name'         => isset($data['business_name'])?trim($data['business_name']): '',
                    'address'               => isset($data['address'])?trim($data['address']): '',
                    'lat'                   => isset($data['lat'])?trim($data['lat']): '',
                    'lang'                  => isset($data['lang'])?trim($data['lang']): '',
                    'zip_code'              => isset($data['zip_code'])?trim($data['zip_code']): '',
                    'city'                   => isset($data['city'])?trim($data['city']): '',
                    'state'                  => isset($data['state'])?trim($data['state']): '',
                    'country'              => isset($data['country'])?trim($data['country']): '',
                    'sun_opening_hours_from'=> isset($data['sun_opening_hours_from'])?trim($data['sun_opening_hours_from']): '',
                    'sun_opening_hours_to'  => isset($data['sun_opening_hours_to'])?trim($data['sun_opening_hours_to']): '',

                    'mon_opening_hours_from'=> isset($data['mon_opening_hours_from'])?trim($data['mon_opening_hours_from']): '',
                    'mon_opening_hours_to'  => isset($data['mon_opening_hours_to'])?trim($data['mon_opening_hours_to']): '',

                    'tue_opening_hours_from'=> isset($data['tue_opening_hours_from'])?trim($data['tue_opening_hours_from']): '',
                    'tue_opening_hours_to'  => isset($data['tue_opening_hours_to'])?trim($data['tue_opening_hours_to']): '',

                    'wed_opening_hours_from'=> isset($data['wed_opening_hours_from'])?trim($data['wed_opening_hours_from']): '',
                    'wed_opening_hours_to'  => isset($data['wed_opening_hours_to'])?trim($data['wed_opening_hours_to']): '',

                    'thu_opening_hours_from'=> isset($data['thu_opening_hours_from'])?trim($data['thu_opening_hours_from']): '',
                    'thu_opening_hours_to'  => isset($data['thu_opening_hours_to'])?trim($data['thu_opening_hours_to']): '',

                    'fri_opening_hours_from'=> isset($data['fri_opening_hours_from'])?trim($data['fri_opening_hours_from']): '',
                    'fri_opening_hours_to'  => isset($data['fri_opening_hours_to'])?trim($data['fri_opening_hours_to']): '',

                    'sat_opening_hours_from'=> isset($data['sat_opening_hours_from'])?trim($data['sat_opening_hours_from']): '',
                    'sat_opening_hours_to'  => isset($data['sat_opening_hours_to'])?trim($data['sat_opening_hours_to']): '',

                    'clinic_picture'        => isset($data['image'])?$data['image']: '',
                    'updated_at'            => date('Y-m-d h:i:s')
                ]);
        }
        
        $update_clinic_det = Doctor::with('docs_clinics')->where('id',$dentist_id)->first();

        //Notification change after  any change if already not sent
          $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
          if($dentist_lang_details->sent_notification ==0)
            {
              $send_noti =  Notifications::create([
                                            'from' =>$dentist_id,
                                            'msg' => 'Changed on some steps.',
                                            'type' => 'profile_update',
                                            'link' => '',
                                            'read' => 0,
                                            'hide_status' =>0,
                                            'added_date' =>date('Y-m-d')]); 

              $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
              $this->sendNotificationEmail($dentist_id);
            } 



        if($update_clinic_det)
        {
    /******************************** Elastic email send ***********************************/
     
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
	   $this->mailsend($dentist_id,'Profile Changed Confirmation Step 2', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep2',$data['business_name'],1);

      /******************************** Email End here*******************************/    
            
            echo json_encode(array('status'=>1,'update_clinic_det'=>$update_clinic_det));
        } 
        else
        {
            echo json_encode(array('status'=>0,'update_clinic_det'=>$update_clinic_det));
        }
    }

     /*****************************************************************/
             /***************** STEP- 2 END **************/
    /*****************************************************************/
	
	 /*****************************************************************/
             /***************** STEP- 3 START **************/
/*****************************************************************/

    function postDentistStep3(){
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
       
        $dentist_education = Doctor::with('docs_education','docs_award','docs_insurance')->where('id',$dentist_id)->first(); 

          // Award Details 
          $set_awards= array();
          $set_insurance= array();
          if($dentist_education->docs_award !=NULL)
          {
            $dentist_award = $dentist_education->docs_award->toArray();
          }
          else
          {
            $dentist_award = array();
          }
        //print_r($dentist_award); exit;
          foreach($dentist_award as $each_award)
          {   
                $set_awards[]=array('id'=>$each_award['id'],'award'=>$each_award['award']);
          }  

          // Insurance Details 

          if($dentist_education->docs_insurance !=NULL)
          {
            $dentist_insurance = $dentist_education->docs_insurance->toArray();
          }
          else
          {
            $dentist_insurance = array();
          }
        
          foreach($dentist_insurance as $each_insurance)
          {   
            $set_insurance[]=array('id'=>$each_insurance['id'],'insurance'=>$each_insurance['insurance']);
          }  
         
     return response()->json(['status' => 1,"dentist_education_data"=>$dentist_education->docs_education,
     "dentist_award"=>$set_awards,"dentist_insurance"=>$set_insurance]);
            
    }
  
  function postUpdateDentistProfileStep3()
    {
      $data = Request::all();
      
      $access_token   = Request::header('Access-Token');
      $user_dtls    = DoctorLoginDetails::where('token',$access_token)->first();    
      $dentist_id   = $user_dtls['doctor_id']; 
      //echo $dentist_id; exit;

      $document_array = isset($data['documents'])?$data['documents']:'';
      $documentName = json_decode($data['documentName']);


      DoctorAward::where('doctor_id',$dentist_id)->delete();
      DoctorInsurance::where('doctor_id',$dentist_id)->delete();
      
      $awards_arr = json_decode($data['dentist_awards'],true);
      $insurance_arr = json_decode($data['dentist_insurance'],true);
      //print_r($awards_arr); exit;
      
      foreach($awards_arr as $eachaward)
      {
        if(isset($eachaward['award']))  // checking atleast one value is there in array
          {
              $saveable=[
                      'doctor_id' => $dentist_id,
                      'award'=> trim($eachaward['award']),
                      'created_at' => date('Y-m-d h:i:s'),
                      'updated_at' => date('Y-m-d h:i:s')
                    ];

             $doc_award   = DoctorAward::create($saveable);   

          }
      }

      foreach($insurance_arr as $eachinsurance)
      {
        if(isset($eachinsurance['insurance']))  // checking atleast one value is there in array
          {
             $doc_award   = DoctorInsurance::create([
                      'doctor_id' => $dentist_id,
                      'insurance'=> trim($eachinsurance['insurance']),
                      'created_at' => date('Y-m-d h:i:s'),
                      'updated_at' => date('Y-m-d h:i:s')
                    ]);         
          }
      }


      
      $all_dentist_exp = DoctorEducation::where('doctor_id',$dentist_id)->get()->toArray();
      $path = 'uploads/certificate/';
      if(!empty($all_dentist_exp))
      {
        DoctorEducation::where('doctor_id',$dentist_id)->delete();
        
        foreach($all_dentist_exp as $experience)
        {
          // unlink old photo
               // @unlink('uploads/certificate/'.$experience['certificate']);
        }     
        
      }
    
    /////////////////////////////////
          $files = Input::file('documents');

          if(!empty($documentName))
          {
            //print_r($documentName); exit;
            $dn = 0;
            foreach($documentName as $kf=>$kv )
            {              
              $degree = (isset($documentName[$kf]->degree) && $documentName[$kf]->degree!='')?$documentName[$kf]->degree:''; 
              $completed_year = (isset($documentName[$kf]->completed_year) && $documentName[$kf]->completed_year!='')?$documentName[$kf]->completed_year:''; 
              $medical_school = (isset($documentName[$kf]->medical_school) && $documentName[$kf]->medical_school!='')?$documentName[$kf]->medical_school:''; 
              $location = (isset($documentName[$kf]->location) && $documentName[$kf]->location!='')?$documentName[$kf]->location:''; 
              
              //print_r($files); exit;
              if(isset($files[$kf]))
              {   
                $file=$files[$kf];
                $org_certificate = $files[$kf]->getClientOriginalName();
                $destinationPath = 'uploads/certificate/'; // upload path
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $file_name = rand(11111,99999).time().'.'.$extension; // renameing image
                $allow_extension_doc = array('pdf','jpg','jpeg','png');
                if(in_array(strtolower($extension), $allow_extension_doc))
                {
                  if($file->move($destinationPath, $file_name)) // uploading file to given path
                  {
                    
                  }
                  else
                  {
                    $file_name='';
                  }
                }
              }
              else
              { 
                $file_name=isset($documentName[$kf]->certificate)?$documentName[$kf]->certificate:'';
                $org_certificate = isset($documentName[$kf]->org_certificate)?$documentName[$kf]->org_certificate:'';
              }


              if($degree!='' || $completed_year!='' || $medical_school!='' || $location!='' || $file_name!='' || $org_certificate!='')
              {

                $doc_upload= array(
                    'doctor_id'=>$dentist_id,
                    'degree'=>isset($degree)?$degree:'',     
                    'completed_year'=>isset($completed_year)?$completed_year:'',     
                    'medical_school'=>isset($medical_school)?$medical_school:'',     
                    'location'=>isset($location)?$location:'', 
                    'certificate'=>isset($file_name)?$file_name:'',
                    'org_certificate'=>isset($org_certificate)?$org_certificate:'',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'));

                $save_doc = DoctorEducation::create($doc_upload);
              }
              
              $dn++;
            }            
          }

/////////////////////////////////////// 

            //Notification change after  any change if already not sent
              $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
              if($dentist_lang_details->sent_notification ==0)
                {
                  $send_noti =  Notifications::create([
                                                'from' =>$dentist_id,
                                                'msg' => 'Changed on some steps.',
                                                'type' => 'profile_update',
                                                'link' => '',
                                                'read' => 0,
                                                'hide_status' =>0,
                                                'added_date' =>date('Y-m-d')]); 

                  $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
                  $this->sendNotificationEmail($dentist_id);
                } 

     /******************************** email send ***********************************/
      
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
		$bussinessname  = $dr_bussiness['business_name'];
		
		$this->mailsend($dentist_id,'Profile Changed Confirmation Step 3', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep3' , $bussinessname,1);	
   
      /******************************** Email End here*******************************/  

             echo json_encode(
                    array(
                        'status'    => 1
                    )
                );
       
        
    }

    function postRemoveEducation()
      {
        $data = Request::all();
        
        DoctorEducation::where('id',$data['exp_id'])->delete();
        
        
        
        $all_dentist_education = DoctorEducation::where('id',$data['exp_id'])->get()->toArray();
        $path = 'uploads/certificate/';
        if(!empty($all_dentist_education))
        {
          DoctorEducation::where('id',$data['exp_id'])->delete();
          
          foreach($all_dentist_education as $education)
          {
            // unlink old photo
                  @unlink('uploads/certificate/'.$education['certificate']);
          } 
        }
        echo json_encode(array('status'=>1));
      }

      function postRemoveCertificate()
      {
        $data = Request::all();
        //print_r($data);exit;
        $all_dentist_education = DoctorEducation::where('id',$data['remove_id'])->get()->toArray();
        $path = 'uploads/certificate/';
        if(!empty($all_dentist_education))
        {
          DoctorEducation::where('id',$data['remove_id'])->update([
                      'certificate' => '',
                      'org_certificate'=> ''
                    ]);
          
          foreach($all_dentist_education as $education)
          {
            // unlink old photo
                  @unlink('uploads/certificate/'.$education['certificate']);
          } 
        }
        echo json_encode(array('status'=>1));
      }

/*****************************************************************/
             /***************** STEP- 3 END **************/
/*****************************************************************/

    /*****************************************************************/
             /***************** STEP- 4 START **************/
    /*****************************************************************/

        function postDentistStep4()
        {
            $access_token   = Request::header('Access-Token');
            $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
            $dentist_id     = $user_dtls['doctor_id'];
           
            $dentist_license = Doctor::with('docs_license')->where('id',$dentist_id)->first();  
            return response()->json(['status' => 1,"dentist_license_data"=>$dentist_license->docs_license]);                
        }
      
      function postUpdateDentistProfileStep4()
        {
          $data = Request::all();
          //print_r($data); exit;
          $access_token   = Request::header('Access-Token');
          $user_dtls    = DoctorLoginDetails::where('token',$access_token)->first();    
          $dentist_id   = $user_dtls['doctor_id']; 
          //echo $dentist_id; exit;

          $document_array = isset($data['documents'])?$data['documents']:'';

          $documentName = json_decode($data['documentName']);

          $all_dentist_license = DoctorLicense::where('doctor_id',$dentist_id)->get()->toArray();
          $path = 'uploads/license_photo/';
          if(!empty($all_dentist_license))
          {
            DoctorLicense::where('doctor_id',$dentist_id)->delete();
          }
        
        /////////////////////////////////
              $files = Input::file('documents');

              if(!empty($documentName))
              {
                $dn = 0;
                
                foreach($documentName as $kf=>$kv )
                {         
                    $licenseName = (isset($documentName[$kf]->license) && $documentName[$kf]->license!='')?$documentName[$kf]->license:'';  // For Document Name user provide in text box
                    $org_name = isset($documentName[$kf]->doc_file_name)?$documentName[$kf]->doc_file_name:'';

                    $doc_upload= array(
                          'doctor_id'=>$dentist_id,                          
                          'license_details'=>isset($licenseName)?$licenseName:'',                   
                          'created_at'=>date('Y-m-d H:i:s'),
                          'updated_at'=>date('Y-m-d H:i:s'));                        
                      
                    if(isset($files[$kf]))
                    {
                      $file=$files[$kf];
                      $org_license = $files[$kf]->getClientOriginalName();
                      //echo $org_license; exit;
                      $destinationPath = 'uploads/license_photo/'; // upload path
                      $extension = $file->getClientOriginalExtension(); // getting image extension
                      $file_name = rand(11111,99999).time().'.'.$extension; // renameing image
                      $allow_extension_doc = array('pdf','jpg','jpeg','png');
                      if(in_array(strtolower($extension), $allow_extension_doc))
                      {                        
                        if($file->move($destinationPath, $file_name)) // uploading file to given path
                        {
                          $doc_upload['license_photo']=$file_name;
                          $doc_upload['org_license']=$org_license;
                        }
                      }
                    }
                    else
                    { 
                      $file_name=isset($documentName[$kf]->license_doc)?$documentName[$kf]->license_doc:'';
                      $org_license = isset($documentName[$kf]->org_license)?$documentName[$kf]->org_license:'';
                      $doc_upload['license_photo']=$file_name;
                      $doc_upload['org_license']=$org_license;
                    }

                    if($licenseName!='' || $file_name !='' || $org_license!='')
                    {
                      $save_doc = DoctorLicense::create($doc_upload);
                    }
                  
                  $dn++;
                }
                //exit;
              }

    ///////////////////////////////////////  

          //Notification change after  any change if already not sent
              $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
              if($dentist_lang_details->sent_notification ==0)
                {
                  $send_noti =  Notifications::create([
                                                'from' =>$dentist_id,
                                                'msg' => 'Changed on some steps.',
                                                'type' => 'profile_update',
                                                'link' => '',
                                                'read' => 0,
                                                'hide_status' =>0,
                                                'added_date' =>date('Y-m-d')]); 

                  $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
                  $this->sendNotificationEmail($dentist_id);
                }  

     /******************************** email send ***********************************/
      
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
		$bussinessname  = $dr_bussiness['business_name'];
		$this->mailsend($dentist_id,'Profile Changed Confirmation Step 4', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep4' , $bussinessname,1);	

      /******************************** Email End here*******************************/  
        
                 echo json_encode(
                        array(
                            'status'    => 1
                        )
                    );
           
            
        }


      function postRemoveLicense()
      {
        $data = Request::all();
        
        DoctorLicense::where('id',$data['license_id'])->delete();
        
        
        
        $all_dentist_license = DoctorLicense::where('id',$data['license_id'])->get()->toArray();
        $path = 'uploads/license_photo/';
        if(!empty($all_dentist_license))
        {
          DoctorLicense::where('id',$data['license_id'])->delete();
          
          foreach($all_dentist_license as $license)
          {
            // unlink old photo
                  @unlink('uploads/license_photo/'.$license['license_photo']);
          }     
          
        }
        
        
        echo json_encode(array(
                     'status'=>1
                     )
                 );
      }

      function postRemoveLicenseImage()
      {
        $data = Request::all();
        //print_r($data);exit;
        $all_dentist_education = DoctorLicense::where('id',$data['remove_id'])->get()->toArray();
        $path = 'uploads/certificate/';
        if(!empty($all_dentist_education))
        {
          DoctorLicense::where('id',$data['remove_id'])->update([
                      'license_photo' => '',
                      'org_license'=> ''
                    ]);
          
          foreach($all_dentist_education as $education)
          {
            // unlink old photo
                  @unlink('uploads/license_photo/'.$license['license_photo']);
          } 
        }
        echo json_encode(array('status'=>1));
      }

      
      
    /*****************************************************************/
             /***************** STEP- 4 END **************/
    /*****************************************************************/  



	
  /*****************************************************************/
             /***************** STEP- 5 START **************/
    /*****************************************************************/

    function postDentistStep5(){
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
       
        $dentist_experience = Doctor::with('docs_experiece')->where('id',$dentist_id)->first();  
     return response()->json(['status' => 1,"dentist_experience_data"=>$dentist_experience->docs_experiece]);
            
    }
  
  function postUpdateDentistProfileStep5()
    {
      $data = Request::all();
      //print_r($data); exit;
      $access_token   = Request::header('Access-Token');
      $user_dtls    = DoctorLoginDetails::where('token',$access_token)->first();    
      $dentist_id   = $user_dtls['doctor_id']; 
      //echo $dentist_id; exit;
      
      $document_array = isset($data['documents'])?$data['documents']:'';
         
          $documentName = json_decode($data['documentName']);
      
      $all_dentist_exp = DoctorExperience::where('doctor_id',$dentist_id)->get()->toArray();
      $path = 'uploads/exp_document/';
      if(!empty($all_dentist_exp))
      {
        DoctorExperience::where('doctor_id',$dentist_id)->delete();
        
        foreach($all_dentist_exp as $experience)
        {
          // unlink old photo
              //  @unlink('uploads/exp_document/'.$experience['exp_photo']);
        }     
        
      }
    
    /////////////////////////////////
          $files = Input::file('documents');

          if(!empty($documentName))
          {
        //print_r($documentName); exit;
            $dn = 0;
            foreach($documentName as $kf=>$kv )
            {
              $profExp = (isset($documentName[$kf]->prof_exp) && $documentName[$kf]->prof_exp!='')?$documentName[$kf]->prof_exp:''; 
              $dentalBusiness = (isset($documentName[$kf]->dental_business) && $documentName[$kf]->dental_business!='')?$documentName[$kf]->dental_business:''; 
              $location = (isset($documentName[$kf]->location) && $documentName[$kf]->location!='')?$documentName[$kf]->location:''; 
              $expFrom = (isset($documentName[$kf]->exp_from) && $documentName[$kf]->exp_from!='')?$documentName[$kf]->exp_from:''; 
              $expTo = (isset($documentName[$kf]->exp_to) && $documentName[$kf]->exp_to!='')?$documentName[$kf]->exp_to:''; 
              $present = (isset($documentName[$kf]->present) && $documentName[$kf]->present!='')?$documentName[$kf]->present:'';

              $doc_upload= array(
                    'doctor_id'=>$dentist_id,
                    'prof_exp'=>isset($profExp)?$profExp:'',     
                    'dental_business'=>isset($dentalBusiness)?$dentalBusiness:'',     
                    'location'=>isset($location)?$location:'',     
                    'exp_from'=>isset($expFrom)?$expFrom:'',     
                    'exp_to'=>isset($expTo)?$expTo:'',     
                    'present'=>isset($present)?$present:'',
                    //'exp_photo'=>isset($file_name)?$file_name:'',
                                  
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'));

              if(isset($files[$kf]))
              {
                $file=$files[$kf];
                $org_exp = $files[$kf]->getClientOriginalName();
                $destinationPath = 'uploads/exp_document/'; // upload path
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $file_name = rand(11111,99999).time().'.'.$extension; // renameing image
                $allow_extension_doc = array('pdf','jpg','jpeg','png');
                if(in_array(strtolower($extension), $allow_extension_doc))
                {
                  if($file->move($destinationPath, $file_name)) // uploading file to given path
                  {
                    //echo "success<br/>";
                    $doc_upload['exp_photo']=$file_name; 
                    $doc_upload['org_exp']=$org_exp;                   
                  }
                }
              }
              else
              { 
                $file_name=isset($documentName[$kf]->experience_doc)?$documentName[$kf]->experience_doc:'';
                $org_exp = isset($documentName[$kf]->org_exp)?$documentName[$kf]->org_exp:'';
                $doc_upload['exp_photo']=$file_name;
                $doc_upload['org_exp']=$org_exp;
              }
              
              if($profExp!='' || $dentalBusiness !='' || $location!='' || $expFrom!='' || $expTo!='' || $file_name!='' || $org_exp!='')
              {
                $save_doc = DoctorExperience::create($doc_upload);
              }

              $dn++;
            }
          }

///////////////////////////////////////  

          //Notification change after  any change if already not sent
          $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
          if($dentist_lang_details->sent_notification ==0)
            {
              $send_noti =  Notifications::create([
                                            'from' =>$dentist_id,
                                            'msg' => 'Changed on some steps.',
                                            'type' => 'profile_update',
                                            'link' => '',
                                            'read' => 0,
                                            'hide_status' =>0,
                                            'added_date' =>date('Y-m-d')]); 

              $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
              $this->sendNotificationEmail($dentist_id);
            }  

    /******************************** email send ***********************************/
      
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
		 $bussinessname  = $dr_bussiness['business_name'];
		
		$this->mailsend($dentist_id,'Profile Changed Confirmation Step 5', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep5' , $bussinessname,1);	

      /******************************** Email End here*******************************/  
    
             echo json_encode(
                    array(
                        'status'    => 1
                    )
                );
       
        
    }


  function postRemoveExperience()
  {
    $data = Request::all();
    
    DoctorExperience::where('id',$data['exp_id'])->delete();
    
    
    
    $all_dentist_exp = DoctorExperience::where('id',$data['exp_id'])->get()->toArray();
    $path = 'uploads/exp_document/';
    if(!empty($all_dentist_exp))
    {
      DoctorExperience::where('id',$data['exp_id'])->delete();
      
      foreach($all_dentist_exp as $license)
      {
        // unlink old photo
              @unlink('uploads/exp_document/'.$license['exp_document']);
      }     
      
    }
    
    
    echo json_encode(array(
                 'status'=>1
                 )
             );
  }


  function postRemoveExperienceImage()
      {
        $data = Request::all();
        //print_r($data);exit;
        $all_dentist_education = DoctorExperience::where('id',$data['remove_id'])->get()->toArray();
        $path = 'uploads/certificate/';
        if(!empty($all_dentist_education))
        {
          DoctorExperience::where('id',$data['remove_id'])->update([
                      'exp_photo' => '',
                      'org_exp'=> ''
                    ]);
          
          foreach($all_dentist_education as $education)
          {
            // unlink old photo
                  @unlink('uploads/exp_document/'.$license['exp_document']);
          } 
        }
        echo json_encode(array('status'=>1));
      }


    /*****************************************************************/
             /***************** STEP- 5 END **************/
    /*****************************************************************/ 
	
	
	
	/*****************************************************************/
             /***************** STEP- 6 START **************/
    /*****************************************************************/

    function postDentistStep6(){
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
       
        $dentist_skills = Doctor::with('docs_skill')->where('id',$dentist_id)->first();        
      // print_r($dentist_skills); exit;

        $set_skills = [];
    		if($dentist_skills !=NULL)
    		{
    			$dentist_skills = $dentist_skills->toArray();
    		}
    		else
    		{
    			$dentist_skills = array();
    		}
    		
            foreach($dentist_skills['docs_skill'] as $each_skill)
            {   
                $set_skills[]=array('id'=>$each_skill['id'],'skills'=>$each_skill['skills']);
            }    
    		 echo json_encode(
    				array(
    					'status'    => 1,
    					'dentist_skills' => $set_skills
    				)
    			);
            
    }

    function postUpdateDentistProfileStep6()
    {
      $data = Request::all();
      //print_r($data); exit;
      $access_token   = Request::header('Access-Token');
  		$user_dtls 		= DoctorLoginDetails::where('token',$access_token)->first();
  		
  		$dentist_id		= $user_dtls['doctor_id']; 
  		//echo $dentist_id; exit;
  		DoctorSkill::where('doctor_id',$dentist_id)->delete();
  		
  		$skills_arr = json_decode($data['dentist_skills'],true);
  		//print_r($skills_arr); exit;

      //Notification change after  any change if already not sent
      $dentist_lang_details = Doctor::where('id',$dentist_id)->first(); 
      if($dentist_lang_details->sent_notification ==0)
        {
          $send_noti =  Notifications::create([
                                        'from' =>$dentist_id,
                                        'msg' => 'Changed on some steps.',
                                        'type' => 'profile_update',
                                        'link' => '',
                                        'read' => 0,
                                        'hide_status' =>0,
                                        'added_date' =>date('Y-m-d')]); 

          $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
          $this->sendNotificationEmail($dentist_id);
        }    

    		foreach($skills_arr as $eachskill)
    		{
    			if(isset($eachskill['skills']))  // checking atleast one value is there in array
    				{
    				   $doc_skill		= DoctorSkill::create([
    										'doctor_id' => $dentist_id,
    										'skills'=> trim($eachskill['skills']),
    										'created_at' => date('Y-m-d h:i:s'),
    										'updated_at' => date('Y-m-d h:i:s')
    									]);				  
    				}
    		}   
    		
    		
     /******************************** email send ***********************************/
      
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
		$bussinessname  = $dr_bussiness['business_name'];
		$this->mailsend($dentist_id,'Profile Changed Confirmation Step 6', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep6' , $bussinessname,1);	

      /******************************** Email End here*******************************/  
        echo json_encode(array('status'    => 1));        
    }
	
	function postRemoveSkill()
	{
		$data = Request::all();
		DoctorSkill::where('id',$data['skill_id'])->delete();
		echo json_encode(array(
							   'status'=>1
							   )
						 );
	}

     /*****************************************************************/
             /***************** STEP- 6 END **************/
    /*****************************************************************/
	
	

    /*****************************************************************/
             /***************** STEP- 7 START **************/
    /*****************************************************************/

    function postDentistStep7(){
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
        
        $dentist_lang_details = Doctor::with('docs_language')->where('id',$dentist_id)->first();   
        $selected_language = trim($dentist_lang_details['docs_language']['languages'],',');
        if($selected_language != ''){
            $languages_det = Language::where('status',1)->orderByRaw("FIELD(id,$selected_language) DESC")->orderBy('id','ASC')->get()->toArray();
        }else{
            $languages_det = Language::where('status',1)->orderBy('id','ASC')->get()->toArray();
        }

        
        //print_r($languages_det);exit;
        $languages = [];
        foreach($languages_det as $each_lang)
        {     
            $languages[]=array('id'=>$each_lang['id'],'label'=>$each_lang['name']);
        }  
          
        if($dentist_lang_details){
             echo json_encode(
                    array(
                        'status'    => 1,
                        'dentist_lang_details' => $dentist_lang_details,
                        'languages' => $languages
                    )
                );

        }else{
             echo json_encode(
                    array(
                        'status'    => 0,
                        'dentist_lang_details' => array(),
                    )
                );

        }       

    }

    function postUpdateDentistProfileStep7()
    {
        $data = Request::all();
        
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
        
        $doc_language = DoctorLanguage::where('doctor_id',$dentist_id)->get()->toArray();  
        
        // Delete all edited user's data before update

        DoctorLanguage::where('doctor_id',$dentist_id)->delete();

        $select_language = json_decode($data['allLanguage'],true);
        //print_r($select_language); exit;
        $get_lang = array();
        if(!empty($select_language))
        {
            foreach($select_language as $eachlangid)
            {
                $get_lang[] = $eachlangid['id'];
            }
            
        }
        //print_r($get_lang); exit;

        $doc_language = Language::whereIn('id', $select_language)->get(['name'])->toArray(); 
        //print_r($doc_language); exit;
        $doc_language_arr = [];
        foreach($doc_language as $doc_lang)
        {
            $doc_language_arr[] = $doc_lang['name'];
        }
        //print_r($doc_language_arr); exit;

        if(!empty($data))       
        {
          if(isset($get_lang) || isset($data['personal_statement']))  // checking atleast one value is there in array
          {
            $doc_language   = DoctorLanguage::create([
                                    'doctor_id' => $dentist_id,
                                    'languages'=> implode(',',$get_lang),
                                    'language_name'=>implode(',',$doc_language_arr),
                                    'personal_statement'=> isset($data['personal_statement'])?trim($data['personal_statement']):'',
                                    'created_at' => date('Y-m-d h:i:s'),
                                    'updated_at' => date('Y-m-d h:i:s')
                                ]);
          }               
        
        } // Data If End   

        $dentist_lang_details = Doctor::with('docs_language')->where('id',$dentist_id)->first();        
        
        $languages_det = Language::where('status',1)->get()->toArray();

        $languages = [];
        foreach($languages_det as $each_lang)
        {     
            $languages[]=array('id'=>$each_lang['id'],'label'=>$each_lang['name']);
        }  
        
        // Update Publish 
        $need_approval = Doctor::where('id', $dentist_id)  
                    ->update(['publish_status'=>1]);

        if($dentist_lang_details->sent_notification ==0) //Notification change after  any change if already not sent
        {
          $send_noti =  Notifications::create([
                                        'from' =>$dentist_id,
                                        'msg' => 'Changed on some steps.',
                                        'type' => 'profile_update',
                                        'link' => '',
                                        'read' => 0,
                                        'hide_status' =>0,
                                        'added_date' =>date('Y-m-d')]); 

          $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
          $this->sendNotificationEmail($dentist_id);
        }                 


        if($doc_language){
            
    /******************************** email send ***********************************/
      
 		$user_details = DoctorDetails::where('doctor_id', $dentist_id)->first();
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $dentist_id)->first();
		$bussinessname  = $dr_bussiness['business_name'];
		$this->mailsend($dentist_id,'Profile Changed Confirmation Step 7', $demail, $dname ,$user_details['first_name'],$user_details['last_name'], 'updatestep7' , $bussinessname,1);	

      /******************************** Email End here*******************************/     
            
             echo json_encode(
                    array(
                        'status'    => 1,
                        'dentist_lang_details' => $dentist_lang_details,
                        'languages' => $languages
                    )
                );

        }else{
             echo json_encode(
                    array(
                        'status'    => 0,
                        'dentist_lang_details' => $dentist_lang_details,
                        'languages' => $languages
                    )
                );

        }  
        
        
    }

     /*****************************************************************/
             /***************** STEP- 7 END **************/
    /*****************************************************************/


/*****************************************************************/
      /***************** SAVE PREVIEW START **************/
/*****************************************************************/

    function postSaveShowPreview()
    {
      $data = Request::all();
        
        $access_token   = Request::header('Access-Token');
        $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
        $dentist_id     = $user_dtls['doctor_id'];
        
        $doc_language = DoctorLanguage::where('doctor_id',$dentist_id)->get()->toArray();  
        
        // Delete all edited user's data before update

        DoctorLanguage::where('doctor_id',$dentist_id)->delete();

        $select_language = json_decode($data['allLanguage'],true);
        //print_r($select_language); exit;
        $get_lang = array();
        if(!empty($select_language))
        {
            foreach($select_language as $eachlangid)
            {
                $get_lang[] = $eachlangid['id'];
            }
            
        }
        //print_r($get_lang); exit;

        $doc_language = Language::whereIn('id', $select_language)->get(['name'])->toArray(); 
        //print_r($doc_language); exit;
        $doc_language_arr = [];
        foreach($doc_language as $doc_lang)
        {
            $doc_language_arr[] = $doc_lang['name'];
        }
 
        if(!empty($data))       
        {
          if(isset($get_lang) || isset($data['personal_statement']))  // checking atleast one value is there in array
          {
            $doc_language   = DoctorLanguage::create([
                                    'doctor_id' => $dentist_id,
                                    'languages'=> implode(',',$get_lang),
                                    'language_name'=>implode(',',$doc_language_arr),
                                    'personal_statement'=> isset($data['personal_statement'])?trim($data['personal_statement']):'',
                                    'created_at' => date('Y-m-d h:i:s'),
                                    'updated_at' => date('Y-m-d h:i:s')
                                ]);
          }               
        
        } // Data If End   

        $dentist_lang_details = Doctor::with('docs_language')->where('id',$dentist_id)->first();        
        
        $languages_det = Language::where('status',1)->get()->toArray();

        $languages = [];
        foreach($languages_det as $each_lang)
        {     
            $languages[]=array('id'=>$each_lang['id'],'label'=>$each_lang['name']);
        }  
        
        // Update Publish 
        $need_approval = Doctor::where('id', $dentist_id)  
                    ->update(['publish_status'=>1]);

        if($dentist_lang_details->sent_notification ==0) //Notification change after  any change if already not sent
        {
          $send_noti =  Notifications::create([
                                        'from' =>$dentist_id,
                                        'msg' => 'Changed on some steps.',
                                        'type' => 'profile_update',
                                        'link' => '',
                                        'read' => 0,
                                        'hide_status' =>0,
                                        'added_date' =>date('Y-m-d')]); 

          $need_approval = Doctor::where('id', $dentist_id)->update(['sent_notification'=>1]);
        }                 


        if($doc_language){
             echo json_encode(array('status'    => 1,
                        'dentist_lang_details' => $dentist_lang_details,
                        'languages' => $languages
                    )
                );

        }else{
             echo json_encode(array('status'    => 0,
                        'dentist_lang_details' => $dentist_lang_details,
                        'languages' => $languages
                    )
                );
        }  
    }

/*****************************************************************/
      /***************** SAVE PREVIEW END **************/
/*****************************************************************/

	function postChangePassword(){
		
		$data				= Request::all();
		
		$access_token 		= Request::header('Access-Token');
		$user_dtls 			= DoctorLoginDetails::where('token',$access_token)->first();
		$doctor_id			= $user_dtls['doctor_id'];

		$user_details = DoctorDetails::where('doctor_id', $doctor_id)->first();
		$dfirstname=$user_details['first_name'];
		$dlastname=$user_details['last_name'];
        $dname          = $user_details['first_name'].' '.$user_details['last_name'];
		$demail         = $user_details['email'];
		
		$dr_bussiness = DoctorClinic::where('doctor_id', $doctor_id)->first();
		$bussinessname  = $dr_bussiness['business_name'];

        $tmp_user_details = TmpDoctorDetails::where('doctor_id', $doctor_id)->count();

		
		$input = $data['dentist'];
		
		if(Hash::check($input['old_password'],$user_details['password']))
		{
			$create_user = DoctorDetails::where('doctor_id', $doctor_id)	
									  ->update([
										'password'          => Hash::make($data['dentist']['password'])
									]);

      if($tmp_user_details>0) // For temporary table  update
      {
        $create_user = TmpDoctorDetails::where('doctor_id', $doctor_id)  
                      ->update([
                      'password'          => Hash::make($data['dentist']['password'])
                    ]);
      }
      
      /******************************** Elastic email send ***********************************/
      $this->mailsend($doctor_id,"Your DentalChat.com password has been changed", $demail,$dname, $dfirstname,$dlastname , 'passwordchanged' , $bussinessname);
      /******************************** Email End here*******************************/

			echo json_encode(array('status'=>1));
		}
		else
		{
			echo json_encode(array('status'=>0));
		}
		
			
	}
	
    function postChangeStatus() 
    {
        $data = Request::all();
        if(DoctorReviewRating::where('id',$data['review_id'])->update(['is_hide'=>$data['status']]))
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }
    
	
	/*  DENTIST PROFILE START */
	
	function postDentistProfile() // For dentist profile page  Data came from temporary files
	{
		$data = Request::all();
		//print_r($data); exit;
		$url_params = $data['params'];
		$dentist_id_arr = explode('-',$url_params);
		$dentist_id = end($dentist_id_arr);
		
		$check_dentist = TmpDoctorDetails::where('doctor_id',$dentist_id)->first();

        $check_dentist_pro = Doctor::where('id',$dentist_id)->first();
		
		if($check_dentist != NUll)
		{
			$check_dentist = $check_dentist->toArray();
		}
		else
		{
			$check_dentist = array();
		}
        

    // only doc table data
    if($check_dentist_pro != NUll)
    {
      $check_dentist_pro = $check_dentist_pro->toArray();
    }
    else
    {
      $check_dentist_pro = array();
    }

    //print_r($check_dentist);
       // print_r($check_dentist_pro['profile_updated']); exit;

    if(isset($check_dentist_pro['profile_updated']) && $check_dentist_pro['profile_updated'] ==1) // Only visible this status 1 otherwise redirect to home page
    {

      if(!empty($check_dentist))
      {
        $make_url = strtolower($check_dentist['first_name']).'-'.strtolower($check_dentist['last_name']).'-'.$dentist_id;
        if(trim($make_url) == trim($url_params))  // checking requested dentist id and profile are same then only show.//
        {
          $dentist_details = Doctor::with('tmp_docs_details','tmp_docs_clinics','tmp_docs_education','tmp_docs_award','tmp_docs_insurance','tmp_docs_experiece','tmp_docs_license','tmp_docs_skill','tmp_docs_language')->where('id',$dentist_id)->first()->toArray();

          $doctor_review_ratings = DoctorReviewRating::with('get_patient_details')->where(['doctor_id'=>$dentist_id])->get();
          $doctor_avg_rating = DoctorReviewRating::where(['doctor_id'=>$dentist_id,'is_hide'=>'0'])
                                                ->avg('rating');

        $dentist_details['review_rating'] = $doctor_review_ratings;
        $dentist_details['doctor_avg_rating'] = floor($doctor_avg_rating);
            
            
           
        
          
          echo json_encode(array('status'=>1,'dentist_details'=>$dentist_details));
        }
        else
        {
          echo json_encode(array('status'=>0));
        }
      }
      else
      {
        echo json_encode(array('status'=>0));
      }
    }	
    else
    {
        echo json_encode(array('status'=>0));
    }	
				
	}
	
  /**********************************************************/
  function postDentistPreview() // Preview page  all data to show from tmp tables
  {      
    $access_token   = Request::header('Access-Token');    
    $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
    $dentist_id     = $user_dtls['doctor_id']; 

    $dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$dentist_id)->first()->toArray();  


    echo json_encode(array('status'=>1,'dentist_details'=>$dentist_details));
  }

  function postDentistPublish()  // Preview page Done  button Ajax
  {
    $access_token   = Request::header('Access-Token');    
    $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
    $dentist_id     = $user_dtls['doctor_id']; 

    Doctor::where('id', $dentist_id)->update(['publish_status'=>1]);
    echo json_encode(array('status'=>1));
  }

 // Check  Steps are empty or not. So that steps bubble set grey if not field values and green if value insert
  function getEmptyStepCheck() 
  {
    $access_token   = Request::header('Access-Token');  
    //print_r($access_token); exit;  
    $user_dtls      = DoctorLoginDetails::where('token',$access_token)->first();
    $dentist_id     = $user_dtls['doctor_id']; 

    $dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$dentist_id)->first();

    $step2 = $step3 = $step4 = $step5 = $step6 = $step7 = '';

    if($dentist_details !=NULL)
    {
      $dentist_details = $dentist_details->toArray(); 
    }
    else
    {
      $dentist_details = array(); 
    }
//print_r($dentist_details); exit;
    if(empty($dentist_details['docs_clinics']))
    {
      $step2 = 1;
    }
    if(empty($dentist_details['docs_education']) && empty($dentist_details['docs_award']) && empty($dentist_details['docs_insurance']))
    {
      $step3 = 1;
    }    
    if(empty($dentist_details['docs_license']))
    {
      $step4 = 1;
    }
    if(empty($dentist_details['docs_experiece']))
    {
      $step5 = 1;
    } 
    if(empty($dentist_details['docs_skill']))
    {
      $step6 = 1;
    }
    if(empty($dentist_details['docs_language']))
    {
      $step7 = 1;
    }

     echo json_encode(array('status'=>1,'step2'=>$step2,'step3'=>$step3,'step4'=>$step4,'step5'=>$step5,'step6'=>$step6,'step7'=>$step7));
  }

  function hello()
  {
    echo 1; exit;
  }
  public function get_patients_post()
  {
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         $patient_post_id = array();
         $keyword=(isset($user_response['keyword']) && !empty($user_response['keyword'])) ? $user_response['keyword']  : '';
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();
         
          $page=$user_response['page'];
          $perpage=$user_response['perpage'];
         // $page = ($pagenum - 1) * $perpage;
           
      $more_count=$page+$perpage;
         $more=0;

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_post  = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )
       ELSE posted_date END AS activity_date'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
       ->with('get_attachments')
       ->with('get_patient')
       ->where('patient_posts.patient_id','!=',$admin_post_details->id);
       if($keyword){
       $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
        $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
       $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');

           
       }
              $patient_post ->orderBy('activity_date','DESC');
               $patient_posts_count=$patient_post->get()->count();    
           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1');
                    if($keyword){
                        
                $qyer_var->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $qyer_var->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
               $qyer_var->orWhere('patients.name', 'like', '%' . $keyword. '%');
           
                }
               $qyer_var->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('activity_date','DESC');

               $patient_posts_count = $qyer_var->get()->count();
               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_post = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id);
         if($keyword){
                       $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
               $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');
       }
                $patient_post->orderBy('activity_date','DESC');
                           $patient_posts_count=$patient_post->get()->count();

                           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                
                   }
                }
          }
		

       
        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        
        //print_r($patient_posts);exit;
       if(!empty($patient_posts))
       {
              foreach($patient_posts as $v)
                {
                    $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])
                                        ->where('is_read','=',0)->where('doctor_content','=','')
                                        ->count();
                    
                    // Show only unread message ============== 04-06-2017


                    //$patient_post_arr = arra
                    $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->count();
                    if($chat_history_count > 0)
                    {
                        $chat_history = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->first()
                        ->toArray();

                        $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->get()
                        ->toArray();

                        $sent_time  =   $chat_history['sent_time'];
                        //$sent_time  =   date("Y-m-d g:i a",$chat_history['sent_time']);
                        /*if($sent_time == date("Y-m-d"))
                        {
                            $sent_time = date("g:i a",$chat_history['sent_time']);
                        }
                        else
                        {
                            $sent_time = date("Y-m-d",$chat_history['sent_time']);
                        }*/

                        $chat_history_arr   =   array(
                            'id'                    =>  $chat_history['id'],
                            'patient_id'            =>  $chat_history['patient_id'],
                            'doctor_id'             =>  $chat_history['doctor_id'],
                            'post_id'               =>  $chat_history['post_id'],
                            'patient_content'       =>  $chat_history['patient_content'],
                            'doctor_content'        =>  $chat_history['doctor_content'],
                            'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                        );
                        $is_attendant = '1';
                    }
                    else
                    {
                        $chat_history_arr = array();
                        $chat_history_list = array();
                        $is_attendant = '0';
                    }

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                                ->where('doctor_id','=',$doctor_id)
                                                ->where('post_id','=',$v['id'])
                                                ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'pain_level'            =>  $v['pain_level'],
                        'insurance'      =>  $v['insurance'],
                        'insurance_information' =>  $v['insurance_information'],
                        'last_cleaning'            =>  $v['last_cleaning'],
                        'src_syst'            =>  $v['src_syst'],
                        'emergency_raw'         =>  $emergency_raw,
                        'appointment'          =>  $is_appointment,
                        'is_status'          =>  $is_status,
                        'appointment_time'          =>  $appointment_time,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'posted_date'           =>  $v['posted_date'],
                        'posted_datetime'       =>  strtotime($v['posted_date']),
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'=>$unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  $is_attendant,

                    );


                }
       }
       
        if($patient_posts_count>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'total_count'=>$patient_posts_count,
            'more'=>$more
        );

        echo json_encode($response_arr);exit();
        
    }
  }
  public function get_patients_post_new()
  {
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         $patient_post_id = array();
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();
         
          $page=$user_response['page'];
          $perpage=$user_response['perpage'];
         // $page = ($pagenum - 1) * $perpage;
           $more_count=$page+$perpage;

         $more=0;

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_post  = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->orderBy('activity_date','DESC');
               $patient_posts_count=$patient_post->get()->count();    
           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1')
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('activity_date','DESC');

               $patient_posts_count = $qyer_var->get()->count();


               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_post = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))
                ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)
                ->orderBy('activity_date','DESC');
                           $patient_posts_count=$patient_post->get()->count();

                           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                
                   }
                }
          }
		

       
        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        
        //print_r($patient_posts);exit;
       if(!empty($patient_posts))
       {
              foreach($patient_posts as $v)
                {
                    $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])
                                        ->where('is_read','=',0)->where('doctor_content','=','')
                                        ->count();
                    
                    // Show only unread message ============== 04-06-2017


                    //$patient_post_arr = arra
                    $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->count();
                    if($chat_history_count > 0)
                    {
                        $chat_history = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->first()
                        ->toArray();

                        $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->get()
                        ->toArray();

                        $sent_time  =   $chat_history['sent_time'];
                        //$sent_time  =   date("Y-m-d g:i a",$chat_history['sent_time']);
                        /*if($sent_time == date("Y-m-d"))
                        {
                            $sent_time = date("g:i a",$chat_history['sent_time']);
                        }
                        else
                        {
                            $sent_time = date("Y-m-d",$chat_history['sent_time']);
                        }*/

                        $chat_history_arr   =   array(
                            'id'                    =>  $chat_history['id'],
                            'patient_id'            =>  $chat_history['patient_id'],
                            'doctor_id'             =>  $chat_history['doctor_id'],
                            'post_id'               =>  $chat_history['post_id'],
                            'patient_content'       =>  $chat_history['patient_content'],
                            'doctor_content'        =>  $chat_history['doctor_content'],
                            'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                        );
                        $is_attendant = '1';
                    }
                    else
                    {
                        $chat_history_arr = array();
                        $chat_history_list = array();
                        $is_attendant = '0';
                    }

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                                ->where('doctor_id','=',$doctor_id)
                                                ->where('post_id','=',$v['id'])
                                                ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'pain_level'            =>  $v['pain_level'],
                        'emergency_raw'         =>  $emergency_raw,
                        'appointment'          =>  $is_appointment,
                        'is_status'          =>  $is_status,
                        'appointment_time'          =>  $appointment_time,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'posted_date'           =>  $v['posted_date'],
                        'posted_datetime'       =>  strtotime($v['posted_date']),
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'=>$unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  $is_attendant,

                    );


                }
       }
       
        if($patient_posts_count>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'total_count'=>$patient_posts_count,
            'more'=>$more
        );

        echo json_encode($response_arr);exit();
        
    }
  }

  public function get_patients_post_old()
  {
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         $patient_post_id = array();
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->orderBy('activity_date','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1')
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('activity_date','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::select('patient_posts.*',DB::raw('CASE WHEN (SELECT count(1) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" )> 0 
       THEN (SELECT  max(FROM_UNIXTIME(sent_time,"%Y-%m-%d %H:%i:%s")) FROM `chat_histories` where post_id=patient_posts.id and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'")
       ELSE posted_date END AS activity_date'))
                ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)
                ->orderBy('activity_date','DESC')->get()->toArray();
                   }
                }
          }
		

       
        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        
        //print_r($patient_posts);exit;
       if(!empty($patient_posts))
       {
              foreach($patient_posts as $v)
                {
                    $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])
                                        ->where('is_read','=',0)->where('doctor_content','=','')
                                        ->count();
                    
                    // Show only unread message ============== 04-06-2017


                    //$patient_post_arr = arra
                    $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->count();
                    if($chat_history_count > 0)
                    {
                        $chat_history = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->first()
                        ->toArray();

                        $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                        ->where('doctor_id','=',$doctor_id)
                        ->where('patient_id','=',$v['patient_id'])
                        ->orderBy('id','DESC')
                        ->get()
                        ->toArray();

                        $sent_time  =   $chat_history['sent_time'];
                        //$sent_time  =   date("Y-m-d g:i a",$chat_history['sent_time']);
                        /*if($sent_time == date("Y-m-d"))
                        {
                            $sent_time = date("g:i a",$chat_history['sent_time']);
                        }
                        else
                        {
                            $sent_time = date("Y-m-d",$chat_history['sent_time']);
                        }*/

                        $chat_history_arr   =   array(
                            'id'                    =>  $chat_history['id'],
                            'patient_id'            =>  $chat_history['patient_id'],
                            'doctor_id'             =>  $chat_history['doctor_id'],
                            'post_id'               =>  $chat_history['post_id'],
                            'patient_content'       =>  $chat_history['patient_content'],
                            'doctor_content'        =>  $chat_history['doctor_content'],
                            'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                        );
                        $is_attendant = '1';
                    }
                    else
                    {
                        $chat_history_arr = array();
                        $chat_history_list = array();
                        $is_attendant = '0';
                    }

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                                ->where('doctor_id','=',$doctor_id)
                                                ->where('post_id','=',$v['id'])
                                                ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'pain_level'            =>  $v['pain_level'],
                        'emergency_raw'         =>  $emergency_raw,
                        'appointment'          =>  $is_appointment,
                        'is_status'          =>  $is_status,
                        'appointment_time'          =>  $appointment_time,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'posted_date'           =>  $v['posted_date'],
                        'posted_datetime'       =>  strtotime($v['posted_date']),
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'=>$unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  $is_attendant,

                    );


                }
       }
       

        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr
        );

        echo json_encode($response_arr);exit();
        
    }
  }
  public function get_attendant_post_new()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
        $patient_post_id = array();
        $doctor_id=$user_response['doctor_id'];
       // $doc_dts_chat = ChatHistory::select('post_id')->get()->toArray();

         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first(); 
         
        $page=(isset($user_response['page'])) ? $user_response['page']:'0';
         $perpage=(isset($user_response['perpage'])) ? $user_response['perpage'] : '15';
         $more_count=$page+$perpage;
         $more=0;

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_post  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
             ->with('get_attachments')->with('get_patient')
             ->join('chat_histories', 'chat_histories.post_id', '=', 'patient_posts.id')
            ->where('chat_histories.doctor_id','=',$doctor_id)
             ->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                
           $patient_posts_count=$patient_post->get()->count();    
           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();

         }
        else
        { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
                ->with('get_attachments')
                ->with('get_patient')
                ->join('chat_histories', 'chat_histories.post_id', '=', 'patient_posts.id')
            ->where('chat_histories.doctor_id','=',$doctor_id)
                ->where('patient_posts.is_active','=','1')
                ->whereIn('patient_posts.id',$doc_dts_chat)
                ->orderBy('sent_time','DESC')
                ->orderBy('patient_posts.id','DESC');

                $patient_posts_count = $qyer_var->get()->count();

               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }

                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_post = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
                                  ->join('chat_histories', 'chat_histories.post_id', '=', 'patient_posts.id')
            ->where('chat_histories.doctor_id','=',$doctor_id)
                     ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)
                        ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                        
                         $patient_posts_count=$patient_post->get()->count();

                           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                   }
                }
        }


        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_attendant_post = 0;
        
         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();

                //$patient_post_arr = arra
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                    $chat_history = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->first()
                    ->toArray();

                    $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->get()
                    ->toArray();

                    $sent_time  =   $chat_history['sent_time'];
                    //$sent_time  =   date("Y-m-d H:i:s",$chat_history['sent_time']);
                    /*if($sent_time == date("Y-m-d"))
                    {
                        $sent_time = date("g:i a",$chat_history['sent_time']);
                    }
                    else
                    {
                        $sent_time = date("Y-m-d",$chat_history['sent_time']);
                    }*/

                    $chat_history_arr   =   array(
                        'id'                    =>  $chat_history['id'],
                        'patient_id'            =>  $chat_history['patient_id'],
                        'doctor_id'             =>  $chat_history['doctor_id'],
                        'post_id'               =>  $chat_history['post_id'],
                        'patient_content'       =>  $chat_history['patient_content'],
                        'doctor_content'        =>  $chat_history['doctor_content'],
                        'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                    );

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$chat_history['patient_id'])
                                            ->where('doctor_id','=',$doctor_id)
                                            ->where('post_id','=',$v['id'])
                                            ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'pain_level'            =>  $v['pain_level'],
                        'posted_date'           =>  $v['posted_date'],
                        'appointment'           =>  $is_appointment,
                        'is_status'             =>  $is_status,
                        'appointment_time'      =>  $appointment_time,
                        'emergency_raw'         =>  $emergency_raw,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'    =>  $unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  '1'
                    );
                }
                else
                {
                    $chat_history_arr = array();
                }
            }
         }

         //echo '<pre>';print_r($patient_post_arr);exit;
      if($this->attendantpost_count($doctor_id,$keyword)>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_attendant_post'=>  $this->attendantpost_count($doctor_id,$keyword),
            'more'=>$more
        );

        echo json_encode($response_arr);exit();

    }
  }
  public function get_attendant_post()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
        $patient_post_id = array();
       $keyword=(isset($user_response['keyword']) && !empty($user_response['keyword'])) ? $user_response['keyword']  : '';
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first(); 
         
        $page=(isset($user_response['page'])) ? $user_response['page']:'0';
         $perpage=(isset($user_response['perpage'])) ? $user_response['perpage'] : '15';
         $more_count=$page+$perpage;
         $more=0;

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_post  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
             ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id);
            
            if(!empty($user_response['emergency'])){
                    if($user_response['emergency']==0){
                        $patient_post->where('patient_posts.emergency','=',0);
                    }else{
                        $patient_post->where('patient_posts.emergency','=',1);
                        }
                 }
                if($keyword){
                    if(!empty($user_response['emergency'])){
                        $patient_post->orWhere('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }else{
                        $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }
                    $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                   $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');
               
                    }

          /*if($keyword){
                $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');
         }*/
          
               $patient_post->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                
           $patient_posts_count=$patient_post->get()->count();    
           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();

         }
        else
        { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1');
            if($keyword){
                $qyer_var->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $qyer_var->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
               $qyer_var->orWhere('patients.name', 'like', '%' . $keyword. '%');
           
                }
               $qyer_var->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('sent_time','DESC')
                ->orderBy('patient_posts.id','DESC');

                $patient_posts_count = $qyer_var->get()->count();

               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }

                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_post = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                       ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id);
                  
                  if(!empty($user_response['emergency'])){
                    if($user_response['emergency']==0){
                        $qyer_var->where('patient_posts.emergency','=',0);
                    }else{
                        $qyer_var->where('patient_posts.emergency','=',1);
                        }
                 }
                if($keyword){
                    if(!empty($user_response['emergency'])){
                        $qyer_var->orWhere('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }else{
                        $qyer_var->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }
                    $qyer_var->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                   $qyer_var->orWhere('patients.name', 'like', '%' . $keyword. '%');
               
                    }     
                  
                  /*if($keyword){
                 $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                  $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');

                   }*/
                      $patient_post->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                        
                         $patient_posts_count=$patient_post->get()->count();

                           $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                   }
                }
        }


        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_attendant_post = 0;
        
         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();

                //$patient_post_arr = arra
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                    $chat_history = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->first()
                    ->toArray();

                    $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->get()
                    ->toArray();

                    $sent_time  =   $chat_history['sent_time'];
                    //$sent_time  =   date("Y-m-d H:i:s",$chat_history['sent_time']);
                    /*if($sent_time == date("Y-m-d"))
                    {
                        $sent_time = date("g:i a",$chat_history['sent_time']);
                    }
                    else
                    {
                        $sent_time = date("Y-m-d",$chat_history['sent_time']);
                    }*/

                    $chat_history_arr   =   array(
                        'id'                    =>  $chat_history['id'],
                        'patient_id'            =>  $chat_history['patient_id'],
                        'doctor_id'             =>  $chat_history['doctor_id'],
                        'post_id'               =>  $chat_history['post_id'],
                        'patient_content'       =>  $chat_history['patient_content'],
                        'doctor_content'        =>  $chat_history['doctor_content'],
                        'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                    );

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$chat_history['patient_id'])
                                            ->where('doctor_id','=',$doctor_id)
                                            ->where('post_id','=',$v['id'])
                                            ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'insurance'      =>  $v['insurance'],
                        'insurance_information' =>  $v['insurance_information'],
                        'last_cleaning'            =>  $v['last_cleaning'],
                        'src_syst'            =>  $v['src_syst'],
                        'pain_level'            =>  $v['pain_level'],
                        'posted_date'           =>  $v['posted_date'],
                        'appointment'           =>  $is_appointment,
                        'is_status'             =>  $is_status,
                        'appointment_time'      =>  $appointment_time,
                        'emergency_raw'         =>  $emergency_raw,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'    =>  $unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  '1'
                    );
                }
                else
                {
                    $chat_history_arr = array();
                }
            }
         }

         //echo '<pre>';print_r($patient_post_arr);exit;
      if($this->attendantpost_count($doctor_id,$keyword)>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_attendant_post'=>  $this->attendantpost_count($doctor_id,$keyword),
            'more'=>$more
        );

        echo json_encode($response_arr);exit();

    }
  }
  public function get_attendant_post_old()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
        $patient_post_id = array();

         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first(); 

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
             ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC')->get()->toArray();

         }
        else
        { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1')
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('sent_time','DESC')
                ->orderBy('patient_posts.id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))
                     ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)
                        ->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC')->get()->toArray();
                   }
                }
        }


        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_attendant_post = 0;
        
         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();

                //$patient_post_arr = arra
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                    $chat_history = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->first()
                    ->toArray();

                    $chat_history_list = ChatHistory::where('post_id','=',$v['id'])
                    ->where('doctor_id','=',$doctor_id)
                    ->where('patient_id','=',$v['patient_id'])
                    ->orderBy('id','DESC')
                    ->get()
                    ->toArray();

                    $sent_time  =   $chat_history['sent_time'];
                    //$sent_time  =   date("Y-m-d H:i:s",$chat_history['sent_time']);
                    /*if($sent_time == date("Y-m-d"))
                    {
                        $sent_time = date("g:i a",$chat_history['sent_time']);
                    }
                    else
                    {
                        $sent_time = date("Y-m-d",$chat_history['sent_time']);
                    }*/

                    $chat_history_arr   =   array(
                        'id'                    =>  $chat_history['id'],
                        'patient_id'            =>  $chat_history['patient_id'],
                        'doctor_id'             =>  $chat_history['doctor_id'],
                        'post_id'               =>  $chat_history['post_id'],
                        'patient_content'       =>  $chat_history['patient_content'],
                        'doctor_content'        =>  $chat_history['doctor_content'],
                        'sent_time'             =>  $sent_time//date("Y-m-d H:i:s",$chat_history['sent_time']),
                    );

                    if($v['emergency'] == '1')
                    {
                        $emergency_raw = "Emergency = Yes";
                    }
                    else
                    {
                        $emergency_raw = "Emergency = No"; 
                    }
                    $appointment = PatientDoctorAppointments::select('is_status','is_appointment','appointment_time')->where(array('patient_id'=>$v['patient_id'],'post_id'=>$v['id'],'doctor_id'=>$doctor_id))->first();
                    $is_appointment = 0;
                    $is_status = 0;
                    $appointment_time = "0000-00-00 00:00:00";
                    if($appointment){
                        $is_appointment = $appointment->is_appointment;
                        $is_status = $appointment->is_status;
                        $appointment_time = date("Y-m-d h:i A",strtotime($appointment->appointment_time));
                    }
                    $is_closed_chat = ClosedPostChat::where('patient_id','=',$chat_history['patient_id'])
                                            ->where('doctor_id','=',$doctor_id)
                                            ->where('post_id','=',$v['id'])
                                            ->count();

                    $patient_post_arr[] = array(
                        'post_id'               =>  $v['id'],
                        'patient_id'            =>  $v['patient_id'],
                        'current_location'      =>  $v['current_location'],
                        'emergency'             =>  $v['emergency'],
                        'pain_level'            =>  $v['pain_level'],
                        'posted_date'           =>  $v['posted_date'],
                        'appointment'           =>  $is_appointment,
                        'is_status'             =>  $is_status,
                        'appointment_time'      =>  $appointment_time,
                        'emergency_raw'         =>  $emergency_raw,
                        'post_title'            =>  $v['post_title'],
                        'description'           =>  $v['description'],
                        'is_active'             =>  $v['is_active'],
                        'is_closed_chat'        =>  $is_closed_chat,
                        'get_patient'           =>  $v['get_patient'],
                        'get_attachments'       =>  $v['get_attachments'],
                        'unread_chat_history_count'    =>  $unread_chat_history_count,
                        'chat_history_count'    =>  $chat_history_count,
                        'chat_history_arr'      =>  $chat_history_arr,
                        'chat_history_list'     =>  $chat_history_list,
                        'is_attendant'          =>  '1'
                    );
                }
                else
                {
                    $chat_history_arr = array();
                }
            }
         }

         //echo '<pre>';print_r($patient_post_arr);exit;

        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_attendant_post'    =>  $tot_attendant_post
        );

        echo json_encode($response_arr);exit();

    }
  }
  public function attendantpost_count($doctor_id=false,$keyword_text=false)
  {

         $patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$doctor_id)->first();
         $get_doc_dts = Doctor::where('id',$doctor_id)->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$doctor_id.'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
             ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id);
             
          if($keyword_text){
                $patient_posts->where('patient_posts.post_title', 'like', '%' . $keyword_text. '%');
                $patient_posts->orWhere('patient_posts.current_location', 'like', '%' . $keyword_text. '%');
                $patient_posts->orWhere('patients.name', 'like', '%' . $keyword_text. '%');
         }
            $patient_posts->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                
           $patient_posts=$patient_posts->get()->toArray();
         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$doctor_id)->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

           
               $qyer_var = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$doctor_id.'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                ->with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1');
            if($keyword_text){
                $qyer_var->where('patient_posts.post_title', 'like', '%' . $keyword_text. '%');
                $qyer_var->orWhere('patient_posts.current_location', 'like', '%' . $keyword_text. '%');
               $qyer_var->orWhere('patients.name', 'like', '%' . $keyword_text. '%');
           
                }
               $qyer_var->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('sent_time','DESC')
                ->orderBy('patient_posts.id','DESC');
                $patient_posts = $qyer_var->get()->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->join('patients', 'patient_posts.patient_id', '=', 'patients.id')->where('patient_id','=',$admin_post_details->id)->orderBy('patient_posts.id','DESC')->get()->toArray();
                     
                            $patient_posts = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$doctor_id.'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                       ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id);
                       
                  if($keyword_text){
                     $patient_posts->where('patient_posts.post_title', 'like', '%' . $keyword_text. '%');
                     $patient_posts->orWhere('patient_posts.current_location', 'like', '%' . $keyword_text. '%');
                    $patient_posts->orWhere('patients.name', 'like', '%' . $keyword_text. '%');

                   }
                      $patient_posts->orderBy('sent_time','DESC')->orderBy('patient_posts.id','DESC');
                    
                           $patient_posts=$patient_posts->get()->toArray();
                     
                   }
                }
          }



       
        $patient_post_arr = array();
        $tot_attendant_post = 0;

         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                //$patient_post_arr = arra
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                }

            }
         }

        return $tot_attendant_post;
 
  }
  
  
   public function upgrade_plan(){
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         //print_r($user_response); die;
        
            $plan = $user_response['plan'];
            $doctor_id = $user_response['doctor_id'];

            $doctor_details = DoctorDetails::where('doctor_id',$doctor_id)->update([
                                            'plan' => $plan,
                                            'updated_at'      => date('Y-m-d h:i:s'),
                                            'subscribe_date' => date('Y-m-d')
                                        ]);

            if($plan==0){
                $response_arr = array(
                'status'    =>  0
            );
            }else{
                $response_arr = array(
                'status'    =>  1
            );
            }
        echo json_encode($response_arr);exit();

    }
  }

   public function web_url_update(){
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         //print_r($user_response); die;
        
            $web_address = $user_response['web_address'];
            $doctor_id = $user_response['doctor_id'];

            $doctor_details = DoctorClinic::where('doctor_id',$doctor_id)->update([
                                            'web_address' => $web_address,
                                            'updated_at'      => date('Y-m-d h:i:s')
                                        ]);

            /*$response_arr = array(
               'status'    =>  1
            );*/
            if(empty($web_address)){
                $response_arr = array(
                    'status'    =>  0
                );
            }else{
                $response_arr = array(
                    'status'    =>  1
                );
            }
        echo json_encode($response_arr);exit();

    }
  }    
  
   public function tot_attendant_post_count()
  {
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         $patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','!=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('is_active','=','1')
                ->whereIn('id', $patient_post_id)
                ->orderBy('id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();
                   }
                }
          }



        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_attendant_post = 0;
        $emergency=0;
        $unemergency=0;
         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                //$patient_post_arr = arra
           
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                   if($v['emergency']==1){
                      $emergency++;  
                    }else{
                     $unemergency++;  
                     }
                }

            }
         }
        $response_arr = array(
            'status'    =>  '1',
            'tot_attendant_post'    =>  $tot_attendant_post,
            'emergency'=>$emergency,
            'unemergency'=>$unemergency
        );

        echo json_encode($response_arr);exit();
    }
  }
/*  public function tot_attendant_post_count()
  {
    if(Request::isMethod('post'))
    {
         $user_response = Request::all();
         $patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','!=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('is_active','=','1')
                ->whereIn('id', $patient_post_id)
                ->orderBy('id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();
                   }
                }
          }



        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_attendant_post = 0;

         if(!empty($patient_posts))
         {
            foreach($patient_posts as $v)
            {
                //$patient_post_arr = arra
                $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                ->where('doctor_id','=',$doctor_id)
                ->where('patient_id','=',$v['patient_id'])
                ->count();
                if($chat_history_count > 0)
                {
                    $tot_attendant_post++;
                }

            }
         }
        $response_arr = array(
            'status'    =>  '1',
            'tot_attendant_post'    =>  $tot_attendant_post
        );

        echo json_encode($response_arr);exit();
    }
  }*/

  public function get_unattendant_post_old()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
		$patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)->orderBy('patient_posts.id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1')
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('patient_posts.id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)->orderBy('patient_posts.id','DESC')->get()->toArray();
                   }
                }
          }

        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_unattendant_post = 0;

     if(!empty($patient_posts))
     {
        foreach($patient_posts as $v)
        {
             $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();
                                        
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;

                if($v['emergency'] == '1')
                {
                    $emergency_raw = "Emergency = Yes";
                }
                else
                {
                    $emergency_raw = "Emergency = No"; 
                }

                $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('post_id','=',$v['id'])
                                        ->count();

                $patient_post_arr[] = array(
                    'post_id'               =>  $v['id'],
                    'patient_id'            =>  $v['patient_id'],
                    'posted_date'           =>  $v['posted_date'],
                    'posted_datetime'       =>  strtotime($v['posted_date']),
                    'current_location'      =>  $v['current_location'],
                    'emergency'             =>  $v['emergency'],
                    'pain_level'            =>  $v['pain_level'],
                    'emergency_raw'         =>  $emergency_raw,
                    'post_title'            =>  $v['post_title'],
                    'description'           =>  $v['description'],
                    'is_active'             =>  $v['is_active'],
                    'is_closed_chat'            =>  $is_closed_chat,
                    'get_patient'           =>  $v['get_patient'],
                    'get_attachments'       =>  $v['get_attachments'],
                    'unread_chat_history_count'    =>  $unread_chat_history_count,
                    'chat_history_count'    =>  $chat_history_count,
                    'chat_history_arr'      =>  array(),
                    'chat_history_list'     =>  array(),
                    'is_attendant'          =>  '0'
                );
            }            
        }
     }


        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_unattendant_post'    =>  $tot_unattendant_post
        );

        echo json_encode($response_arr);exit();

    }
  }
  public function get_unattendant_post_new()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
		$patient_post_id = array();
		
        // $page=$user_response['page'];
        // $perpage=$user_response['perpage'];
                  $page=(isset($user_response['page'])) ? $user_response['page']:'0';
         $perpage=(isset($user_response['perpage'])) ? $user_response['perpage'] : '15';
         $more_count=$page+$perpage;
         $more=0;
         
        $doc_dts_chat = ChatHistory::select('post_id')->get()->toArray();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_post  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->whereNotIn('patient_posts.id',$doc_dts_chat)
                ->orderBy('patient_posts.id','DESC');
                $patient_posts_count = $patient_post->get()->count();
                $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('patient_posts.is_active','=','1')
                 >whereNotIn('patient_posts.id',$doc_dts_chat)
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('patient_posts.id','DESC');

             $patient_posts_count = $qyer_var->get()->count();


               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }
            
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','=',$admin_post_details->id)->whereNotIn('patient_posts.id',$doc_dts_chat)->orderBy('patient_posts.id','DESC');
                     
                     $patient_posts_count=$patient_post->get()->count();
                      $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                   }
                }
          }

        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_unattendant_post = 0;

     if(!empty($patient_posts))
     {
        foreach($patient_posts as $v)
        {
             $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();
                                        
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;

                if($v['emergency'] == '1')
                {
                    $emergency_raw = "Emergency = Yes";
                }
                else
                {
                    $emergency_raw = "Emergency = No"; 
                }

                $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('post_id','=',$v['id'])
                                        ->count();

                $patient_post_arr[] = array(
                    'post_id'               =>  $v['id'],
                    'patient_id'            =>  $v['patient_id'],
                    'posted_date'           =>  $v['posted_date'],
                    'posted_datetime'       =>  strtotime($v['posted_date']),
                    'current_location'      =>  $v['current_location'],
                    'emergency'             =>  $v['emergency'],
                    'pain_level'            =>  $v['pain_level'],
                    'emergency_raw'         =>  $emergency_raw,
                    'post_title'            =>  $v['post_title'],
                    'description'           =>  $v['description'],
                    'is_active'             =>  $v['is_active'],
                    'is_closed_chat'            =>  $is_closed_chat,
                    'get_patient'           =>  $v['get_patient'],
                    'get_attachments'       =>  $v['get_attachments'],
                    'unread_chat_history_count'    =>  $unread_chat_history_count,
                    'chat_history_count'    =>  $chat_history_count,
                    'chat_history_arr'      =>  array(),
                    'chat_history_list'     =>  array(),
                    'is_attendant'          =>  '0'
                );
            }            
        }
     }

     if($patient_posts_count>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_unattendant_post'  =>$patient_posts_count,
            'more'=>$more
        );
        echo json_encode($response_arr);exit();

    }
  }
  public function get_unattendant_post()
  {
    if(Request::isMethod('post'))
    {
		
        $user_response = Request::all();
		$patient_post_id = array();
		       $keyword=(isset($user_response['keyword']) && !empty($user_response['keyword'])) ? $user_response['keyword']  : '';
        // $page=$user_response['page'];
        // $perpage=$user_response['perpage'];
                  $page=(isset($user_response['page'])) ? $user_response['page']:'0';
         $perpage=(isset($user_response['perpage'])) ? $user_response['perpage'] : '15';
         $more_count=$page+$perpage;
         $more=0;
         
        $doc_dts_chat = ChatHistory::select('post_id')->get()->toArray();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             /*$patient_post  = PatientPost::with('get_attachments')->with('get_patient')->join('patients', 'patient_posts.patient_id', '=', 'patients.id')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->whereNotIn('patient_posts.id',$doc_dts_chat);*/
                 $patient_post  = PatientPost::select('patient_posts.*',DB::raw('(SELECT `sent_time` FROM `chat_histories` WHERE `chat_histories`.`post_id` = `patient_posts`.`id` and `chat_histories`.`doctor_id` = "'.$user_response['doctor_id'].'" ORDER BY  sent_time DESC LIMIT 1) as sent_time'))->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
             ->with('get_attachments')->with('get_patient')->where('patient_posts.patient_id','!=',$admin_post_details->id)
                ->whereNotIn('patient_posts.id',$doc_dts_chat);
                
                if(!empty($user_response['emergency'])){
                    if($user_response['emergency']==0){
                        $patient_post->where('patient_posts.emergency','=',0);
                    }else{
                        $patient_post->where('patient_posts.emergency','=',1);
                        }
                 }
                if($keyword){
                    if(!empty($user_response['emergency'])){
                        $patient_post->orWhere('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }else{
                        $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }
                    $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                   $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');
               
                    }
                $patient_post->orderBy('patient_posts.id','DESC');
                $patient_posts_count = $patient_post->get()->count();
                $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')->join('patients', 'patient_posts.patient_id', '=', 'patients.id')
                ->where('patient_posts.is_active','=','1');

                if(!empty($user_response['emergency'])){
                    if($user_response['emergency']==0){
                        $qyer_var->where('patient_posts.emergency','=',0);
                    }else{
                        $qyer_var->where('patient_posts.emergency','=',1);
                        }
                 }
                if($keyword){
                    if(!empty($user_response['emergency'])){
                        $qyer_var->orWhere('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }else{
                        $qyer_var->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                    }
                    $qyer_var->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                   $qyer_var->orWhere('patients.name', 'like', '%' . $keyword. '%');
               
                    }
                $qyer_var->whereNotIn('patient_posts.id',$doc_dts_chat)
                ->whereIn('patient_posts.id', $patient_post_id)
                ->orderBy('patient_posts.id','DESC');

             $patient_posts_count = $qyer_var->get()->count();


               if($patient_posts_count > 0)
                {
                  // $patient_posts = $qyer_var->toArray();
                  $patient_posts=$qyer_var->offset($page)->limit($perpage)->get()->toArray();
                }
            
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_post = PatientPost::with('get_attachments')->with('get_patient')->join('patients', 'patient_posts.patient_id', '=', 'patients.id')->where('patient_posts.patient_id','=',$admin_post_details->id)->whereNotIn('patient_posts.id',$doc_dts_chat);
                                    if($keyword){
                $patient_post->where('patient_posts.post_title', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patient_posts.current_location', 'like', '%' . $keyword. '%');
                $patient_post->orWhere('patients.name', 'like', '%' . $keyword. '%');
              }
                     $patient_post->orderBy('patient_posts.id','DESC');
                     
                     $patient_posts_count=$patient_post->get()->count();
                      $patient_posts=$patient_post->offset($page)->limit($perpage)->get()->toArray();
                   }
                }
          }

        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_unattendant_post = 0;

     if(!empty($patient_posts))
     {
        foreach($patient_posts as $v)
        {
             $unread_chat_history_count = ChatHistory::where('post_id','=',$v['id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('patient_id','=',$v['patient_id'])->where('doctor_content','=','')
                                        ->where('is_read','=',0)
                                        ->count();
                                        
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;

                if($v['emergency'] == '1')
                {
                    $emergency_raw = "Emergency = Yes";
                }
                else
                {
                    $emergency_raw = "Emergency = No"; 
                }

                $is_closed_chat = ClosedPostChat::where('patient_id','=',$v['patient_id'])
                                        ->where('doctor_id','=',$doctor_id)
                                        ->where('post_id','=',$v['id'])
                                        ->count();

                $patient_post_arr[] = array(
                    'post_id'               =>  $v['id'],
                    'patient_id'            =>  $v['patient_id'],
                    'posted_date'           =>  $v['posted_date'],
                    'posted_datetime'       =>  strtotime($v['posted_date']),
                    'current_location'      =>  $v['current_location'],
                    'emergency'             =>  $v['emergency'],
                    'insurance'      =>  $v['insurance'],
                    'insurance_information' =>  $v['insurance_information'],
                    'last_cleaning'            =>  $v['last_cleaning'],
                     'src_syst'            =>  $v['src_syst'],
                    'pain_level'            =>  $v['pain_level'],
                    'emergency_raw'         =>  $emergency_raw,
                    'post_title'            =>  $v['post_title'],
                    'description'           =>  $v['description'],
                    'is_active'             =>  $v['is_active'],
                    'is_closed_chat'            =>  $is_closed_chat,
                    'get_patient'           =>  $v['get_patient'],
                    'get_attachments'       =>  $v['get_attachments'],
                    'unread_chat_history_count'    =>  $unread_chat_history_count,
                    'chat_history_count'    =>  $chat_history_count,
                    'chat_history_arr'      =>  array(),
                    'chat_history_list'     =>  array(),
                    'is_attendant'          =>  '0'
                );
            }            
        }
     }

     if($patient_posts_count>$more_count){
             $more=1;
            } 
        $response_arr = array(
            'status'    =>  '1',
            'patient_posts' =>  $patient_post_arr,
            'tot_unattendant_post'  =>$patient_posts_count,
            'more'=>$more
        );
        echo json_encode($response_arr);exit();

    }
  }
  
   public function unattendantpost_count($doctor_id=false)
  {
  
     
		$patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$doctor_id)->first();
         $get_doc_dts = Doctor::where('id',$doctor_id)->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','!=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$doctor_id)->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('is_active','=','1')
                ->whereIn('id', $patient_post_id)
                ->orderBy('id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();
                   }
                }
          }


     
        $patient_post_arr = array();
        $tot_unattendant_post = 0;

       if(!empty($patient_posts))
       {
          foreach($patient_posts as $v)
          {
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;
            }

         }
       }
       

     return $tot_unattendant_post;
      

   
  }
public function tot_unattendant_post_count()
  {
    if(Request::isMethod('post'))
    {
        $user_response = Request::all();
		$patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','!=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('is_active','=','1')
                ->whereIn('id', $patient_post_id)
                ->orderBy('id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();
                   }
                }
          }


        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_unattendant_post = 0;
        $emergency=0;
        $unemergency=0;
       if(!empty($patient_posts))
       {
          foreach($patient_posts as $v)
          {
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;
                if($v['emergency']==1){
                      $emergency++;  
                    }else{
                     $unemergency++;  
                     }
            }

         }
       }
       

        $response_arr = array(
            'status'    =>  '1',
            'tot_unattendant_post'    =>  $tot_unattendant_post,
             'emergency'=>$emergency,
            'unemergency'=>$unemergency
        );

        echo json_encode($response_arr);exit();
    }
  }  
/*  public function tot_unattendant_post_count()
  {
    if(Request::isMethod('post'))
    {
        $user_response = Request::all();
		$patient_post_id = array();
        
         $doc_dts = DoctorDetails::select('email')->where('doctor_id',$user_response['doctor_id'])->first();
         $get_doc_dts = Doctor::where('id',$user_response['doctor_id'])->first();
         $dos_active_status = $get_doc_dts->is_active;
         $admin_post_details = Patient::where('email','=','welcomenotification@dentalchat.com')->first();

        if($doc_dts->email == 'dentist@dentalchat.com')
         {
             $patient_posts  = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','!=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();

         }
        else
         { 
               $dentist_post_details = DoctorNotification::select('patient_post_id','doctor_id')->where('doctor_id','=',$user_response['doctor_id'])->get()->toArray();
                 
                 if(!empty($dentist_post_details))
                 {
                     foreach ($dentist_post_details as $dentist_post_detail) 
                      {
                        $patient_post_id[] = $dentist_post_detail['patient_post_id'];
                      }
                 }

                $qyer_var = PatientPost::with('get_attachments')
                ->with('get_patient')
                ->where('is_active','=','1')
                ->whereIn('id', $patient_post_id)
                ->orderBy('id','DESC')->get();

               $patient_posts = $qyer_var->count();


               if($patient_posts > 0)
                {
                   $patient_posts = $qyer_var->toArray();
                }
                else
                {
                  if($dos_active_status == 0)
                   {
                     $patient_posts = PatientPost::with('get_attachments')->with('get_patient')->where('patient_id','=',$admin_post_details->id)->orderBy('id','DESC')->get()->toArray();
                   }
                }
          }


        $doctor_id  = $user_response['doctor_id'];
        $patient_post_arr = array();
        $tot_unattendant_post = 0;

       if(!empty($patient_posts))
       {
          foreach($patient_posts as $v)
          {
            //$patient_post_arr = arra
            $chat_history_count = ChatHistory::where('post_id','=',$v['id'])
            ->where('doctor_id','=',$doctor_id)
            ->where('patient_id','=',$v['patient_id'])
            ->count();
            if($chat_history_count == 0)
            {
                $tot_unattendant_post++;
            }

         }
       }
       

        $response_arr = array(
            'status'    =>  '1',
            'tot_unattendant_post'    =>  $tot_unattendant_post
        );

        echo json_encode($response_arr);exit();
    }
  }*/

  public function list_doctor_details()
  {
    if(Request::isMethod('post'))
    {
        $user_response = Request::all();
        $doctor_id  = $user_response['doctor_id'];

        $doctor_awards = DoctorAward::where('doctor_id','=',$doctor_id)->get();
        $doctor_clinics = DoctorClinic::where('doctor_id','=',$doctor_id)->get();
        $doctor_details = DoctorDetails::where('doctor_id','=',$doctor_id)->get();
        $doctor_educations = DoctorEducation::where('doctor_id','=',$doctor_id)->get();
        $doctor_experiences = DoctorExperience::where('doctor_id','=',$doctor_id)->get();
        $doctor_insurances = DoctorInsurance::where('doctor_id','=',$doctor_id)->get();
        $doctor_languages = DoctorLanguage::where('doctor_id','=',$doctor_id)->get();
        $doctor_licenses = DoctorLicense::where('doctor_id','=',$doctor_id)->get();
        $doctor_skill = DoctorSkill::where('doctor_id','=',$doctor_id)->get();
        $doctor_review_ratings = DoctorReviewRating::with('get_patient_details')->where(['doctor_id'=>$doctor_id,'is_hide'=>'0'])->get();

        $doctor_avg_rating = DoctorReviewRating::where(['doctor_id'=>$doctor_id,'is_hide'=>'0'])
                                                ->avg('rating');

        $response_arr = array(
            'status'                        =>  '1',
            'doctor_awards'                 =>  $doctor_awards,
            'doctor_clinics'                =>  $doctor_clinics,
            'doctor_details'                =>  $doctor_details,
            'doctor_educations'             =>  $doctor_educations,
            'doctor_experiences'            =>  $doctor_experiences,
            'doctor_insurances'             =>  $doctor_insurances,
            'doctor_languages'              =>  $doctor_languages,
            'doctor_licenses'               =>  $doctor_licenses,
            'doctor_review_ratings'         =>  $doctor_review_ratings,
            'doctor_avg_rating'             =>  floor($doctor_avg_rating),
            'doctor_skill'                  =>  $doctor_skill,
        );

        echo json_encode($response_arr);exit();
    }
  }
/*start code by covetus 4-9-2017*/
    public function get_barcharts_data(){ 
        $doctor_id = Request::get('doctor_id',0);
        $type = Request::get('type','');
        if(!empty($doctor_id)){
            $doc_dts = DoctorDetails::join('doctors','doctor_details.doctor_id','=','doctors.id')->select('email','is_active')->where('doctor_id',$doctor_id)->first();
            switch ($type) {
                case '1':
                    $formatstring = DB::raw("str_to_date(concat(yearweek(posted_date), ' monday'), '%X%V %W') as formatted_dob");
                    $word_post_relation_string = ' patient_posts.posted_date BETWEEN DATE_SUB(now(), INTERVAL 30 DAY) AND now()';
                    break;
                case '2':
                    $word_post_relation_string = ' MONTH(patient_posts.`posted_date`) = MONTH(CURRENT_DATE()) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                case '3':
                    $word_post_relation_string = ' QUARTER(patient_posts.`posted_date`) = QUARTER(CURRENT_DATE()) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                case '4':
                    $word_post_relation_string = ' YEAR(patient_posts.`posted_date`) = YEAR(CURRENT_DATE()) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                case '5':
                    $word_post_relation_string = ' YEAR(patient_posts.`posted_date`) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(patient_posts.posted_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                case '6':
                    $word_post_relation_string = ' YEAR(patient_posts.`posted_date`) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND QUARTER(patient_posts.posted_date) = QUARTER(CURRENT_DATE - INTERVAL 1 QUARTER) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                case '7':
                    $word_post_relation_string = ' YEAR(patient_posts.`posted_date`) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(patient_posts.posted_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ';
                    $formatstring =  DB::raw("DATE_FORMAT(posted_date, '%b/%Y') as formatted_dob");
                    break;
                default:
                    $word_post_relation_string = ' MONTH(patient_posts.`posted_date`) = MONTH(CURRENT_DATE()) ';
                    $formatstring = DB::raw("DATE_FORMAT(posted_date, '%Y') as formatted_dob");
            }
            $word_post_relation = [];
            if($doc_dts->email == 'dentist@dentalchat.com')
            {
                $word_post_relation = DB::select("SELECT dental_word.word, COUNT(patient_posts.id) as total FROM `patient_posts` INNER JOIN dental_word ON patient_posts.post_title LIKE CONCAT('%', dental_word.word, '%') OR patient_posts.description LIKE CONCAT('%', dental_word.word, '%') WHERE dental_word.symtom=1 and patient_posts.is_active=1 and ".$word_post_relation_string."  GROUP BY word limit 10");
                //$patient_posts = PatientPost::select(DB::raw("count(patient_posts.id) as count"),'emergency',$formatstring)->join('patients','patient_posts.patient_id','=','patients.id')->where('email','!=','welcomenotification@dentalchat.com')->orderBy('formatted_dob','ASC')->groupBy('formatted_dob')->groupBy('emergency')->get();
                $patient_posts = PatientPost::select(DB::raw("count(patient_posts.id) as total, sum(case when chat_histories.id is null then 0 else 1 end) as count_notnull, sum(case when chat_histories.id is null then 1 else 0 end) as count_null"),'emergency')->join('patients','patient_posts.patient_id','=','patients.id')->leftJoin('chat_histories','chat_histories.patient_id','=','patient_posts.id')->whereRaw($word_post_relation_string)->where(array('patient_posts.is_active'=>'1'))->where('email','!=','welcomenotification@dentalchat.com')->groupBy('emergency')->get();
            }
            else
            { 
                $word_post_relation = DB::select("SELECT dental_word.word, COUNT(patient_posts.id) as total FROM `patient_posts` INNER JOIN dental_word ON patient_posts.post_title LIKE CONCAT('%', dental_word.word, '%') OR patient_posts.description LIKE CONCAT('%', dental_word.word, '%') JOIN doctor_notifications ON patient_posts.id=doctor_notifications.patient_post_id WHERE dental_word.symtom=1 and doctor_notifications.doctor_id = ".$doctor_id." and patient_posts.is_active=1 and ".$word_post_relation_string." GROUP BY word limit 10");
                //$qyer_var = PatientPost::select(DB::raw("count(patient_posts.id) as count"),'emergency',$formatstring)->join('doctor_notifications','patient_posts.id','=','doctor_notifications.patient_post_id')->where(array('doctor_id'=>$doctor_id,'is_active'=>'1'))->orderBy('formatted_dob','ASC')->groupBy('formatted_dob')->groupBy('emergency')->get();
                $qyer_var = PatientPost::select(DB::raw("count(patient_posts.id) as total, sum(case when chat_histories.id is null then 0 else 1 end) as count_notnull, sum(case when chat_histories.id is null then 1 else 0 end) as count_null"),'emergency')->join('doctor_notifications','patient_posts.id','=','doctor_notifications.patient_post_id')->leftJoin('chat_histories','chat_histories.patient_id','=','patient_posts.id')->whereRaw($word_post_relation_string)->where(array('doctor_notifications.doctor_id'=>$doctor_id,'patient_posts.is_active'=>'1'))->groupBy('emergency')->get();
                $patient_posts = $qyer_var->count();
                if($patient_posts > 0)
                {
                    $patient_posts = $qyer_var;
                }
                else
                {
                    if($doc_dts->is_active == 0)
                    {
                        //$patient_posts = PatientPost::select(DB::raw("count(patient_posts.id) as count"),$formatstring)->join('patients','patient_posts.patient_id','=','patients.id')->where('email','=','welcomenotification@dentalchat.com')->orderBy('formatted_dob','ASC')->groupBy('formatted_dob')->groupBy('emergency')->get();
                        $patient_posts = PatientPost::select(DB::raw("count(patient_posts.id) as total, sum(case when chat_histories.id is null then 0 else 1 end) as count_notnull, sum(case when chat_histories.id is null then 1 else 0 end) as count_null"),'emergency')->join('patients','patient_posts.patient_id','=','patients.id')->leftJoin('chat_histories','chat_histories.patient_id','=','patient_posts.id')->whereRaw($word_post_relation_string)->where(array('patient_posts.is_active'=>'1'))->where('email','=','welcomenotification@dentalchat.com')->groupBy('emergency')->get();
                    }
                }
            }
            $data = [];
            $labels = [];
            $progressbar = ['emergency_total'=>0,'emergency_attended_total'=>0,'emergency_unattended_total'=>0,
                            'nonemergency_total'=>0,'nonemergency_attended_total'=>0,'nonemergency_unattended_total'=>0];
            if($patient_posts){
                foreach($patient_posts as $key => $val){
                    if($val->emergency){
                        $progressbar['emergency_total']                 +=   $val->total;
                        $progressbar['emergency_attended_total']        +=   $val->count_notnull;
                        $progressbar['emergency_unattended_total']      +=   $val->count_null;
                    }else{
                        $progressbar['nonemergency_total']              +=   $val->total;
                        $progressbar['nonemergency_attended_total']     +=   $val->count_notnull;
                        $progressbar['nonemergency_unattended_total']   +=   $val->count_null;
                    }
                }
            }
            
            if($word_post_relation){
                foreach($word_post_relation as $key => $val){
                    $data[] = $val->total;
                    $labels[] = $val->word;
                }
            }
            if(empty($data)){
                $data[] = 0;
            }
            if(empty($labels)){
                $labels[] = 'No Data';
            }
            
            $chartdata = ['data'=>$data,'labels'=>$labels];
            $res = array('status'=>1,'msg'=>'Data found', 'chartdata'=>$chartdata,'progressbar'=>$progressbar);
        }else{
            $res = array('status'=>0,'msg'=>'Data were not found');
        }
        echo json_encode($res);
        exit();
    }
/*end code by covetus 4-9-2017*/

    /* start code by covetus 6-9-2017*/
    ########### Doctors Response againts to appointment request --STARTS##########
    public function set_appointment_doctors()
    {
        $patient_id = Request::get('patient_id');
        $doctor_id = Request::get('doctor_id');
        $post_id = Request::get('post_id');
        $is_status = Request::get('is_status');
        $proposedate = Request::get('proposedate');
        $proposedate = date("Y-m-d H:i:s",strtotime($proposedate));
        
        $appointment = PatientDoctorAppointments::where(array('doctor_id'=>$doctor_id,'patient_id'=>$patient_id,'post_id'=>$post_id))->count();
        $current_date = date("Y-m-d H:i:s");
        
        if(!empty($appointment)){
            $where = [];
            $msg = 'Appointment has been cancelled successfully.';
            if($is_status==1){
                $where = ['is_status'=> $is_status,'appointment_time'=>$proposedate,'updated_at'=>$current_date];
                $msg = 'Appointment successfully Proposed to the Patient';
            }else if($is_status==2){
                $where = ['is_appointment'=>'0','is_status'=> '0','appointment_time'=>"0000-00-00 00:00:00",'updated_at'=>$current_date];
                $msg = 'Appointment has been re-enabled.';
            }
            PatientDoctorAppointments::where(array('doctor_id'=>$doctor_id,'patient_id'=>$patient_id,'post_id'=>$post_id))->update($where);
            
            $response_arr = array(
                'status'    =>  '1',
                'msg'       =>  $msg,
                'is_status' => $is_status,
                'proposedate' => date("Y-m-d h:i A",strtotime($proposedate))
            );
        }else{
            $response_arr = array(
                'status'    =>  '0',
                'msg'       =>  'Appointment were not found.',
            );
        }
        
        echo json_encode($response_arr);
        exit(0);
    }
    ###########  Doctors Response againts to appointment request --ENDS##########
    /* end code by covetus 6-9-2017 */
    
    /* start code by covetus 7-11-2017*/
    public function read_chat_dentist(){
        $request_details = Request::all();
        $doctor_id = $request_details['doctor_id'];
        $post_id = $request_details['post_id'];

        ChatHistory::where(['doctor_id'=>$doctor_id,'post_id'=>$post_id,'doctor_content'=>''])
        				->update(['is_read'	=>	'1']);

    	$response_arr = array(
    		'status'	=>	'1',
    		'msg'		=>	"You read the message successfully."
		);
		echo json_encode($response_arr);
		exit();
    }
    /* end code by covetus 7-11-2017*/
    /* start by anil 15-12-2017 *************************/
    public function getUnverifieduesr(){
        
		$user_dtls 		= Doctor::with('docs_details','docs_clinics')->where('email_status',0)->where('email_activate_remind','=',0)->get()->toArray();
        if(!empty($user_dtls)){
            
		foreach($user_dtls as $user_dtl){
		    
		   $doctor_id = $user_dtl['docs_details']['doctor_id'];
	       $email = $user_dtl['docs_details']['email'];
		   $name = $user_dtl['docs_details']['first_name'].' '.$user_dtl['docs_details']['last_name'];
		   $business_name = $user_dtl['docs_clinics']['business_name'];
		   
           Doctor::where('id', $doctor_id)->update(['email_activate_remind'=>1]);
           
     /******************************** email send ***********************************/
      
  		$this->mailsend($doctor_id,'Your DentalChat.com email Id verification is still pending', $email, $name ,$user_dtl['docs_details']['first_name'],$user_dtl['docs_details']['last_name'], 'unactivateusercron' , $business_name);	

      /******************************** Email End here*******************************/ 
           

		}
      }
    }
  public function demailunsubscribe(){
      
      	$data = Request::all();
	   $doctor_id = $data['dentist_id'];
      
     DoctorDetails::where('doctor_id', $doctor_id)->update(['email_unsubscribe'=>1]);
    
  }
  public function getUncomprofile(){
      
       $get_all_dentist=Doctor::select('id')->get()->toArray();  
         foreach($get_all_dentist as $dentistids){
             $dentist_id=$dentistids['id'];
         $dentist_details = Doctor::with('docs_details','docs_clinics','docs_education','docs_award','docs_insurance','docs_experiece','docs_license','docs_skill','docs_language')->where('id',$dentist_id)->first();

    $step2 = $step3 = $step4 = $step5 = $step6 = $step7 = '';
    $profile_percent=14.28;
    if($dentist_details !=NULL)
    {
      $dentist_details = $dentist_details->toArray(); 
    }
    else
    {
      $dentist_details = array(); 
    }
    if(empty($dentist_details['docs_clinics']))
    {
      $step2 = 1;
      $profile_percent = $profile_percent +14.28;
    }
    if(empty($dentist_details['docs_education']) && empty($dentist_details['docs_award']) && empty($dentist_details['docs_insurance']))
    {
      $step3 = 1;
      $profile_percent = $profile_percent +14.28;

    }    
    if(empty($dentist_details['docs_license']))
    {
      $step4 = 1;
      $profile_percent = $profile_percent +14.28;

    }
    if(empty($dentist_details['docs_experiece']))
    {
      $step5 = 1;
     $profile_percent = $profile_percent +14.28;

    } 
    if(empty($dentist_details['docs_skill']))
    {
      $step6 = 1;
     $profile_percent = $profile_percent +14.28;

    }
    if(empty($dentist_details['docs_language']))
    {
      $step7 = 1;
     $profile_percent = $profile_percent +14.28;

    }
     $dname=$dentist_details['docs_details']['first_name'].' '.$dentist_details['docs_details']['last_name'];
     $demail=$dentist_details['docs_details']['email'];
     $business=$dentist_details['docs_clinics']['business_name'];
     

     if($profile_percent<75){
          echo json_encode(array('status'=>1,'step2'=>$step2,'step3'=>$step3,'step4'=>$step4,'step5'=>$step5,'step6'=>$step6,'step7'=>$step7,'profile_completeness'=>$profile_percent,'email'=>$demail,'name'=>$dname,'businessname'=>$business));
          
    $this->mailsend($dentist_id,'Complete your DentalChat Profile, attract more new patients ', $demail, $dname ,$dentist_details['docs_details']['first_name'],$dentist_details['docs_details']['last_name'], 'uncompleteprofilcron' , $business);	
     }
     
         }
  }

    private function mailsend($user_id=false,$subject= false, $email = false,$name=false, $fname = false,$lname = false, $mailtemplate = false, $business_name= false,$cc = false){
             
  $url = 'http://outbound.dentalchat.com/mailsend';
  
   try{
        $post = array(
        'user_id'=>$user_id,
        'name' => $name,
        'firstname'=>$fname,
        'lastname'=>$lname,
        'business_name'=>$business_name,
        'template_name'=>$mailtemplate,
        'email'=>$email,
        'subject'=>$subject
        );
    if($cc){
        $post['cc']=1;
    }   
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false
        ));
       
        $result=curl_exec ($ch);
        curl_close ($ch);
         }
         catch(Exception $ex){
             //echo $ex->getMessage();
         }
    }
    /* End here Anil ************************************/

    public function dentist_popup(){
        
        if(Request::isMethod('post'))
        {
            $user_response = Request::all();
            $popup_status = $user_response['popup_status'];
            $doctor_id = $user_response['doctor_id'];

            $doctor = Doctor::where('id',$doctor_id)->update([
                                            'popup_status' => $popup_status,
                                            'updated_at'      => date('Y-m-d h:i:s')
                                        ]);


            $response_arr = array(
                'status'    =>  '1'
            );

        echo json_encode($response_arr);exit();
        }

    }
}


    