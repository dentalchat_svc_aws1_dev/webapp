<?php
/**
* @package WordPress
* @subpackage medicaldirectory
* @since 1.0
*/
if(!function_exists('medicaldirectory_log')){
  function medicaldirectory_log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}
/*Create My Story category*/
function create_my_story_category_post_type() {
    
    /* Post Custom Taxonomy My story */
    register_taxonomy('mystory-categories', 'mystory', array(
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        'query_var' => 'mystories',
        'public' => true,
        'show_ui' => null,
        'show_tagcloud' => null,
        '_builtin' => false,
        'labels' => array(
            'name' => _x('Category', 'taxonomy general name'),
            'singular_name' => _x('Category', 'taxonomy singular name'),
            'search_items' => __('Search Categories'),
            'all_items' => __('All Categories'),
            'edit_item' => __('Edit Categories'),
            'view_item' => __('View Categories'),
            'update_item' => __('Update Categories'),
            'add_new_item' => __('Add New Categories'),
            'new_item_name' => __('New Categories')),
        'capabilities' => array(),
        'show_in_nav_menus' => null,
        'label' => __('Categories'),
        'sort' => true,
        'args' => array('orderby' => 'term_order'))
    );    
    $labels = array(
        'name'               => _x( 'My Stories', 'Post type general name' ),
        'singular_name'      => _x( 'My Story', 'Post type singular name' ),
        'menu_name'          => _x( 'My Story', 'Admin menu' ),
        'name_admin_bar'     => _x( 'My Story', 'Add new on admin bar' ),
        'add_new'            => _x( 'Add New', 'Story' ),
        'add_new_item'       => __( 'Add New Story' ),
        'new_item'           => __( 'New Story' ),
        'edit_item'          => __( 'Edit Story' ),
        'view_item'          => __( 'View Story' ),
        'all_items'          => __( 'All Stories' ),
        'search_items'       => __( 'Search Stories' ),
        'parent_item_colon'  => __( 'Parent Stories:' ),
        'not_found'          => __( 'No books found.' ),
        'not_found_in_trash' => __( 'No books found in Trash.' )
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'mystory' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'menu_icon'          => 'dashicons-testimonial'
    );
    register_post_type( 'mystory', $args );
}
add_action('init', 'create_my_story_category_post_type');

/*Add Category Details*/
add_action( 'mystory-categories_edit_form_fields', 'extra_edit_tax_fields_mystory', 15, 2 );
add_action( 'mystory-categories_add_form_fields', 'extra_add_tax_fields_mystory', 10, 2 );
add_action( 'edited_mystory-categories', 'save_extra_taxonomy_fields_mystory', 10, 2 );
add_action( 'create_mystory-categories', 'save_extra_taxonomy_fields_mystory', 10, 2 );   

// Edit existing taxonomy
function extra_edit_tax_fields_mystory( $tag ) 
{
    // Check for existing taxonomy meta for term ID.
    $t_id = $tag->term_id;
    $term_meta = get_option( "mystory-categories_$t_id" ); 
   ?>
    <tr class="form-field">
    <th scope="row" valign="top">
        <label for="cat_img_size"><?php _e( 'Image Size' ); ?></label>
    </th>
        <td>
            <input type="text" name="term_meta[img]" id="term_meta[img]" value="<?php echo esc_attr( $term_meta['img'] ) ? esc_attr( $term_meta['img'] ) : ''; ?>">
            <p class="description"><?php _e( 'Enter the size of image used for this category(Size in:MB)' ); ?></p>
        </td>
    </tr>
    <?php
}

// Add taxonomy page
function extra_add_tax_fields_mystory( $tag ) 
{
    ?>
    <div class="form-field">
        <label for="cat_img_size"><?php _e( 'Image Size' ); ?></label>
        <input type="text" name="term_meta[img]" id="term_meta[img]" value="">
        <p class="description"><?php _e( 'Enter the size of image used for this category(Size in:MB)' ); ?></p>
    </div>
    <?php
}

// Save extra taxonomy fields callback function.
function save_extra_taxonomy_fields_mystory( $term_id ) 
{
    if ( isset( $_POST['term_meta'] ) ) 
    {
        $t_id = $term_id;
        $term_meta = get_option( "mystory-categories_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
        foreach ( $cat_keys as $key ) 
        {
            if ( isset ( $_POST['term_meta'][$key] ) )
                $term_meta[$key] = $_POST['term_meta'][$key];
        }
        // Save the option array.
        update_option( "mystory-categories_$t_id", $term_meta );
    }
}  

add_action( 'admin_menu', 'add_pending_bubble' );

function add_pending_bubble() {
    global $menu;
    $custom_post_count = wp_count_posts('mystory');
    $custom_post_pending_count = $custom_post_count->draft;

    if ( $custom_post_pending_count ) {
        foreach ( $menu as $key => $value ) {
            if ( $menu[$key][2] == 'edit.php?post_type=mystory' ) {
                $menu[$key][0] .= ' <span class="update-plugins count-' . $custom_post_pending_count . '"><span class="plugin-count">' . $custom_post_pending_count . '</span></span>';
                return;
            }
        }
    }
}
add_filter( 'bulk_actions-edit-mystory', 'register_my_bulk_actions' );
 
function register_my_bulk_actions($bulk_actions) {
  $bulk_actions['approve_the_story'] = __( 'Approve Story', 'approve_the_story');
  return $bulk_actions;
}

add_filter( 'handle_bulk_actions-edit-mystory', 'my_bulk_action_handler', 10, 3 );
 
function my_bulk_action_handler( $redirect_to, $doaction, $post_ids ) {
  if ( $doaction !== 'approve_the_story' ) {
    return $redirect_to;
  }
  if(!empty($post_ids)){
  foreach ( $post_ids as $post_id ) {
    //echo '<pre>'; print_r($post_id);echo '</pre>';
       wp_update_post( array(
			'ID'          => $post_id,
			'post_status' => 'publish',
    )); 
  }   
 }
 $redirect_to = add_query_arg( 'bulk_approve_the_story', count( $post_ids ), $redirect_to );
 return $redirect_to;
}
add_action( 'admin_notices', 'my_bulk_action_admin_notice' );
 
function my_bulk_action_admin_notice() {
  if ( ! empty( $_REQUEST['bulk_approve_the_story'] ) ) {
    $emailed_count = intval( $_REQUEST['bulk_approve_the_story'] );
    printf( '<div id="message" class="updated notice is-dismissible">' .
      _n( 'Approved %s posts Succesfully.',
        'Approved %s posts Succesfully.',
        $emailed_count,
        'approve_the_story'
      ) . '</div>', $emailed_count );
  }
}


function audio_video_add_meta_box() {
        add_meta_box(
                'audio_video_sectionid', 'Enter Audio/Video URL:', 'audio_video_meta_box_callback', 'mystory'
        ); 
        
}

add_action( 'add_meta_boxes', 'audio_video_add_meta_box' );

function audio_video_meta_box_callback( $post ) {
        wp_nonce_field( 'audio_video_meta_box', 'audio_video_meta_box_nonce' );
        $value = get_post_meta( $post->ID, 'video_audio_url', true );
        $value_extention = get_post_meta( $post->ID, 'file_type_for_check', true );
     if(!empty($value)){      
        ?>
        <p><?php echo $value; ?></p>
        <input type="hidden" name="audio_video_text" value="<?php echo $value; ?>" style="width:400px;">
        
        <?php } else { echo 'No Audio/Video'; } 
    if($value_extention =='audio/mp3' || $value_extention =='audio/mpeg'){ ?>
    <p><audio controls>
         <source src="<?php echo $value; ?>" type="audio/mp3">
    </audio></p>
        
 <?php  } if($value_extention =='video/mp4'){ ?>
       <p> <video width="320" height="240" controls>
            <source src="<?php echo $value; ?>" type="video/mp4">
        </video></p>
   <?php } 
    
}
function audio_video_save_meta_box_data( $post_id ) {     
        if ( !isset( $_POST['audio_video_meta_box_nonce'] ) ) {
                return;
        }
        if ( !wp_verify_nonce( $_POST['audio_video_meta_box_nonce'], 'audio_video_meta_box' ) ) { return; }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
         { return; }

        if ( !current_user_can( 'edit_post', $post_id ) ) 
            { return; }

        $new_meta_value = ( isset( $_POST['audio_video_text'] ) ?  $_POST['audio_video_text']  : '' );

        update_post_meta( $post_id, 'video_audio_url', $new_meta_value );
}

add_action( 'save_post', 'audio_video_save_meta_box_data' );

function my_custom_admin_head() {
	echo '<style>.mce-widget.mce-notification.mce-notification-error.mce-has-close.mce-in{
    display:none;
}</style>';
}
add_action( 'admin_head', 'my_custom_admin_head' );

// Sidebar Widget for my story page
    register_sidebar(array(
        'name' => __('My Story Page Widgets', 'medical-directory'),
        'id' => 'mystory-page-text-widgets',
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="widgettitle">',
        'after_title' => '</div>',
    ));

/*-------------------------------------------------------------------------
  START INITIALIZE FILE LINK
------------------------------------------------------------------------- */

require_once(TEMPLATEPATH . '/framework/functions.php');

function cm_redirect_users_by_role() {
 
    if ( ! defined( 'DOING_AJAX' ) ) {
 
        $current_user   = wp_get_current_user();
        $role_name      = $current_user->roles[0];
 
        if ( 'author' === $role_name ) {
            wp_redirect( 'https://test.dentalchat.com/blog/my-story/' );
        } 
         if ( 'contributor' === $role_name ) {
            wp_redirect( 'https://test.dentalchat.com/blog/my-story/' );
        } 
    } // if DOING_AJAX
 
} // cm_redirect_users_by_role
add_action( 'admin_init', 'cm_redirect_users_by_role' );

function redirect_homepage() {
    if( ! is_home() && ! is_front_page() )
        return;

    wp_redirect( site_url().'/blog/', 301 );
    exit;
}

add_action( 'template_redirect', 'redirect_homepage' );
?>
