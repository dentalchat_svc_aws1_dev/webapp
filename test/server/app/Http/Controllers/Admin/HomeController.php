<?php namespace App\Http\Controllers\Admin;

use App\Book;
use App\User;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Helper\helpers;
use App\Model\Country;  /* Model name*/
use App\Model\Booking;  /* Model name*/
use App\Model\Newsletter;  /* Model name*/
//use App\Model\SiteUser;  /* Model name*/
use App\Model\PaymentHistory; /* Model name*/
use App\Model\Job;


use Input; /* For input */
use Validator;
use Session;
use DB;
use Mail;
use Hash;
use Auth;

class HomeController extends BaseController {

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
  public function __construct() {
      parent::__construct();
      view()->share('home_class','active');
      if(Auth::user()){
        $admin_role = Auth::user()->role;
        view()->share('user_role',$admin_role);
      }
    }
  public function index()
  {
   return view('admin.home.index');

  }

  // For Admin Edit Profile
  public function getProfile()
  {
    
    $user=User::find(Auth::id());
      // return view('admin.books.edit',compact('book'));
    return view('admin.home.edit_profile',compact('user'));
  }
  
  public function show($id)
  {
     $user=User::find(Auth::id());
    return view('admin.home.edit_profile',compact('user'));
  }


  

  public function update(Request $request, $id)
  {
  
    $userUpdate=Request::all();
    $user=User::find($id); 

    if (Input::hasFile('image'))
    {
      $destinationPath = 'uploads/admin_profile/'; // upload path
      $thumb_path = 'uploads/admin_profile/thumb/';
      $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(111111111,999999999).'.'.$extension; // renameing image
      Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
      // $this->create_thumbnail($thumb_path,$fileName,$extension); 
      $user['admin_icon']=$fileName;

      // unlink old photo
      @unlink('uploads/admin_profile/'.Request::input('admin_icon'));
    }
    else
       $user['admin_icon']=Request::input('admin_icon');


     $user->update($userUpdate);
     Session::flash('success', 'Your profile updated successfully.'); 
     return redirect('admin/admin-profile');
  }

  public function forgotPassword()
    {
        if (Auth::check())
        {
            return redirect('admin/home');
        }
        else{
            return view('admin.home.forgotpassword');
        }
      
    }

  public function forgotpasswordcheck()
    {
        if(Request::isMethod('post'))
        {
          $email = Request::input('email');

          $users = USER::where('email',$email)->get();

          //echo $users->count();exit();
          if($users->count() == 0){
            Session::flash('failure_message', 'Invalid email id!'); 
            Session::flash('alert-class', 'alert alert-danger'); 
            return redirect('admin/forgotpassword/');
          }else{
            $random_code = mt_rand();
            User::where('email', '=', $email)->update(['code_number' => $random_code]);

            $sitesettings = DB::table('sitesettings')->where('name','=','Email')->first();
            $admin_users_email = $sitesettings->value;


            ###################################Sening mail starts##############################
            $user_name = User::where('email',$email)->first()->name;
            $user_email = User::where('email',$email)->first()->email;
            $resetpassword_link = url().'/admin/resetpassword/'.base64_encode($user_email).'-'.base64_encode($random_code);

            $sent = Mail::send('admin.home.reset_password_link', array('name'=>$user_name,'email'=>$user_email,'reset_password_link'=>$resetpassword_link), 
            function($message) use ($admin_users_email, $user_email,$user_name)
            {
                $message->from($admin_users_email,'Dental Chat');
                $message->to($user_email, $user_name)->subject('Forgot Password Email!');
            });

            if( ! $sent) 
            {
              Session::flash('error', 'something went wrong!! Mail not sent.'); 
              return redirect('admin/forgotpassword');
            }
            else
            {
              Session::flash('success', 'Please check your email to reset your password.'); 
              return redirect('auth/login');
            }  
            ###################################Sening mail ends##############################
          }
          
        }
        
    }
  
   public function resetpassword($email)
    {
      //$user_email = base64_decode($email);
        if (Auth::check())
        {
            return redirect('admin/home');
        }
        else{
            
            $user_email = explode('-',$email);
      
            //$user_email = base64_decode($user_email[0]);
            //echo "h= ".$user_email; exit;
            $usr_email = base64_decode($user_email[0]);
            $users = USER::where('email',$usr_email)->get();
            if($users->count() == 0){
              Session::flash('failure_message', 'Invalid Link. Please Try again.'); 
              return redirect('admin/forgotpassword');
            }
            else{
              compact('users');
              if($users[0]->code_number==''){
                    Session::flash('failure_message', 'Invalid Link. Please Try again.'); 
                    return redirect('admin/forgotpassword');
              }
              else{
                return view('admin.home.resetpassword',array('title'=>'Reset Password','admin_email'=>$user_email[0]));
              }
            }
        }
      
    }
    
    public function usedResetPassword(){}
 public function updatePassword($email)
    {
      $user_email = base64_decode($email);
      $password = Request::input('password');
      $password = Request::input('con_password');
      //echo "h= ".$user_email; exit;Hash::make()
      DB::table('users')
            ->where('email', $user_email)
            ->update(['password' => Hash::make($password),'code_number'=>'']);

      Session::flash('success', 'Password successfully changed.'); 
      //return view('admin.home.resetpassword');
      //return redirect('admin/resetpassword/'.$email);
      //Session::flash('success', 'Please check your email to reset your password.'); 
      return redirect('auth/login');
    }

  public function changePass()
  {
    if(Request::isMethod('post'))
    {
      $old_password = Request::input('old_password');
      

      $password = Request::input('password');
      $conf_pass = Request::input('conf_pass');

      // Get Admin's password
      $user=User::find(Auth::id());

      if(Hash::check($old_password, $user['password']))
      {
        if($password!=$conf_pass){
          Session::flash('error', 'Password and confirm password is not matched.'); 
          return redirect('admin/change-password');

        }
        else{
          DB::table('users')->where('id', Auth::id())->update(array('password' => Hash::make($password)));
          
          Session::flash('success', 'Password successfully changed.You can now login.'); 
          return redirect('admin/change-password');
        }
      }
      else{
        Session::flash('error', 'Old Password does not match.'); 
        return redirect('admin/change-password');
      }
    }

    return view('admin.home.changepassword',array('title' => 'Change Password','module_head'=>'Change Password'));
  }
  
	public function multiple_record_operations()
	{
		$data = Request::all();
		/*echo "<pre>";
		print_r($data);
		exit;*/
		$record_ids = base64_decode($data['record_ids']);
		$return_url = base64_decode($data['return_url']);
		$table_name	= base64_decode($data['table_name']);
		if($table_name == 'doctors'){
			DB::table($table_name)->whereRaw("`id` IN (".$record_ids.")")
				->update(array(
							   'admin_status'	=> $data['status']
						   )
					   );

		}else{
			DB::table($table_name)->whereRaw("`id` IN (".$record_ids.")")
				->update(array(
							   'status'	=> $data['status']
						   )
					   );
			
		}
		
		Session::flash('success_message', 'Status updated successfully.'); 
		return redirect($return_url);
	}
   

}
