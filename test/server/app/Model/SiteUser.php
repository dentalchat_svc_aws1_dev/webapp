<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiteUser extends Model
{
	public $timestamps = false;
    protected $guarded=[];

    protected $table = 'site_users';
    public function docs_Category(){
    	return $this->hasMany('App\Model\DocCategory', 'doctor_id','id');
    }
     public function docs_qualification(){
    	return $this->hasMany('App\Model\DocQualification', 'doctor_id','id');
    }
    public function docs_speciality(){
    	return $this->hasMany('App\Model\DocSpecialist', 'doctor_id','id');
    }
    public function docs_research(){
    	return $this->hasMany('App\Model\DocResearch', 'doctor_id','id');
    }
    public function docs_location(){
    	return $this->hasMany('App\Model\DocPractice', 'doctor_id','id');

    }
     public function city_details(){
        return $this->belongsTo('App\Model\City', 'city','id');

    }
    public function get_primary_card(){
        return $this->hasMany('App\Model\SiteUserCard', 'site_user_id','id')->where('primary_card','=',1);

    }
    public function docs_Category_ids(){
        
    }

}
