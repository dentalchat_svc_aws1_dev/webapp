/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    'use strict';

    angular.module('doctorchat.pages.cms', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {       

			$stateProvider.state('about', {
						url: '/about',
						templateUrl: 'scripts/pages/cms/about.html',
						controller: 'CmsPageCtrl',
						})
			.state('privacypolicy', {
						url: '/privacy-policy',
						templateUrl: 'scripts/pages/cms/privacy_policy.html',
						controller: 'CmsPageCtrl',
						})
			.state('terms', {
						url: '/terms-and-conditions',
						templateUrl: 'scripts/pages/cms/terms.html',
						controller: 'CmsPageCtrl',
						})
			.state('hipaa-authorization', {
						url: '/hipaa-authorization',
						templateUrl: 'scripts/pages/cms/hipaa_authorization.html',
						controller: 'CmsPageCtrl',
						})
			.state('contact', {
						url: '/contact-us',
						templateUrl: 'scripts/pages/cms/contact.html',
						controller: 'CmsPageCtrl',
						})
			.state('online-dental-chat-marketing', {
						url: '/online-dental-chat-marketing',
						templateUrl: 'scripts/pages/cms/marketing.html',
						controller: 'CmsPageCtrl',
						})
			.state('online-dental-communication', {
						url: '/online-dental-communication',
						templateUrl: 'scripts/pages/cms/communication.html',
						controller: 'CmsPageCtrl',
						})
			.state('local-dental-chatting-online', {
						url: '/local-dental-chatting-online',
						templateUrl: 'scripts/pages/cms/chatonline.html',
						controller: 'CmsPageCtrl',
						})
			.state('online-dentists-chat-blog', {
						url: '/online-dentists-chat-blog',
						templateUrl: 'scripts/pages/cms/chatblog.html',
						controller: 'CmsPageCtrl',
						})
			.state('online-dentists-marketing', {
						url: '/online-dentists-marketing',
						templateUrl: 'scripts/pages/cms/dentists-marketing.html',
						controller: 'CmsPageCtrl',
						})
			.state('local-emergency', {
						url: '/local-emergency',
						templateUrl: 'scripts/pages/cms/local-emergency.html',
						controller: 'CmsPageCtrl',
						})
			.state('subscribe', {
						url: '/subscribe',
						templateUrl: 'scripts/pages/cms/subscribe.html',
						controller: 'CmsPageCtrl',
						})
			.state('faq', {
						url: '/faq',
						templateUrl: 'scripts/pages/cms/faq.html',
						controller: 'CmsPageCtrl',
						})
		   .state('subscribe-now', {
						url: '/subscribe-now',
						templateUrl: 'scripts/pages/cms/subscribe_now.html',
						controller: 'CmsPageCtrl',
						})
            .state('offer', {								
            	url: '/offer',
            	templateUrl: 'scripts/pages/cms/exhaustive.html',
            	controller: 'CmsPageCtrl',
            })						
		    .state('sansitivity', {
						url: '/sansitivity',
						templateUrl: 'scripts/pages/cms/sansitivity.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-implant', {
						url: '/dental-implant',
						templateUrl: 'scripts/pages/cms/dentalimplant.html',
						controller: 'CmsPageCtrl',
						})
		    .state('tooth-abscess', {
						url: '/tooth-abscess',
						templateUrl: 'scripts/pages/cms/tooth-abscess.html',
						controller: 'CmsPageCtrl',
						})
		    .state('tooth-pain-decay', {
						url: '/tooth-pain-decay',
						templateUrl: 'scripts/pages/cms/tooth-swelling.html',
						controller: 'CmsPageCtrl',
						})
		    .state('broken-filling', {
						url: '/broken-filling',
						templateUrl: 'scripts/pages/cms/broken-filling.html',
						controller: 'CmsPageCtrl',
						})
		    .state('broken-bonding', {
						url: '/broken-bonding',
						templateUrl: 'scripts/pages/cms/broken-bonding.html',
						controller: 'CmsPageCtrl',
						})
		    .state('denture-repair', {
						url: '/denture-repair',
						templateUrl: 'scripts/pages/cms/denture-repair.html',
						controller: 'CmsPageCtrl',
						})
			.state('root-canal', {
						url: '/root-canal',
						templateUrl: 'scripts/pages/cms/root-canal.html',
						controller: 'CmsPageCtrl',
						})
			.state('painful-gums', {
						url: '/painful-gums',
						templateUrl: 'scripts/pages/cms/painful-gums.html',
						controller: 'CmsPageCtrl',
						})
			.state('tooth-ache', {
						url: '/tooth-ache',
						templateUrl: 'scripts/pages/cms/tooth-ache.html',
						controller: 'CmsPageCtrl',
						})
		    .state('avulsed-teeth', {
						url: '/avulsed-teeth',
						templateUrl: 'scripts/pages/cms/avulsed-teeth.html',
						controller: 'CmsPageCtrl',
						})
		    .state('losse-crowns', {
						url: '/losse-crowns',
						templateUrl: 'scripts/pages/cms/losse-crowns.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-public-health', {
						url: '/dental-public-health',
						templateUrl: 'scripts/pages/cms/dental-health.html',
						controller: 'CmsPageCtrl',
						})
		    .state('endodontics', {
						url: '/endodontics',
						templateUrl: 'scripts/pages/cms/endodontics.html',
						controller: 'CmsPageCtrl',
						})
		    .state('oral-pathology', {
						url: '/oral-pathology',
						templateUrl: 'scripts/pages/cms/oral-pathology.html',
						controller: 'CmsPageCtrl',
						})
		    .state('oral-radiology', {
						url: '/oral-radiology',
						templateUrl: 'scripts/pages/cms/oral-radiology.html',
						controller: 'CmsPageCtrl',
						})
		    .state('oral-surgery', {
						url: '/oral-surgery',
						templateUrl: 'scripts/pages/cms/oral-surgery.html',
						controller: 'CmsPageCtrl',
						})
		    .state('orthodontics-orthopedics', {
						url: '/orthodontics-orthopedics',
						templateUrl: 'scripts/pages/cms/orthodontics-orthopedics.html',
						controller: 'CmsPageCtrl',
						})
		    .state('pediatrics-dentistry', {
						url: '/pediatrics-dentistry',
						templateUrl: 'scripts/pages/cms/pediatrics-dentistry.html',
						controller: 'CmsPageCtrl',
						})
		    .state('periodontics', {
						url: '/periodontics',
						templateUrl: 'scripts/pages/cms/periodontics.html',
						controller: 'CmsPageCtrl',
						})
		    .state('prosthodontics', {
						url: '/prosthodontics',
						templateUrl: 'scripts/pages/cms/prosthodontics.html',
						controller: 'CmsPageCtrl',
						})
		    .state('cosmetic-dentistry', {
						url: '/cosmetic-dentistry',
						templateUrl: 'scripts/pages/cms/cosmetic-dentistry.html',
						controller: 'CmsPageCtrl',
						})
		    .state('general-dentistry', {
						url: '/general-dentistry',
						templateUrl: 'scripts/pages/cms/general-dentistry.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-implant-failure', {
						url: '/dental-implant-failure',
						templateUrl: 'scripts/pages/cms/dental-implant-failure.html',
						controller: 'CmsPageCtrl',
						})
		    .state('wisdom-teeth-pain', {
						url: '/wisdom-teeth-pain',
						templateUrl: 'scripts/pages/cms/wisdom-teeth-pain.html',
						controller: 'CmsPageCtrl',
						})
		    .state('teeth-sensitivity', {
						url: '/teeth-sensitivity',
						templateUrl: 'scripts/pages/cms/teeth-sensitivity.html',
						controller: 'CmsPageCtrl',
						})
		    .state('bad-breath', {
						url: '/bad-breath',
						templateUrl: 'scripts/pages/cms/bad-breath.html',
						controller: 'CmsPageCtrl',
						})
		    .state('grinding-teeth', {
						url: '/grinding-teeth',
						templateUrl: 'scripts/pages/cms/grinding-teeth.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-dry-socket', {
						url: '/dental-dry-socket',
						templateUrl: 'scripts/pages/cms/dental-dry-socket.html',
						controller: 'CmsPageCtrl',
						})
		   .state('emergency-dentist-question', {
						url: '/emergency-dentist-question',
						templateUrl: 'scripts/pages/cms/emergency-dentist-question.html',
						controller: 'CmsPageCtrl',
						})
		    .state('local-dentists-communication', {
						url: '/local-dentists-communication',
						templateUrl: 'scripts/pages/cms/local-dentists-communication.html',
						controller: 'CmsPageCtrl',
						})

		    .state('local-emergency-dental-questions-online', {
						url: '/local-emergency-dental-questions-online',
						templateUrl: 'scripts/pages/cms/local-emergency-dental-questions-online.html',
						controller: 'CmsPageCtrl',
						})

		    .state('emergency-dental-care-question', {
						url: '/emergency-dental-care-question',
						templateUrl: 'scripts/pages/cms/emergency-dental-care-question.html',
						controller: 'CmsPageCtrl',
						})
		    .state('local-root-canal-chat', {
						url: '/local-root-canal-chat',
						templateUrl: 'scripts/pages/cms/local-root-canal-chat.html',
						controller: 'CmsPageCtrl',
						})
		    .state('getting-braces-orthodontist-treatment-info', {
						url: '/getting-braces-orthodontist-treatment-info',
						templateUrl: 'scripts/pages/cms/getting-braces-orthodontist-treatment-info.html',
						controller: 'CmsPageCtrl',
						})
            .state('online-emergency-dentist-question', {
						url: '/online-emergency-dentist-question',
						templateUrl: 'scripts/pages/cms/online-emergency-dentist-question.html',
						controller: 'CmsPageCtrl',
						})
						
						
		   .state('urgent-dental-care-chat', {
						url: '/urgent-dental-care-chat',
						templateUrl: 'scripts/pages/cms/urgent-dental-care-chat.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-question-blogging', {
						url: '/dental-question-blogging',
						templateUrl: 'scripts/pages/cms/dental-question-blogging.html',
						controller: 'CmsPageCtrl',
						})
		    .state('dental-implant-questions', {
						url: '/dental-implant-questions',
						templateUrl: 'scripts/pages/cms/dental-implant-questions.html',
						controller: 'CmsPageCtrl',
						})
            .state('common-dental-question', {
						url: '/common-dental-question',
						templateUrl: 'scripts/pages/cms/common-dental-question.html',
						controller: 'CmsPageCtrl',
						})
			.state('pricing', {
						url: '/pricing',
						templateUrl: 'scripts/pages/cms/pricing.html',
						controller: 'CmsPageCtrl',
						})

    }
})();
